import React, {useEffect, useContext} from 'react';
import { GlobalStateContext } from '../Layout/HomeLayout';
import type from '../../reducers/type';
import zapiilogo from './imgs/zapii.gif';
import './stylesheet.scss';

const WelcomePageComponent = ()=>{

    const [globalState, dispatch] = useContext(GlobalStateContext);

    useEffect(()=>{
        setTimeout(()=>{
            dispatch({type: type.SHOW_WELCOME_PAGE, hideWelcomePage:true});
        },1000);
    }, []);

    return (
        <div className='welcompage-group'>
            <div className='in'>
                <img src={zapiilogo}/>
            </div>
        </div>
    )
}

export default WelcomePageComponent;