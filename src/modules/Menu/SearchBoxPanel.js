import React, { useState, useRef, useContext } from 'react';
import { BounceLoader } from 'react-spinners';
import { Link } from 'react-router-dom';
import { isEmpty, debounce } from 'lodash';
import short from 'short-uuid';
import Avatar from 'react-avatar';
import type from '../../reducers/type';
import { GlobalStateContext } from '../Layout/HomeLayout';
import DataIsEmpty from '../Common/DataIsEmpty';
import { CON_TYPE, DB, fireStore, realTime } from './../../utils/DBUtils';
import R from './../../locale/R';
import { removeAccents } from '../../utils/StringUtils';

const SearchBoxPanel = ({ id, isScroll }) => {

    const [localState, setLocalState] = useState({ users: [], clothes: [], loading: false });
    const [globalState, dispatch] = useContext(GlobalStateContext);
    const searchRef = useRef('');

    var trans = short();

    const searchHandler = async e => {
        const value = searchRef.current.value;

        setLocalState({ ...localState, loading: true });

        if (!isEmpty(value)) {
            const splitValue = value.split(' ').map(e=>removeAccents(e.toLowerCase()));
            var bounced = debounce(async () => {
                const userResult = await fireStore.findLimitStatic(
                    DB.USER.name(),
                    50,
                    ['keywords', CON_TYPE.ARRAY_CONTAINS_ANY, splitValue],
                );
                if (!isEmpty(userResult)) {
                    setLocalState({ ...localState, users: userResult, loading: false });
                } else {
                    setLocalState({ ...localState, users: userResult, loading: false });
                }

                const productResult = await fireStore.findLimitStatic(
                    DB.CLOTHES.name(),
                    50,
                    ['keywords', CON_TYPE.ARRAY_CONTAINS_ANY, splitValue],
                );

                setLocalState({ ...localState, users: userResult, clothes: productResult, loading: false });

            }, 500);

            bounced();
        } else {
            setLocalState({ ...localState, users: [], clothes: [], loading: false });
        }
    }

    const closeModalHandler = () => {
        document.getElementById(`close-id-${id}`).click();
    }

    const renderSearchResult = () => {
        const { searchItem } = globalState;
        if (searchItem === 1) {
            return (
                !isEmpty(localState.users)
                    ?
                    localState.users.map(e =>
                        <Link key={'us_' + e._id} to={`/${trans.fromUUID(e._id)}`} className='avatar-g nonhidden' onClick={closeModalHandler}>
                            <Avatar className='zavatar nonhidden of_cover' src={e.avatar} name={e.name} size='3rem' round={true} />
                            <div className='name-group nonhidden'>
                                <div className='t nonhidden' style={isEmpty(e.about) ? { marginTop: '0.75rem' } : {}}>{e.name}</div>
                                {
                                    !isEmpty(e.about)
                                    &&
                                    <div className='t2 nonhidden' style={{width:'10rem'}}>{e.about}</div>
                                }
                            </div>
                        </Link>
                    )
                    :
                    <div>
                        <DataIsEmpty hideIcon={true} label={R.common.no_data} color='#eee' />
                    </div>
            )
        } else {
            return (
                !isEmpty(localState.clothes)
                    ?
                    localState.clothes.map(e =>
                        <Link key={'us_' + e._id} to={`/detail_mobile/product/${trans.fromUUID(e._id)}`} className='avatar-g nonhidden' onClick={closeModalHandler}>
                            <img className='zimg nonhidden' src={isEmpty(e.image_detail) ? e.image : e.image_detail[0]} alt={e.name} />
                            <div className='name-group nonhidden'>
                                <div className='t text-ellipsis nonhidden' style={isEmpty(e.description) ? { marginTop: '0.75rem' } : {}}>{e.name}</div>
                                {
                                    !isEmpty(e.description)
                                    &&
                                    <div className='t2 nonhidden'>{e.description.replace(/<br\/>/g, "")}</div>
                                }
                            </div>
                        </Link>
                    )
                    :
                    <div>
                        <DataIsEmpty hideIcon={true} label={R.common.no_data} color='#eee' />
                    </div>
            )
        }
    }

    return (
        <div className="modal show" id={id} tabIndex="-1" role="dialog">
            <div className={`modal-dialog fullscreen`} role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <button id={`close-id-${id}`} type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <i className="material-icons">keyboard_backspace</i>
                        </button>
                        <input maxLength={50} ref={searchRef} type="search" onKeyUp={searchHandler} className='sbx' placeholder={R.common.search_all} />
                    </div>
                    <div className="modal-body list-user-search-rs">

                        {

                            <div className='search-result-group nonhidden'>
                                <div className='st'>
                                    <div className='stg nonhidden'>
                                        <div onClick={() => { dispatch({ type: type.CHANGE_SEARCH_ITEM, searchItem: 1 }) }} className={`stgi nonhidden ${globalState.searchItem === 1 ? 'active' : ''}`}>{R.common.people}</div>
                                        <div onClick={() => { dispatch({ type: type.CHANGE_SEARCH_ITEM, searchItem: 2 }) }} className={`stgi nonhidden ${globalState.searchItem === 2 ? 'active' : ''}`}>{R.common.product}</div>
                                    </div>
                                </div>
                                {
                                    localState.loading
                                        ?
                                        <div className='zloading-item'>
                                            <BounceLoader
                                                sizeUnit={"px"}
                                                size={50}
                                                loading={true}
                                                color='#777' />
                                        </div>
                                        :
                                        <div className='sc nonhidden'>
                                            {renderSearchResult()}
                                        </div>
                                }
                            </div>
                        }


                    </div>
                </div>
            </div>
        </div>
    )
}

export default SearchBoxPanel;