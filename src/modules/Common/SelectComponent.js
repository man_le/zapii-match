import React from 'react';
import R from '../../locale/R';

const SelectComponent = ({ aria_labelledby, selectQuestionType }) => {
    return (

        <div className="dropdown zemoji">
            <a className="dropdown-toggle new-question-group" style={{ justifyContent: 'left', marginTop: '0.75rem' }} href="#" id="filterStoriesTypeDropdown" data-toggle="dropdown" aria-expanded="false">
                <i className="material-icons">home</i>
                <div className='l'>{R.questionType['select_all']}</div>
            </a>
            <div className="dropdown-menu" aria-labelledby={aria_labelledby}>
                <span onClick={() => selectQuestionType('local_cafe')} className="dropdown-item pointer">
                    <i className="material-icons">local_cafe</i>
                    <div className='l'>{R.questionType.local_cafe}</div>
                </span>
                <span onClick={() => selectQuestionType('airplanemode_active')} className="dropdown-item pointer">
                    <i className="material-icons">airplanemode_active</i>
                    <div className='l'>{R.questionType.airplanemode_active}</div>
                </span>
            </div>
        </div>
    )
}

export default SelectComponent;