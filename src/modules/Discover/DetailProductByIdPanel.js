import { isEmpty } from 'lodash';
import React, { useContext, useState, useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom';
import CurrencyFormat from 'react-currency-format';
import { BounceLoader } from 'react-spinners';
import short from 'short-uuid';
import ReactHtmlParser from 'react-html-parser';
import Autolinker from 'autolinker';
import * as firebase from "firebase/app";
import { GlobalStateContext } from '../Layout/HomeLayout';
import DataIsEmpty from '../Common/DataIsEmpty';
import BagIcon from '../Common/Icon/BagIcon';
import CloseIcon from '../Common/Icon/CloseIcon';
import CoverAllPage from '../Common/CoverAllPage';
import { RouteContext } from '../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import type from '../../reducers/type';
import R from '../../locale/R';
import SizeComponent from './../SizeComponent/SizeComponent';
import './stylesheet.scss';

const DetailProductPanel = ({ match, history }) => {

    const { params: { clothes_id } } = match;
    const translator = short();

    let product_id_build = clothes_id;
    if (!isEmpty(clothes_id)) {
        try {
            product_id_build = translator.toUUID(clothes_id);
        } catch (err) {
            product_id_build = null;
        }
    }

    const [localState, setLocalState] = useState({ size: 'M', loading: false, init: false, inBag: false, data: {}, image_detail_index: 0 });
    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);

    const fetchData = async () => {
        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            setLocalState({ ...localState, loading: true });

            let result = null;
            if (!isEmpty(product_id_build)) {
                result = await fireStore.find(DB.CLOTHES.name(), [DB.CLOTHES.ID, CON_TYPE.EQ, product_id_build]);
            }

            if (!isEmpty(result)) {
                result = result[0];

                // Check item in bag or not
                let in_bag_ids = await fireStore.find(
                    DB.IN_BAG.name(),
                    [DB.IN_BAG.ID_GEN, CON_TYPE.EQ, `${product_id_build}$${user.extData[0]._id}`],
                    [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
                );

                setLocalState({
                    ...localState,
                    data: result,
                    inBag: !isEmpty(in_bag_ids),
                    loading: false,
                    init: true
                })
            } else {
                setLocalState({ ...localState, loading: false, init: true });
            }
        }
    }

    useEffect(() => {
        let isCompleted = false;
        if (!isCompleted) {
            fetchData();
            isCompleted = true;
        }
        return () => isCompleted === true;
    }, [user])

    const closeModalHandler = () => {
        // history.push('/');
        history.push(globalState.currentRouter === window.location.pathname ? '/' : globalState.currentRouter);
    }


    const inBagHandler = async (inBag, sizeType) => {
        const { data } = localState;

        // Add into bag
        if (inBag === false) {
            setLocalState({ ...localState, showDetail: false });

            const newData = {
                [DB.IN_BAG.ID_GEN]: `${product_id_build}$${user.extData[0]._id}`,
                [DB.IN_BAG.CLOTHES_ID]: product_id_build,
                [DB.IN_BAG.USER_ID]: user.extData[0]._id,
                [DB.IN_BAG.ORDER_ID]: 'new',
                name: data.name,
                image: data.image,
                price: data.new_price,
                [sizeType]: firebase.firestore.FieldValue.increment(1),
            };

            await fireStore.updateMultiConditions(
                DB.IN_BAG.name(),
                newData,
                true,
                [DB.IN_BAG.CLOTHES_ID, CON_TYPE.EQ, product_id_build],
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new']
            );

            // Remove in bag
        } else {
            setLocalState({ ...localState, showDetail: false });
            await fireStore.remove(
                DB.IN_BAG.name(),
                [DB.IN_BAG.CLOTHES_ID, CON_TYPE.EQ, product_id_build],
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
            )
        }

        dispatch({ type: type.UPDATE_IN_BAG });
    }

    const removeBagHandler = () => {
        let { itemsInBag } = globalState;
        if (itemsInBag.indexOf(product_id_build + '$' + user.extData[0]._id) === -1) return;
        itemsInBag = itemsInBag.filter(e => e !== product_id_build + '$' + user.extData[0]._id);
        dispatch({ type: type.CHANGE_ITEM_IN_BAG, itemsInBag });

        const { inBag } = localState;
        if (!inBag) return;
        inBagHandler(true);

        setLocalState({ ...localState, inBag: false });
    }

    const addBagHandler = () => {
        let { itemsInBag } = globalState;
        if (itemsInBag.indexOf(product_id_build + '$' + user.extData[0]._id) === -1) itemsInBag.push(product_id_build + '$' + user.extData[0]._id);
        dispatch({ type: type.CHANGE_ITEM_IN_BAG, itemsInBag });

        inBagHandler(false, localState.size);
        setLocalState({ ...localState, inBag: true });
    }

    const sizeHandler = (type) => {
        setLocalState({ ...localState, size: type });
    }

    const renderImageSlider = (image_photo) => {
        const { image_detail_index } = localState;
        let max = 0;
        if (!isEmpty(image_photo) && image_photo.length > 1) {
            max = image_photo.length;
            if (image_photo.length >= 5) max = 5;
        }
        let result = [];
        if (max > 0) {
            for (let i = 0; i < max; i++) {
                result.push(
                    <div key={'imgslider1_' + i} onClick={() => { setLocalState({ ...localState, image_detail_index: i }) }} className='pi noselect' style={image_detail_index == i ? { background: '#777' } : {}}></div>
                )
            }
        }
        return result;
    }

    const renderBody = () => {
        const { data, init, loading } = localState;

        if (loading && !init) {
            return (
                <div className={`zloading-item`}><BounceLoader
                    sizeUnit={"px"}
                    size={50}
                    loading={true}
                    color='#777'
                /></div>
            )
        } else if (isEmpty(data) && init) {
            return (
                <DataIsEmpty />
            )
        }

        return (
            <>
                <div className='preview'>
                    <img src={!isEmpty(data.image_detail) ? data.image_detail[localState.image_detail_index] : data.image} alt={data.name} />
                    <div className='p-select'>
                        {renderImageSlider(data.image_detail)}
                    </div>
                </div>
                <div className='pname'>
                    <div className='cntype'>
                        {R.discover[data.type]}
                    </div>
                    {data.name}
                </div>
                <div className='price'>
                    <div className='old_price'>
                        <CurrencyFormat value={data.old_price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                    </div>
                    <div className='new_price'>
                        <CurrencyFormat value={data.new_price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                    </div>
                </div>
                <div className='pdescription'>
                    {data.description &&
                        data.description.split("<br/>").map((item, i) => {
                            return (
                                <span key={`${data._id}_${i}`}>
                                    {ReactHtmlParser(Autolinker.link(item))}
                                    <br />
                                </span>
                            )
                        })
                    }
                     <SizeComponent sex={data.type} clothes={data.filter_type}/>
                </div>
                <div className='config'>
                    <div className='size'>
                        {
                            !isEmpty(data.size)
                            &&
                            <>
                                <div className='lb'>
                                    {R.discover.size}
                                </div>
                                <div className='stypegroup'>
                                    {
                                        data.size.map((e, i) =>
                                            <div onClick={() => sizeHandler(e)} key={'size_' + i} className={`stype ${localState.size === e ? 'active' : ''}`}>
                                                {e}
                                            </div>
                                        )
                                    }
                                </div>
                            </>
                        }
                    </div>
                </div>
                <div id='id-msg-add2cart' className='msg-group hidden'>
                    <div className='msgt'>
                        {R.discover.add2bag}
                    </div>
                </div>
                <div id='id-msg-remove2cart' className='msg-group hidden'>
                    <div className='msgt'>
                    {R.discover.remove2bag}
                    </div>
                </div>
                <div className='fixed-bar'>
                    <div className='g'>
                        <div id='id-remove2cart' onClick={removeBagHandler} className={`ins f noselect ${!localState.inBag ? 'zdisabled' : ''}`}>
                            <CloseIcon style={{ marginTop: '0.27rem' }} width='15' height='15' color='#fff' />
                            <div className='l'>
                                {
                                    R.discover.removeBag
                                }
                            </div>
                        </div>
                        <div id='id-add2cart' onClick={addBagHandler} className='ins noselect zpointer'>
                            <BagIcon color='#fff' />
                            <div className='l'>
                                {
                                    R.discover.addBag
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }

    return (
        <div className='discover-group'>
            <div className='list-product'>
                <div className='item'>
                    <div className='pdetail'>
                        <div className="zcontent">
                            <div className="zheader noselect">
                                <button onClick={closeModalHandler} type="button" className="close zscale-effect">
                                    <i className="material-icons">keyboard_backspace</i>
                                </button>
                                <div className='ztitle'>
                                    {R.discover.detail}
                                </div>
                            </div>
                            <div className="zbody">
                                {renderBody()}
                            </div>
                            <CoverAllPage />
                        </div>
                    </div>
                </div>
            </div>
        </div>





    )
}

export default withRouter(DetailProductPanel);