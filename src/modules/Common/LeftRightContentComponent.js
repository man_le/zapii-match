import React from 'react';
import RightComponent from '../HomePage/RightComponent';

const Comp = ({LeftComponent, RightComponent : RComponentParam}) => {

    const RightComp = <RightComponent/>;

    return (
        <div className='row lrcontent'>
            <div className='col-md-3 l'>
                {LeftComponent}
            </div>
            <div className='col-md-6'></div>
            <div className='col-md-2 r'>
                {RComponentParam || RightComp}
            </div>
        </div>
    )
}

export default Comp