import React from 'react';
const Icon = ({ width, height, color, style, className, isSelected }) => {
    if(isSelected){
        return (
            <svg className={`full ${className?className:''}`} height={height||25} viewBox="0 0 488.471 488.471" width={width||25} style={style} fill={color||'#262626'}><path d="m122.118 0h-61.059v61.059h-61.059v61.059h61.059v305.294h274.765v-61.059h-213.706z"/><path d="m427.412 366.353v-305.294h-274.765v61.059h213.706v366.353h61.059v-61.059h61.059v-61.059z"/></svg>
        )
    }
	return (
		<svg className={`outlined ${className?className:''}`} style={style} fill={color||'#262626'} height={height||25} viewBox="0 0 488.471 488.471" width={width||25} ><path d="m91.589 0h-30.53l-.004 61.059h-61.055v30.529h61.059v335.823h305.294v-30.529l-274.795.03z"/><path d="m488.471 396.882h-61.059l-.03-335.793-305.264-.03v30.529h274.765v396.882h30.529l-.004-61.059h61.062z"/></svg>
	)
}

export default Icon;
