import React from 'react';
import R from './../../locale/R';

const ModalComponent = ({ hideHeader, size, isScroll, id, title, children, submitFn, submitFnFromRaw, hasClose, hasSubmit, submitText, closeHandler, style, styleModalDialg, fromShareModal }) => {
    
    const submitHandler = (submitFn, submitFnFromRaw) => {
        if (submitFn)
            if (submitFnFromRaw) {
                submitFn();
                document.getElementById(`close-id-${id}`).click();
            } else {
                submitFn(isSuccess => {
                    if (isSuccess) document.getElementById(`close-id-${id}`).click();
                });
            }
    }

    let buildSize = '';
    if(size === 'large') buildSize = 'modal-lg';
    else if(size==='fullscreen') buildSize='fullscreen'
    
    return (
        <div className={`modal`} id={id} tabIndex="-1" role="dialog">
            <div style={styleModalDialg} className={`modal-dialog ${buildSize} ${isScroll ? 'modal-dialog-scrollable' : ''}`} role="document">
                <div className="modal-content" style={style}>
                    <div className="modal-header" style={hideHeader?{display:'none'}:{display:''}}>
                        {title && <h5 className="modal-title">{title}</h5>}
                        <button id={`close-id-${id}`} type="button" onClick={closeHandler && closeHandler} className="close btnCloseModal" data-dismiss="modal" aria-label="Close">
                            <i className="material-icons">clear</i>
                        </button>
                    </div>

                    <div className="modal-body" style={fromShareModal?{padding:'unset', paddingTop:'0.5rem', marginBottom:'-1rem'}:{}}>
                        {children}
                    </div>
                    <div className="modal-footer">
                        {hasClose && <button id='btnCloseModal' onClick={closeHandler && closeHandler} type="button" data-dismiss="modal" aria-label="Close" className="btn btn-link btnCloseModal">{R.common.close}</button>}
                        {
                            hasSubmit
                            &&
                            <button type="button" onClick={e => submitHandler(submitFn, submitFnFromRaw)} className="btn btn-danger btn-link">
                                {submitText ? submitText : R.newquiz.submit}
                                <div className="ripple-container">
                                    <div className="ripple-decorator ripple-on ripple-out"></div>
                                </div>
                            </button>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ModalComponent;