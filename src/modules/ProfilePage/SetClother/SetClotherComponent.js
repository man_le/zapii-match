import { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useContext, useEffect, useState } from 'react';
import InfiniteScroll from "react-infinite-scroll-component";
import { Link, withRouter } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import short from 'short-uuid';
import R from '../../../locale/R';
import type from '../../../reducers/type';
import { RouteContext } from '../../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../../utils/DBUtils';
import { formatMoment } from '../../../utils/StringUtils';
import DataIsEmpty from '../../Common/DataIsEmpty';
import BagIcon from '../../Common/Icon/BagIcon';
import HatIcon from '../../Common/Icon/HatIcon';
import ShirtIcon from '../../Common/Icon/ShirtIcon';
import ShoesIcon from '../../Common/Icon/ShoesIcon';
import TrousersIcon from '../../Common/Icon/TrousersIcon';
import ModalComponent from '../../Common/ModalComponent';
import MoreActionComponent from '../../Common/MoreActionComponent';
import { GlobalStateContext } from '../../Layout/HomeLayout';
import NewStatusComponent from '../../NewStatus/NewStatusComponent';
import NoImage from './imgs/no_image.png';
import './stylesheet.scss';

const SetClotherComponent = ({ fromOtherProfile, otherProfileUser_id, history }) => {

    const [localState, setLocalState] = useState({
        data: [],
        lastVisible: null,
        init: false,
        loading: false
    });

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);

    const fetchData = async () => {
        const { data, lastVisible } = localState;

        setLocalState({ ...localState, loading: true });

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {
            let userInfo = user.extData[0];

            let user_id = userInfo._id;
            if (otherProfileUser_id) user_id = otherProfileUser_id

            const result = await fireStore.findLimit(
                DB.CLOTHES_SET.name(),
                [DB.CLOTHES_SET.USER_ID, CON_TYPE.EQ, user_id],
                DB.CLOTHES_SET.CREATED_DATE,
                6,
                lastVisible,
                null
            );

            let more = true;
            if (isEmpty(result) || isEmpty(result.data)) {
                more = false;
            } else if (!isEmpty(result) && !isEmpty(result.data)) {
                result.data.forEach(e => {
                    if (isEmpty(data.filter(ei => ei._id === e._id))) data.push(e);
                })
            }

            setLocalState({
                ...localState,
                lastVisible: result ? result.lastVisible : null,
                data,
                more,
                init: true,
                loading: false
            });
        }
    }

    const closeAllModal = () => {
        let elems = document.querySelectorAll('.modal.show .btnCloseModal');
        if (elems.length > 0) {
            elems[0].click();
            window.location.replace("#");
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();

            if (!window.location.hash) {
                closeAllModal();
            }

            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [user, window.location.hash]);

    const fetchMoreData = () => {
        fetchData();
    }

    const gotoURL = _id => {
        history.push('/_mix/' + short().fromUUID(_id));
    }

    const preGotoURL = () => {
        dispatch({ type: type.CHANGE_ROUTER, currentRouter: window.location.pathname });
    }

    const getShirt = items => {
        if (isEmpty(items)) return null;
        const filter = items.filter(e => e.filter_type === 'dress');
        if (isEmpty(filter)) {
            return items[0];
        } else return null;
    }

    const getDress = items => {
        if (isEmpty(items)) return null;
        const filter = items.filter(e => e.filter_type === 'dress');
        if (!isEmpty(filter)) {
            return filter[0];
        } else return null;
    }

    const getTrousers = (itemsTrousers, itemsShirt) => {
        if (isEmpty(itemsTrousers)) return null;
        const filter = itemsShirt.filter(e => e.filter_type === 'dress');
        if (isEmpty(filter)) {
            return itemsTrousers[0];
        } else return null;
    }

    const deleteHandler = async _id => {
        let { data } = localState;

        // Remove clother set
        await fireStore.remove(DB.CLOTHES_SET.name(), [DB.CLOTHES_SET.ID, CON_TYPE.EQ, _id]);
        data = data.filter(e => e._id !== _id);

        // Remove status related
        await fireStore.remove(DB.STATUS.name(), ['setClothes._id', CON_TYPE.EQ, _id]);

        setLocalState({ ...localState, data });
    }

    const shareHandler = async clother_set_id => {
        dispatch({ type: type.GET_CLOTHES_SET_LOADING });

        document.getElementById('id_share_info_modal').click();

        if (window.innerWidth <= 991) {
            history.push('#new');
        }

        let setClothes = await fireStore.find(DB.CLOTHES_SET.name(), [DB.CLOTHES_SET.ID, CON_TYPE.EQ, clother_set_id]);
        if (!isEmpty(setClothes)) {
            setClothes = setClothes[0];
            dispatch({ type: type.GET_CLOTHES_SET, setClothes });
        }
    }

    const { data, init, more } = localState;

    if (isEmpty(data)) {

        if (!init) return (
            <div className={`zcenter`} style={{ marginTop: '0.75rem' }}>
                <BounceLoader
                    sizeUnit={"px"}
                    size={50}
                    loading={true}
                    color='#777'
                />
            </div>
        )
        else return (
            <div className='set-clother-group'>
                <DataIsEmpty className='zmargin_center' />
                {!fromOtherProfile
                    &&
                    <div className='snewg'>
                        <Link onClick={preGotoURL} to='/mix' className='snew'>
                            {R.set.new}
                        </Link>
                    </div>
                }
            </div>
        )
    }

    return (

        <div className='full set-clother-group zmain'>
            <div className='row bar'>
                <div className='col-md-6 zbor'>

                    {!fromOtherProfile
                        &&
                        <div className='snewg' style={{ textAlign: 'right', marginBottom: '1rem' }}>
                            <Link onClick={preGotoURL} to='/mix' className='snew'>
                                {R.set.new}
                            </Link>
                        </div>
                    }

                    <InfiniteScroll
                        dataLength={data.length}
                        next={fetchMoreData}
                        hasMore={more}
                    >
                        {
                            data.map((e, index) => {

                                const shirt = getShirt(e.itemsInMix_shirt);
                                const dress = getDress(e.itemsInMix_shirt);
                                const trousers = getTrousers(e.itemsInMix_trousers, e.itemsInMix_shirt);

                                return (
                                    <div key={`set_${e._id}`} className={`ci ${index === 0 ? 'first' : ''}`}>
                                        <div className='ic' >
                                            <div onClick={() => gotoURL(e._id)} className='imgg zpointer'>
                                                {
                                                    e.image
                                                    ?
                                                    <img src={e.image}/>
                                                    :
                                                    <img src={NoImage} style={{width:'5rem'}}/>
                                                }
                                            </div>
                                        </div>
                                        <div className='cn'>
                                            {e.name}
                                            <div className='cnd'>
                                                {formatMoment(moment, e.createdDate)}
                                            </div>
                                        </div>
                                        <MoreActionComponent className='smo' data={
                                            !fromOtherProfile
                                                ?
                                                [
                                                    {
                                                        label: R.set.detail,
                                                        onClickHandler: () => gotoURL(e._id)
                                                    },
                                                    {
                                                        label: R.set.share,
                                                        onClickHandler: () => shareHandler(e._id)
                                                    },
                                                    '-',
                                                    {
                                                        label: R.set.delete,
                                                        onClickHandler: () => deleteHandler(e._id)
                                                    },
                                                ]
                                                :
                                                [
                                                    {
                                                        label: R.set.detail,
                                                        onClickHandler: () => gotoURL(e._id)
                                                    },
                                                    {
                                                        label: R.set.share,
                                                        onClickHandler: () => shareHandler(e._id)
                                                    },
                                                ]
                                        } />
                                    </div>
                                )
                            })
                        }
                    </InfiniteScroll>
                </div>
            </div>


            {/* Share Modal */}
            <ModalComponent id={'share_info_modal'} hideHeader={true} size={window.innerWidth <= 991 ? 'fullscreen' : 'large'} fromShareModal>
                <div className='new-status-title noselect'>
                    <i className="material-icons zscale-effect" onClick={closeAllModal}>keyboard_backspace</i>
                    <div className='ti'>
                        {R.set.newStatus}
                    </div>
                </div>
                <NewStatusComponent fromShareModal />

            </ModalComponent>
            <div id='id_share_info_modal' className='hidden' data-toggle={"modal"} data-target={`#share_info_modal`}></div>



        </div >

    )
}

export default withRouter(SetClotherComponent);