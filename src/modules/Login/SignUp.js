import { isEmpty } from 'lodash';
import React, { useRef, useState } from 'react';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import { Link } from 'react-router-dom';
import { createAcc } from '../../utils/AuthUtils';
import { buildKeyWords } from '../../utils/StringUtils';
import { validate } from '../../utils/ValidateUtils';
import R from './../../locale/R';
import dAndroid from './Static/img/downloadandroid.png';
import dIos from './Static/img/downloadios.png';
import { DB, fireStore, realTime } from './../../utils/DBUtils';
import Footer from './Footer';
import './stylesheet.scss';

const signUpHandler = (email, password, localState, setLocalState) => {
    if (isEmpty(email) || isEmpty(password)) return;

    const valid = validate({ email, require: password });
    if (valid === true && localState.isSignUp === false) {
        setLocalState({ isSignUp: true });
        createAcc(email, password, async (code, userInfo) => {
            setLocalState({ isSignUp: false });
            if (isEmpty(code)) {
                const user = userInfo

                const data = {
                    name: user.email.split('@')[0],
                    avatar: '',
                    userUid: user.uid,
                    link: '',
                    notify_like_comment: true,
                    notify_like_status: true,
                    notify_share_status: true,
                    notify_post_new: true,
                    notify_comment: true,
                    noti_following: true,
                    inactive: true,
                    email: email,
                    keywords: buildKeyWords(user.email.split('@')[0])
                }

                const _id = await fireStore.insert(DB.USER.name(), data);

                // Send the active code via email 
                realTime.update(
                    'user_need_active',
                    { [_id]: email },
                    null,
                    _id);

                NotificationManager.success(R.access.signup_success, '', 3000);

                window.location.reload(true);

            } else {
                if (code === 'auth/email-already-in-use') NotificationManager.error(R.access.email_exist, '', 3000);
                else if (code === 'auth/weak-password') NotificationManager.error(R.access.pass_at_least_6, '', 3000);
                else NotificationManager.error(R.access.common_error2, '', 3000);
            }
        });
    }
}

const SignUp = () => {

    const [localState, setLocalState] = useState({ isSignUp: false, disabledBtn:true });
    const emailRef = useRef(null);
    const passRef = useRef(null);

    const onChangeHandler = ()=>{
        if(!isEmpty(emailRef.current.value) && !isEmpty(passRef.current.value)){
            setLocalState({...localState, disabledBtn:false});
        }else{
            setLocalState({...localState, disabledBtn:true});
        }
    }

    return (
        <div className='container access-group noselect'>
            <div className='row'>
                <div className='col-md-3'>
                    <div className='zlogo'>
                        <img src='https://zapii.me/assets/data/logo.png' />
                    </div>

                    <div className='zi'>
                        <input maxLength={50} ref={emailRef} onKeyUp={onChangeHandler} onChange={onChangeHandler} type="email" className='zt' placeholder={R.access.email}/>
                    </div>
                    <div className='zi'>
                        <input maxLength={10} ref={passRef} onKeyUp={onChangeHandler} onChange={onChangeHandler} type="password" className='zt' placeholder={R.access.password}/>
                    </div>
                    <div className='zi2'>
                        <button type='button' disabled={localState.isSignUp || localState.disabledBtn} onClick={() => signUpHandler(emailRef.current.value.substring(0,50), passRef.current.value.substring(0,10), localState, setLocalState)}  className={`zabtn zscale-effect2 ${localState.disabledBtn ? 'zdisabled':''}`}>{R.access.sign_up2}</button>
                    </div>
                    <div className='zbt'>
                        <Link to='/forgot' className='lk'>
                            {R.access.forgot_pass}
                        </Link>
                        <Link to='/login' className='lk'>
                            {R.access.login}
                        </Link>
                    </div>
                </div>

                <div className='dng col-md-3'>
                    <div className='dngt'>
                        {R.access.downloadapp}
                    </div>
                    <div className='appg'>
                        <img className='android zdisabled' src={dAndroid} />
                        <img className='iod zdisabled' src={dIos} />
                    </div>
                </div>

                <Footer />
                <NotificationContainer />

            </div>

        </div>

    );
}

export default SignUp;
