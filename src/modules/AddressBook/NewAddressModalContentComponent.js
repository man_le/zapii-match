import React, { useState, useRef, useEffect, useContext } from 'react';
import { isEmpty } from 'lodash';
import R from './../../locale/R';
import type from '../../reducers/type';
import { showNotification } from './../../utils/NotificationUtils';
import { RouteContext } from '../../routes/routes';
import { GlobalStateContext } from '../Layout/HomeLayout';
import { CON_TYPE, DB, fireStore } from './../../utils/DBUtils';
import { validateAddress } from './../../utils/ValidateUtils';
import countries from './flat-divisions.json';
import './stylesheet.scss';
import InputSelectComponent from '../Common/InputSelectComponent';

const NewAddressModalContentComponent = ({ closeModalHandler, addressObj }) => {

    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const [localState, setLocalState] = useState({
        disabled: true, loading: false,
        cityCode: null,
        districtCode: null,
        wardCode: null,
        districtOptions: [],
        wardOptions: []
    });
    const nameRef = useRef(null);
    const phoneRef = useRef(null);
    const addressRef = useRef(null);

    const submitHandler = async () => {

        const { loading, disabled, cityCode, districtCode, wardCode } = localState;

        if (loading) return;
        if (
            isEmpty(nameRef.current.value)
            || isEmpty(phoneRef.current.value)
            || isEmpty(addressRef.current.value)
            || !cityCode
            || !districtCode
            || !wardCode
        ) {
            showNotification('danger', R.addressBook.error_miss);
            return;
        }

        setLocalState({ ...localState, loading: true });

        let cityName = countries.find(e => e.province_code === cityCode);
        if (cityName) cityName = cityName.province_name;

        let districtName = countries.find(e => e.district_code === districtCode);
        if (districtName) districtName = districtName.district_name;

        let wardName = countries.find(e => e.ward_code === wardCode);
        if (wardName) wardName = wardName.ward_name;

        let addressDetail = addressRef.current.value;
        if (!isEmpty(wardName)) addressDetail += ', ' + wardName;
        if (!isEmpty(districtName)) addressDetail += ', ' + districtName;
        if (!isEmpty(cityName)) addressDetail += ', ' + cityName;

        const data = {
            [DB.ADDRESS.NAME]: nameRef.current.value,
            [DB.ADDRESS.PHONE]: phoneRef.current.value,
            [DB.ADDRESS.ADDRESS]: addressRef.current.value,
            addressDetail,
            cityCode,
            districtCode,
            wardCode,
            [DB.ADDRESS.USER_ID]: user.extData[0]._id
        }


        if (isEmpty(addressObj) || isEmpty(addressObj._id)) await fireStore.insert(DB.ADDRESS.name(), data);
        else await fireStore.updateMultiConditions(DB.ADDRESS.name(), data, true, [DB.ADDRESS.ID, CON_TYPE.EQ, addressObj._id]);

        closeModalHandler();

        dispatch({ type: type.UPDATE_ADDRESS });

        setLocalState({ ...localState, loading: false });

    }

    const cancelHandler = () => {
        nameRef.current.value = '';
        phoneRef.current.value = '';
        addressRef.current.value = '';
        setLocalState({ ...localState, name: '', phone: '', address: '' });
        closeModalHandler();
    }

    const buildData = async () => {
        let { name, phone, address, districtOptions, wardOptions } = localState;

        let cityCode = null;
        let districtCode = null;
        let wardCode = null;

        if (!isEmpty(addressObj)) {
            const {
                name: addressName,
                phone: addressPhone,
                address: addressValue,
                _id: address_id,
            } = addressObj;

            cityCode = addressObj.cityCode;
            districtCode = addressObj.districtCode;
            wardCode = addressObj.wardCode;

            if (cityCode) districtOptions = cityOnChangeHandler({ value: cityCode }, true);
            if (districtCode) wardOptions = districtOnChangeHandler({ value: districtCode }, true);

            if (!isEmpty(address_id)) {
                nameRef.current.value = addressName;
                phoneRef.current.value = addressPhone;
                addressRef.current.value = addressValue;

            }
        }

        if (isEmpty(name) || isEmpty(phone) || isEmpty(address)) {
            setLocalState({ ...localState, disabled: true, cityCode, districtCode, wardCode, districtOptions, wardOptions });
        } else {
            if (address.length < 10) {
                setLocalState({ ...localState, disabled: true, cityCode, districtCode, wardCode, districtOptions, wardOptions });
            } else {
                setLocalState({ ...localState, disabled: false, cityCode, districtCode, wardCode, districtOptions, wardOptions });
            }
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            buildData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;

    }, [addressObj]);

    const buildCityOptions = () => {
        const result = [];
        for (let i = 1; i < 100; i++) {
            const city = countries.find(e => e.province_code === i);
            if (city) {
                result.push({
                    value: city.province_code,
                    label: city.province_name
                })
            }
        }
        return result;
    }

    const cityOnChangeHandler = (e, resutlValue) => {
        const value = e.value;
        let { districtOptions } = localState;
        districtOptions = [];
        const district = countries.filter(e => e.province_code === Number(value));
        if (!isEmpty(district)) {
            for (let i = 0; i < district.length; i++) {
                const item = district[i];
                if (isEmpty(districtOptions.filter(e => e.value === item.district_code))) {
                    districtOptions.push({
                        value: item.district_code,
                        label: item.district_name
                    })
                }
            }
        }
        if (!resutlValue) setLocalState({ ...localState, districtOptions, cityCode: value });
        else return districtOptions;
    }

    const districtOnChangeHandler = (e, resultValue) => {
        const value = e.value;
        let { wardOptions } = localState;
        wardOptions = [];
        const district = countries.filter(e => e.district_code === Number(value));
        if (!isEmpty(district)) {
            for (let i = 0; i < district.length; i++) {
                const item = district[i];
                if (isEmpty(wardOptions.filter(e => e.value === item.ward_code))) {
                    wardOptions.push({
                        value: item.ward_code,
                        label: item.ward_name
                    })
                }
            }
        }
        if (!resultValue) setLocalState({ ...localState, wardOptions, districtCode: value });
        else return wardOptions;
    }

    const wardOnChangeHandler = e => {
        const value = e.value;
        setLocalState({ ...localState, wardCode: value });
    }

    return (
        <div className='address-new-modal-content'>
            <div className="zheader noselect">
                <button onClick={closeModalHandler} type="button" className="close zscale-effect">
                    <i className="material-icons">close</i>
                </button>
                <div className='ztitle'>
                    {R.addressBook.addnew}
                </div>
            </div>
            <div className='modal-item-group'>
                <div className='modal-item'>
                    <input ref={nameRef} maxLength={50} type='text' className='txt' placeholder={R.addressBook.name} />
                </div>
                <div className='modal-item'>
                    <input ref={phoneRef} maxLength={20} type='number' className='txt' placeholder={R.addressBook.phone} />
                </div>
                <div className='modal-item zgr'>
                    <InputSelectComponent className='ddlist' value={localState.cityCode} options={buildCityOptions()} onChange={e => cityOnChangeHandler(e)} isSearchable placeholder={R.addressBook.select_city} />
                    <InputSelectComponent className='ddlist' value={localState.districtCode} options={localState.districtOptions} onChange={e => districtOnChangeHandler(e)} isSearchable placeholder={R.addressBook.select_district} />
                    <InputSelectComponent className='ddlist lst' value={localState.wardCode} options={localState.wardOptions} onChange={e => wardOnChangeHandler(e)} isSearchable placeholder={R.addressBook.select_ward} />
                </div>

                <div className='zgrmobile'>
                    <div className='modal-item'>
                        <InputSelectComponent className='ddlist' value={localState.cityCode} options={buildCityOptions()} onChange={e => cityOnChangeHandler(e)} isSearchable placeholder={R.addressBook.select_city} />
                    </div>
                    <div className='modal-item'>
                    <InputSelectComponent className='ddlist' value={localState.districtCode} options={localState.districtOptions} onChange={e => districtOnChangeHandler(e)} isSearchable placeholder={R.addressBook.select_district} />
                    </div>
                    <div className='modal-item'>
                    <InputSelectComponent className='ddlist lst' value={localState.wardCode} options={localState.wardOptions} onChange={e => wardOnChangeHandler(e)} isSearchable placeholder={R.addressBook.select_ward} />
                    </div>
                </div>

                <div className='modal-item'>
                    <div className='textarea-group'>
                        <textarea ref={addressRef} maxLength={300} type='text' className='txt' placeholder={R.addressBook.address} />
                    </div>
                </div>

                <div className='bottom-g'>
                    <div onClick={cancelHandler} className='g noselect zpointer zscale-effect'>
                        <i className='material-icons'>close</i>
                    </div>
                    <div onClick={submitHandler} className={`g zpointer noselect ${localState.loading ? 'zdisabled' : 'zscale-effect'}`}>
                        <i className='material-icons'>check</i>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default NewAddressModalContentComponent;