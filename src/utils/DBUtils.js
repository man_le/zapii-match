import * as firebase from "firebase/app";
import _ from 'lodash';
import uuid from 'uuid';
import "firebase/auth";
import "firebase/firestore";
import "firebase/database";
import "firebase/firebase-functions";
import { init } from "./AuthUtils";

init();
var db = firebase.firestore();

/**
 * Find data
 * @param {*} dbName 
 * @param {*} condition 
 */
const find = async (dbName, ...condition) => {
    try {
        if (_.isEmpty(condition) || _.isEmpty(condition[0]) || condition[0].length < 3) {
            console.error('Cannot find without condition!');
            return false;
        }
        const result = [];

        var query = db.collection(dbName);

        for (let cond of condition) {
            query = query.where(cond[0], cond[1], cond[2]);
        }

        const docs = await query.get();
        if (docs && !_.isEmpty(docs.docs)) {
            docs.docs.map(e => {
                result.push({ id: e.id, ...e.data() });
            })
        }
        return result;
    } catch (err) {
        console.error(err);
    }
    return null;
}

/**
 * Find data
 * @param {*} dbName 
 * @param {*} condition 
 */
const findLimitStatic = async (dbName, limit, ...condition) => {
    try {
        if (_.isEmpty(condition) || _.isEmpty(condition[0]) || condition[0].length < 3) {
            console.error('Cannot delete without condition!');
            return false;
        }
        const result = [];

        var query = db.collection(dbName);

        for (let cond of condition) {
            query = query.where(cond[0], cond[1], cond[2]);
        }

        const docs = await query.orderBy('createdDate', 'desc').limit(limit).get();
        if (docs && !_.isEmpty(docs.docs)) {
            docs.docs.map(e => {
                result.push({ id: e.id, ...e.data() });
            })
        }
        return result;
    } catch (err) {
        console.error(err);
    }
    return null;
}

/**
 * Find data with limit
 * @param {*} dbName
 * @param {*} orderBy
 * @param {*} limit 
 * @param {*} lastVisible
 */
const findLimit = async (dbName, condition, orderBy, limit, lastVisible, filter, orderType) => {
    const result = [];
    let lastDoc = null;

    try {
        if (!orderBy || !limit) {
            console.error('Cannot update without orderBy or limit !');
            return false;
        }

        let query = db.collection(dbName);
        if (condition) query = query.where(condition[0], condition[1], condition[2]);

        if (_.isEmpty(lastVisible)) {
            await query
                .orderBy(orderBy, orderType||'desc')
                .limit(limit)
                .get()
                .then(querySnapshot => {
                    let docs = querySnapshot.docs;
                    if (filter) docs = filter(docs);
                    lastDoc = docs[querySnapshot.docs.length - 1];
                    return docs.forEach((doc) => {
                        result.push(doc.data());
                    });
                });
        } else {
            await query
                .orderBy(orderBy, orderType||'desc')
                .startAfter(lastVisible)
                .limit(limit)
                .get()
                .then(querySnapshot => {
                    let docs = querySnapshot.docs;
                    if (filter) docs = filter(docs);
                    lastDoc = docs[querySnapshot.docs.length - 1];

                    return docs.forEach((doc) => {
                        const data = doc.data();
                        result.push(data);
                    });
                });
        }

    } catch (err) {
        console.error(err);
        return null;
    }

    return { data: result, lastVisible: lastDoc };
}

/**
 * Find data with limit
 * @param {*} dbName
 * @param {*} orderBy
 * @param {*} limit 
 * @param {*} lastVisible
 */
const findLimitMultiConditions = async (dbName, limit, lastVisible, filter, orderBy, orderType, ...condition) => {
    const result = [];
    let lastDoc = null;

    try {
        if (!orderBy || !limit) {
            console.error('Cannot update without orderBy or limit !');
            return false;
        }

        let query = db.collection(dbName);
        for (let cond of condition) {
            if(!_.isEmpty(cond)){
                query = query.where(cond[0], cond[1], cond[2]);
            }
        }

        if (_.isEmpty(lastVisible)) {
            await query
                .orderBy(orderBy, orderType || 'desc')
                .limit(limit)
                .get()
                .then(querySnapshot => {
                    let docs = querySnapshot.docs;
                    if (filter) docs = filter(docs);
                    lastDoc = docs[querySnapshot.docs.length - 1];
                    return docs.forEach((doc) => {
                        result.push(doc.data());
                    });
                });
        } else {
            await query
                .orderBy(orderBy, orderType || 'desc')
                .startAfter(lastVisible)
                .limit(limit)
                .get()
                .then(querySnapshot => {
                    let docs = querySnapshot.docs;
                    if (filter) docs = filter(docs);
                    lastDoc = docs[querySnapshot.docs.length - 1];

                    return docs.forEach((doc) => {
                        const data = doc.data();
                        result.push(data);
                    });
                });
        }

    } catch (err) {
        console.error(err);
        return null;
    }

    return { data: result, lastVisible: lastDoc };
}

/**
 * Insert data
 * @param {*} dbName 
 * @param {*} data 
 */
const insert = async (dbName, data) => {
    try {
        const _id = data._id || uuid.v4();
        const newData = { ...data, ...{ _id, createdDate: _.now(), updatedDate: _.now() } };
        const docRef = await db.collection(dbName).add(newData);
        return _id;
    } catch (err) {
        console.error(err);
    }
    return null;
}

/**
 * @param condition  ex: {'name', '==', 'Zet'}
 * @param dbName ex: 'user'
 * @param data ex: {'name':'Man'}
 * @param isUpsert ex: true
 */
const update = async (dbName, condition, data, isUpsert) => {
    if (_.isEmpty(condition) || condition.length < 3) {
        console.error('Cannot delete without condition!');
        return false;
    }
    try {
        var query = db.collection(dbName);
        query = query.where(condition[0], condition[1], condition[2]);

        const docs = await query.get();
        if (docs && !_.isEmpty(docs.docs)) {
            docs.docs.map(e => {
                db.collection(dbName).doc(e.id).update({ ...data, ...{ updatedDate: _.now() } });
            })
        } else if (isUpsert) {
            insert(dbName, data);
        }
        return true;
    } catch (err) {
        console.error(err);
    }
    return false;
}

/**
 * @param condition  ex: {'name', '==', 'Zet'}
 * @param dbName ex: 'user'
 * @param data ex: {'name':'Man'}
 * @param isUpsert ex: true
 */
const updateMultiConditions = async (dbName, data, isUpsert, ...condition) => {
    if (_.isEmpty(condition) || _.isEmpty(condition[0]) || condition[0].length < 3) {
        console.error('Cannot update without condition!');
        return false;
    }
    try {
        var query = db.collection(dbName);
        for (let cond of condition) {
            query = query.where(cond[0], cond[1], cond[2]);
        }

        const docs = await query.get();
        if (docs && !_.isEmpty(docs.docs)) {
            docs.docs.map(e => {
                db.collection(dbName).doc(e.id).update({ ...data, ...{ updatedDate: _.now() } });
            })
        } else if (isUpsert) {
            return await insert(dbName, data);
        }
        return true;
    } catch (err) {
        console.error(err);
    }
    return false;
}

/**
 * Delete data
 * @param {*} dbName 
 * @param {*} condition 
 */
const del = async (dbName, ...condition) => {
    if (_.isEmpty(condition) || _.isEmpty(condition[0]) || condition[0].length < 3) {
        console.error('Cannot delete without condition!');
        return false;
    }
    try {
        var query = db.collection(dbName);

        for (let cond of condition) {
            query = query.where(cond[0], cond[1], cond[2]);
        }
        const docs = await query.get();
        if (docs && !_.isEmpty(docs.docs)) {
            docs.docs.map(e => {
                db.collection(dbName).doc(e.id).delete();
            })
        }
        return true;
    } catch (err) {
        console.error(err);
    }
    return false;
}

const fireStore = {
    find,
    findLimit,
    findLimitStatic,
    findLimitMultiConditions,
    insert,
    update,
    updateMultiConditions,
    remove: del,
}

/////// Start - Realtime ////////////

const dbRealTime = firebase.database();

const findRealTime = async (dbName, cb, ...key) => {
    try {

        let database = dbRealTime.ref().child(dbName)
        for (let k of key) {
            database = database.child(k);
        }
        database.on('value', snap => {
            if (!_.isNull(cb)) cb(snap);
        })
    } catch (err) {
        console.error(err);
    }
}
const updateRealTime = async (dbName, data, cb, ...key) => {
    try {

        let database = dbRealTime.ref().child(dbName)

        for (let k of key) {
            database = database.child(k);
        }

        // Clear data if user_online is empty
        if(!_.isNull(cb)) cb();

        if (data) database.update(data);
        else database.set(null);


    } catch (err) {
        console.error(err);
    }
}

const onDisconnect = async (dbName, ...key) => {
    try {
        var presenceRef = dbRealTime.ref().child(dbName);
        for (let k of key) {
            presenceRef = presenceRef.child(k);
        }
        presenceRef.onDisconnect().set(null);

    } catch (err) {
        console.error(err);
    }
}

const insertRealTime = async (dbName, data, key) => {
    try {
        let database = dbRealTime.ref().child(dbName).child(key);
        if (data) database.push(data);
        else database.set(null);
    } catch (err) {
        console.error(err);
    }
}

const realTime = {
    find: findRealTime,
    update: updateRealTime,
    insert: insertRealTime,
    onDisconnect,
}
/////// End - Realtime ////////////



/**
 * Constants defined here
 */
const DB = {
    RT_NOTIFICATION: {
        name: () => 'rt_notification',
    },
    RT_NOTIFICATION_INFO: {
        name: () => 'rt_notification_info',
    },
    RT_USER_ONLINE: {
        name: () => 'rt_user_online',
    },
    NOTIFICATION:{
        name: ()=> 'notification',
        ID: '_id',
        USER_ID: 'user_id',
        USER_ID_DID: 'user_id_did',
        OBJECT_ID: 'object_id', // status_id or comment_id
        OBJECT_TYPE: 'object_type',
        IS_READ: 'isRead',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate',
    },
    USER: {
        name: () => 'user',
        ID: '_id',
        USERUID: 'userUid',
        NOTI_LIKE_COMMENT: 'notify_like_comment',
        NOTI_LIKE_STATUS: 'notify_like_status',
        NOTI_COMMENT: 'notify_comment',
        NOTI_SHARE_STATUS: 'notify_share_status',
        NOTI_FOLLOWING: 'notify_following',
        TOTAL_NOTIFICATION: 'total_notification',
        NAME: 'name'
    },
    STATUS: {
        name: () => 'status',
        ID: '_id',
        TYPE: 'type', // trend, status, set
        USERUID: 'userUid',
        USER_ID: 'user_id',
        CREATED_DATE: 'createdDate'
    },
    FOLLOWING: {
        name: () => 'following',
        ID: '_id',
        USER_ID_FROM: 'userid_From',
        USER_ID_TO: 'userid_To',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate',
    },
    LIKE: {
        name: () => 'like',
        ID: '_id',
        USER_ID: 'user_id',
        STATUS_ID: 'status_id',
        COMMENT_ID: 'comment_id',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate',
    },
    COMMENT: {
        name: () => 'comment',
        ID: '_id',
        ID_GEN: '_id_gen',
        USER_ID: 'user_id',
        REPLY_USER_ID: 'reply_user_id',
        IS_REPLY_FOR_MYSELF: 'reply_for_myself',
        STATUS_ID: 'status_id',
        STATUS_OWNER: 'status_owner',
        CONTENT: 'content',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate',
    },
    REPORT: {
        name: () => 'report',
        ID: '_id',
        USER_ID: 'user_id',
        STATUS_ID: 'status_id',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    HISTORY_ACCESS: {
        name: () => 'history',
        ID: '_id',
        USER_ID: 'user_id',
        UPDATED_DATE_READABLE: 'updatedDateReadable', 
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    CLOTHES:{
        name: () => 'clothes',
        ID: '_id',
        TYPE:'type',
        MIX_TYPE:'mix_type',  // 'hat', 'shoes', 'trousers', 'shirt', 'bag' // non, giay, quan, ao, tui sach
        FILTER_TYPE: 'filter_type', // 'hat', 'shoes', 'trousers', 'short', 'shirt', 'tshirt', 'jacket', 'bag', 'dress'
        NAME:'name',
        IMAGE:'image',
        OUT_OF_STOCK: 'outOfStock',
        OLD_PRICE:'old_price',
        NEW_PRICE:'new_price',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    BOOKMARK:{
        name: () => 'bookmark',
        ID: '_id',
        ID_GEN:'_id_gen', // clothes_id$user_id
        CLOTHES_ID:'clothes_id',
        USER_ID:'user_id',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    IN_DESIGN:{
        name: () => 'in_design',
        ID: '_id',
        ID_GEN:'_id_gen', // clothes_id$user_id
        CLOTHES_ID:'clothes_id',
        USER_ID:'user_id',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    IN_BAG:{
        name: () => 'in_bag',
        ID: '_id',
        ID_GEN:'_id_gen', // clothes_id$user_id
        CLOTHES_ID:'clothes_id',
        ORDER_ID:'order_id', // if new, this value is : 'new'
        USER_ID:'user_id',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    ADDRESS:{
        name: () => 'address',
        ID: '_id',
        NAME: 'name',
        PHONE: 'phone',
        ADDRESS: 'address',
        USER_ID:'user_id',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    VOUNCHER:{
        name: () => 'vouncher',
        ID: '_id',
        CODE: 'code', // MAX 6 character
        TYPE: 'type', // 'all' or user_id
        NUMBER_VALID_DATE: 'numberValidDate',
        DISCOUNT_TYPE: 'discountType', // 'percent' or 'value',
        PERCENT_DISCOUNT_VALUE: 'percentDiscountValue', // ex 10%
        MAX_DISCOUNT:'maxDiscount', // value money, ex 15000
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    VOUNCHER_USED:{
        name: () => 'vouncher_used',
        ID: '_id',
        CODE: 'code',
        VOUNCHER_ID: 'vouncher_id',
        USER_ID:'user_id',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    ORDER:{
        name: () => 'order',
        ID: '_id',
        STATUS: 'status', // 1-pending, 2-received, 3-sending, 4-sent,
        NOTE:'note',
        ORDER_ID:'order_id',
        BEFORE_PRICE:'beforePrice',
        ADDRESS_VALUE:'address_value',
        ADDRESS_ID:'address_id',
        TOTAL_PRICE:'totalPrice',
        DISCOUNT_MESSAGE: 'disscountMsg',
        USER_ID: 'user_id',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    CLOTHES_SET:{
        name: () => 'in_mix',
        ID: '_id',
        USER_ID: 'user_id',
        ITEMS:'itemsId', // Array of clotherset id
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    SCORE_ACCUMULATE:{
        name: () => 'score_accumulate',
        ID: '_id',
        USER_ID: 'user_id',
        SCORE: 'score',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    SCORE_ACCUMULATE_STATUS:{
        name: () => 'score_accumulate_status',
        ID: '_id',
        USER_ID: 'user_id',
        STATUS_ID: 'status_id',
        TYPE: 'type', // share, like
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    },
    ADS:{
        name: () => 'ads',
        ID: '_id',
        IMAGE: 'image',
        LINK: 'link',
        SHOW:'show',
        CREATED_DATE: 'createdDate',
        UPDATED_DATE: 'updatedDate'
    }
}
const CON_TYPE = {
    EQ: '==',
    IN: 'in',
    ARRAY_CONTAINS_ANY: 'array-contains-any',
    GR: '>=',
    LE: '<='
}

export {
    DB, CON_TYPE, fireStore, realTime
}
