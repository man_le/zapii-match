import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import R from '../../locale/R';
import BagIcon from '../Common/Icon/BagIcon';
import BookmarkIcon from '../Common/Icon/BookmarkIcon';
import LocationIcon from '../Common/Icon/LocationIcon';

const Comp = ({ history, menuHandler, menuType }) => {
    return (
        <>
            <div className='left-content shadow-s' style={{height:'14rem'}}>
                <ul>
                    <li onClick={() => menuHandler(5)} className={menuType === 5 ? 'active' : ''}>
                        <BookmarkIcon isSelected={true} width='18' height='18' color='#777' style={{ marginTop: '0.3rem' }} />
                        <div>{R.profile.bookmarked}</div>
                    </li>
                    <li onClick={() =>menuHandler(6)} className={menuType === 6 ? 'active' : ''}>
                        <LocationIcon isSelected={true} width='20' height='20' color='#777' style={{ marginTop: '0.15rem' }} />
                        <div>{R.profile.address}</div>
                    </li>
                    <li onClick={() => menuHandler(7)} className={menuType === 7 ? 'active' : ''}>
                        <BagIcon isSelected={true} width='20' height='20' color='#777' style={{ marginTop: '0.15rem' }} />
                        <div>{R.profile.orderStatus}</div>
                    </li>
                    <li onClick={() => menuHandler(8)} className={menuType === 8 ? 'active' : ''}>
                        <i className='material-icons' style={{ fontSize: '1.45rem !important' }}>card_giftcard</i>
                        <div>{R.profile.vouncher_list}</div>
                    </li>
                </ul>
            </div>

            <div className='left-content zapii-info'>

                <div className='zcont noselect'>
                    <Link to='/_privacy'>
                        {R.access.policy}
                    </Link>
                </div>
                <div className='zcont noselect'>
                    <Link to='/_terms'>
                        {R.access.term}
                    </Link>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_qa'>
                        {R.access.qa}
                    </Link>
                </div>

                <div className='zline noselect'>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_about'>
                        ©Zapii.me {new Date().getFullYear()}
                    </Link>
                </div>
            </div>

        </>
    )
}

export default withRouter(Comp);