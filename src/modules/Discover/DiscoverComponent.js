import { isEmpty, isUndefined } from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import InfiniteScroll from "react-infinite-scroll-component";
import { Link } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import type from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import { buildProductFilterTypes } from '../../utils/JSONUtils';
import DataIsEmpty from '../Common/DataIsEmpty';
import ShirtOutlinedIcon from '../Common/Icon/ShirtOutlinedIcon';
import LeftRightContentComponent from '../Common/LeftRightContentComponent';
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from './../../locale/R';
import { CON_TYPE, DB, fireStore } from './../../utils/DBUtils';
import DiscoverItemComponent from './DiscoverItemComponent';
import LeftComponent from './LeftComponent';
import './stylesheet.scss';

const DiscoverComponent = () => {
    const [localState, setLocalState] = useState({
        lastVisible: null,
        clothes: [],
        sort_value: DB.CLOTHES.CREATED_DATE,
        sort_type: 'desc',
        filter_type: 'all',
        isChangeFilterType: false,
        loading: false,
        clothes_type: 'women',
        more: true,
        init: false,

        forceFetch: true
    });

    // Get param typetype
    const params = new URLSearchParams(window.location.search);
    const queryString_filterType = params.get('ft') || 'all';
    const queryString_type = params.get('t') || 'women';

    const [user, setUser] = useContext(RouteContext);

    const [globalState, dispatch] = useContext(GlobalStateContext);

    const buildData = async (loadMore) => {
        let { clothes, lastVisible, forceFetch, filter_type, clothes_type, sort_value, sort_type } = localState;

        setLocalState({ ...localState, loading: true });

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            let condition2 = [];
            let condition1 = [];

            if (!loadMore) {
                if (queryString_filterType !== 'all') {
                    condition2 = [DB.CLOTHES.FILTER_TYPE, CON_TYPE.EQ, queryString_filterType];
                }
                condition1 = [DB.CLOTHES.TYPE, CON_TYPE.EQ, queryString_type];
            } else {
                if (filter_type !== 'all') {
                    condition2 = [DB.CLOTHES.FILTER_TYPE, CON_TYPE.EQ, filter_type];
                }
                condition1 = [DB.CLOTHES.TYPE, CON_TYPE.EQ, clothes_type];
            }

            const result = await fireStore.findLimitMultiConditions(
                DB.CLOTHES.name(),
                10, lastVisible, null, sort_value, sort_type,
                condition1,
                [DB.CLOTHES.OUT_OF_STOCK, CON_TYPE.EQ, false],
                condition2,
            );

            let more = true;
            if (isEmpty(result) || isEmpty(result.data)) {
                more = false;
            }

            if (!isEmpty(result) && !isEmpty(result.data)) {
                const build_ids_gen = [];
                result.data.forEach(e => {
                    if (build_ids_gen.indexOf(`${e._id}$${user.extData[0]._id}`) === -1) build_ids_gen.push(`${e._id}$${user.extData[0]._id}`);
                    if (isEmpty(clothes.filter(i => i._id === e._id))) clothes.push(e);
                });

                // Get bookmark based on clothes_id and user_id
                let bookmark_ids = await fireStore.find(DB.BOOKMARK.name(), [DB.BOOKMARK.ID_GEN, CON_TYPE.IN, build_ids_gen]);
                if (!isEmpty(bookmark_ids)) {
                    bookmark_ids = bookmark_ids.map(e => e._id_gen);

                    let { itemsInBookmark } = globalState;
                    for (let i = 0; i < bookmark_ids.length; i++) {
                        if (itemsInBookmark.indexOf(bookmark_ids[i]) === -1) {
                            itemsInBookmark.push(bookmark_ids[i]);
                        }
                    }
                    dispatch({ type: type.CHANGE_ITEM_IN_BOOKMARK, itemsInBookmark });
                }

                // Get item in bag based on clothes_id and user_id
                let in_bag_ids = await fireStore.find(
                    DB.IN_BAG.name(),
                    [DB.IN_BAG.ID_GEN, CON_TYPE.IN, build_ids_gen],
                    [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
                );
                if (!isEmpty(in_bag_ids)) {

                    // Remove all bag have no items
                    for (let i = 0; i < in_bag_ids.length; i++) {
                        const bag = in_bag_ids[i];

                        if (!isUndefined(bag.S) && bag.S > 0) continue;
                        if (!isUndefined(bag.M) && bag.M > 0) continue;
                        if (!isUndefined(bag.L) && bag.L > 0) continue;
                        if (!isUndefined(bag.XL) && bag.XL > 0) continue;

                        // Size of shoes
                        let haveProduct = false;
                        for (let sz = 32; sz <= 42; sz++) {
                            if (!isUndefined(bag[sz] && bag[sz] > 0)) {
                                haveProduct = true;
                                break;
                            }
                        }

                        if (!haveProduct) {
                            in_bag_ids[i] = null;
                        }
                    }

                    in_bag_ids = in_bag_ids.filter(e => e);
                    in_bag_ids = in_bag_ids.map(e => e && e._id_gen);

                    let { itemsInBag } = globalState;
                    for (let i = 0; i < in_bag_ids.length; i++) {
                        if (itemsInBag.indexOf(in_bag_ids[i]) === -1) {
                            itemsInBag.push(in_bag_ids[i]);
                        }
                    }
                    dispatch({ type: type.CHANGE_ITEM_IN_BAG, itemsInBag });
                }

                filter_type = !loadMore ? queryString_filterType : filter_type;
                clothes_type = !loadMore ? queryString_type : clothes_type;

                setLocalState({
                    ...localState,
                    clothes,
                    lastVisible: result.lastVisible,
                    more,
                    init: true,
                    loading: false,
                    forceFetch: false,
                    filter_type,
                    clothes_type
                });
            } else {
                setLocalState({ ...localState, more: false, init: true, loading: false });
            }
        } else {
            setLocalState({ ...localState, loading: false });
        }
    }

    const buildDataWhenChange = async (filter_typeParam, clothes_typeParam, sort_valueParam, sort_typeParam) => {
        let { loading, init, clothes, lastVisible, filter_type, clothes_type, sort_value, sort_type } = localState;

        setLocalState({ ...localState, loading: true, init: false, clothes: [], lastVisible: null });

        lastVisible = null;
        clothes = [];
        filter_type = filter_typeParam || filter_type;
        clothes_type = clothes_typeParam || clothes_type;
        sort_value = sort_valueParam || sort_value;
        sort_type = sort_typeParam || sort_type;

        let condition2 = [];
        let condition1 = [];

        if (filter_type !== 'all') {
            condition2 = [DB.CLOTHES.FILTER_TYPE, CON_TYPE.EQ, filter_type];
        }
        condition1 = [DB.CLOTHES.TYPE, CON_TYPE.EQ, clothes_type];

        const result = await fireStore.findLimitMultiConditions(
            DB.CLOTHES.name(),
            10, lastVisible, null, sort_value, sort_type,
            condition1,
            [DB.CLOTHES.OUT_OF_STOCK, CON_TYPE.EQ, false],
            condition2,
        );

        if (!isEmpty(result) && !isEmpty(result.data)) {
            const build_ids_gen = [];
            result.data.forEach(e => {
                if (build_ids_gen.indexOf(`${e._id}$${user.extData[0]._id}`) === -1) build_ids_gen.push(`${e._id}$${user.extData[0]._id}`);
                if (isEmpty(clothes.filter(i => i._id === e._id))) clothes.push(e);
            });

            // Get bookmark based on clothes_id and user_id
            let bookmark_ids = await fireStore.find(DB.BOOKMARK.name(), [DB.BOOKMARK.ID_GEN, CON_TYPE.IN, build_ids_gen]);
            if (!isEmpty(bookmark_ids)) {
                bookmark_ids = bookmark_ids.map(e => e._id_gen);

                let { itemsInBookmark } = globalState;
                for (let i = 0; i < bookmark_ids.length; i++) {
                    if (itemsInBookmark.indexOf(bookmark_ids[i]) === -1) {
                        itemsInBookmark.push(bookmark_ids[i]);
                    }
                }
                dispatch({ type: type.CHANGE_ITEM_IN_BOOKMARK, itemsInBookmark });
            }

            // Get item in bag based on clothes_id and user_id
            let in_bag_ids = await fireStore.find(
                DB.IN_BAG.name(),
                [DB.IN_BAG.ID_GEN, CON_TYPE.IN, build_ids_gen],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
            );
            if (!isEmpty(in_bag_ids)) {

                // Remove all bag have no items
                for (let i = 0; i < in_bag_ids.length; i++) {
                    const bag = in_bag_ids[i];

                    if (!isUndefined(bag.S) && bag.S > 0) continue;
                    if (!isUndefined(bag.M) && bag.M > 0) continue;
                    if (!isUndefined(bag.L) && bag.L > 0) continue;
                    if (!isUndefined(bag.XL) && bag.XL > 0) continue;

                    // Size of shoes
                    let haveProduct = false;
                    for (let sz = 32; sz <= 42; sz++) {
                        if (!isUndefined(bag[sz] && bag[sz] > 0)) {
                            haveProduct = true;
                            break;
                        }
                    }

                    if (!haveProduct) {
                        in_bag_ids[i] = null;
                    }
                }

                in_bag_ids = in_bag_ids.filter(e => e);
                in_bag_ids = in_bag_ids.map(e => e && e._id_gen);

                let { itemsInBag } = globalState;
                for (let i = 0; i < in_bag_ids.length; i++) {
                    if (itemsInBag.indexOf(in_bag_ids[i]) === -1) {
                        itemsInBag.push(in_bag_ids[i]);
                    }
                }
                dispatch({ type: type.CHANGE_ITEM_IN_BAG, itemsInBag });
            }

            init = true;
            loading = false;
            lastVisible = result.lastVisible;

        } else {
            init = true;
            loading = false;
            clothes = [];
        }

        setLocalState({
            ...localState,
            clothes,
            lastVisible,
            more: true,
            init: true,
            loading: false,
            filter_type,
            clothes_type,
            sort_type,
            sort_value
        });
    }

    const fetchMoreData = () => {
        if (localState.loading) return;
        buildData(true);
    }

    const updateSort = (value, type) => {
        closeAllModal();
        buildDataWhenChange(localState.filter_type, localState.clothes_type, value, type);
    }

    const updateFilterType = (filter_type) => {
        closeAllModal();
        buildDataWhenChange(filter_type, localState.clothes_type, localState.sort_value, localState.sort_type);
    }

    const updateType = type => {
        closeAllModal();
        buildDataWhenChange(localState.filter_type, type, localState.sort_value, localState.sort_type);
    }

    const closeAllModal = () => {
        let elems = document.querySelectorAll('.modal.show .btnCloseModal');
        if (elems.length > 0) {
            elems[0].click();
            window.location.replace("#");
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            buildData();
            loadCompleted = true;

            if (!window.location.hash) {
                closeAllModal();
            }
        }

        return () => loadCompleted == true;

    }, [
        user,
        // localState.sort_type,
        // localState.sort_value,
        // localState.clothes_type,
        // localState.filter_type,

        window.location.hash
    ]);

    const renderFilterType = type => {
        let key = ''
        if (type === 'short') {
            if (localState.clothes_type === 'man') {
                key = 'short_man';
            } else {
                key = 'short';
            }
        } else {
            key = type;
        }
        return key;
    }


    const isOpenFromIPhoneIPad = () => {
        const platform = window.navigator.appVersion;
        if (platform.indexOf('iPhone') != -1 || platform.indexOf('iPad') != -1) {
            return true;
        }
        return false;
    }

    const filterTypeChangeHandler = e => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;

        updateFilterType(e.target.value);
    }

    const typeChangeHandler = e => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;

        updateType(e.target.value);
    }

    const sortChangeHandler = e => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;

        const value = e.target.value;
        if (value === 'new') {
            updateSort('updatedDate', 'desc');
        } else if (value === 'price_az') {
            updateSort('new_price', 'asc');
        } else if (value === 'price_za') {
            updateSort('new_price', 'desc');
        }
    }

    return (
        <InfiniteScroll
            dataLength={localState.clothes.length}
            next={fetchMoreData}
            hasMore={localState.more && !localState.loading}>

            <div className='zmain full discover-group'>
                <div className='row bar' style={isOpenFromIPhoneIPad() ? { position: 'absolute' } : {}}>
                    {/* Mobile */}
                    {
                        !isOpenFromIPhoneIPad()
                            ?
                            <div className='col-md-6 mobile iphone'>
                                <i className='material-icons'>highlight_alt</i>
                                <select onChange={typeChangeHandler} style={{marginRight:'0.5rem'}}>
                                    <option value="women">{R.discover.women}</option>
                                    <option value="man">{R.discover.man}</option>
                                </select>
                                <div className='spr'></div>
                                <i className='material-icons'>filter_list</i>
                                <select onChange={filterTypeChangeHandler}>
                                    {
                                        buildProductFilterTypes(localState.clothes_type).map((e, i) =>
                                            <option value={e}>
                                                {R.product[renderFilterType(e)]}
                                            </option>
                                        )
                                    }
                                </select>
                                <div className='spr'></div>
                                <i className='material-icons'>sort</i>
                                <select onChange={sortChangeHandler}>
                                    <option value='new'>{R.discover.new}</option>
                                    <option value='price_az'>{R.discover.price_az}</option>
                                    <option value='price_za'>{R.discover.price_za}</option>
                                </select>
                            </div>
                            :
                            <div className='col-md-6 mobile'>
                                <Link to='#type'>
                                    <div data-toggle={!localState.loading ? "modal" : ""} data-target={`#clothes_type`} className={`type ${localState.loading ? "zdisabled" : ""}`}>
                                        <ShirtOutlinedIcon width='15' height='15' style={{ marginTop: '0.25rem', marginRight: '0.25rem' }} />
                                        <div>
                                            {R.discover[localState.clothes_type]}
                                        </div>
                                    </div>
                                </Link>

                                <Link to='#filter'>
                                    <div data-toggle={!localState.loading ? "modal" : ""} data-target={`#clothes_filter`} className={`select ${localState.loading ? "zdisabled" : ""}`}>
                                        <i className='material-icons'>filter_list</i>
                                        <div>
                                            {R.discover.filter}
                                        </div>
                                    </div>
                                </Link>
                                <Link to='#sort'>
                                    <div data-toggle={!localState.loading ? "modal" : ""} data-target={`#clothes_sort`} className={`sort ${localState.loading ? "zdisabled" : ""}`}>
                                        <i className='material-icons'>sort</i>
                                        <div>
                                            {R.discover.sort}
                                        </div>
                                    </div>
                                </Link>
                            </div>

                    }

                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <div className='row list-product'>
                            {
                                isEmpty(localState.clothes) && !localState.init
                                    ? (
                                        <div className='zcenter' style={{ paddingTop: '3.6rem', margin: '0 auto' }}>
                                            <BounceLoader
                                                sizeUnit={"px"}
                                                size={50}
                                                loading={true}
                                                color='#777'
                                            />
                                        </div>
                                    )
                                    :
                                    (
                                        isEmpty(localState.clothes) && localState.init
                                            ? (
                                                <div className='zcenter' style={{ paddingTop: '3.6rem', margin: '0 auto' }}>
                                                    <DataIsEmpty />
                                                </div>
                                            )
                                            :
                                            localState.clothes.map(e =>
                                                <div key={e._id} className='col-md-4 col-6'>
                                                    <DiscoverItemComponent
                                                        globalState={globalState}
                                                        dispatch={dispatch}
                                                        data={e}
                                                        loggedUser={user.extData[0]}
                                                    />
                                                </div>
                                            )
                                    )
                            }
                        </div>
                    </div>
                </div>
            </div>

            <LeftRightContentComponent
                LeftComponent={<LeftComponent clothes_type={localState.clothes_type} filter_type={localState.filter_type} sort_value={localState.sort_value} sort_type={localState.sort_type} typeChangeHandler={typeChangeHandler} filterTypeChangeHandler={filterTypeChangeHandler} sortChangeHandler={sortChangeHandler} />}
            />

        </InfiniteScroll>
    )
}

export default DiscoverComponent