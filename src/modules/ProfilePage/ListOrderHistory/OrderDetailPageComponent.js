import { isEmpty, isUndefined } from 'lodash';
import moment from 'moment';
import 'moment/locale/vi';
import React, { useContext, useEffect, useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import CurrencyFormat from 'react-currency-format';
import short from 'short-uuid';
import { BounceLoader } from 'react-spinners';
import R from '../../../locale/R';
import { RouteContext } from '../../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../../utils/DBUtils';
import { showNotification } from '../../../utils/NotificationUtils';
import { formatMoment } from '../../../utils/StringUtils';
import DataIsEmpty from '../../Common/DataIsEmpty';
import OrderCancelIcon from '../../Common/Icon/OrderCancelIcon';
import OrderCompleteIcon from '../../Common/Icon/OrderCompleteIcon';
import OrderPackingIcon from '../../Common/Icon/OrderPackingIcon';
import OrderPendingIcon from '../../Common/Icon/OrderPendingIcon';
import OrderSendingIcon from '../../Common/Icon/OrderSendingIcon';
import { GlobalStateContext } from '../../Layout/HomeLayout';
import './stylesheet.scss';

const Comp = ({ history, match }) => {

    const { params: { orderCode } } = match;

    const [localState, setLocalState] = useState({
        data: {},
        products: [],
        init: false,
        loading: false
    });

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);

    const cancelOrderHandler = async () => {
        await fireStore.updateMultiConditions(
            DB.ORDER.name(),
            { 'isCancel': true },
            [DB.ORDER.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
            [DB.ORDER.ORDER_ID, CON_TYPE.EQ, localState.data.order_id],
        )

        if (localState.data.vouncher) {
            await fireStore.remove(DB.VOUNCHER_USED.name(), [DB.VOUNCHER_USED.CODE, CON_TYPE.EQ, localState.data.vouncher]);
        }
        showNotification('success', R.profile.cancel_order_success);

        data.isCancel = true;

        history.push(`/${short().fromUUID(user.extData[0]._id)}/t/7`);
    }

    const buildListSize = (e) => {
        let value = '';
        if (!isUndefined(e['S']) && Number(e['S'] > 0)) {
            value += 'S:' + e['S'];
        }
        if (!isUndefined(e['M']) && Number(e['M'] > 0)) {
            value += ' M:' + e['M'];
        }
        if (!isUndefined(e['L']) && Number(e['L'] > 0)) {
            value += ' L:' + e['L'];
        }
        return value;
    }

    const fetchData = async () => {

        setLocalState({ ...localState, loading: true, init: false, data: null });

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {
            const userInfo = user.extData[0];

            let orderState = null;

            if (isEmpty(orderState)) {
                const result = await fireStore.find(
                    DB.ORDER.name(),
                    [DB.ORDER.USER_ID, CON_TYPE.EQ, userInfo._id],
                    [DB.ORDER.ORDER_ID, CON_TYPE.EQ, orderCode],
                );

                if (!isEmpty(result)) {
                    orderState = result[0];
                }
            }

            let products = null;
            if (!isEmpty(orderState)) {
                products = await fireStore.find(
                    DB.IN_BAG.name(),
                    [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, orderState._id]
                )
            }

            setLocalState({
                ...localState,
                data: orderState,
                products,
                init: true,
                loading: false
            });
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [user, globalState.isOpenTabOrderId]);

    const { data, products, init } = localState;

    if (isEmpty(data)) {

        if (!init) return (
            <div className={`zcenter`} style={{ paddingTop: '6rem' }}>
                <BounceLoader
                    sizeUnit={"px"}
                    size={50}
                    loading={true}
                    color='#777'
                />
            </div>
        )
        else return (
            <div className='set-order-group'>
                <DataIsEmpty className='zmargin_center' />
            </div>

        )
    }

    return (

        <div className='full set-order-group zmain'>
            <div className='row bar'>
                <div className='col-md-6 zbor order-detail order-detail-on-page'>

                    <div className='snewg'>
                        <div className='gr'>
                            <i onClick={() => { history.push(`/${short().fromUUID(user.extData[0]._id)}/t/7`) }} style={{ marginTop: '-0.15rem' }} className='material-icons zpointer zscale-effect'>chevron_left</i>
                            <div className='t'>{R.profile.orderStatus + ' / #' + data.order_id}</div>
                        </div>
                    </div>

                    <div className='order-detail-info'>
                        <div className='oi'>
                            <div className='oil'>
                                {R.profile.order_code}
                            </div>
                            <div className='oiv'>
                                {data.order_id}
                            </div>
                        </div>
                        <div className='oi'>
                            <div className='oil'>
                                {R.profile.date}
                            </div>
                            <div className='oiv'>
                                {formatMoment(moment, data.createdDate)}
                            </div>
                        </div>
                        <div className='oi'>
                            <div className='oil'>
                                {R.profile.orderStatus2}
                            </div>
                            {
                                data.isCancel
                                    ?
                                    <div className='oiv'>
                                        <div className='oivs'>
                                            <OrderCancelIcon width='35' height='35' />
                                        </div>
                                        <div className='oivs2'>
                                            {R.profile.cancel_order_status}
                                        </div>
                                    </div>
                                    :
                                    <div className='oiv' style={{ marginLeft: '21%' }}>
                                        <div className='oivs'>
                                            <div>
                                                <OrderPendingIcon width='35' height='35' style={data.status === 'pending' || data.status === 'received' || data.status === 'sending' || data.status === 'sent' ? {} : { filter: "grayscale(100%)" }} />
                                                <div className='it' style={data.status === 'pending' || data.status === 'received' || data.status === 'sending' || data.status === 'sent' ? {} : { color: '#777' }}>
                                                    {
                                                        data.status === 'pending'
                                                            ?
                                                            R.profile.pending
                                                            :
                                                            R.profile.pended
                                                    }
                                                </div>
                                            </div>
                                            <div className='line'></div>

                                            <div>
                                                <OrderPackingIcon width='35' height='35' style={data.status === 'received' || data.status === 'sending' || data.status === 'sent' ? {} : { filter: "grayscale(100%)" }} />
                                                <div className='it' style={data.status === 'received' || data.status === 'sending' || data.status === 'sent' ? {} : { color: '#777' }}>
                                                    {
                                                        data.status === 'pending' || data.status === 'received'
                                                            ?
                                                            R.profile.receiving
                                                            :
                                                            R.profile.received
                                                    }
                                                </div>
                                            </div>
                                            <div className='line'></div>

                                            <div>
                                                <OrderSendingIcon width='40' height='40' style={data.status === 'sending' || data.status === 'sent' ? { marginTop: '-0.25rem' } : { marginTop: '-0.25rem', filter: "grayscale(100%)" }} />
                                                <div className='it' style={data.status === 'sending' || data.status === 'sent' ? {} : { color: '#777' }}>
                                                    {
                                                        R.profile.sending
                                                    }
                                                </div>
                                            </div>
                                            <div className='line'></div>

                                            <div>
                                                <OrderCompleteIcon className='orderCompleteIcon' width='38' height='38' style={data.status === 'sent' ? { marginTop: '-0.15rem' } : { marginTop: '-0.15rem', filter: "grayscale(100%)" }} />
                                                <div className='it' style={data.status === 'sent' ? { marginLeft: '-0.5rem' } : { marginLeft: '-0.5rem', color: '#777' }}>
                                                    {
                                                        R.profile.sent
                                                    }
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                            }

                        </div>
                        <div className='oi'>
                            <div className='oil'>
                                {R.profile.list_product}
                            </div>
                            <div className='oiv'>
                                {
                                    products.map(e =>
                                        !isEmpty(buildListSize(e))
                                        &&
                                        <div key={`product_order_detail_${e._id}`} className='ci2'>
                                            <div className='ic'>
                                                <img src={e.image} />
                                            </div>
                                            <div className='cn'>
                                                <div className='na'>{`${e.name} ${buildListSize(e)}`}</div>
                                                <div className='pr'>
                                                    <CurrencyFormat value={e.price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }
                            </div>
                        </div>

                        {
                            isEmpty(data.disscountMsg)
                                ?
                                <div className='oi'>
                                    <div className='oiv'>
                                        <div className='tt3'>
                                            {R.profile.total}
                                            <span style={{ paddingLeft: '0.5rem' }} className='tt3i'>
                                                <CurrencyFormat value={data.totalPrice} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                :
                                <div className='oi'>
                                    <div className='oil'>
                                        {R.profile.total}
                                    </div>
                                    <div className='oiv'>
                                        <div className='tt1'>
                                            {R.profile.total1}
                                            <CurrencyFormat value={data.beforePrice} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                        </div>
                                        <div className='tt2'>
                                            {data.disscountMsg}
                                        </div>
                                        <div className='tt3'>
                                            {R.profile.left}
                                            <span className='tt3i'>
                                                <CurrencyFormat value={data.totalPrice} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                        }

                        {
                            data.status === 'pending' && (isUndefined(data.isCancel) || data.isCancel === false)
                            &&
                            <div className='oi'>
                                <div onClick={cancelOrderHandler} className='cancel-group zpointer scale-effect'>
                                    {R.profile.cancel_order}
                                </div>
                            </div>
                        }

                    </div>

                </div>
            </div>
        </div>

    )
}

export default withRouter(Comp);