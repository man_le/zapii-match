import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useState, useRef } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { BounceLoader } from 'react-spinners';
import format from 'string-template';
import { RouteContext } from '../../routes/routes';
import moment from 'moment';
import 'moment/locale/vi';
import { formatMoment } from '../../utils/StringUtils';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import NotificationItemComponent from './NotificationItemComponent';
import DataIsEmpty from '../Common/DataIsEmpty';
import { GlobalStateContext } from '../Layout/HomeLayout';
import type from '../../reducers/type';
import R from '../../locale/R';

const NotificationsPanelComponent = () => {

    const [localState, setLocalState] = useState({
        items: [],
        more: false,
        lastVisible: null
    })

    const comNode = useRef();

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);

    const buildData = async () => {
        let { items, lastVisible } = localState;

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            const condition = [DB.NOTIFICATION.USER_ID, CON_TYPE.EQ, user.extData[0]._id];
            const condition2 = ['createdByMe', CON_TYPE.EQ, false];
            let result = await fireStore.findLimitMultiConditions(DB.NOTIFICATION.name(), 10, lastVisible, null, DB.NOTIFICATION.UPDATED_DATE, 'desc', condition, condition2);

            let more = true;
            if (isEmpty(result) || isEmpty(result.data)) {
                more = false;
            }

            if (!isEmpty(result) && !isEmpty(result.data)) {
                const user_ids = [];
                result.data.forEach(e => {
                    user_ids.push(e.user_id_did)
                });
                lastVisible = result.lastVisible;

                const userResult = []

                // Seperate big array to multiple array with 10 items inside
                let maxLen = 100;
                if (user_ids.length < 100) maxLen = user_ids.length;
                let arrayMax10 = [];
                let step = 0;
                for (let i = 0; i <= maxLen / 10; i++) {
                    arrayMax10.push(user_ids.slice(step, step + 10));
                    step += 10;
                }
                for (let i = 0; i < arrayMax10.length; i++) {
                    if (!isEmpty(arrayMax10[i]) && arrayMax10[i].length <= 10) {
                        const _result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.IN, arrayMax10[i]]);
                        if (!isEmpty(_result)) userResult.push(..._result);
                    }
                }
                // End seperating

                if (!isEmpty(userResult)) {

                    user_ids.forEach((id, i) => {
                        const userInfoFound = userResult.filter(e => e._id === id);
                        if (!isEmpty(userInfoFound)) {
                            items.push({
                                ...userInfoFound[0],
                                type: result.data[i].object_type,
                                obj_id: result.data[i].object_id,
                                notification_id: result.data[i]._id,
                                user_id: result.data[i].user_id,
                                creation_date: result.data[i].createdDate,
                                orderStatus: result.data[i].orderStatus,
                                orderCode: result.data[i].orderCode,
                            });
                        }
                    });
                }
                setLocalState({ ...localState, items, lastVisible: result.lastVisible, more, hasInit: true });
            } else {
                setLocalState({ ...localState, more: false, hasInit: true })
            }
        }
    }

    const handleClickOut = (e) => {
        try {
            if (e.target.className.indexOf('nonhidden') !== -1) {
                return;
            }
        } catch (err) {
        }
        dispatch({ type: type.CLOSE_NOTIFICATION_DIALOG });
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            document.addEventListener('mousedown', handleClickOut);

            buildData();
            loadCompleted = true;
        }

        return () => {
            document.removeEventListener('mousedown', handleClickOut);
            return loadCompleted == true;
        }

    }, [user]);



    const fetchMoreData = () => {
        buildData();
    }

    const renderMessageByNotificationType = (type, orderStatus, orderCode) => {
        let msg = null;
        switch (type) {
            case 'like_status':
                msg = R.notification.like_status;
                break;

            case 'like_comment':
                msg = R.notification.like_comment;
                break;

            case 'comment_quiz':
                msg = R.notification.comment_quiz;
                break;

            case 'reply_comment':
                msg = R.notification.reply_comment;
                break;

            case 'share_status':
                msg = R.notification.share_status;
                break;

            case 'score_status':
                msg = format(R.notification.addScore, { score: 1 });
                break;

            case 'order_status':
                switch (orderStatus) {
                    case 'received':
                        msg = format(R.notification.receivedOrder, { orderCode: orderCode });
                        break;

                    case 'sending':
                        msg = format(R.notification.sendingOrder, { orderCode: orderCode });
                        break;

                    case 'sent':
                        msg = format(R.notification.sentOrder, { orderCode: orderCode });
                        break;
                    case 'cancel':
                        msg = format(R.notification.cancelOrder, { orderCode: orderCode });
                        break;
                }
                break;

            case 'following':
                msg = R.notification.following;
                break;

            default:
                msg = R.notification.like_status;
        }
        return msg;
    }

    if (isEmpty(localState.items) || isEmpty(user)) {
        if (!localState.hasInit) {
            return (
                <div className={`zloading-item`} style={{ marginTop: '-3rem' }}><BounceLoader
                    sizeUnit={"px"}
                    size={50}
                    loading={true}
                    color='#777'
                /></div>
            )
        } else {
            return (
                <div style={{ paddingTop: '3rem' }}>
                    <DataIsEmpty label={R.common.no_data} />
                </div>
            )
        }
    }

    return (
        <div ref={comNode} className='sp-st-noti nonhidden' style={{ height: '20rem', overflow: 'auto' }}>
            <InfiniteScroll
                pageStart={0}
                loadMore={fetchMoreData}
                hasMore={localState.more}
                useWindow={false}
            >
                {
                    localState.items.map(e =>
                        <NotificationItemComponent
                            key={'notification_' + Math.random()}
                            userData={
                                e.type !== 'score_status' && e.type !== 'order_status'
                                    ?
                                    { ...e, label: renderMessageByNotificationType(e.type, e.orderStatus, e.orderCode) }
                                    :
                                    {
                                        _id: e.user_id,
                                        avatar: 'https://zapii.me/assets/data/logo2.jpg',
                                        name: R.notification.noti,
                                        orderCode: e.orderCode,
                                        label: renderMessageByNotificationType(e.type, e.orderStatus, e.orderCode)
                                    }
                            }
                            description={formatMoment(moment, e.creation_date)}
                            content_id={e.obj_id}
                            notificationId={e.notification_id}
                            globalState={globalState}
                            dispatch={dispatch}
                            type={e.type}
                        />
                    )
                }

            </InfiniteScroll>
        </div>
    )
}

export default NotificationsPanelComponent;