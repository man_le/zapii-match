import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import InfiniteScroll from "react-infinite-scroll-component";
import { withRouter } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import { updateFollowing } from '../../reducers';
import type from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import DataIsEmpty from '../Common/DataIsEmpty';
import ModalComponent from '../Common/ModalComponent';
import UserItem from '../Common/UserItem';
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from '../../locale/R';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import NewStatusComponent from '../NewStatus/NewStatusComponent';
import './stylesheet.scss';
import TimelineItemComponent from './TimelineItemComponent';
import PageNotFound from '../Common/PageNotFound';

const TimelineComponent = ({ fromProfile, createdByMe, isViewOtherProfile, otherProfileUser_id, history, hideNewStatus, statusType, viewFromHomePage, hide }) => {

    const [globalState, dispatch] = useContext(GlobalStateContext);

    // Get items from Global State
    let items = [];
    let lastVisible = null;
    if (!isEmpty(globalState)) {
        if (statusType === 'status') {
            items = globalState.timelineStatusData.items;
            lastVisible = globalState.timelineStatusData.lastVisible;
        } else if (statusType === 'set') {
            items = globalState.timelineSetData.items;
            lastVisible = globalState.timelineSetData.lastVisible;
        } else if (statusType === 'trend') {
            items = globalState.timelineTrendData.items;
            lastVisible = globalState.timelineTrendData.lastVisible;
        }
    }

    const [localState, setLocalState] = useState({
        items,
        more: false,
        hasInit: false,
        likeInfo: [], // array of {status_id, likeInfoObject},
        followingUserIds: [],
        likeUsers: [],
        isRemoveOldComment: true,
        newComments_id_gened: [],
        currentStatus_id: null,
        lastVisible
    });
    const [user, setUser] = useContext(RouteContext);

    const buildData = async () => {

        let { items, lastVisible } = localState;

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0]) && !hide) {

            let statusResult = null;
            setLocalState({...localState, loading:true});

            // My Page
            if (createdByMe) {
                statusResult = await fireStore.findLimitMultiConditions(
                    DB.STATUS.name(),
                    6,
                    lastVisible,
                    null,
                    DB.STATUS.CREATED_DATE,
                    'desc',
                    [DB.STATUS.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                    statusType ? [DB.STATUS.TYPE, CON_TYPE.EQ, statusType] : null,
                );

                // Other Profile Page
            } else if (isViewOtherProfile) {
                statusResult = await fireStore.findLimitMultiConditions(
                    DB.STATUS.name(),
                    6,
                    lastVisible,
                    null,
                    DB.STATUS.CREATED_DATE,
                    'desc',
                    [DB.STATUS.USER_ID, CON_TYPE.EQ, otherProfileUser_id],
                    statusType ? [DB.STATUS.TYPE, CON_TYPE.EQ, statusType] : null,
                );

                // Home Page
            } else {
                statusResult = await fireStore.findLimitMultiConditions(
                    DB.STATUS.name(),
                    6,
                    lastVisible,
                    null,
                    DB.STATUS.CREATED_DATE,
                    'desc',
                    statusType ? [DB.STATUS.TYPE, CON_TYPE.EQ, statusType] : null
                );
            }

            let more = true;
            if (isEmpty(statusResult) || isEmpty(statusResult.data)) {
                more = false;
            }
            lastVisible = statusResult.lastVisible;

            // Get more data if from homepage
            while (viewFromHomePage && more && isEmpty(statusResult.data.filter(e => e.user_id !== user.extData[0]._id)) && lastVisible) {
                statusResult = await fireStore.findLimitMultiConditions(
                    DB.STATUS.name(),
                    6,
                    lastVisible,
                    null,
                    DB.STATUS.CREATED_DATE,
                    'desc',
                    statusType ? [DB.STATUS.TYPE, CON_TYPE.EQ, statusType] : null
                );
                lastVisible = statusResult.lastVisible;
                if (isEmpty(statusResult) || isEmpty(statusResult.data)) {
                    more = false;
                }
            }

            // Get more data if data from home page is empty
            if (more && viewFromHomePage && !isEmpty(statusResult.data)) {
                const dataFilter = statusResult.data.filter(e => e.user_id !== user.extData[0]._id);
                if (isEmpty(dataFilter)) {
                    setLocalState({ ...localState, fetchMoreData: true, lastVisible: statusResult.lastVisible });
                    return;
                }
            }

            if (!isEmpty(statusResult) && !isEmpty(statusResult.data)) {
                statusResult.data.forEach(e => {
                    if (isEmpty(items.find(ei => ei._id === e._id))) items.push(e);
                });
                lastVisible = statusResult.lastVisible;

                // Get Following Userids
                const followingUserIds = []
                if (!isEmpty(user.extData)) {
                    let followingResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id]);
                    if (!isEmpty(followingResult)) {
                        followingResult.map(e => {
                            if (followingUserIds.indexOf(e.userid_To) === -1) followingUserIds.push(e.userid_To)
                        });
                    }
                }
                setLocalState({ ...localState, items, totalTimeline: items.length, lastVisible: statusResult.lastVisible, more, followingUserIds, hasInit: true, viewFromHomePage, fetchMoreData: false, loading:false });

                // Build Global State
                if (statusType === 'trend') {
                    let timelineTrendData = globalState.timelineTrendData;
                    if (isEmpty(timelineTrendData)) {
                        timelineTrendData = { items, lastVisible: statusResult.lastVisible };
                    } else {
                        for (let i = 0; i < items.length; i++) {
                            const findData = timelineTrendData.items.find(e => e._id === items[i]._id);
                            if (isEmpty(findData)) timelineTrendData.items.push(items[i]);
                        }
                        timelineTrendData.lastVisible = statusResult.lastVisible
                    }
                    dispatch({ type: type.BUILD_TIMELINE_DATA, timelineTrendData, statusType });

                } else if (statusType === 'status') {
                    let timelineStatusData = globalState.timelineStatusData;
                    if (isEmpty(timelineStatusData)) {
                        timelineStatusData = { items, lastVisible: statusResult.lastVisible };
                    } else {
                        for (let i = 0; i < items.length; i++) {
                            const findData = timelineStatusData.items.find(e => e._id === items[i]._id);
                            if (isEmpty(findData)) timelineStatusData.items.push(items[i]);
                        }
                        timelineStatusData.lastVisible = statusResult.lastVisible
                    }
                    dispatch({ type: type.BUILD_TIMELINE_DATA, timelineStatusData, statusType });

                } else if (statusType === 'set') {
                    let timelineSetData = globalState.timelineSetData;
                    if (isEmpty(timelineSetData)) {
                        timelineSetData = { items, lastVisible: statusResult.lastVisible };
                    } else {
                        for (let i = 0; i < items.length; i++) {
                            const findData = timelineSetData.items.find(e => e._id === items[i]._id);
                            if (isEmpty(findData)) timelineSetData.items.push(items[i]);
                        }
                        timelineSetData.lastVisible = statusResult.lastVisible
                    }
                    dispatch({ type: type.BUILD_TIMELINE_DATA, timelineSetData, statusType });
                }

            } else {
                setLocalState({ ...localState, more: false, hasInit: true, viewFromHomePage, fetchMoreData: false, loading:false })
            }
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {

            if(localState.loading) return;
            buildData();

            if (!window.location.hash) {
                closeAllModal();
            }

            loadCompleted = true;
        }

        return () => loadCompleted == true;

    }, [user,
        localState.fetchMoreData === true,
        window.location.hash,
        hide,
    ]);

    const fetchMoreData = () => {
        if(localState.loading) return;
        buildData();
    }

    const followingHandler = async (isUnFollowing, followUserId) => {
        let newStateFollowingUserIds = localState.followingUserIds;
        if (isUnFollowing) {
            newStateFollowingUserIds = newStateFollowingUserIds.filter(e => e !== followUserId);
            await fireStore.remove(DB.FOLLOWING.name(),
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id],
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, followUserId]
            );
        } else {
            const data = {
                [DB.FOLLOWING.USER_ID_FROM]: user.extData[0]._id,
                [DB.FOLLOWING.USER_ID_TO]: followUserId,
            }
            if (newStateFollowingUserIds.indexOf(followUserId) === -1) {
                newStateFollowingUserIds.push(followUserId);
            }
            await fireStore.updateMultiConditions(
                DB.FOLLOWING.name(),
                data, true,
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id],
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, followUserId]
            );
        }

        updateFollowing(dispatch, { followingUserIds: newStateFollowingUserIds })

        setLocalState({ ...localState, followingUserIds: newStateFollowingUserIds });
    }

    const renderTotalLikeComment = () => {
        const { commentLikeUsers, newLikes_comment_users, currentComment_id } = globalState;
        let count = 0;
        if (!isEmpty(commentLikeUsers)) {
            count += commentLikeUsers.length;
        }
        if (!isEmpty(newLikes_comment_users) && !isEmpty(newLikes_comment_users[currentComment_id])) {
            count += newLikes_comment_users[currentComment_id].length;
        }
        return count;
    }

    const renderTotalLike = () => {
        const { likeUserIds, currentStatus_id } = localState;
        const { newLikes_userIds } = globalState;
        let count = 0;
        if (!isEmpty(likeUserIds)) {
            count += likeUserIds.length;
        }
        if (!isEmpty(newLikes_userIds) && !isEmpty(newLikes_userIds[currentStatus_id])) {
            count += newLikes_userIds[currentStatus_id].length;
        }
        return count;
    }

    const renderTotalShare = () => {
        const { shareUserIds, currentStatus_id } = localState;
        const { newShares_userIds } = globalState;
        let count = 0;
        if (!isEmpty(shareUserIds)) {
            count += shareUserIds.length;
        }
        if (!isEmpty(newShares_userIds) && !isEmpty(newShares_userIds[currentStatus_id])) {
            count += newShares_userIds[currentStatus_id].length;
        }
        return count;
    }

    const shareHandler = async (clother_set_id, statusId, statusId_Owner, ownerStatusDate, ownerDescription, ownerFileAttached) => {
        if(createdByMe) return;
        dispatch({ type: type.GET_CLOTHES_SET_LOADING });
        document.getElementById(`id_share_info_modal_from_timeline${statusType?'_'+statusType:''}`).click();

        if (window.innerWidth <= 991) {
            history.push('#new');
        }

        let setClothes = await fireStore.find(DB.CLOTHES_SET.name(), [DB.CLOTHES_SET.ID, CON_TYPE.EQ, clother_set_id]);

        if (!isEmpty(setClothes)) {
            setClothes = setClothes[0];

            let setClothesOwner = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, setClothes.user_id]);
            if (!isEmpty(setClothesOwner)) setClothesOwner = setClothesOwner[0];

            dispatch({ type: type.GET_CLOTHES_SET, setClothes, setClothesOwner, ownerStatusDate, ownerDescription, statusId_Owner, statusId, ownerFileAttached });
        }
    }

    const closeAllModal = () => {
        let elems = document.querySelectorAll('.modal.show .btnCloseModal');
        if (elems.length > 0) {
            elems[0].click();
            window.location.replace("#");
        }
    }

    if (isEmpty(localState.items) && localState.more && isEmpty(user) && isEmpty(user.extData)) return (
        <div className={`zloading-item`}><BounceLoader
            sizeUnit={"px"}
            size={50}
            loading={true}
            color='#777'
        /></div>
    )

    if (isEmpty(user.extData)) return null;

    return (
        <InfiniteScroll
            dataLength={localState.items.length}
            next={fetchMoreData}
            hasMore={localState.more}
        >

            {
                !isViewOtherProfile && !hideNewStatus && !hide
                &&
                <NewStatusComponent viewFromHomePage={!createdByMe && !isViewOtherProfile} />
            }

            <div key={'scroll_timeline_'+(statusType?'_'+statusType:'')} style={hide?{display:'none'}:{}} className={`container-timeline ${fromProfile ? 'zf-pr' : ''} ${statusType==='trend'||statusType==='set' ?'fromTrendStatus':''}`}>
                <div className={`item-timeline ${!hideNewStatus ? 'hasPostPanel' : ''} ${window.location.pathname === '/' ? 'fromHomePage' : ''}`}>
                    <div id="timeline" className={`col-md-6 ${statusType === 'trend' ? 'trend-status' : ''}`}>
                        <data>
                            {
                                (
                                    (isEmpty(localState.items) && !localState.more && localState.hasInit) ||
                                    localState.totalTimeline === 0
                                )
                                    ?
                                    <DataIsEmpty label={R.common.no_data} />
                                    :
                                    localState.items.map((e, i) => {

                                        if (localState.viewFromHomePage && e.user_id === user.extData[0]._id) return null;
                                        else {
                                            return (
                                                <TimelineItemComponent
                                                    createdByMe={createdByMe}
                                                    parentState={localState}
                                                    setParentState={setLocalState}
                                                    followingUserIds={localState.followingUserIds}
                                                    loggedUser={user.extData[0]}
                                                    data={e}
                                                    key={e._id}
                                                    totalTimeline={localState.totalTimeline}
                                                    shareHandler={shareHandler}
                                                    history={history}

                                                    statusType={statusType}
                                                    isTrendStatus={e.type === 'trend'}

                                                // timelineData = {items}
                                                // timelineLastVisible = {lastVisible}
                                                />
                                            )
                                        }

                                    })
                            }
                        </data>
                    </div>
                </div>
            </div>

            <ModalComponent id={'timeline_likeModal' + (statusType?'_'+statusType:'')} title={`${R.timeline.like} (${renderTotalLike()})`}>
                <div>
                    {
                        isEmpty(localState.likeUsers) && isEmpty(globalState.newLikes_users[localState.currentStatus_id]) &&
                        <DataIsEmpty hideIcon={true} label={R.timeline.no_like} color='#eee' />
                    }
                    {
                        !isEmpty(localState.likeUsers)
                        &&
                        localState.likeUsers.map((e, i) =>
                            <UserItem modalId={'timeline_likeModal' + (statusType?'_'+statusType:'')} key={i} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )

                    }
                    {
                        !isEmpty(globalState.newLikes_users) && localState.currentStatus_id && !isEmpty(globalState.newLikes_users[localState.currentStatus_id])
                        &&
                        globalState.newLikes_users[localState.currentStatus_id].map((e, i) =>
                            <UserItem modalId={'timeline_likeModal' + (statusType?'_'+statusType:'')} key={'new_like_' + e._id} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )
                    }

                </div>
            </ModalComponent>

            {/* Share list modal */}
            <ModalComponent id={'timeline_listShareModal' + (statusType?'_'+statusType:'')} title={`${R.timeline.share} (${renderTotalShare()})`}>
                <div>
                    {
                        isEmpty(localState.shareUsers) && isEmpty(globalState.newShares_users[localState.currentStatus_id]) &&
                        <DataIsEmpty hideIcon={true} label={R.timeline.no_share} color='#eee' />
                    }
                    {
                        !isEmpty(localState.shareUsers)
                        &&
                        localState.shareUsers.map((e, i) =>
                            <UserItem modalId={'timeline_listShareModal' + (statusType?'_'+statusType:'')} key={e._id} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )

                    }
                    {

                        !isEmpty(globalState.newShares_users) && localState.currentStatus_id && !isEmpty(globalState.newShares_users[localState.currentStatus_id])
                        &&
                        globalState.newShares_users[localState.currentStatus_id].map((e, i) =>
                            <UserItem modalId={'timeline_listShareModal' + (statusType?'_'+statusType:'')} key={'new_share_' + e._id} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )
                    }

                </div>
            </ModalComponent>

            <ModalComponent id={'comment_likeModal' + (statusType?'_'+statusType:'')} title={`${R.timeline.like} (${renderTotalLikeComment()})`}>
                <div>
                    {
                        isEmpty(globalState.commentLikeUsers) && isEmpty(globalState.newLikes_comment_users[globalState.currentComment_id])
                        &&
                        <DataIsEmpty hideIcon={true} label={R.timeline.no_like} color='#eee' />
                    }
                    {
                        !isEmpty(globalState.commentLikeUsers)
                        &&
                        globalState.commentLikeUsers.map((e, i) =>
                            <UserItem modalId={'comment_likeModal' + (statusType?'_'+statusType:'')} key={i} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )
                    }

                    {
                        !isEmpty(globalState.newLikes_comment_users) && globalState.currentComment_id && !isEmpty(globalState.newLikes_comment_users[globalState.currentComment_id])
                        &&
                        globalState.newLikes_comment_users[globalState.currentComment_id].map((e, i) =>
                            <UserItem modalId={'comment_likeModal' + (statusType?'_'+statusType:'')} key={i} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )
                    }
                </div>
            </ModalComponent>

            {/* Share Modal */}

            {
                statusType === 'set'
                &&
                <ModalComponent id={'share_info_modal_from_timeline' + (statusType?'_'+statusType:'')} hideHeader={true} size={window.innerWidth <= 991 ? 'fullscreen' : 'large'} fromShareModal>
                    <div className='new-status-title noselect'>
                        <i className="material-icons zscale-effect" onClick={closeAllModal}>keyboard_backspace</i>
                        <div className='ti'>
                            {R.timeline.share}
                        </div>
                    </div>
                    <NewStatusComponent fromShareModal />

                </ModalComponent>
            }

            {
                statusType === 'set'
                &&
                <div id={'id_share_info_modal_from_timeline' + (statusType?'_'+statusType:'')} className='hidden' data-toggle={"modal"} data-target={`#share_info_modal_from_timeline` + (statusType?'_'+statusType:'')}></div>
            }

        </InfiniteScroll>
    )
}

export default TimelineComponent;