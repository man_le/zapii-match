import React, { useState } from 'react';
import { isEmpty, isUndefined } from 'lodash';

const ToggleComponent = ({ isActive, user, field, label, callback }) => {

    const [localState, setLocalState] = useState({
        value: !isEmpty(isActive) ? isActive : isEmpty(user) ? true : user[field]
    });

    const changeToggleHandler = async () => {
        let { value } = localState;
        value = !value;
        setLocalState({ ...localState, value });
        if (callback) callback(field, value);
    }

    return (
        <div className="form-group zedit-item">
            <div className="togglebutton">
                <label>
                    <div>
                        <input onChange={changeToggleHandler} type="checkbox" defaultChecked={localState.value}/>
                        <span className="toggle"></span>
                    </div>
                    <div>
                        <span className={localState.value ? 'zactive' : ''}>{label}</span>
                    </div>
                </label>
            </div>
        </div>
    )
}

export default ToggleComponent