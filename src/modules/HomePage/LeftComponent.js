import React from 'react';
import R from '../../locale/R';
import { Link } from 'react-router-dom';
import BagIcon from '../Common/Icon/BagIcon';
import ClothesIcon from '../Common/Icon/ClothesIcon';

const Comp = ({ changeStatusType, statusType }) => {
    return (
        <>
            <div className='left-content shadow-s'>
                <ul>
                    <li onClick={() => changeStatusType(1)} className={statusType === 1 ? 'active' : ''}>
                        <i className='material-icons'>timeline</i>
                        <div>{R.timeline.trend}</div>
                    </li>
                    <li onClick={() => changeStatusType(2)} className={statusType === 2 ? 'active' : ''}>
                        <i className='material-icons'>comment</i>
                        <div>{R.timeline.status}</div>
                    </li>
                    <li onClick={() => changeStatusType(3)} className={statusType === 3 ? 'active' : ''}>
                        <i className='material-icons' style={{ fontSize: '1.35rem !important' }}>view_quilt</i>
                        <div>{R.timeline.setc}</div>
                    </li>
                    <li className='seperator'></li>
                    <Link to='/mix'>
                        <li>
                            <ClothesIcon isSelected={true} width='20' height='20' color='#777' style={{ marginTop: '0.15rem' }} />
                            <div>{R.timeline.mixs}</div>
                        </li>
                    </Link>
                    <Link to='/bag/list'>
                        <li>
                            <BagIcon isSelected={true} width='16' height='16' color='#777' style={{ marginTop: '0.35rem' }} />
                            <div>{R.timeline.bags}</div>
                        </li>
                    </Link>
                </ul>
            </div>

            <div className='left-content zapii-info'>

                <div className='zcont noselect'>
                    <Link to='/_privacy'>
                        {R.access.policy}
                    </Link>
                </div>
                <div className='zcont noselect'>
                    <Link to='/_terms'>
                        {R.access.term}
                    </Link>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_qa'>
                        {R.access.qa}
                    </Link>
                </div>

                <div className='zline noselect'>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_about'>
                        ©Zapii.me {new Date().getFullYear()}
                    </Link>
                </div>
            </div>

        </>
    )
}

export default Comp;