import React, { useEffect, useContext, useState } from 'react';
import CurrencyFormat from 'react-currency-format';
import { isEmpty, isUndefined } from 'lodash';
import * as firebase from "firebase/app";
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from '../../locale/R';
import { BounceLoader } from 'react-spinners';
import CoverAllPage from '../Common/CoverAllPage';
import type from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import './stylesheet.scss';
import InBag2Panel from './InBag2Panel';
import DataIsEmpty from '../Common/DataIsEmpty';
import emptyCartImg from '../InBag/img/emptycart.png';
import { productHaveItems } from '../../utils/JSONUtils';

const InBagPanel = ({ onBack, isShow, }) => {

    const [localState, setLocalState] = useState({ items: [], init: false, lastVisible: null, hasUpdate: null, loading: false, showInBagPage2: false });
    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const closeModalHandler = () => {
        window.location.replace("#");
        onBack();
    }

    const upHandler = async (data) => {

        if (localState.loading) return;

        let newData = {
            [data.key]: firebase.firestore.FieldValue.increment(1),
        };
        if (data[data.key] >= 10) {
            newData = {
                [data.key]: 10
            }
        }

        fireStore.updateMultiConditions(
            DB.IN_BAG.name(),
            newData,
            false,
            [DB.IN_BAG.ID, CON_TYPE.EQ, data._id],
            [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
            [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
        );

        let { items } = localState;
        let dataItem = items.find(e => e._id === data._id && e.key === data.key);
        dataItem[data.key] = Number(dataItem[data.key] < 10) ? Number(dataItem[data.key]) + 1 : 10;
        setLocalState({ ...localState, items });
    }

    const downHandler = async (data) => {

        if (localState.loading) return;

        let newData = {
            [data.key]: firebase.firestore.FieldValue.increment(-1),
        };
        if (data[data.key] <= 0) {
            newData = {
                [data.key]: 0
            }
        }

        // If have no item with current size AND no item with another size, delete this itsm in db
        if (data[data.key] - 1 <= 0) {
            const isExist = productHaveItems(data);
            if (!isExist) {
                fireStore.remove(DB.IN_BAG.name(), [DB.IN_BAG.ID, CON_TYPE.EQ, data._id],
                    [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                    [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'])
            }
        } else {
            fireStore.updateMultiConditions(
                DB.IN_BAG.name(),
                newData,
                false,
                [DB.IN_BAG.ID, CON_TYPE.EQ, data._id],
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
            );
        }

        let { items } = localState;
        let dataItem = items.find(e => e._id === data._id && e.key === data.key);
        dataItem[data.key] = Number(dataItem[data.key] > 0) ? Number(dataItem[data.key]) - 1 : 0;
        setLocalState({ ...localState, items });
    }

    const seperateProductBySize = (items) => {
        let result = [];
        for (let i = 0; i < items.length; i++) {

            if (!isUndefined(items[i].S)) {
                let itemClone = { ...items[i] };
                delete itemClone.M;
                delete itemClone.L;
                delete itemClone.XL;
                itemClone['key'] = 'S';
                result.push(itemClone);
            }
            if (!isUndefined(items[i].M)) {
                let itemClone = { ...items[i] };
                delete itemClone.S;
                delete itemClone.L;
                delete itemClone.XL;
                itemClone['key'] = 'M';
                result.push(itemClone);
            }
            if (!isUndefined(items[i].L)) {
                let itemClone = { ...items[i] };
                delete itemClone.S;
                delete itemClone.M;
                delete itemClone.XL;
                itemClone['key'] = 'L';
                result.push(itemClone);
            }
            if (!isUndefined(items[i].XL)) {
                let itemClone = { ...items[i] };
                delete itemClone.S;
                delete itemClone.M;
                delete itemClone.L;
                itemClone['key'] = 'XL';
                result.push(itemClone);
            }


            for (let sz = 32; sz <= 42; sz++) {
                if (!isUndefined(items[i][sz])) {
                    let itemClone = { ...items[i] };

                    // Remove another size
                    for (let szremove = 32; szremove <= 42; szremove++) {
                        if (szremove === sz) continue;
                        delete itemClone[szremove];
                    }

                    itemClone['key'] = sz.toString();
                    result.push(itemClone);
                }
            }

        }
        return result;
    }

    const buildData = async () => {
        let { items, lastVisible } = localState;

        setLocalState({ ...localState, loading: true });

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            const result = await fireStore.find(
                DB.IN_BAG.name(),
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
            )
            if (!isEmpty(result)) {
                const items = seperateProductBySize(result);
                setLocalState({ ...localState, items, init: true, loading: false });
            } else {
                setLocalState({ ...localState, items: [], init: true, loading: false });
            }
        } else {
            setLocalState({ ...localState, loading: false })
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            buildData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [user, globalState.isUpdateInBag]);

    const renderNum = (num) => {
        if (num >= 10) return 10;
        else if (num <= 0) return 0;
        else return num;
    }

    const buildTotalPrice = () => {
        const { items } = localState;
        let result = 0;
        for (let i = 0; i < items.length; i++) {
            let total = items[i][items[i].key];
            if (total < 0) total = 0;
            const value = items[i].price * total;
            result += value;
        }
        if (result <= 0) result = 0;
        return result;
    }

    const nextPanelHandler = () => {
        if (isEmpty(localState.items) || buildTotalPrice() === 0) return;

        setLocalState({ ...localState, showInBagPage2: true });
        if (isEmpty(globalState.order_id)) {
            dispatch({ type: type.BUILD_ORDER_ID });
        }
    }

    const page2OnBackHandler = () => {
        setLocalState({ ...localState, showInBagPage2: false });
    }

    const renderContent = () => {
        if (localState.init === true) {
            if (!isEmpty(localState.items)) {
                return (
                    localState.items.map(e =>
                        renderNum(e[e.key]) > 0
                        &&
                        <div key={`items_order_${e._id}_${e.key}`} className='pro-item'>
                            <div className='pro-preview'>
                                <img src={e.image} />
                            </div>
                            <div className='pro-name'>
                                {`${e.name} (${e.key})`}
                            </div>

                            <div className='price-count noselect'>
                                <div className='pro-count'>
                                    <div onClick={() => downHandler(e)} className='up zscale-effect'>
                                        -
                                        </div>
                                    <div className='num'>
                                        {renderNum(e[e.key])}
                                    </div>
                                    <div onClick={() => upHandler(e)} className='down zscale-effect'>
                                        +
                                        </div>
                                </div>
                                <div className='pro-price'>
                                    <CurrencyFormat value={e.price * renderNum(e[e.key])} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                </div>
                            </div>
                        </div>
                    )
                )
            } else {
                return (<DataIsEmpty />)
            }
        } else {
            return (
                <div className={`zcenter`}><BounceLoader
                    sizeUnit={"px"}
                    size={50}
                    loading={true}
                    color='#777'
                /></div>
            )
        }
    }

    return (
        <div className="inBag pdetail" style={isShow ? { display: '' } : { display: 'none' }}>
            <div className="zheader noselect">
                <button onClick={closeModalHandler} type="button" className="close zscale-effect">
                    <i className="material-icons">keyboard_backspace</i>
                </button>
                <div className='ztitle'>
                    {R.bag.title}
                </div>
            </div>
            <div className="zbody">
                {
                    renderContent()
                }

                {
                    buildTotalPrice() === 0
                    &&
                    <div className='zcenter'>
                        <div>
                            <img src={emptyCartImg} className='empty' />
                            <div className='emptyt'>{R.common.empty_bag}</div>
                        </div>
                    </div>
                }

                {
                    buildTotalPrice() > 0
                    &&
                    <div className='fixed-bar'>
                        {
                            !isEmpty(localState.items)
                            &&
                            <div className='total'>
                                <div className='l'>
                                    {R.bag.total}
                                </div>
                                <div className='r'>
                                    <CurrencyFormat value={buildTotalPrice()} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                </div>
                            </div>
                        }
                        <div onClick={nextPanelHandler} className={`g ${isEmpty(localState.items) || buildTotalPrice() === 0 ? 'zdisabled' : ''}`}>
                            <div className='ins noselect zpointer'>
                                <div className='l'>
                                    {
                                        R.bag.next
                                    }
                                </div>
                                <i className='material-icons'>navigate_next</i>
                            </div>
                        </div>
                    </div>

                }

            </div>
            <InBag2Panel onBack={page2OnBackHandler} isShow={localState.showInBagPage2} totalPrice={buildTotalPrice()} />
            <CoverAllPage />
        </div>
    )
}

export default InBagPanel;