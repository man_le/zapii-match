import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useReducer, useState } from 'react';
import { NotificationContainer } from 'react-notifications';
import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import globalReducer from '../../reducers';
import ModalComponent from '../Common/ModalComponent';
import WelcomePageComponent from '../Common/WelcomePageComponent';
import InActiveComponent from '../Common/InActiveComponent';
import DiscoverComponent from '../Discover/DiscoverComponent';
import HomePageComponent from '../HomePage/HomePageComponent';
import MenuComponent from '../Menu/MenuComponent';
import { buildKeyWords } from '../../utils/StringUtils';
import R from './../../locale/R';
import { RouteContext } from './../../routes/routes';
import { CON_TYPE, DB, fireStore } from './../../utils/DBUtils';
import './stylesheet.scss';
import DesignComponent from '../Design/DesignComponent';

export const GlobalStateContext = React.createContext([{}, () => { }]);
const HomeLayout = ({ children }) => {
  const [state, setState] = useState({ userSaveInDb:false});
  const initState = {
    currentRouter: '/',
    currentRouterIndex: 1,
    newComments: {},
    currentAnswerUsers: {},
    newLikes_userIds: {}, newLikes_users: {},
    newShares_userIds: {}, newShares_users: {},
    newLikes_comment_userIds: {}, newLikes_comment_users: {},
    isRemoveOldComment: true,
    total_notification: 0,
    totalNewCommentsNotificationSkip: -1,
    isOpenNotificationDialog: false,
    isOpenSearchDialog: false,
    use_total_notification_from_state: false,
    realTimeCommentInit: false,
    realTimeLikeCommentInit: false,
    realTimeLikeStatusInit: false,
    realTimeScoreStatusInit: false,
    realTimeOrderStatusInit: false,
    realTimeShareStatusInit: false,
    realTimeFollowingInit: false,
    hideWelcomePage: false,

    itemsInBag: [],
    itemsInBookmark: [],
    itemsInDesign: [],

    clothesDataDetail: null,
    in_bag_ids: [],
    searchItem: 1,

    // Start - For Design Page
    itemsInMix_hat: [],
    itemsInMix_shirt: [],
    itemsInMix_trousers: [],
    itemsInMix_shoes: [],
    itemsInMix_bag: [],
    // End - For Design Page

    setClothes: {},

    // Timelinedata
    dataForStatusData: [], // userResult, likeResult,... for each status
    timelineTrendData: {
      items: [], lastVisible: null
    },
    timelineStatusData: {
      items: [], lastVisible: null
    },
    timelineSetData: {
      items: [], lastVisible: null
    },

    ads: []

  };
  const [globalState, dispatch] = useReducer(globalReducer, initState);

  // Set user for the first times (after signup)
  const [user, setUser] = useContext(RouteContext);
  const fetchData = async () => {

    if(!state.userSaveInDb && !isEmpty(user) && user.uid){
      const data = {
        name: user.displayName,
        avatar: user.photoURL,
        userUid: user.uid,
        link: '',
        notify_like_comment: true,
        notify_like_status: true,
        notify_share_status: true,
        notify_post_new: true,
        notify_comment: true,
        noti_following: true,
        email: user.email,
        social_id: !isEmpty(user.providerData) ? user.providerData[0].uid : null,
        phoneNumber: user.phoneNumber,
        keywords: buildKeyWords(user.displayName)
    }
      const isSuccess = await fireStore.update(DB.USER.name(), [DB.USER.USERUID, CON_TYPE.EQ, user.uid], data, true);
      if(isSuccess){
        const userInfo = await fireStore.find(DB.USER.name(), [DB.USER.USERUID, CON_TYPE.EQ, user.uid]);
        let loggedInTmp = user;
        loggedInTmp.extData = userInfo;
        setUser(loggedInTmp);
      }
      setState({...state, userSaveInDb:true});
    }
  }
  useEffect(() => {
    fetchData();
  }, [user])

  // Handle to hide HomeComponent
  let displayHomeComponent = false;
  let displayDiscoverComponent = false;
  let displayDesignComponent = false;

  const pathname = window.location.pathname;
  if (pathname === '/' || pathname === '/status' || pathname === '/set') displayHomeComponent = true;
  else if (pathname === '/discover') displayDiscoverComponent = true;
  else if (pathname.indexOf('/mix') !== -1 && pathname.indexOf('/mix/addAll') === -1) displayDesignComponent = true;

  const { path } = children.props.match;
  return (
    <GlobalStateContext.Provider value={[globalState, dispatch]}>
      <ReactNotification />

      {
        !globalState.hideWelcomePage
          ?
          <WelcomePageComponent />
          :
          <div>

            {/* Menu */}
            {
              window.location.pathname.indexOf('_qa') !== -1 ||
                window.location.pathname.indexOf('_privacy') !== -1 ||
                window.location.pathname.indexOf('_about') !== -1 ||
                window.location.pathname.indexOf('_terms') !== -1
                ?
                null
                :
                <MenuComponent path={path} />
            }

            <div>

              {
                !isEmpty(user.extData) && user.extData[0].inactive
                  ?
                  <InActiveComponent user={user} />
                  :
                  children && children
              }

              {
                !isEmpty(user.extData) && !user.extData[0].inactive
                &&
                <div className='novScroll' id='home_content' style={!displayHomeComponent ? { display: 'none' } : {}}>
                  <HomePageComponent />
                </div>
              }

              {
                !isEmpty(user.extData) && !user.extData[0].inactive
                &&
                <div id='discover_content' style={!displayDiscoverComponent ? { display: 'none' } : {}}>
                  <DiscoverComponent />
                </div>
              }

              {
                !isEmpty(user.extData) && !user.extData[0].inactive
                &&
                <div id='design_content' style={!displayDesignComponent ? { display: 'none' } : {}}>
                  <DesignComponent />
                </div>
              }

              <ModalComponent id={'pay_account_modal'} hideHeader={true}>
                <div className='account-info'>
                  <div className='ztitle'>
                    {R.accountInfo.info}
                  </div>
                  <div className='account-group'>
                    <div className='item'>
                      <div className='lbl'>{R.accountInfo.name}</div>
                      <div className='val'>Le Minh Duy</div>
                    </div>
                    <div className='item'>
                      <div className='lbl'>{R.accountInfo.no}</div>
                      <div className='val'>0531002555633</div>
                    </div>
                    <div className='item'>
                      <div className='lbl'>{R.accountInfo.bank}</div>
                      <div className='val'>Vietcombank</div>
                    </div>
                    <div className='item'>
                      <div className='lbl'>{R.accountInfo.account_w}</div>
                      <div className='val'>CN Đông Sài Gòn</div>
                    </div>
                    <div className='item'>
                      <div className='lbl'>{R.accountInfo.note}</div>
                      <div className='val'>{R.accountInfo.note1}#{globalState && globalState.order_id}</div>
                    </div>
                  </div>

                  <div className='account-group'>
                    <div className='item'>
                      <div className='lbl'>{R.accountInfo.name}</div>
                      <div className='val'>Le Minh Duy</div>
                    </div>
                    <div className='item'>
                      <div className='lbl'>{R.accountInfo.no}</div>
                      <div className='val'>060229415630</div>
                    </div>
                    <div className='item'>
                      <div className='lbl'>{R.accountInfo.bank}</div>
                      <div className='val'>Sacombank</div>
                    </div>
                    <div className='item'>
                      <div className='lbl'>{R.accountInfo.account_w}</div>
                      <div className='val'>CN TPHCM</div>
                    </div>
                    <div className='item'>
                      <div className='lbl'>{R.accountInfo.note}</div>
                      <div className='val'>{R.accountInfo.note1}#{globalState && globalState.order_id}</div>
                    </div>
                  </div>
                </div>
              </ModalComponent>

              <ModalComponent id={'timeline_shareURL'}>
                <div className='share-group'>
                  <div className='t'>
                    {R.timeline.copy_share_anywhere}
                  </div>

                  <div className='txt-group'>
                    <input id={'id_test_url'} readOnly type='text' value={globalState.linkTest} />
                    <i id={'id_cp_d'} className='material-icons'>file_copy</i>
                  </div>
                  <div id={'id_cp_msg'} className='zcp transf'>
                    {R.timeline.copied}
                  </div>
                </div>
              </ModalComponent>
              <div id={'id_btn_timeline_shareURL'} className='hidden' data-toggle="modal" data-target={`#timeline_shareURL`}></div>
            </div>
            <NotificationContainer />
          </div>
      }
    </GlobalStateContext.Provider>
  );
}

export default HomeLayout;
