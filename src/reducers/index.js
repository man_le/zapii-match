import moment from 'moment';
import { isEmpty } from 'lodash';
import type from './type';
import { updateFollowing, updateFollowingOffline, updateFollower } from './followEpic';

export default (state = {}, action) => {
  switch (action.type) {
    case type.UPDATE_FOLLOWING: return { ...state, following: action.following }

    case type.UPDATE_FOLLOWING_OFFLINE:
      const index = state.follower.findIndex(e => e._id === action.useridUnfollow);
      const newFollower = state.follower;
      if (index !== -1) {
        newFollower[index].following = false;
      }
      return { ...state, following: action.following, follower: newFollower }

    case type.UPDATE_FOLLOWER: return { ...state, follower: action.follower }
    case type.CHANGE_ROUTER: return { ...state, currentRouter: action.currentRouter, currentRouterIndex: action.currentRouterIndex }
    case type.UPDATE_CURRENT_USERS_ANSWER: return { ...state, loading: action.loading, currentAnswerUsers: action.currentAnswerUsers || [] }

    case type.LIKE_COMMENT_MODAL_LOADING: return { ...state, loading: true }
    case type.LIKE_COMMENT_MODAL: return { ...state, loading: false, commentLikeUsers: action.commentLikeUsers, currentComment_id: action.currentComment_id }
    case type.BUILD_LINK_TEST: return { ...state, linkTest: action.linkTest }

    case type.BUILD_NEW_COMMENT: return { ...state, newComments: action.newComments }
    case type.BUILD_NEW_LIKE: return { ...state, newLikes_userIds: action.newLikes_userIds, newLikes_users: action.newLikes_users }
    case type.BUILD_NEW_SHARE: return { ...state, newShares_userIds: action.newShares_userIds, newShares_users: action.newShares_users }
    case type.BUILD_NEW_LIKE_A_COMMENT: return { ...state, newLikes_comment_userIds: action.newLikes_comment_userIds, newLikes_comment_users: action.newLikes_comment_users }
    case type.INCREASE_TOTAL_NOTIFICATION: return { ...state, total_notification: action.total_notification }
    case type.RESET_TOTAL_NOTIFICATION: return { ...state, total_notification: 0, use_total_notification_from_state: true }

    case type.OPEN_NOTIFICATION_DIALOG: return { ...state, isOpenNotificationDialog: true }
    case type.CLOSE_NOTIFICATION_DIALOG: return { ...state, isOpenNotificationDialog: false }

    case type.OPEN_SEARCH_DIALOG: return { ...state, isOpenSearchDialog: true }
    case type.CLOSE_SEARCH_DIALOG: return { ...state, isOpenSearchDialog: false }

    case type.INIT_TOTAL_NOTIFICATION: return { ...state, total_notification: action.total_notification, initTotalNotification: action.initTotalNotification }

    case type.REALTIME_COMMENT_INIT: return { ...state, realTimeCommentInit: true }
    case type.REALTIME_LIKE_COMMENT_INIT: return { ...state, realTimeLikeCommentInit: true }
    case type.REALTIME_LIKE_STATUS_INIT: return { ...state, realTimeLikeStatusInit: true }
    case type.REALTIME_SCORE_STATUS_INIT: return { ...state, realTimeScoreStatusInit: true }
    case type.REALTIME_ORDER_STATUS_INIT: return { ...state, realTimeOrderStatusInit: true }
    case type.REALTIME_SHARE_STATUS_INIT: return { ...state, realTimeShareStatusInit: true }
    case type.REALTIME_FOLLOWING_INIT: return { ...state, realTimeFollowingInit: true }

    case type.UPDATE_IN_BAG: return { ...state, isUpdateInBag: Math.random() }
    case type.CHANGE_ITEM_IN_BAG: return { ...state, itemsInBag: action.itemsInBag }
    case type.CHANGE_ITEM_IN_BOOKMARK: return { ...state, itemsInBookmark: action.itemsInBookmark }
    case type.CHANGE_ITEM_IN_DESIGN: return { ...state, itemsInDesign: action.itemsInDesign }

    case type.UPDATE_ADDRESS: return { ...state, isUpdateAddress: Math.random() }
    case type.UPDATE_ADDRESS_WITH_ID: return { ...state, isUpdateAddress: Math.random(), addressName: action.addressName, addressPhone: action.addressPhone, addressValue: action.addressValue, address_id: action.address_id }

    case type.BUILD_ORDER_ID: return { ...state, order_id: moment(moment().valueOf()).format("YYDDHHmmss") };
    case type.RESET_ORDER_ID: return { ...state, order_id: null, orderCompleted: false };
    case type.SHOW_WELCOME_PAGE: return {...state, hideWelcomePage: action.hideWelcomePage}

    case type.UPDATE_ITEMS_DESIGN:
      return {
        ...state,
        itemsInMix_hat: action.itemsInMix_hat ? action.itemsInMix_hat : state.itemsInMix_hat,
        itemsInMix_shirt: action.itemsInMix_shirt ? action.itemsInMix_shirt : state.itemsInMix_shirt,
        itemsInMix_trousers: action.itemsInMix_trousers ? action.itemsInMix_trousers : state.itemsInMix_trousers,
        itemsInMix_shoes: action.itemsInMix_shoes ? action.itemsInMix_shoes : state.itemsInMix_shoes,
        itemsInMix_bag: action.itemsInMix_bag ? action.itemsInMix_bag : state.itemsInMix_bag,
        isUpdateItemsInMix: Math.random()
      };

    case type.CHANGE_SEARCH_ITEM: return { ...state, searchItem: action.searchItem }

    case type.GET_CLOTHES_SET_LOADING: return { ...state, loading: true };
    case type.GET_CLOTHES_SET: return {
      ...state,
      loading: false,
      setClothes: action.setClothes,
      setClothesOwner: action.setClothesOwner,
      ownerStatusDate: action.ownerStatusDate,
      statusId_Owner: action.statusId_Owner, // id of owner shared status
      statusId: action.statusId, // id of current status
      ownerDescription: action.ownerDescription,
      ownerFileAttached: action.ownerFileAttached
    };

    case type.OPEN_PROFILE_TAB_SCORE_ID: return { ...state, isOpenTabScore: Math.random() }
    case type.OPEN_PROFILE_TAB_ORDER_ID: return { ...state, isOpenTabOrderId: Math.random() }


    // Timeline data
    case type.BUILD_TIMELINE_DATA:
      const stype = action.statusType;
      if (stype === 'trend') {
        return { ...state, timelineTrendData: action.timelineTrendData}
      } else if (stype === 'status') {
        return { ...state, timelineStatusData: action.timelineStatusData}
      } else if (stype === 'set') {
        return { ...state, timelineSetData: action.timelineSetData}
      }

      case type.BUILD_DATA_IN_SIDE_STATUS_DATA:
        const likeUsers = action.likeUsers;
        const shareUsers = action.shareUsers;
        const userInfo = action.userInfo;
        const comments = action.comments;
        const commentLastVisible = action.commentLastVisible;
        const setClothesOwner = action.setClothesOwner;
        const status_id = action.status_id;
        
        const dataForStatusData = state.dataForStatusData;
        
        if(isEmpty(dataForStatusData.find(e=>e.status_id===status_id))){
          dataForStatusData.push({
            status_id, 
            likeUsers,
            shareUsers,
            userInfo,
            comments,
            setClothesOwner,
            commentLastVisible
          })
        }
        return { ...state, dataForStatusData }

        case type.BUILD_ADS: return { ...state, ads: action.ads }

  }



}

export {
  updateFollowing,
  updateFollowingOffline,
  updateFollower,
}