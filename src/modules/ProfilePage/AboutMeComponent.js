import React from 'react';
import { isEmpty } from 'lodash';
import { Link } from 'react-router-dom';
import './stylesheet.scss';
import R from './../../locale/R';


const AboutMeComponent = ({ user, fromOtherProfile, hideProfile }) => {

    const renderLink = () => {
        if (hideProfile) {
            return <div className='color_placeholder'>{R.profile.secret}</div>
        } else {
            if (isEmpty(user.link)) {
                if (fromOtherProfile) return <div to='/setting/profile' className='color_placeholder'>{R.common.no_info}</div>
                else return <Link to='/setting/profile' className='color_placeholder zpointer'>{R.profile.tell_us_your_link}</Link>
            } else {
                const linkSplit = user.link.split(',');
                return linkSplit.map((e, i) => {
                    const link = '//' + e.trim().replace('http://').replace('https://');
                    return <div key={i}><a className='zlink' target='_blank' href={link}>{e.trim()}</a></div>
                })
            }
        }
    }

    const renderLocation = () => {
        if (hideProfile) {
            return <div className='color_placeholder'>{R.profile.secret}</div>
        } else {
            if (isEmpty(user.location)) {
                if (fromOtherProfile) return <div to='/setting/profile' className='color_placeholder'>{R.common.no_info}</div>
                else return <Link to='/setting/profile' className='color_placeholder zpointer'>{R.profile.tell_us_your_location}</Link>
            } else return user.location
        }
    }

    const renderAbout = () => {
        if (hideProfile) {
            return <div className='color_placeholder'>{R.profile.secret}</div>
        } else {
            if (isEmpty(user.about)) {
                if (fromOtherProfile) return <div to='/setting/profile' className='color_placeholder'>{R.common.no_info}</div>
                else return <Link to='/setting/profile' className='color_placeholder zpointer'>{R.profile.tell_us_about}</Link>
            } else {
                return user.about
            }
        }
    }

    return (
        <div>
            <div className="item-timeline">
                <div id="timeline" className='col-md-6'>
                    <div className={`timeline-group row`}>
                        <div className='col-md about-me-group'>
                            <div className='zi-group'>
                                <i className='material-icons'>link</i>
                                <div>
                                    {renderLink()}
                                </div>
                            </div>
                            <div className='zi-group'>
                                <i className='material-icons'>room</i>
                                <div>
                                    {renderLocation()}
                                </div>
                            </div>
                            <div className='zi-group'>
                                <i className='material-icons'>info</i>
                                <div>
                                    {renderAbout()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AboutMeComponent;