import cookies from 'js-cookie';
import React, { useState, useRef } from 'react';
import './stylesheet.scss';
import { Link } from 'react-router-dom';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { forgotPass } from '../../utils/AuthUtils';
import { validate } from '../../utils/ValidateUtils';
import dAndroid from './Static/img/downloadandroid.png';
import dIos from './Static/img/downloadios.png';
import { isEmpty } from 'lodash';
import Footer from './Footer';
import R from './../../locale/R';

const forgotPassHandler = (email, localState, setLocalState) => {
    if (isEmpty(email)) return;

    const valid = validate({ email });
    if (valid === true && localState.isLoading === false) {
        setLocalState({ isLoading: true });
        forgotPass(email, code => {
            setLocalState({ isLoading: false });
            if (isEmpty(code)) {
                NotificationManager.success(R.access.link_sent, '', 5000);
            } else {
                if (code === 'auth/email-already-in-use') NotificationManager.error(R.access.email_exist, '', 3000);
                else if (code === 'auth/weak-password') NotificationManager.error(R.access.pass_at_least_6, '', 3000);
                else NotificationManager.error(R.access.common_error2, '', 3000);
            }
        });
    }
}

const ForgotPass = () => {

    const [localState, setLocalState] = useState({ isLoading: false, disabledBtn: true });
    const emailRef = useRef(null);

    const onChangeHandler = ()=>{
        if(!isEmpty(emailRef.current.value)){
            setLocalState({...localState, disabledBtn:false});
        }else{
            setLocalState({...localState, disabledBtn:true});
        }
    }

    return (
        <div className='container access-group noselect'>
            <div className='row'>
                <div className='col-md-3'>
                    <div className='zlogo'>
                        <img src='https://zapii.me/assets/data/logo.png' />
                    </div>

                    <div className='zi'>
                        <input maxLength={50} ref={emailRef} onKeyUp={onChangeHandler} onChange={onChangeHandler} type="email" className='zt' placeholder={R.access.email} />
                    </div>
                    <div className='zi2'>
                        <button type='button' disabled={localState.isLoading || localState.disabledBtn} onClick={() => forgotPassHandler(emailRef.current.value.substring(0,50), localState, setLocalState)} className={`zabtn zscale-effect2 ${localState.disabledBtn ? 'zdisabled' : ''}`}>{R.access.reset_pass}</button>
                    </div>
                    <div className='zbt'>
                        <Link to='/login' className='lk'>
                            {R.access.login}
                        </Link>
                        <Link to='/reg' className='lk'>
                            {R.access.sign_up2}
                        </Link>
                    </div>
                </div>

                <div className='dng col-md-3'>
                    <div className='dngt'>
                        {R.access.downloadapp}
                    </div>
                    <div className='appg'>
                        <img className='android zdisabled' src={dAndroid} />
                        <img className='iod zdisabled' src={dIos} />
                    </div>
                </div>

                <Footer />
                <NotificationContainer />

            </div>

        </div>
    );
}

export default ForgotPass;
