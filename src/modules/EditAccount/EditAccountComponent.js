import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { RouteContext } from '../../routes/routes';
import { linkAccount, unLinkAccount, updatePassword, updateEmail } from '../../utils/AuthUtils';
import { validateEmail } from '../../utils/ValidateUtils';
import ModalComponent from '../Common/ModalComponent';
import R from './../../locale/R';
import './stylesheet.scss';
import LeftComponent from './../EditNotification/LeftComponent';
import LeftRightContentComponent from '../Common/LeftRightContentComponent';
import { showNotification } from '../../utils/NotificationUtils';

const EditAccountComponent = () => {

    const [user, setUser] = useContext(RouteContext);
    const [localState, setLocalState] = useState({ 

        isLinkFB: false,
        isLinkGoogle: false,

        'facebook.com': false, 
        'google.com': false, 
        disabledGG: false, 
        disableFB: false, 
        isChangePassSuccess: false, 
        isChangeEmailSuccess:false, email:'' 
    });

    const oldPassRef = useRef(null);
    const oldPassEmailRef = useRef(null);
    const passRef = useRef(null);
    const emailRef = useRef(null);

    useEffect(() => {
        if (user && user.providerData) {
            if(user.providerData.length === 1){
                if (user.providerData[0].providerId === 'google.com') setLocalState({...localState, disabledGG: true });
                else if (user.providerData[0].providerId === 'facebook.com') setLocalState({...localState, disableFB: true });
            }else{
                let {isLinkFB, isLinkGoogle} = localState;
                for(let i=0;i<user.providerData.length;i++){
                    if (user.providerData[i].providerId === 'facebook.com')isLinkFB = true;
                    else if (user.providerData[i].providerId === 'google.com')isLinkGoogle = true;
                    setLocalState({...localState, isLinkFB, isLinkGoogle});
                }
            }
            
        }
    },[user])


    const updatePass = (callback) => {
        const oldPass = oldPassRef.current.value;
        const newPass = passRef.current.value;
        if (!isEmpty(oldPass) && !isEmpty(newPass)) {
            if(oldPass === newPass){
                showNotification('danger', R.setting.old_new_no_equals);
                callback(false);
                return;
            }
            updatePassword(!localState.isChangeEmailSuccess?user.email:localState.email, oldPass, newPass, code => {
                if (isEmpty(code)) {
                    showNotification('success', R.setting.update_pass_success);
                    setLocalState({...localState, isChangePassSuccess: true });
                    oldPassRef.current.value='';
                    passRef.current.value='';
                    callback(true);
                } else {
                    if (code === 'auth/weak-password') showNotification('danger', R.setting.password_at_least_6);
                    else if (code === 'auth/requires-recent-login') showNotification('danger', R.setting.password_need_login_again);
                    else if(code ==='auth/wrong-password') showNotification('danger', R.setting.old_password_wrong);
                    else showNotification('danger', R.setting.password_change_fail);
                    setLocalState({...localState, isChangePassSuccess: false });
                    callback(false);
                }
            });
        }
        callback(false);
    }
    
    const updateEmailHandler = (callback) => {
        const oldPass = oldPassEmailRef.current.value;
        const newEmail = emailRef.current.value;
        if (!isEmpty(oldPass) && !isEmpty(newEmail)) {
            if(!validateEmail(newEmail)){
                showNotification('danger', R.common.email_not_valid);
                callback(false);
                return;
            }else if(newEmail === user.email){
                showNotification('danger', R.common.email_not_valid2);
                callback(false);
                return;
            }
            updateEmail(user.email, oldPass, newEmail, code => {
                if (isEmpty(code)) {
                    showNotification('success', R.setting.update_email_success);
                    setLocalState({...localState, isChangeEmailSuccess: true, email: newEmail });
                    oldPassEmailRef.current.value='';
                    emailRef.current.value='';
                    callback(true);
                } else {
                    if (code === 'auth/weak-password') showNotification('danger', R.setting.password_at_least_6);
                    else if (code === 'auth/requires-recent-login') showNotification('danger', R.setting.password_need_login_again);
                    else if(code ==='auth/wrong-password') showNotification('danger', R.setting.old_password_wrong);
                    else if(code==='auth/email-already-in-use')showNotification('danger', R.setting.email_used_another_account);
                    else showNotification('danger', R.setting.email_change_fail);
                    setLocalState({...localState, isChangeEmailSuccess: false });
                    callback(false);
                }
            });
        }
        callback(false);
    }



    const linkAccountHandler = (type, providers) => {

        let isAlreadyLink = false;
        let providerId = `${type}.com`;
        if (!isEmpty(providers)) {
            const p = providers.find(e => e.providerId === providerId);
            if (type === 'google' && (!isEmpty(p) || localState.isLinkGoogle)) isAlreadyLink = true;
            else if (type === 'facebook' && (!isEmpty(p) || localState.isLinkFB)) isAlreadyLink = true;
        }
    
        if (!isAlreadyLink) {
            linkAccount(type, code => {
                if (isEmpty(code)) {
                    showNotification('success', R.setting.account_link_success);
                    
                    if(type==='google'){
                        setLocalState({...localState, isLinkGoogle: true });
                    }else if(type==='facebook'){
                        setLocalState({...localState, isLinkFB: true });
                    }
                    
                } else {
                    if (code === 'auth/credential-already-in-use') showNotification('danger', R.setting.account_linked_another);
                    else showNotification('danger', R.timeline.error_went_wrong);
                }
            });
        } else {
            unLinkAccount(type, code => {
                if (isEmpty(code)) {

                    if(type==='google'){
                        setLocalState({...localState, isLinkGoogle: false });
                    }else if(type==='facebook'){
                        setLocalState({...localState, isLinkFB: false });
                    }
                    showNotification('success', R.setting.account_un_link_success);
                } else {
                    if (code === 'auth/credential-already-in-use') showNotification('danger', R.setting.account_linked_another);
                    else showNotification('danger', R.timeline.error_went_wrong);
                }
            });
        }
    }
    
    const renderLinkFBLabel = (providers, type) => {
        if (!isEmpty(providers)) {
            const p = providers.find(e => e.providerId === 'facebook.com');
            if (localState.isLinkFB) {
                return R.setting.disconnect;
            }
            else {
                return R.setting.connect;
            }
        }
        return '';
    }

    const renderLinkGGLabel = (providers) => {
        if (!isEmpty(providers)) {
            const p = providers.find(e => e.providerId === 'google.com');
            if (localState.isLinkGoogle) {
                return R.setting.disconnect;
            }
            else {
                return R.setting.connect;
            }
        }
        return '';
    }

    return (
        <>
        <div className='container'>
            <div className='row zaccount-group'>
                <div className='col-md-6'>

                    <div className='t'>{R.setting.account}</div>

                    <div className="form-group zedit-item">
                        <label htmlFor="email" className="bmd-label-floating">{R.setting.email}</label>
                        <div>
                            <span>{!localState.isChangeEmailSuccess ? user.email : localState.email}</span>
                        </div>
                        <div data-toggle="modal" data-target="#changeEmail" className='zchange-pass'>
                            {R.setting.change_email}
                        </div>
                    </div>
                    <div className="form-group zedit-item">
                        <label htmlFor="name" className="bmd-label-floating">{R.setting.password}</label>
                        <div data-toggle="modal" data-target="#changePassword" className='zchange-pass'>
                            {R.setting.change_pass}
                        </div>
                    </div>

                    <div className="form-group zedit-item">
                        <label htmlFor="name" className="bmd-label-floating">{R.setting.connect_social}</label>
                        <div className='zsocial-group'>
                            <div className='zi'>
                                <svg xmlns="http://www.w3.org/2000/svg" fill='#3C5A99' width="24" height="24" viewBox="0 0 24 24"><path d="M22.675 0h-21.35c-.732 0-1.325.593-1.325 1.325v21.351c0 .731.593 1.324 1.325 1.324h11.495v-9.294h-3.128v-3.622h3.128v-2.671c0-3.1 1.893-4.788 4.659-4.788 1.325 0 2.463.099 2.795.143v3.24l-1.918.001c-1.504 0-1.795.715-1.795 1.763v2.313h3.587l-.467 3.622h-3.12v9.293h6.116c.73 0 1.323-.593 1.323-1.325v-21.35c0-.732-.593-1.325-1.325-1.325z" /></svg>
                            </div>
                            <div className={`zl noselect ${localState.disableFB ? 'zdisabled' : ''}`} onClick={() => linkAccountHandler('facebook', user.providerData)}>
                                {renderLinkFBLabel(user.providerData)}
                            </div>
                        </div>
                        <div className='zsocial-group'>
                            <div className='zi'>
                                <svg xmlns="http://www.w3.org/2000/svg" fill='#DB4437' width="24" height="24" viewBox="0 0 24 24"><path d="M19 0h-14c-2.761 0-5 2.239-5 5v14c0 2.761 2.239 5 5 5h14c2.762 0 5-2.239 5-5v-14c0-2.761-2.238-5-5-5zm-10.333 16.667c-2.581 0-4.667-2.087-4.667-4.667s2.086-4.667 4.667-4.667c1.26 0 2.313.46 3.127 1.22l-1.267 1.22c-.347-.333-.954-.72-1.86-.72-1.593 0-2.893 1.32-2.893 2.947s1.3 2.947 2.893 2.947c1.847 0 2.54-1.327 2.647-2.013h-2.647v-1.6h4.406c.041.233.074.467.074.773-.001 2.666-1.787 4.56-4.48 4.56zm11.333-4h-2v2h-1.334v-2h-2v-1.333h2v-2h1.334v2h2v1.333z" /></svg>
                            </div>
                            <div className={`zl noselect ${localState.disabledGG ? 'zdisabled' : ''}`} onClick={() => linkAccountHandler('google', user.providerData)}>
                                {renderLinkGGLabel(user.providerData)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <ModalComponent submitText={R.common.OK} id='changePassword' hasClose hasSubmit title={R.setting.change_pass} submitFn={callback => updatePass(callback)} isSubmitSuccess={localState.isChangeEmailSuccess}>
                <div className="form-group zedit-item">
                    <input placeholder={R.setting.enter_old_pass} maxLength={10} ref={oldPassRef} type="password" className="form-control"/>
                </div>
                <div className="form-group zedit-item">
                    <input placeholder={R.setting.enter_new_pass} maxLength={10} ref={passRef} type="password" className="form-control"/>
                </div>
            </ModalComponent>

            <ModalComponent submitText={R.common.OK} id='changeEmail' hasClose hasSubmit title={R.setting.change_email} submitFn={callback => updateEmailHandler(callback)} isSubmitSuccess={localState.isChangePassSuccess}>
                <div className="form-group zedit-item">
                    <input placeholder={R.setting.enter_new_email} maxLength={50} ref={emailRef} type="email" className="form-control" />
                </div>
                <div className="form-group zedit-item">
                    <input placeholder={R.setting.enter_current_pass} maxLength={10} ref={oldPassEmailRef} type="password" className="form-control" />
                </div>
            </ModalComponent>

        </div>
        
        <LeftRightContentComponent
                LeftComponent={<LeftComponent />}
            />
        </>
    )
}  

export default EditAccountComponent;