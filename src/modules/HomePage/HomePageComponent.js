import React from 'react';
import { withRouter } from 'react-router-dom';
import R from '../../locale/R';
import LeftRightContentComponent from '../Common/LeftRightContentComponent';
import TimelineComponent from '../Status/TimelineComponent';
import LeftComponent from './LeftComponent';
import './stylesheet.scss';

const HomePageComponent = ({ history }) => {

    let statusType = 1;
    const pathname = window.location.pathname;
    if (pathname === '/status') statusType = 2;
    else if (pathname === '/set') statusType = 3;
    else statusType = 1;

    // const [localState, setLocalState] = useState({ statusType: sType })

    const changeStatusType = statusType => {

        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera

        switch (statusType) {
            case 1:
                history.push('/')
                break;
            case 2:
                history.push('/status')
                break;
            case 3:
                history.push('/set')
                break;
        }
    }

    return (
        <>
            {/* For Mobile */}
            <div className='zmain full status-switch-group mobile shadow-top'>
                <div className='row bar'>
                    <div className='col-md-6 contain'>
                        <div onClick={() => changeStatusType(1)} to='/' className={`ci f noselect ${statusType === 1 ? 'active' : ''}`}>
                            {R.timeline.trend}
                        </div>
                        <div onClick={() => changeStatusType(2)} to='/status' className={`ci noselect c ${statusType === 2 ? 'active' : ''}`}>
                            {R.timeline.status}
                        </div>
                        <div onClick={() => changeStatusType(3)} to='/set' className={`ci noselect r ${statusType === 3 ? 'active' : ''}`}>
                            {R.timeline.setc}
                        </div>
                    </div>
                </div>
            </div>

            <TimelineComponent key={'ztimeline_1'} hideNewStatus={true} statusType={'trend'} hide={statusType !== 1} history={history} />
            <TimelineComponent key={'ztimeline_2'} statusType={'status'} viewFromHomePage hide={statusType !== 2} history={history} />
            <TimelineComponent key={'ztimeline_3'} hideNewStatus={true} statusType={'set'} viewFromHomePage hide={statusType !== 3} history={history} />

            <LeftRightContentComponent
                LeftComponent={<LeftComponent changeStatusType={changeStatusType} statusType={statusType} />}
            />
        </>
    )
}

export default withRouter(HomePageComponent);