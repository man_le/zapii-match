import React from 'react';
import { Link } from 'react-router-dom';
import R from '../../locale/R';
import BagIcon from '../Common/Icon/BagIcon';
import ClothesIcon from '../Common/Icon/ClothesIcon';
import ShirtIcon from '../Common/Icon/ShirtIcon';

const Comp = ({ itemsInBag }) => {

    return (
        <>
            <div className='left-content shadow-s' style={{height:'13rem'}}>
                <ul>
                    <Link to='/discover'>
                        <li style={{paddingBottom:'unset'}}>
                            <ShirtIcon width='18' height='18' color='#777' style={{ marginTop: '0.25rem' }} />
                            <div>{R.discover.title}</div>
                        </li>
                    </Link>
                    <li className='seperator'></li>
                    <Link to='/mix'>
                        <li>
                            <ClothesIcon isSelected={true} width='20' height='20' color='#777' style={{ marginTop: '0.15rem' }} />
                            <div>{R.timeline.mixs}</div>
                        </li>
                    </Link>
                    <Link to='/bag/list'>
                        <li>
                            <BagIcon isSelected={true} width='16' height='16' color='#777' style={{ marginTop: '0.35rem' }} />
                            {/* {
                                !isEmpty(itemsInBag)
                                ?
                                <p className='havenew ofp'></p>
                                :
                                <p className='havenew ofp' style={{background:'#fff'}}></p>
                            } */}
                            <div>{R.timeline.bags}</div>
                        </li>
                    </Link>
                </ul>
            </div>

            <div className='left-content zapii-info'>

                <div className='zcont noselect'>
                    <Link to='/_privacy'>
                        {R.access.policy}
                    </Link>
                </div>
                <div className='zcont noselect'>
                    <Link to='/_terms'>
                        {R.access.term}
                    </Link>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_qa'>
                        {R.access.qa}
                    </Link>
                </div>

                <div className='zline noselect'>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_about'>
                        ©Zapii.me {new Date().getFullYear()}
                    </Link>
                </div>
            </div>

        </>
    )
}

export default Comp;