import React from 'react';
import { Link } from 'react-router-dom';
import R from '../../locale/R';
import { signOut } from './../../utils/AuthUtils';


const Comp = ({ }) => {

    return (
        <>
            <div className='left-content shadow-s' style={{ height: '13rem' }}>
                <ul>
                    <Link to='/setting/account'>
                        <li style={{ paddingBottom: 'unset' }}>
                            <div>{R.setting.account}</div>
                        </li>
                    </Link>
                    <Link to='/setting/profile'>
                        <li style={{ paddingBottom: 'unset' }}>
                            <div>{R.setting.profile}</div>
                        </li>
                    </Link>
                    <Link to='/setting/notification'>
                        <li style={{ paddingBottom: 'unset' }}>
                            <div>{R.setting.notification}</div>
                        </li>
                    </Link>
                    <li className='seperator'></li>
                    <li onClick={signOut} style={{ paddingBottom: 'unset' }}>
                        <div>{R.common.logout}</div>
                    </li>
                </ul>
            </div>

            <div className='left-content zapii-info'>

                <div className='zcont noselect'>
                    <Link to='/_privacy'>
                        {R.access.policy}
                    </Link>
                </div>
                <div className='zcont noselect'>
                    <Link to='/_terms'>
                        {R.access.term}
                    </Link>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_qa'>
                        {R.access.qa}
                    </Link>
                </div>

                <div className='zline noselect'>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_about'>
                        ©Zapii.me {new Date().getFullYear()}
                    </Link>
                </div>
            </div>

        </>
    )
}

export default Comp;