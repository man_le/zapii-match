import React from 'react';
import './stylesheet.scss';

const MoreActionComponent = ({className, icon, data}) => {
    return (
        <div className={className}>
            <div data-toggle="dropdown" aria-expanded="false">
                <i className="material-icons noselect zscale-effect zcur">{icon||'more_horiz'}</i>
            </div>
            <div className="dropdown-menu dropdown-menu-right pointer">
                {
                    data
                    &&
                    data.map((e, i) => (
                        e==='-'
                        ?
                        <div key={'dd_'+i} className="dropdown-divider"></div>
                        :
                        <span key={'dd'+i} className="dropdown-item noselect" onClick={()=>e.onClickHandler()}>
                            {e.label}
                        </span>
                    ))
                }
            </div>
        </div>
    )
}

export default MoreActionComponent;