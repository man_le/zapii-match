import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import InfiniteScroll from "react-infinite-scroll-component";
import { withRouter } from 'react-router-dom';
import short from 'short-uuid';
import { BounceLoader } from 'react-spinners';
import { updateFollowing } from '../../reducers';
import type from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import DataIsEmpty from '../Common/DataIsEmpty';
import ModalComponent from '../Common/ModalComponent';
import UserItem from '../Common/UserItem';
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from '../../locale/R';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import NewStatusComponent from '../NewStatus/NewStatusComponent';
import './stylesheet.scss';
import TimelineItemComponent from './TimelineItemComponent';
import LeftRightContentComponent from '../Common/LeftRightContentComponent';
import LeftComponent from './LeftComponent';

const Comp = ({ match, history }) => {

    const { params: { status_id } } = match;

    let id = null;
    try {
        id = short().toUUID(status_id);
    } catch (err) {
        id = null;
    }

    const [localState, setLocalState] = useState({
        data: {},
        hasInit: false,
        likeInfo: [], // array of {status_id, likeInfoObject},
        followingUserIds: [],
        likeUsers: [],
        isRemoveOldComment: true,
        newComments_id_gened: [],
        currentStatus_id: null,
    });
    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const buildData = async () => {
        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            let statusResult = await fireStore.find(DB.STATUS.name(), [DB.STATUS.ID, CON_TYPE.EQ, id]);

            if (!isEmpty(statusResult) && !isEmpty(statusResult)) {

                // Get Following Userids
                const followingUserIds = []
                if (!isEmpty(user.extData)) {
                    let followingResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id]);
                    if (!isEmpty(followingResult)) {
                        followingResult.map(e => {
                            if (followingUserIds.indexOf(e.userid_To) === -1) followingUserIds.push(e.userid_To)
                        });
                    }
                }
                setLocalState({ ...localState, data: statusResult[0], followingUserIds, hasInit: true });
            } else {
                setLocalState({ ...localState, hasInit: true });
            }
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            buildData();

            if (!window.location.hash) {
                closeAllModal();
            }

            loadCompleted = true;
        }

        return () => loadCompleted == true;

    }, [user,
        localState.fetchMoreData === true,
        window.location.hash,
    ]);

    const fetchMoreData = () => {
        buildData();
    }

    const followingHandler = async (isUnFollowing, followUserId) => {
        let newStateFollowingUserIds = localState.followingUserIds;
        if (isUnFollowing) {
            newStateFollowingUserIds = newStateFollowingUserIds.filter(e => e !== followUserId);
            await fireStore.remove(DB.FOLLOWING.name(),
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id],
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, followUserId]
            );
        } else {
            const data = {
                [DB.FOLLOWING.USER_ID_FROM]: user.extData[0]._id,
                [DB.FOLLOWING.USER_ID_TO]: followUserId,
            }
            if (newStateFollowingUserIds.indexOf(followUserId) === -1) {
                newStateFollowingUserIds.push(followUserId);
            }
            await fireStore.updateMultiConditions(
                DB.FOLLOWING.name(),
                data, true,
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id],
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, followUserId]
            );
        }

        updateFollowing(dispatch, { followingUserIds: newStateFollowingUserIds })

        setLocalState({ ...localState, followingUserIds: newStateFollowingUserIds });
    }

    const renderTotalLikeComment = () => {
        const { commentLikeUsers, newLikes_comment_users, currentComment_id } = globalState;
        let count = 0;
        if (!isEmpty(commentLikeUsers)) {
            count += commentLikeUsers.length;
        }
        if (!isEmpty(newLikes_comment_users) && !isEmpty(newLikes_comment_users[currentComment_id])) {
            count += newLikes_comment_users[currentComment_id].length;
        }
        return count;
    }

    const renderTotalLike = () => {
        const { likeUserIds, currentStatus_id } = localState;
        const { newLikes_userIds } = globalState;
        let count = 0;
        if (!isEmpty(likeUserIds)) {
            count += likeUserIds.length;
        }
        if (!isEmpty(newLikes_userIds) && !isEmpty(newLikes_userIds[currentStatus_id])) {
            count += newLikes_userIds[currentStatus_id].length;
        }
        return count;
    }

    const renderTotalShare = () => {
        const { shareUserIds, currentStatus_id } = localState;
        const { newShares_userIds } = globalState;
        let count = 0;
        if (!isEmpty(shareUserIds)) {
            count += shareUserIds.length;
        }
        if (!isEmpty(newShares_userIds) && !isEmpty(newShares_userIds[currentStatus_id])) {
            count += newShares_userIds[currentStatus_id].length;
        }
        return count;
    }

    const shareHandler = async (clother_set_id, statusId, statusId_Owner, ownerStatusDate, ownerDescription) => {
        dispatch({ type: type.GET_CLOTHES_SET_LOADING });

        document.getElementById('id_share_info_modal_from_timeline').click();

        if (window.innerWidth <= 991) {
            history.push('#new');
        }

        let setClothes = await fireStore.find(DB.CLOTHES_SET.name(), [DB.CLOTHES_SET.ID, CON_TYPE.EQ, clother_set_id]);

        if (!isEmpty(setClothes)) {
            setClothes = setClothes[0];

            let setClothesOwner = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, setClothes.user_id]);
            if (!isEmpty(setClothesOwner)) setClothesOwner = setClothesOwner[0];

            dispatch({ type: type.GET_CLOTHES_SET, setClothes, setClothesOwner, ownerStatusDate, ownerDescription, statusId_Owner, statusId });
        }
    }

    const closeAllModal = () => {
        let elems = document.querySelectorAll('.modal.show .btnCloseModal');
        if (elems.length > 0) {
            elems[0].click();
            window.location.replace("#");
        }
    }

    if (isEmpty(localState.data) && !localState.hasInit) return (
        <div className={`zloading-item`}><BounceLoader
            sizeUnit={"px"}
            size={50}
            loading={true}
            color='#777'
        /></div>
    )

    return (
        <InfiniteScroll
            dataLength={localState.data.length}
            hasMore={false}
        >

            {
                localState.data.type !== 'trend'
                &&
                <NewStatusComponent viewFromHomePage={true} />
            }


            <div className={`container-timeline`}>
                <div className="item-timeline hasPostPanel">
                    <div id="timeline" className='col-md-6'>
                        <data>
                            {
                                (
                                    (isEmpty(localState.data) && localState.hasInit)
                                )
                                    ?
                                    <DataIsEmpty label={R.common.no_data} />
                                    :
                                    <TimelineItemComponent
                                        createdByMe={localState.data.user_id === user.extData[0]._id}
                                        parentState={localState}
                                        setParentState={setLocalState}
                                        followingUserIds={localState.followingUserIds}
                                        loggedUser={user.extData[0]}
                                        data={localState.data}
                                        key={localState.data._id}
                                        totalTimeline={1}
                                        shareHandler={shareHandler}

                                        history={history}
                                        statusType={null}
                                        isTrendStatus={localState.data.type === 'trend'}
                                    />
                            }
                        </data>
                    </div>
                </div>
            </div>

            <ModalComponent id={'timeline_likeModal'} title={`${R.timeline.like} (${renderTotalLike()})`}>
                <div>
                    {
                        isEmpty(localState.likeUsers) && isEmpty(globalState.newLikes_users[localState.currentStatus_id]) &&
                        <DataIsEmpty hideIcon={true} label={R.timeline.no_like} color='#eee' />
                    }
                    {
                        !isEmpty(localState.likeUsers)
                        &&
                        localState.likeUsers.map((e, i) =>
                            <UserItem modalId={'timeline_likeModal'} key={i} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )

                    }
                    {
                        !isEmpty(globalState.newLikes_users) && localState.currentStatus_id && !isEmpty(globalState.newLikes_users[localState.currentStatus_id])
                        &&
                        globalState.newLikes_users[localState.currentStatus_id].map((e, i) =>
                            <UserItem modalId={'timeline_likeModal'} key={i} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )
                    }

                </div>
            </ModalComponent>

            {/* Share list modal */}
            <ModalComponent id={'timeline_listShareModal'} title={`${R.timeline.share} (${renderTotalShare()})`}>
                <div>
                    {
                        isEmpty(localState.shareUsers) && isEmpty(globalState.newShares_users[localState.currentStatus_id]) &&
                        <DataIsEmpty hideIcon={true} label={R.timeline.no_share} color='#eee' />
                    }
                    {
                        !isEmpty(localState.shareUsers)
                        &&
                        localState.shareUsers.map((e, i) =>
                            <UserItem modalId={'timeline_listShareModal'} key={i} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )

                    }
                    {
                        !isEmpty(globalState.newShares_users) && localState.currentStatus_id && !isEmpty(globalState.newShares_users[localState.currentStatus_id])
                        &&
                        globalState.newShares_users[localState.currentStatus_id].map((e, i) =>
                            <UserItem modalId={'timeline_listShareModal'} key={i} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )
                    }

                </div>
            </ModalComponent>

            <ModalComponent id={'comment_likeModal'} title={`${R.timeline.like} (${renderTotalLikeComment()})`}>
                <div>
                    {
                        isEmpty(globalState.commentLikeUsers) && isEmpty(globalState.newLikes_comment_users[globalState.currentComment_id])
                        &&
                        <DataIsEmpty hideIcon={true} label={R.timeline.no_like} color='#eee' />
                    }
                    {
                        !isEmpty(globalState.commentLikeUsers)
                        &&
                        globalState.commentLikeUsers.map((e, i) =>
                            <UserItem modalId={'comment_likeModal'} key={i} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )
                    }

                    {
                        !isEmpty(globalState.newLikes_comment_users) && globalState.currentComment_id && !isEmpty(globalState.newLikes_comment_users[globalState.currentComment_id])
                        &&
                        globalState.newLikes_comment_users[globalState.currentComment_id].map((e, i) =>
                            <UserItem modalId={'comment_likeModal'} key={i} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                actionData={[
                                    {
                                        label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                        onClickHandler: () => followingHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )
                    }
                </div>
            </ModalComponent>

            <ModalComponent id={'timeline_shareURL'}>
                <div className='share-group'>
                    <div className='t'>
                        {R.timeline.copy_share_anywhere}
                    </div>

                    <div className='txt-group'>
                        <input id='id_test_url' readOnly type='text' value={globalState.linkTest} />
                        <i id='id_cp_d' className='material-icons'>file_copy</i>
                    </div>
                    <div id='id_cp_msg' className='zcp transf'>
                        {R.timeline.copied}
                    </div>
                </div>
            </ModalComponent>
            <div id='id_btn_timeline_shareURL' className='hidden' data-toggle="modal" data-target={`#timeline_shareURL`}></div>

            {/* Share Modal */}
            <ModalComponent id={'share_info_modal_from_timeline'} hideHeader={true} size={window.innerWidth <= 991 ? 'fullscreen' : 'large'} fromShareModal>
                <div className='new-status-title noselect'>
                    <i className="material-icons zscale-effect" onClick={closeAllModal}>keyboard_backspace</i>
                    <div className='ti'>
                        {R.timeline.share}
                    </div>
                </div>
                <NewStatusComponent fromShareModal />

            </ModalComponent>
            <div id='id_share_info_modal_from_timeline' className='hidden' data-toggle={"modal"} data-target={`#share_info_modal_from_timeline`}></div>


            <LeftRightContentComponent
                LeftComponent={<LeftComponent />}
            />

        </InfiniteScroll>
    )
}

export default withRouter(Comp);