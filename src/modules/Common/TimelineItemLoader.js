import React from 'react';
import ContentLoader from 'react-content-loader';
import './stylesheet.scss';

const TimelineItemLoader = () => {
    return (
        <div className='time-line-item-loader'>
            <ContentLoader
                height={89}
                width={400}
                speed={2}
                primaryColor="#f3f3f3"
                secondaryColor="#ecebeb"
            >
                <rect x="54" y="9" rx="4" ry="4" width="117" height="6" />
                <rect x="56" y="25" rx="3" ry="3" width="85" height="6" />
                <rect x="-3" y="49" rx="3" ry="3" width="350" height="6" />
                <rect x="-11" y="64" rx="3" ry="3" width="380" height="6" />
                <rect x="-2" y="80" rx="3" ry="3" width="201" height="6" />
                <circle cx="20" cy="20" r="20" />
            </ContentLoader>
        </div>

    )
}

export default TimelineItemLoader;
