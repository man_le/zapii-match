import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import Avatar from 'react-avatar';
import NumericLabel from 'react-pretty-numbers';
import { withRouter } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import short from 'short-uuid';
import { updateFollower, updateFollowing, updateFollowingOffline } from '../../reducers';
import reducerType from '../../reducers/type';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import AddressBookComponent from '../AddressBook/AddressBookComponent';
import DataIsEmpty from '../Common/DataIsEmpty';
import AccountIcon from '../Common/Icon/AccountIcon';
import AccountOutlinedIcon from '../Common/Icon/AccountOutlinedIcon';
import ClothesIcon from '../Common/Icon/ClothesIcon';
import LocationIcon from '../Common/Icon/LocationIcon';
import SendIcon from '../Common/Icon/SendIcon';
import ModalComponent from '../Common/ModalComponent';
import MoreActionComponent from '../Common/MoreActionComponent';
import PageNotFound from '../Common/PageNotFound';
import UserItem from '../Common/UserItem';
import { GlobalStateContext } from '../Layout/HomeLayout';
import TimelineComponent from '../Status/TimelineComponent';
import R from './../../locale/R';
import AboutMeComponent from './AboutMeComponent';
import ListBookmarkedComponent from './ListBookmarked/ListBookmarkedComponent';
import ListOrderHistory from './ListOrderHistory/ListOrderHistory';
import ListVouncherComponent from './ListVouncher/ListVouncherComponent';
import SetClotherComponent from './SetClother/SetClotherComponent';
import DefaultCovert from './imgs/default_cover.jpg';
import LeftRightContentComponent from '../Common/LeftRightContentComponent';
import LeftComponent from './LeftComponent';
import RightComponent from './RightComponent';
import './stylesheet.scss';

const Comp = ({ match, history, id }) => {
    const { params: { userId, tabId } } = match;
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const [localState, setLocalState] = useState({
        user: {},
        id: null,
        loadCompleted: false,
        type: !isEmpty(tabId) ? Number(tabId) : 1,
        following: [],  // User following
        follower: [],  // User Follower
        followingResult: [], followerResult: [],
    });
    let translator = short();


    const menuHandler = (type) => {
        dispatch({ type: reducerType.CHANGE_ROUTER, currentRouter: `/${translator.fromUUID(localState.user._id)}/t/${type}` });
        setLocalState({ ...localState, type });
    }

    const fetchData = async () => {
        let result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, id]);
        let userResult = null;
        if (!isEmpty(result)) {
            userResult = result[0];
        }

        let followingResult = [];
        let followerResult = [];

        if (!isEmpty(userResult)) {
            followingResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, userResult._id]);
            followerResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, userResult._id]);
        }

        // Get Following Users
        const followingUserIds = [];
        followingResult.map(e => {
            if (followingUserIds.indexOf(e.userid_To) === -1) followingUserIds.push(e.userid_To)
        });
        updateFollowing(dispatch, { followingUserIds, followingResult });

        // Get Follower Users
        const followerUserIds = [];
        followerResult.map(e => {
            if (followerUserIds.indexOf(e.userid_From) === -1) followerUserIds.push(e.userid_From)
        });
        updateFollower(dispatch, { followingUserIds, followerUserIds, followerResult });

        setLocalState({
            ...localState,
            user: userResult,
            loadCompleted: true,
            key: Math.random(),
            type: tabId ? Number(tabId) : 1
        });
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;

    }, [id, globalState.isOpenTabScore]);


    // Handle Follow button
    const followingHandler = async (isUnFollowing, followUserId) => {
        if (isUnFollowing) {
            await fireStore.remove(DB.FOLLOWING.name(),
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, id],
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, followUserId]
            );
        } else {
            const data = {
                [DB.FOLLOWING.USER_ID_FROM]: id,
                [DB.FOLLOWING.USER_ID_TO]: followUserId,
            }
            await fireStore.updateMultiConditions(
                DB.FOLLOWING.name(),
                data, true,
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, id],
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, followUserId]
            );
        }

        // Get Following Users
        let followingResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, id]);
        const followingUserIds = []
        followingResult.map(e => {
            if (followingUserIds.indexOf(e.userid_To) === -1) followingUserIds.push(e.userid_To)
        });
        updateFollowing(dispatch, { followingUserIds, followingResult });

        // Get Followers info
        let followerResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, id]);
        const followerUserIds = [];
        followerResult.map(e => {
            if (followerUserIds.indexOf(e.userid_From) === -1) followerUserIds.push(e.userid_From)
        });
        updateFollower(dispatch, { followingUserIds, followerUserIds, followerResult });

    }

    // Handle Un-Follow action from Following Modal
    const unFollowFromFollowingHandler = async (user_id) => {
        await fireStore.remove(DB.FOLLOWING.name(),
            [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, id],
            [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, user_id]
        );

        const newFollowingData = globalState.following.filter(e => e._id !== user_id)
        updateFollowingOffline(dispatch, { folowingUsers: newFollowingData, useridUnfollow: user_id });
    }

    const renderTimeLine = () => {
        const { type } = localState;
        switch (type) {
            case 1:
                return <TimelineComponent createdByMe={true} fromProfile={true} key={localState.key} history={history}/>
            case 2:
                return <SetClotherComponent />
            case 3:
                return <AboutMeComponent key={localState.key} user={localState.user} />;
            case 5:
                return <ListBookmarkedComponent />
            case 6:
                return (
                    <div className='full set-bookmark-group zmain noselect'>
                        <div className='row bar'>
                            <div className='col-md-6 zbor' style={{ paddingBottom: '1rem' }}>
                                <div className='snewg'>
                                    <div className='gr'>
                                        <LocationIcon width='25' height='25' style={{ marginTop: '-0.25rem' }} />
                                        <div className='t'>{R.profile.address}</div>
                                    </div>
                                </div>
                                <AddressBookComponent />
                            </div>
                        </div>
                    </div>
                )
            case 7:
                return <ListOrderHistory />
            case 8:
                return <ListVouncherComponent />
        }
    }

    if (!localState.loadCompleted) return (
        <div className={`zloading-item`}>
            <BounceLoader
                sizeUnit={"px"}
                size={50}
                loading={true}
                color='#777'
            /></div>);
    else if (isEmpty(localState.user)) return <PageNotFound />;


    return (
        <div style={{ minHeight: '39rem' }}>
            <div className='row col-md-6 profile-info'>

                <div className='row col-md-6 zcover-g'>
                    <img src={localState.user.cover || DefaultCovert} />
                    <div className='zcover-line'></div>
                </div>

                <div className='col-md-3 zavatar-g'>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <Avatar className='of_cover' src={localState.user.avatar} name={localState.user.name} size='7rem' round={true} />
                    </div>
                </div>

            </div>

            <div className='row col-md-6 uname-g'>
                <div className='fl'>
                    <div className="zlable" data-toggle="modal" data-target="#followId">
                        <strong><NumericLabel>{globalState.following ? globalState.following.length : 0}</NumericLabel></strong> {R.profile.following}
                    </div>
                </div>
                <div className='zname zellipis'>
                    {localState.user.name}
                </div>
                <div className='fl'>
                    <div className="zlable" data-toggle="modal" data-target="#followerId">
                        <strong><NumericLabel>{globalState.follower ? globalState.follower.length : 0}</NumericLabel></strong> {R.profile.follower}
                    </div>
                </div>
            </div>

            <div className='row col-md-6 uname-g-mobile'>
                <div className='zname zellipis'>
                    {localState.user.name}
                </div>
                <div className='fl'>
                    <div className="zlable" data-toggle="modal" data-target="#followId">
                        <strong><NumericLabel>{globalState.following ? globalState.following.length : 0}</NumericLabel></strong> {R.profile.following}
                    </div>
                </div>
                <div className='fl'>
                    <div className="zlable" data-toggle="modal" data-target="#followerId">
                        <strong><NumericLabel>{globalState.follower ? globalState.follower.length : 0}</NumericLabel></strong> {R.profile.follower}
                    </div>
                </div>
            </div>

            <div className='row tag-group' style={localState.type !==1 ? {marginBottom:'0.75rem'}: {}}>
                <div className='col-md-6 ztag'>
                    <div className='ztag-item'>
                        <ul className='g'>
                            <li onClick={() => menuHandler(1)} className={`noselect ${localState.type === 1 ? 'active' : ''}`}>
                                <SendIcon isSelected={localState.type === 1} width='27' height='27' />
                                <div>{R.profile.created}</div>
                            </li>
                            <li onClick={() => menuHandler(2)} className={`noselect ${localState.type === 2 ? 'active' : ''}`}>
                                <ClothesIcon isSelected={localState.type === 2} style={{ marginTop: '-0.15rem' }} />
                                <div>{R.profile.setClo}</div>
                            </li>
                            <li onClick={() => menuHandler(3)} className={`noselect ${localState.type === 2 ? 'active' : ''}`}>
                                {
                                    localState.type === 3
                                        ?
                                        <AccountIcon />
                                        :
                                        <AccountOutlinedIcon style={{ stroke: '#fff', strokeWidth: '7px' }} />
                                }
                                <div>{R.profile.about}</div>
                            </li>
                        </ul>

                        <ul className='g-mobile'>
                            <li onClick={() => menuHandler(1)} className={`noselect ${localState.type === 1 ? 'active' : ''}`}>
                                <SendIcon isSelected={localState.type === 1} width='27' height='27' />
                            </li>
                            <li onClick={() => menuHandler(2)} className={`noselect ${localState.type === 2 ? 'active' : ''}`}>
                                <ClothesIcon isSelected={localState.type === 2} style={{ marginTop: '-0.15rem' }} />
                            </li>
                            <li onClick={() => menuHandler(3)} className={`noselect ${localState.type === 3 ? 'active' : ''}`}>
                                {
                                    localState.type === 3
                                        ?
                                        <AccountIcon />
                                        :
                                        <AccountOutlinedIcon style={{ stroke: '#fff', strokeWidth: '7px' }} />
                                }
                            </li>
                            <li className='zdropdown'>
                                <MoreActionComponent
                                    data={
                                        [
                                            {
                                                label: R.profile.bookmarked,
                                                onClickHandler: () => menuHandler(5)
                                            },
                                            {
                                                label: R.profile.address,
                                                onClickHandler: () => menuHandler(6)
                                            },
                                            {
                                                label: R.profile.orderStatus,
                                                onClickHandler: () => menuHandler(7)
                                            },
                                            {
                                                label: R.profile.vouncher_list,
                                                onClickHandler: () => menuHandler(8)
                                            }
                                        ]
                                    }
                                />
                            </li>
                        </ul>

                    </div>
                </div>
            </div>

            <div className={localState.type === 1 ? 'novScroll' : ''}>
                {renderTimeLine()}
            </div>

            <ModalComponent id='followId' title={`${R.timeline.following} (${globalState.following ? globalState.following.length : 0})`}>
                <div>
                    {
                        !isEmpty(globalState.following)
                            ?
                            globalState.following.map((e, i) =>
                                <UserItem modalId='followId'
                                    hideActionMenu={userId !== translator.fromUUID(id)}
                                    key={i} userData={e}
                                    actionData={[{ label: R.timeline.un_follow, onClickHandler: () => unFollowFromFollowingHandler(e._id) }]}
                                />
                            )
                            :
                            <DataIsEmpty hideIcon={true} label={R.common.no_following} color='#eee' />
                    }

                </div>
            </ModalComponent>

            <ModalComponent id='followerId' title={`${R.timeline.follower} (${globalState.follower ? globalState.follower.length : 0})`}>
                <div>
                    {
                        !isEmpty(globalState.follower)
                            ?
                            globalState.follower.map((e, i) =>
                                <UserItem modalId='followerId' key={i}
                                    description={e.following && R.timeline.following}
                                    userData={e} actionData={[
                                        {
                                            label: e.following ? R.timeline.un_follow : R.timeline.follow,
                                            onClickHandler: () => followingHandler(e.following ? true : false, e._id)
                                        }
                                    ]} />
                            )
                            :
                            <DataIsEmpty hideIcon={true} label={R.common.no_follower} color='#eee' />
                    }

                </div>
            </ModalComponent>


            <LeftRightContentComponent
                LeftComponent={<LeftComponent menuHandler={menuHandler} menuType={localState.type} />}
                RightComponent={<RightComponent/>}
                />

        </div>

    )
}

export default withRouter(Comp);