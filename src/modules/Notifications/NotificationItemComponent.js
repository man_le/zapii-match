import { isEmpty } from 'lodash';
import React, { useState } from 'react';
import Avatar from 'react-avatar';
import { Link, withRouter } from 'react-router-dom';
import short from 'short-uuid';
import type from '../../reducers/type';
import { CON_TYPE, DB, fireStore } from './../../utils/DBUtils';
import UserItemLoader from './../Common/UserItemLoader';
import './stylesheet.scss';

const NotificationItemComponent = ({history, type: notiType, userData, description, content_id, notificationId, extClass, globalState, dispatch }) => {

    const translator = short();
    const [localState, setLocalState] = useState({ isRemoved: false });

    if (isEmpty(userData)) return <UserItemLoader />

    const deleteHandler = () => {
        fireStore.remove(
            DB.NOTIFICATION.name(),
            [DB.NOTIFICATION.ID, CON_TYPE.EQ, notificationId]
        );
        setLocalState({ ...localState, isRemoved: true });
    }

    const hideDialog = () => {
        dispatch({ type: type.CLOSE_NOTIFICATION_DIALOG });
        if (notiType === 'following') {
            history.push(`/${translator.fromUUID(content_id)}`);
        }else if(notiType === 'score_status'){
            dispatch({type: type.OPEN_PROFILE_TAB_SCORE_ID});
            history.push(`/${translator.fromUUID(userData._id)}/t/8`);
        }else if(notiType === 'order_status'){
            dispatch({type: type.OPEN_PROFILE_TAB_ORDER_ID});
            history.push(`/${translator.fromUUID(userData._id)}/order/${userData.orderCode}`);
        }else {
            history.push(`/status/${translator.fromUUID(content_id)}`);
        }
    }

    const renderStyleForName = ()=>{
        let style={};
        if(notiType === 'score_status' || notiType === 'order_status'){
            style={color:'#145222'}
        }
        return style;
    }

    if (localState.isRemoved) return null;

    return (

        <div className={`user-item-group ${extClass && extClass} nonhidden`}>
            <div className='i-av-g nonhidden'>
                <Link className='nonhidden' onClick={hideDialog} to={`/${translator.fromUUID(userData._id)}`}>
                    <Avatar className='i-av nonhidden of_cover' src={userData.avatar} name={userData.name} size='3rem' round={true} />
                </Link>
            </div>
            <div className='i-txt-g nonhidden'>

                <div className='i-txt-1 noselect nonhidden'>
                    <Link className='nonhidden' onClick={hideDialog} to={`/${translator.fromUUID(userData._id)}`}>
                        <strong className='nonhidden' style={renderStyleForName()}>{userData.name}</strong>
                    </Link>
                    &nbsp;
                        <div className='nonhidden zpointer' onClick={hideDialog} style={notiType==='score_status'?{fontWeight:400}:{}}>
                        {userData.label}
                        </div>
                </div>
                {
                    description &&
                    <div className='nonhidden zpointer' onClick={hideDialog}>
                        <div className='i-txt-2 noselect nonhidden'>{description}</div>
                    </div>
                }
            </div>

            <div className='rm-noti'>
                <i onClick={deleteHandler} className="material-icons noselect zscale-effect zcur nonhidden">delete</i>
            </div>

        </div>
    )
}

export default withRouter(NotificationItemComponent);
