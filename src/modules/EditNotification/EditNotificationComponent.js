import { isEmpty } from 'lodash';
import React, { useContext } from 'react';
import { RouteContext } from '../../routes/routes';
import LeftRightContentComponent from '../Common/LeftRightContentComponent';
import ToggleComponent from '../Common/ToggleComponent';
import R from './../../locale/R';
import { CON_TYPE, DB, fireStore } from './../../utils/DBUtils';
import LeftComponent from './LeftComponent';
import './stylesheet.scss';

const EditNotificationComponent = () => {

    const [user, setUser] = useContext(RouteContext);

    const handleToggleChange = (field, value) => {
        const data = {
            [field]: value
        }
        fireStore.update(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, user.extData[0]._id], data, true);
        user.extData[0][field] = value;
        setUser({ ...user, user });
    }

    if (isEmpty(user) || isEmpty(user.extData)) return null;

    return (
        <>
            <div className='container'>
                <div className='row znotification-group'>
                    <div className='col-md-6'>

                        <div className='t'>{R.setting.notification}</div>

                        <ToggleComponent user={user.extData[0]} field={DB.USER.NOTI_COMMENT} callback={(field, value) => handleToggleChange(field, value)} label={R.setting.noti_comment} />
                        <ToggleComponent user={user.extData[0]} field={DB.USER.NOTI_LIKE_COMMENT} callback={(field, value) => handleToggleChange(field, value)} label={R.setting.noti_like_comment} />
                        <ToggleComponent user={user.extData[0]} field={DB.USER.NOTI_LIKE_STATUS} callback={(field, value) => handleToggleChange(field, value)} label={R.setting.noti_like_status} />
                        <ToggleComponent user={user.extData[0]} field={DB.USER.NOTI_SHARE_STATUS} callback={(field, value) => handleToggleChange(field, value)} label={R.setting.noti_share_status} />
                        <ToggleComponent user={user.extData[0]} field={DB.USER.NOTI_FOLLOWING} callback={(field, value) => handleToggleChange(field, value)} label={R.setting.noti_following} />
                    </div>
                </div>
            </div>
            <LeftRightContentComponent
                LeftComponent={<LeftComponent />}
            />
        </>
    )
}

export default EditNotificationComponent;