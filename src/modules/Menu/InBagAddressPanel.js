import React, { useEffect, useContext, useState } from 'react';
import CurrencyFormat from 'react-currency-format';
import { isEmpty, isUndefined } from 'lodash';
import * as firebase from "firebase/app";
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from '../../locale/R';
import ModalComponent from '../Common/ModalComponent';
import { RouteContext } from '../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import './stylesheet.scss';
import AddressBookComponent from '../AddressBook/AddressBookComponent';

const InBagAddressPanel = ({ onBack, isShow, }) => {

    const [localState, setLocalState] = useState({ paytype: 'cod', items: [], init: false, lastVisible: null, hasUpdate: null, loading: false });
    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const closeModalHandler = () => {
        onBack();
    }

    const buildData = async () => {
        let { items, lastVisible } = localState;

        setLocalState({ ...localState, loading: true });

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {


        } else {
            setLocalState({ ...localState, loading: false })
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            buildData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [user]);

    return (
        <div className="inBag address-book-panel pdetail" style={isShow ? { display: '' } : { display: 'none' }}>
            <div className="zheader noselect">
                <button onClick={closeModalHandler} type="button" className="close zscale-effect">
                    <i className="material-icons">keyboard_backspace</i>
                </button>
                <div className='ztitle'>
                    {R.bag.address}
                </div>
            </div>
            <div className="zbody">
                <AddressBookComponent/>
            </div>
        </div>
    )
}

export default InBagAddressPanel;