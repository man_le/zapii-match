import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import CurrencyFormat from 'react-currency-format';
import { BounceLoader } from 'react-spinners';
import format from 'string-template';
import { formatMomentNoTime } from '../../../utils/StringUtils';
import short from 'short-uuid';
import R from '../../../locale/R';
import type from '../../../reducers/type';
import { RouteContext } from '../../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../../utils/DBUtils';
import DataIsEmpty from '../../Common/DataIsEmpty';
import PresentIcon from '../../Common/Icon/PresentIcon';
import { GlobalStateContext } from '../../Layout/HomeLayout';
import './stylesheet.scss';
import { firestore } from 'firebase';

const Comp = ({ history }) => {

    const [localState, setLocalState] = useState({
        data: [],
        lastVisible: null,
        score: 0,
        init: false,
        loading: false
    });

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);

    const fetchData = async () => {
        const { data } = localState;

        setLocalState({ ...localState, loading: true });

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {
            const userInfo = user.extData[0];

            const vouncherAll = await fireStore.findLimitStatic(
                DB.VOUNCHER.name(),
                1000,
                [DB.VOUNCHER.TYPE, CON_TYPE.EQ, 'all'],
            );

            const vouncherCurrentUser = await fireStore.findLimitStatic(
                DB.VOUNCHER.name(),
                1000,
                [DB.VOUNCHER.TYPE, CON_TYPE.EQ, userInfo._id],
            );

            let vouncherUsed = await fireStore.findLimitStatic(
                DB.VOUNCHER_USED.name(),
                1000,
                [DB.VOUNCHER_USED.USER_ID, CON_TYPE.EQ, userInfo._id],
            );

            if (!isEmpty(vouncherUsed)) {
                vouncherUsed = vouncherUsed.map(e => e.code);
            } else {
                vouncherUsed = [];
            }

            // Get score accumulate
            let scoreResult = await fireStore.find(DB.SCORE_ACCUMULATE.name(), [DB.SCORE_ACCUMULATE.USER_ID, CON_TYPE.EQ, userInfo._id]);
            if (!isEmpty(scoreResult)) {
                scoreResult = scoreResult[0].score;
            } else {
                scoreResult = 0;
            }

            let currentDate = moment().valueOf();

            if (!isEmpty(vouncherAll)) {
                for (let i = 0; i < vouncherAll.length; i++) {
                    let dateAfterAdd = moment(vouncherAll[i].createdDate).add(vouncherAll[i].numberValidDate, 'd').valueOf();
                    if (currentDate <= dateAfterAdd) {
                        if (vouncherUsed.indexOf(vouncherAll[i].code) === -1) data.push(vouncherAll[i]);
                    }
                }
            }

            if (!isEmpty(vouncherCurrentUser)) {
                for (let i = 0; i < vouncherCurrentUser.length; i++) {
                    let dateAfterAdd = moment(vouncherCurrentUser[i].createdDate).add(vouncherCurrentUser[i].numberValidDate, 'd').valueOf();
                    if (currentDate <= dateAfterAdd) {
                        if (vouncherUsed.indexOf(vouncherCurrentUser[i].code) === -1) data.push(vouncherCurrentUser[i]);
                    }
                }
            }

            setLocalState({
                ...localState,
                data,
                score: scoreResult,
                init: true,
                loading: false
            });
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [user]);

    const renderDiscountValue = (e) => {
        const formatter = new Intl.NumberFormat('en-US', {
            currency: 'USD',
            style: 'currency',
            minimumFractionDigits: 0
        })

        let messageDiscount = '';
        if (e.discountType === 'percent') {
            messageDiscount = format(R.profile.vouncher_msg, { percent: e.percentDiscountValue, price: formatter.format(e.maxDiscount).replace('$', '') + 'đ' });
        } else {
            messageDiscount = format(R.profile.vouncher_msg2, {
                price: formatter.format(e.maxDiscount).replace('$', '') + 'đ'
            });
        }
        return messageDiscount;
    }

    const { data, init, score } = localState;

    if (isEmpty(data)) {

        if (!init) return (
            <div className={`zcenter`} style={{ marginTop: '0.75rem' }}>
                <BounceLoader
                    sizeUnit={"px"}
                    size={50}
                    loading={true}
                    color='#777'
                />
            </div>
        )
    }

    return (

        <div className='full set-bookmark-group zmain'>
            <div className='row bar'>
                <div className='col-md-6 zbor'>

                    <div className='snewg'>
                        <div className='gr'>
                            <PresentIcon width='25' height='25' style={{ marginTop: '-0.25rem', stroke: '#fff', strokeWidth: '9px' }} />
                            <div className='t'>{R.profile.vouncher_list}</div>
                        </div>
                    </div>

                    <div className='cig'>
                        <div className='it'>
                            {R.profile.bonus}
                        </div>
                        <div className='g1'>
                            <div className='gi'>
                                {R.profile.bonus1} <span>{score}</span>
                            </div>
                            <div className='gi'>
                                {R.profile.desbonus}
                            </div>
                            <div className='gi'>
                                {R.profile.desbonus1}<span>{R.profile.desbonus1_1} </span> {R.profile.desbonus1_2}
                            </div>
                            <div className='gi'>
                                {R.profile.desbonus1_3}
                            </div>
                        </div>
                    </div>
                    {
                        !isEmpty(data)
                        &&
                        <div className='cig'>
                            <div className='it'>
                                {R.profile.bonus2}
                            </div>
                            {
                                data.map(e =>
                                    <div key={`set_${e._id}`} className='ci'>
                                        <div className='t1'>
                                            {R.profile.code} <span className='code'>{e.code}</span>
                                        </div>
                                        <div className='t2'>
                                            <div className='t2_1'>
                                                {renderDiscountValue(e)}
                                            </div>
                                            <div className='t2_2'>
                                                {R.profile.untilDate} {formatMomentNoTime(moment, moment(e.createdDate).add(e.numberValidDate, 'd').valueOf())}
                                            </div>
                                        </div>
                                    </div>

                                )
                            }
                        </div>

                    }

                </div>
            </div>
        </div>

    )
}

export default withRouter(Comp);