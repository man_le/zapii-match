import React, { useEffect, useContext, useState } from 'react';
import R from '../../locale/R';
import { isEmpty } from 'lodash';
import { BounceLoader } from 'react-spinners';
import DataIsEmpty from '../Common/DataIsEmpty';
import type from '../../reducers/type';
import MoreActionComponent from '../Common/MoreActionComponent';
import { GlobalStateContext } from '../Layout/HomeLayout';
import { RouteContext } from '../../routes/routes';
import { CON_TYPE, DB, fireStore } from './../../utils/DBUtils';
import './stylesheet.scss';
import NewAddressModalContentComponent from './NewAddressModalContentComponent';

const AddressBookComponent = () => {

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);

    const [localState, setLocalState] = useState({ items: [], loading: false, init: false, showAddressModal: false, addressObj: null });

    const buildData = async () => {
        let { items, loading, init } = localState;

        setLocalState({ ...localState, loading: true });

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            const result = await fireStore.find(
                DB.ADDRESS.name(),
                [DB.ADDRESS.USER_ID, CON_TYPE.EQ, user.extData[0]._id]);

            if (!isEmpty(result)) {
                setLocalState({ ...localState, items: result, init: true, loading: false });
            } else {
                setLocalState({ ...localState, items: [], init: true, loading: false });
            }
        } else {
            setLocalState({ ...localState, loading: false, init: true });
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            buildData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [user, globalState.isUpdateAddress]);

    const { items, init, loading } = localState;

    if (isEmpty(items) && !init) {
        return (
            <div className={`zloading-item`}><BounceLoader
                sizeUnit={"px"}
                size={50}
                loading={true}
                color='#777'
            /></div>
        )
    }

    const deleteHandler = async (_id) => {
        await fireStore.remove(DB.ADDRESS.name(), [DB.ADDRESS.ID, CON_TYPE.EQ, _id]);
        buildData();
        dispatch({ type: type.UPDATE_ADDRESS });
    }

    const editHandler = (e) => {
        setLocalState({ ...localState, showAddressModal: true, addressObj: e })
    }

    const addNewAddressHandler = () => {
        setLocalState({ ...localState, showAddressModal: true, addressObj: null })
    }

    const closeAddressModal = () => {
        setLocalState({ ...localState, showAddressModal: false });
    }

    const selectAddressHandler = async _id => {

        // Change value localstate
        const { items } = localState;
        items.map(e => { delete e.isDefault });
        const findItemIndex = items.findIndex(e => e._id === _id);
        if (findItemIndex !== -1) {
            items[findItemIndex].isDefault = true;
            setLocalState({ ...localState, items });
        }

        // Update database
        const oldData = {
            isDefault: false
        }
        await fireStore.updateMultiConditions(DB.ADDRESS.name(), oldData, false, [DB.ADDRESS.USER_ID, CON_TYPE.EQ, user.extData[0]._id], ['isDefault', CON_TYPE.EQ, true]);

        const newData = {
            isDefault: true
        }
        await fireStore.updateMultiConditions(DB.ADDRESS.name(), newData, false, [DB.ADDRESS.ID, CON_TYPE.EQ, _id]);

        dispatch({ type: type.UPDATE_ADDRESS });
    }

    return (
        <div className='address-book noselect' style={{paddingLeft:'0.5rem'}}>
            {
                localState.items.map(e =>
                    <div key={`address_${e._id}`} className='address-item'>
                        <div onClick={() => selectAddressHandler(e._id)} className={`zckbox ${e.isDefault ? 'active' : ''}`}>
                            <i className='material-icons'>check</i>
                        </div>
                        <div onClick={() => selectAddressHandler(e._id)} className='address-value noselect'>
                            <div className='name-phone'>
                                {e.name} - {e.phone}
                            </div>
                            <div className='address-detail'>
                                {e.addressDetail ? e.addressDetail : e.address}
                            </div>
                        </div>
                        {
                            window.innerWidth <= 991
                                ?
                                <div className='moremb'>
                                    <i onClick={() => editHandler(e)} className='material-icons noselect zscale-effect'>create</i>
                                    <i onClick={() => deleteHandler(e._id)} className='material-icons noselect zscale-effect'>delete</i>
                                </div>
                                :
                                <div className='more'>
                                    <MoreActionComponent data={
                                        [
                                            {
                                                label: R.addressBook.delete,
                                                onClickHandler: () => deleteHandler(e._id)
                                            },
                                            {
                                                label: R.addressBook.edit,
                                                onClickHandler: () => editHandler(e)
                                            },
                                        ]
                                    } />
                                </div>
                        }

                    </div>
                )
            }
            {isEmpty(items) && init &&
                <DataIsEmpty />
            }
            {
                !localState.showAddressModal
                &&
                <div className='address-add-new'>
                    <div onClick={addNewAddressHandler} className='in'>
                        {R.addressBook.addnew}
                    </div>
                    <div id='editAddressModal' className='hidden'></div>
                </div>
            }

            {/* Modal */}
            {
                localState.showAddressModal
                &&
                <div className='address-modal'>
                    <NewAddressModalContentComponent closeModalHandler={closeAddressModal} addressObj={localState.addressObj} />
                </div>
            }
        </div>
    )
}

export default AddressBookComponent;