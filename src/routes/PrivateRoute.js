import React, { useContext } from 'react';
import { Redirect, Route } from 'react-router-dom';
import SetInBagComponent from '../modules/Design/SetInBag/SetInBagComponent';
import HomeLayout from '../modules/Layout/HomeLayout';
import NotificationsComponent from '../modules/Notifications/NotificationsComponent';
import SettingComponent from '../modules/Setting/SettingComponent';
import { isLoggedIn } from '../utils/AuthUtils';
import { RouteContext } from './routes';

export default ({ loggedIn, component: Component, ...rest }) => {
    const [user, setUser] = useContext(RouteContext);
    setUser(loggedIn);

    let Comp = Component;

    const pathname = window.location.pathname;
    if (pathname === '/' || pathname === '/status' || pathname === '/set') Comp=null;
    else if(pathname==='/discover') Comp=null;
    else if(pathname.indexOf('/mix') !== -1 && pathname.indexOf('/mix/addAll') === -1) Comp = null;

    // This to prevent with route /:userId
    else if(window.location.href.indexOf('/notifications') !== -1) Comp = NotificationsComponent;
    else if(window.location.href.indexOf('/settings') !== -1) Comp = SettingComponent;
    // else if(window.location.href.indexOf('/discover') !== -1) Comp = DiscoverComponent;
    else if(window.location.href.indexOf('/mix/addAll') !== -1) Comp = SetInBagComponent;

    if(!Comp){
        return (
            <Route
                {...rest}
                render={props =>
                    isLoggedIn(loggedIn)? // is authenticated
                        <HomeLayout user={loggedIn}>
                            <div {...props}> </div>
                        </HomeLayout>
                        :
                        <Redirect to={{ pathname: '/login', search: `?continue=${encodeURI(window.location.href)}`, state: { from: props.location } }} />
                }
            />
        )
    }

    return (
        <Route
            {...rest}
            render={props =>
                isLoggedIn(loggedIn)? // is authenticated
                    <HomeLayout user={loggedIn}>
                        <Comp {...props} />
                    </HomeLayout>
                    :
                    <Redirect to={{ pathname: '/login', search: `?continue=${encodeURI(window.location.href)}`, state: { from: props.location } }} />
            }
        />
    )
}
