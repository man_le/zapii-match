import React from 'react';
import './stylesheet.scss';

const Comp = ({ sex, clothes }) => {
    let result = null;
    if (sex === 'man') {
        switch (clothes) {
            case 'shirt':
            case 'tshirt':
                result = (
                    <div className='select-size-group'>
                        <div className='otd'>
                            Hướng dẫn chọn size áo nam
                    </div>
                        <table>
                            <tr>
                                <td></td>
                                <td>{'< 1m68'}</td>
                                <td>1m68-1m74</td>
                                <td>1m75-1m80</td>
                                <td>{'> 1m80'}</td>
                            </tr>
                            <tr>
                                <td>{'< 53Kg'}</td><td>S</td><td>M</td><td>M</td><td>L</td>
                            </tr>
                            <tr>
                                <td>{'53-60Kg'}</td><td>S/M</td><td>M/L</td><td>L/XL</td><td>XL</td>
                            </tr>
                            <tr>
                                <td>{'> 60kg'}</td><td>M</td><td>L/XL</td><td>XL</td><td>XXL</td>
                            </tr>
                        </table>

                    </div>
                )
                break;

            case 'trousers':
            case 'short':
                result = (
                    <div className='select-size-group'>
                        <div className='otd'>
                            Hướng dẫn chọn size quần nam
                            </div>
                        <table>
                            <tr>
                                <td>Size</td>
                                <td>S</td>
                                <td>M</td>
                                <td>L</td>
                                <td>XL</td>
                            </tr>
                            <tr>
                                <td>Cao(cm)</td>
                                <td>165-167</td>
                                <td>168-170</td>
                                <td>170-173</td>
                                <td>173-176</td>
                            </tr>
                            <tr>
                                <td>Nặng(kg)</td>
                                <td>55-60</td>
                                <td>60-65</td>
                                <td>66-70</td>
                                <td>70-76</td>
                            </tr>
                            <tr>
                                <td>Vòng ngực(cm)</td>
                                <td>86-90</td>
                                <td>90-94</td>
                                <td>94-98</td>
                                <td>98-102</td>
                            </tr>
                            <tr>
                                <td>Vòng eo(cm)</td>
                                <td>68-72</td>
                                <td>72-76</td>
                                <td>76-80</td>
                                <td>80-84</td>
                            </tr>
                            <tr>
                                <td>Vòng mông(cm)</td>
                                <td>88-92</td>
                                <td>92-96</td>
                                <td>96-100</td>
                                <td>100-104</td>
                            </tr>
                        </table>

                    </div>
                )
                break;

            case 'shoes':
                result = (
                    <div className='select-size-group'>
                        <div className='otd'>
                            Hướng dẫn chọn size giày nam (công thức: chiều dài chân + 1.5cm)
                    </div>
                        <table>
                            <tr>
                                <td>Size</td>
                                <td>Chiều dài chân + 1.5cm</td>
                            </tr>
                            <tr>
                                <td>39</td>
                                <td>23.5cm</td>
                            </tr>
                            <tr>
                                <td>39-40</td>
                                <td>24.1cm</td>
                            </tr>
                            <tr>
                                <td>40</td>
                                <td>24.4cm</td>
                            </tr>
                            <tr>
                                <td>40-41</td>
                                <td>24.8cm</td>
                            </tr>
                            <tr>
                                <td>41</td>
                                <td>25.4cm</td>
                            </tr>
                            <tr>
                                <td>41-42</td>
                                <td>25.7cm</td>
                            </tr>
                            <tr>
                                <td>42</td>
                                <td>26cm</td>
                            </tr>
                            <tr>
                                <td>42-43</td>
                                <td>26.7cm</td>
                            </tr>
                            <tr>
                                <td>43</td>
                                <td>27cm</td>
                            </tr>
                            <tr>
                                <td>43-44</td>
                                <td>27.3cm</td>
                            </tr>
                            <tr>
                                <td>44</td>
                                <td>27.9cm</td>
                            </tr>
                            <tr>
                                <td>44-45</td>
                                <td>28.3cm</td>
                            </tr>
                            <tr>
                                <td>45</td>
                                <td>28.6cm</td>
                            </tr>
                            <tr>
                                <td>46</td>
                                <td>29.4cm</td>
                            </tr>
                            <tr>
                                <td>47</td>
                                <td>30.2cm</td>
                            </tr>
                            <tr>
                                <td>48</td>
                                <td>31cm</td>
                            </tr>
                            <tr>
                                <td>49</td>
                                <td>31.8cm</td>
                            </tr>
                        </table>

                    </div>
                )
                break;

        }

    } else if (sex === 'women') {
        switch (clothes) {
            case 'shirt':
            case 'tshirt':
                result = (
                    <div className='select-size-group'>
                        <div className='otd'>
                            Hướng dẫn chọn size áo nữ
                    </div>
                        <table>
                            <tr>
                                <td></td>
                                <td>{'< 1m50'}</td>
                                <td>1m50-1m60</td>
                                <td>1m60-1m70</td>
                                <td>{'> 1m70'}</td>
                            </tr>
                            <tr>
                                <td>{'< 40Kg'}</td><td>S</td><td>M</td><td>M</td><td>L</td>
                            </tr>
                            <tr>
                                <td>{'40-50Kg'}</td><td>S/M</td><td>M/L</td><td>L/XL</td><td>XL</td>
                            </tr>
                            <tr>
                                <td>{'50-60kg'}</td><td>M</td><td>L</td><td>L/XL</td><td>XL</td>
                            </tr>
                            <tr>
                                <td>{'> 60kg'}</td><td>L</td><td>L/XL</td><td>XL</td><td>XXL</td>
                            </tr>
                        </table>

                    </div>
                )
                break;

            case 'trousers':
            case 'short':
                result = (
                    <div className='select-size-group'>
                        <div className='otd'>
                            Hướng dẫn chọn size quần nữ
                            </div>
                        <table>
                            <tr>
                                <td>Size</td>
                                <td>S</td>
                                <td>M</td>
                                <td>L</td>
                                <td>XL</td>
                            </tr>
                            <tr>
                                <td>Cao(cm)</td>
                                <td>150-155</td>
                                <td>156-160</td>
                                <td>160-164</td>
                                <td>165-170</td>
                            </tr>
                            <tr>
                                <td>Nặng(kg)</td>
                                <td>38-43</td>
                                <td>43-46</td>
                                <td>46-53</td>
                                <td>53-57</td>
                            </tr>
                            <tr>
                                <td>Vòng ngực(cm)</td>
                                <td>80-84</td>
                                <td>84-88</td>
                                <td>88-92</td>
                                <td>92-96</td>
                            </tr>
                            <tr>
                                <td>Vòng eo(cm)</td>
                                <td>64-68</td>
                                <td>68-72</td>
                                <td>72-76</td>
                                <td>76-80</td>
                            </tr>
                            <tr>
                                <td>Vòng mông(cm)</td>
                                <td>86-90</td>
                                <td>90-94</td>
                                <td>94-98</td>
                                <td>98-102</td>
                            </tr>
                        </table>

                    </div>
                )
                break;

            case 'shoes':
                result = (
                    <div className='select-size-group'>
                        <div className='otd'>
                            Hướng dẫn chọn size giày nữ
                    </div>
                        <table>
                            <tr>
                                <td>Size Việt Nam</td>
                                <td>Chiều dài chân (cm)</td>
                                <td>Size US</td>
                            </tr>
                            <tr>
                                <td>34</td>
                                <td>19.5-20.5</td>
                                <td>3.5-4</td>
                            </tr>
                            <tr>
                                <td>35</td>
                                <td>20.5-21.5</td>
                                <td>4.5-5</td>
                            </tr>
                            <tr>
                                <td>36</td>
                                <td>21.5-22.5</td>
                                <td>5.5-6</td>
                            </tr>
                            <tr>
                                <td>37</td>
                                <td>22.5-23.5</td>
                                <td>6.5-7</td>
                            </tr>
                            <tr>
                                <td>38</td>
                                <td>23.5-24.5</td>
                                <td>7.5-8</td>
                            </tr>
                            <tr>
                                <td>39</td>
                                <td>24.5-25.5</td>
                                <td>8.5-9</td>
                            </tr>
                            <tr>
                                <td>40</td>
                                <td>25.5-26.5</td>
                                <td>9.5-10</td>
                            </tr>
                        </table>

                    </div>
                )
                break;
        }
    }
    return result;
}

export default Comp;