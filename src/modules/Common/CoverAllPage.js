import React from 'react';
import './stylesheet.scss';
import R from '../../locale/R';

const CoverAllPage = () => {
    return (
        <div className='cover-all'>
            <div className='t-group'>
               <div>{R.common.message_cover_display}</div>
               <div>
                    <button onClick={()=>window.location.href='/'} type='button' className="btn btn-default btn-cancel">{R.common.OK}</button>
               </div>
            </div>
        </div>
    )
}

export default CoverAllPage;