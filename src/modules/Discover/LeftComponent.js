import React from 'react';
import R from '../../locale/R';
import { Link } from 'react-router-dom';
import BagIcon from '../Common/Icon/BagIcon';
import ClothesIcon from '../Common/Icon/ClothesIcon';
import ShirtIcon from '../Common/Icon/ShirtIcon';
import { buildProductFilterTypes } from '../../utils/JSONUtils';

const Comp = ({ clothes_type, typeChangeHandler, filterTypeChangeHandler, sortChangeHandler, filter_type, sort_type, sort_value }) => {

    const renderFilterType = type => {
        let key = ''
        if (type === 'short') {
            if (clothes_type === 'man') {
                key = 'short_man';
            } else {
                key = 'short';
            }
        } else {
            key = type;
        }
        return key;
    }

    return (
        <>
            <div className='left-content shadow-s'>
                <ul>
                    <li>
                        <ShirtIcon isSelected={true} width='18' height='18' color='#777' style={{ marginTop: '0.3rem' }} />
                        <div>
                            <select name="type" onChange={typeChangeHandler}>
                                <option selected={clothes_type==='women'} value="women">{R.discover.women1}</option>
                                <option selected={clothes_type==='man'} value="man">{R.discover.man1}</option>
                            </select>
                            <i className='material-icons sel'>arrow_forward_ios</i>
                        </div>
                    </li>
                    <li>
                        <i className="material-icons">filter_list</i>
                        <div>
                            <select name='filter_type' onChange={filterTypeChangeHandler}>
                                {
                                    buildProductFilterTypes(clothes_type || 'women').map((e, i) =>
                                        <option selected={filter_type===e} key={'clothes_type_' + i} value={e}>
                                            {R.product[renderFilterType(e)]}
                                        </option>
                                    )
                                }
                            </select>
                            <i className='material-icons sel'>arrow_forward_ios</i>
                        </div>
                    </li>
                    <li>
                        <i className="material-icons">sort</i>
                        <div>
                            <select name='sort' onChange={sortChangeHandler}>
                                <option selected={sort_value==='updatedDate' && sort_type==='desc'} value={'new'}>{R.discover.new}</option>
                                <option selected={sort_value==='new_price' && sort_type==='asc'} value={'price_az'}>{R.discover.price_az}</option>
                                <option selected={sort_value==='new_price' && sort_type==='desc'} value='price_za'>{R.discover.price_za}</option>
                            </select>
                            <i className='material-icons sel'>arrow_forward_ios</i>
                        </div>
                    </li>
                    <li className='seperator'></li>
                    <Link to='/mix'>
                        <li>
                            <ClothesIcon isSelected={true} width='20' height='20' color='#777' style={{ marginTop: '0.15rem' }} />
                            <div>{R.timeline.mixs}</div>
                        </li>
                    </Link>
                    <Link to='/bag/list'>
                        <li>
                            <BagIcon isSelected={true} width='16' height='16' color='#777' style={{ marginTop: '0.35rem' }} />
                            <div>{R.timeline.bags}</div>
                        </li>
                    </Link>
                </ul>
            </div>

            <div className='left-content zapii-info'>

                <div className='zcont noselect'>
                    <Link to='/_privacy'>
                        {R.access.policy}
                    </Link>
                </div>
                <div className='zcont noselect'>
                    <Link to='/_terms'>
                        {R.access.term}
                    </Link>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_qa'>
                        {R.access.qa}
                    </Link>
                </div>

                <div className='zline noselect'>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_about'>
                        ©Zapii.me {new Date().getFullYear()}
                    </Link>
                </div>
            </div>

        </>
    )
}

export default Comp;