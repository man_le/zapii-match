import { debounce, isEmpty, isFunction } from 'lodash';
import moment from 'moment';
import 'moment/locale/vi';
import React, { useContext, useEffect, useRef, useState } from 'react';
import Avatar from 'react-avatar';
import { store } from 'react-notifications-component';
import { Link } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import short from 'short-uuid';
import type from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import { buildCommentNotification, buildFollowingNotification, buildLikeCommentNotification, buildScoreStatusNotification, buildOrderStatusNotification, buildLikeStatusNotification, buildShareStatusNotification } from '../../utils/RealTimeNotificationUtils';
import { formatMoment, removeAccents } from '../../utils/StringUtils';
import { activeSettingIcons } from '../../utils/URLUtils';
import DataIsEmpty from '../Common/DataIsEmpty';
import AccountIcon from '../Common/Icon/AccountIcon';
import AccountOutlinedIcon from '../Common/Icon/AccountOutlinedIcon';
import BagIcon from '../Common/Icon/BagIcon';
import BellIcon from '../Common/Icon/BellIcon';
import BellOutlinedIcon from '../Common/Icon/BellOutlinedIcon';
import ModalComponent from '../Common/ModalComponent';
import ClothesIcon from '../Common/Icon/ClothesIcon';
import HomeIcon from '../Common/Icon/HomeIcon';
import HomeOutlinedIcon from '../Common/Icon/HomeOutlinedIcon';
import SettingIcon from '../Common/Icon/SettingIcon';
import SettingOutlinedIcon from '../Common/Icon/SettingOutlinedIcon';
import ShirtIcon from '../Common/Icon/ShirtIcon';
import ShirtOutlinedIcon from '../Common/Icon/ShirtOutlinedIcon';
import { GlobalStateContext } from '../Layout/HomeLayout';
import NotificationsPanelComponent from '../Notifications/NotificationsPanelComponent';
import R from './../../locale/R';
import { signOut } from './../../utils/AuthUtils';
import { CON_TYPE, DB, fireStore, realTime } from './../../utils/DBUtils';
import InBagPanel from './InBagPanel';
import SearchBoxPanel from './SearchBoxPanel';
import './stylesheet.scss';

const setMenu = (path, dispatch) => {
    dispatch({ type: type.CHANGE_ROUTER, currentRouter: path });
}

const MenuComponent = ({ path }) => {

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);
    const searchRef = useRef('');
    const [localState, setLocalState] = useState({ users: [], clothes: [], loading: false, showInBagDetail: false });

    let meUrl = '';
    if (!isEmpty(user) && !isEmpty(user.extData)) {
        const id = user.extData[0]._id;
        let translator = short();
        if (!isEmpty(id)) meUrl = `/${translator.fromUUID(id)}`;
    }

    let menu = 1;
    if (activeSettingIcons(path)) {
        menu = 3;
    } else if (
        window.location.href.indexOf('/setting/account') !== -1
        ||
        window.location.href.indexOf('/setting/profile') !== -1
        ||
        window.location.href.indexOf('/setting/notification') !== -1
    ) {
        menu = 5;
    } else if (window.location.href.indexOf('/notification') !== -1) {
        menu = 4;
    } else if (
        window.location.href.indexOf('/discover') !== -1 ||
        window.location.href.indexOf('/detail/product/') !== -1 ||
        window.location.href.indexOf('/bag/list') !== -1 ||
        window.location.href.indexOf('/bag/checkout') !== -1 ||
        window.location.href.indexOf('/mix') !== -1
    ) {
        menu = 6;
    } else if (window.location.href.indexOf('/settings') !== -1) {
        menu = 5;
    } else if (window.location.href.indexOf(meUrl) === -1) {
        menu = 1;
    }
    else {
        switch (path) {
            case '/': menu = 1; break;
            default: menu = 2; break;
        }
    }

    const renderTotalNotification = () => {
        let value = '';
        if (!globalState.use_total_notification_from_state)
            value = globalState.total_notification;

        return value;
    }

    const showSearchDialog = () => {
        dispatch({ type: type.OPEN_SEARCH_DIALOG });
    }

    const showNotificationDialog = () => {
        if (globalState.isOpenNotificationDialog === true) {
            dispatch({ type: type.CLOSE_NOTIFICATION_DIALOG });
        } else {
            dispatch({ type: type.OPEN_NOTIFICATION_DIALOG });
        }
        dispatch({ type: type.RESET_TOTAL_NOTIFICATION });

        // Reset total notification
        const totalNotification = {
            total_notification: 0,
        }
        fireStore.updateMultiConditions(
            DB.USER.name(),
            totalNotification, false,
            [DB.USER.ID, CON_TYPE.EQ, user.extData[0]._id],
        );
    }

    const resetTotalNotification = () => {
        dispatch({ type: type.RESET_TOTAL_NOTIFICATION });

        // Reset total notification
        const totalNotification = {
            total_notification: 0,
        }
        fireStore.updateMultiConditions(
            DB.USER.name(),
            totalNotification, false,
            [DB.USER.ID, CON_TYPE.EQ, user.extData[0]._id],
        );
    }

    const buildData = async () => {
        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            // Set total notification
            if (!globalState.initTotalNotification && !globalState.use_total_notification_from_state) {
                dispatch({ type: type.INIT_TOTAL_NOTIFICATION, total_notification: user.extData[0].total_notification, initTotalNotification: true });
            }

            let total_notification = user.extData[0].total_notification;
            if (globalState.use_total_notification_from_state) {
                total_notification = 0;
            }

            // Add user access history
            const historyData = {
                user_id: user.extData[0]._id,
                name: user.extData[0].name,
                [DB.HISTORY_ACCESS.UPDATED_DATE_READABLE]: formatMoment(moment, moment().valueOf())
            }
            fireStore.update(DB.HISTORY_ACCESS.name(), [DB.HISTORY_ACCESS.USER_ID, CON_TYPE.EQ, user.extData[0]._id], historyData, true);

            // Add user online
            if (!isEmpty(user.extData)) {
                realTime.update(
                    DB.RT_USER_ONLINE.name(),
                    { [user.extData[0]._id]: user.email },
                    null,
                    'user_online');
                realTime.onDisconnect(DB.RT_USER_ONLINE.name(), 'user_online', user.extData[0]._id);
                realTime.onDisconnect(DB.RT_USER_ONLINE.name(), 'user_online', 'undefined');
            }

            // Init RealTime Comment
            if (!globalState.realTimeCommentInit) {

                // Get comment notification to show dialog
                realTime.find(
                    DB.RT_NOTIFICATION_INFO.name(),
                    snap => {
                        if (!isEmpty(snap.val())) {

                            let snapArr = Object.entries(snap.val());

                            const [key, value] = snapArr[snapArr.length - 1];
                            if (!value.isShow && user.extData[0].notify_comment) {
                                buildCommentNotification(value, store, user.extData[0], dispatch, ++total_notification);
                                realTime.update(DB.RT_NOTIFICATION_INFO.name(), { 'isShow': true }, null, `${user.extData[0]._id}_comment_notification`, key);
                            }

                        }
                    },
                    `${user.extData[0]._id}_comment_notification`,
                );

                // Clear comment notificaiton if disconnect
                realTime.onDisconnect(DB.RT_NOTIFICATION_INFO.name(), `${user.extData[0]._id}_comment_notification`);

                dispatch({ type: type.REALTIME_COMMENT_INIT });
            }

            // Init RealTime Like Comment
            if (!globalState.realTimeLikeCommentInit) {

                // Get like comment notification to show dialog
                realTime.find(
                    DB.RT_NOTIFICATION_INFO.name(),
                    snap => {
                        if (!isEmpty(snap.val())) {
                            let snapArr = Object.entries(snap.val());

                            const [key, value] = snapArr[snapArr.length - 1];
                            if (!value.isShow && user.extData[0].notify_like_comment) {
                                buildLikeCommentNotification(value, user.extData[0], store, dispatch, ++total_notification);
                                realTime.update(DB.RT_NOTIFICATION_INFO.name(), { 'isShow': true }, null, `${user.extData[0]._id}_like_comment_notification`, key);
                            }

                        }
                    },
                    `${user.extData[0]._id}_like_comment_notification`,
                );

                // Clear like comment notificaiton if disconnect
                realTime.onDisconnect(DB.RT_NOTIFICATION_INFO.name(), `${user.extData[0]._id}_like_comment_notification`);

                dispatch({ type: type.REALTIME_LIKE_COMMENT_INIT });

            }

            // Init RealTime Like Status
            if (!globalState.realTimeLikeStatusInit) {

                // Get like Status notification to show dialog
                realTime.find(
                    DB.RT_NOTIFICATION_INFO.name(),
                    snap => {
                        if (!isEmpty(snap.val())) {
                            let snapArr = Object.entries(snap.val());

                            const [key, value] = snapArr[snapArr.length - 1];
                            if (!value.isShow && user.extData[0].notify_like_status) {
                                buildLikeStatusNotification(value, user.extData[0], store, dispatch, ++total_notification);
                                realTime.update(DB.RT_NOTIFICATION_INFO.name(), { 'isShow': true }, null, `${user.extData[0]._id}_like_status_notification`, key);
                            }

                        }
                    },
                    `${user.extData[0]._id}_like_status_notification`,
                );

                // Clear like Status notificaiton if disconnect
                realTime.onDisconnect(DB.RT_NOTIFICATION_INFO.name(), `${user.extData[0]._id}_like_status_notification`);

                dispatch({ type: type.REALTIME_LIKE_STATUS_INIT });

            }

            // Init RealTime Score Status
            if (!globalState.realTimeScoreStatusInit) {

                // Get score Status notification to show dialog
                realTime.find(
                    DB.RT_NOTIFICATION_INFO.name(),
                    snap => {
                        if (!isEmpty(snap.val())) {
                            let snapArr = Object.entries(snap.val());

                            const [key, value] = snapArr[snapArr.length - 1];
                            if (!value.isShow) {
                                buildScoreStatusNotification(value, user.extData[0], store, dispatch, ++total_notification);
                                realTime.update(DB.RT_NOTIFICATION_INFO.name(), { 'isShow': true }, null, `${user.extData[0]._id}_score_status_notification`, key);
                            }
                        }
                    },
                    `${user.extData[0]._id}_score_status_notification`,
                );

                // Clear score Status notificaiton if disconnect
                realTime.onDisconnect(DB.RT_NOTIFICATION_INFO.name(), `${user.extData[0]._id}_score_status_notification`);

                dispatch({ type: type.REALTIME_SCORE_STATUS_INIT });

            }

            // Init RealTime Order Status
            if (!globalState.realTimeOrderStatusInit) {

                // Get Order Status notification to show dialog
                realTime.find(
                    DB.RT_NOTIFICATION_INFO.name(),
                    snap => {
                        if (!isEmpty(snap.val())) {
                            let snapArr = Object.entries(snap.val());

                            const [key, value] = snapArr[snapArr.length - 1];
                            if (!value.isShow) {
                                buildOrderStatusNotification(value, user.extData[0], dispatch, ++total_notification);
                                realTime.update(DB.RT_NOTIFICATION_INFO.name(), { 'isShow': true }, null, `${user.extData[0]._id}_order_status_notification`, key);
                            }
                        }
                    },
                    `${user.extData[0]._id}_order_status_notification`,
                );

                // Clear score Status notificaiton if disconnect
                realTime.onDisconnect(DB.RT_NOTIFICATION_INFO.name(), `${user.extData[0]._id}_order_status_notification`);

                dispatch({ type: type.REALTIME_ORDER_STATUS_INIT });

            }

            // Init RealTime Share Status
            if (!globalState.realTimeShareStatusInit) {

                // Get share Status notification to show dialog
                realTime.find(
                    DB.RT_NOTIFICATION_INFO.name(),
                    snap => {
                        if (!isEmpty(snap.val())) {
                            let snapArr = Object.entries(snap.val());

                            const [key, value] = snapArr[snapArr.length - 1];
                            if (!value.isShow && user.extData[0].notify_share_status) {
                                buildShareStatusNotification(value, user.extData[0], store, dispatch, ++total_notification);
                                realTime.update(DB.RT_NOTIFICATION_INFO.name(), { 'isShow': true }, null, `${user.extData[0]._id}_share_status_notification`, key);
                            }

                        }
                    },
                    `${user.extData[0]._id}_share_status_notification`,
                );

                // Clear like Status notificaiton if disconnect
                realTime.onDisconnect(DB.RT_NOTIFICATION_INFO.name(), `${user.extData[0]._id}_share_status_notification`);

                dispatch({ type: type.REALTIME_SHARE_STATUS_INIT });

            }

            // Init RealTime Following
            if (!globalState.realTimeFollowingInit) {

                // Get like following notification to show dialog
                realTime.find(
                    DB.RT_NOTIFICATION_INFO.name(),
                    snap => {
                        if (!isEmpty(snap.val())) {
                            let snapArr = Object.entries(snap.val());

                            const [key, value] = snapArr[snapArr.length - 1];
                            if (!value.isShow && user.extData[0].noti_following) {
                                buildFollowingNotification(value, user.extData[0], store, dispatch, ++total_notification);
                                realTime.update(DB.RT_NOTIFICATION_INFO.name(), { 'isShow': true }, null, `${user.extData[0]._id}_following_notification`, key);
                            }
                        }
                    },
                    `${user.extData[0]._id}_following_notification`,
                );

                // Clear like following notificaiton if disconnect
                realTime.onDisconnect(DB.RT_NOTIFICATION_INFO.name(), `${user.extData[0]._id}_following_notification`);

                dispatch({ type: type.REALTIME_FOLLOWING_INIT });

            }

        }
    }

    const searchHandler = async e => {
        const value = searchRef.current.value;

        setLocalState({ ...localState, loading: true });

        dispatch({ type: type.OPEN_SEARCH_DIALOG });

        if (!isEmpty(value)) {
            const splitValue = value.split(' ').map(e=>removeAccents(e.toLowerCase()));
            var bounced = debounce(async () => {
                const userResult = await fireStore.findLimitStatic(
                    DB.USER.name(),
                    50,
                    ['keywords', CON_TYPE.ARRAY_CONTAINS_ANY, splitValue],
                );

                const productResult = await fireStore.findLimitStatic(
                    DB.CLOTHES.name(),
                    50,
                    ['keywords', CON_TYPE.ARRAY_CONTAINS_ANY, splitValue],
                );

                setLocalState({ ...localState, users: userResult, clothes: productResult, loading: false });

            }, 500);

            bounced();
        } else {
            setLocalState({ ...localState, users: [], clothes: [], loading: false });
        }
    }

    const handleClickOut = (e) => {
        if (!e) return;
        if (isFunction(e.target.className.indexOf) && e.target.className.indexOf('nonhidden') !== -1) {
            return;
        }
        dispatch({ type: type.CLOSE_SEARCH_DIALOG });
    }

    const hideDialog = () => {
        dispatch({ type: type.CLOSE_SEARCH_DIALOG });
    }

    const showBagHandler = () => {
        setLocalState({ ...localState, showInBagDetail: true });
    }

    const closeShowInBagDetailHandler = () => {
        setLocalState({ ...localState, showInBagDetail: false });
    }

    const renderSearchResult = () => {
        const { searchItem } = globalState;
        if (searchItem === 1) {
            return (
                !isEmpty(localState.users)
                    ?
                    localState.users.map(e =>
                        <Link onClick={hideDialog} key={'us_' + e._id} to={`/${trans.fromUUID(e._id)}`} className='avatar-g nonhidden'>
                            <Avatar className='zavatar nonhidden of_cover' src={e.avatar} name={e.name} size='3rem' round={true} />
                            <div className='name-group nonhidden'>
                                <div className='t nonhidden' style={isEmpty(e.about) ? { marginTop: '0.75rem' } : {}}>{e.name}</div>
                                {
                                    !isEmpty(e.about)
                                    &&
                                    <div className='t2 nonhidden'>{e.about}</div>
                                }
                            </div>
                        </Link>
                    )
                    :
                    <div>
                        <DataIsEmpty hideIcon={true} label={R.common.no_data} color='#eee' />
                    </div>
            )
        } else {
            return (
                !isEmpty(localState.clothes)
                    ?
                    localState.clothes.map(e =>
                        <Link onClick={hideDialog} key={'us_' + e._id} to={`/detail/product/${trans.fromUUID(e._id)}`} className='avatar-g nonhidden'>
                            <img className='zimg nonhidden' src={isEmpty(e.image_detail) ? e.image : e.image_detail[0]} alt={e.name} />
                            <div className='name-group nonhidden'>
                                <div className='t nonhidden' style={isEmpty(e.description) ? { marginTop: '0.75rem' } : {}}>{e.name}</div>
                                {
                                    !isEmpty(e.description)
                                    &&
                                    <div className='t2 nonhidden'>{e.description.replace(/<br\/>/g, "")}</div>
                                }
                            </div>
                        </Link>
                    )
                    :
                    <div>
                        <DataIsEmpty hideIcon={true} label={R.common.no_data} color='#eee' />
                    </div>
            )
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {

            document.addEventListener('mousedown', handleClickOut);

            buildData();
            loadCompleted = true;
        }

        return () => {
            document.removeEventListener('mousedown', handleClickOut);
            return loadCompleted == true;
        }

    }, [user]);

    var trans = short();

    return (
        <>
            <nav className="znav navbar navbar-expand-lg bg-danger nav-custom">
                <div className="container col-md-10">

                    <a className="navbar-brand zlogo" href="/">
                        <img src="https://zapii.me/assets/data/logo.png" alt="" />
                    </a>

                    <div className="form-inline">
                        <i className='material-icons'>search</i>
                        <input
                            maxLength={50}
                            ref={searchRef}
                            className={`searchbox ${!globalState.isOpenSearchDialog || (isEmpty(localState.users) && isEmpty(localState.clothes)) ? 'default' : ''}`}
                            onFocus={showSearchDialog}
                            onKeyUp={searchHandler}
                            type="search"
                            placeholder={R.common.search_all}
                            style={localState.loading ? { padding: '0.25rem 1.95rem 0.25rem 2.2rem' } : {}}
                        />

                        {
                            localState.loading
                            &&
                            <div className='load-in-textbox'>
                                <BounceLoader
                                    sizeUnit={"px"}
                                    size={20}
                                    loading={true}
                                    color='#777' />
                            </div>
                        }

                        {
                            globalState.isOpenSearchDialog && (!isEmpty(localState.users) || !isEmpty(localState.clothes))
                            &&
                            <div className='zsbox shadow'>
                                <div className='g nonhidden'>

                                    {
                                        localState.loading
                                            ?
                                            <div className='zloading-item'>
                                                <BounceLoader
                                                    sizeUnit={"px"}
                                                    size={50}
                                                    loading={true}
                                                    color='#777' />
                                            </div>
                                            :
                                            <div className='search-result-group nonhidden'>
                                                <div className='st'>
                                                    <div className='stg nonhidden'>
                                                        <div onClick={() => { dispatch({ type: type.CHANGE_SEARCH_ITEM, searchItem: 1 }) }} className={`stgi nonhidden ${globalState.searchItem === 1 ? 'active' : ''}`}>{R.common.people}</div>
                                                        <div onClick={() => { dispatch({ type: type.CHANGE_SEARCH_ITEM, searchItem: 2 }) }} className={`stgi nonhidden ${globalState.searchItem === 2 ? 'active' : ''}`}>{R.common.product}</div>
                                                    </div>
                                                </div>
                                                <div className='sc nonhidden'>
                                                    {renderSearchResult()}
                                                </div>
                                            </div>

                                    }


                                </div>
                            </div>

                        }

                    </div>

                    <div className="collapse navbar-collapse">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link to="/" onClick={() => setMenu('/', dispatch)} className={`nav-link ${menu === 1 && 'active'}`}>
                                    <HomeIcon />
                                    <HomeOutlinedIcon />
                                </Link>
                            </li>
                            <li className="nav-item zscale-effect">
                                <Link to="/discover" onClick={() => setMenu('/discover', dispatch)} className={`nav-link ${menu === 6 && 'active'}`}>
                                    <ShirtIcon />
                                    <ShirtOutlinedIcon />
                                </Link>
                            </li>
                            <li className="nav-item zscale-effect">
                                <Link to={meUrl} onClick={() => setMenu(meUrl, dispatch)} className={`nav-link ${menu === 2 && 'active'}`}>
                                    <AccountIcon />
                                    <AccountOutlinedIcon />
                                </Link>
                            </li>
                            <li className="nav-item zscale-effect noselect">
                                <div onClick={showNotificationDialog} className={`nav-link ${globalState.isOpenNotificationDialog && 'active'}`}>
                                    <BellIcon />
                                    <BellOutlinedIcon />
                                    {
                                        globalState.total_notification > 0
                                        &&
                                        <div className={`${!globalState.use_total_notification_from_state ? 'notification-num' : 'notification-nonum'}`}>{renderTotalNotification()}</div>
                                    }
                                </div>
                            </li>
                            {
                                globalState.isOpenNotificationDialog
                                &&
                                <div className={`notification-dialog shadow ${!globalState.isOpenSearchDialog && 'default'}`}>
                                    <NotificationsPanelComponent />
                                </div>

                            }

                            <li className="dropdown nav-item">
                                <a className={`dropdown-toggle nav-link ${menu === 3 || menu === 5 && 'active'}`} data-toggle="dropdown" aria-expanded="false">
                                    <SettingOutlinedIcon />
                                    <SettingIcon />
                                    <b className="caret"></b>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <Link className="dropdown-item" to="/setting/profile">
                                        {R.common.profile}
                                    </Link>
                                    <Link className="dropdown-item" to="/setting/account">
                                        {R.common.account}
                                    </Link>
                                    <Link className="dropdown-item" to="/setting/notification">
                                        {R.common.notification}
                                    </Link>
                                    <div className="dropdown-divider"></div>
                                    <span style={{ cursor: 'pointer' }} onClick={signOut} href="#" className="dropdown-item">{R.common.logout}</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            {/* Mobile */}
            <div id='top-header-mobile' className='top-header-mobile'>
                <div className='mb-searchbox'>
                    <div className='g'>
                        <Link to='#bag'>
                            <div onClick={showBagHandler}>
                                <BagIcon />
                                {
                                    !isEmpty(globalState.itemsInBag)
                                    &&
                                    <div className='havenew' style={{ marginLeft: '1rem' }}></div>
                                }
                            </div>
                        </Link>
                        <div className='ig'>
                            <i className='material-icons zscale-effect' data-toggle="modal" data-target={`#searchModal`}>search</i>
                            <Link to='/mix'>
                                <ClothesIcon width='25' height='25' style={{ marginTop: '0.2rem', strokeWidth: '1px', stroke: '#262626' }} />
                                {
                                    !isEmpty(globalState.itemsInDesign)
                                    &&
                                    <div className='havenew' style={{ marginLeft: '1.1rem' }}></div>
                                }

                            </Link>

                        </div>
                    </div>
                </div>
                <nav className='znav-mobile noselect'>
                    <div className='mi'>
                        <Link to="/" onClick={() => setMenu('/', dispatch)} className={`${menu === 1 && 'active'}`}>
                            <HomeIcon />
                            <HomeOutlinedIcon />
                        </Link>
                    </div>
                    <div className='mi zscale-effect'>
                        <Link to="/discover" onClick={() => setMenu('/', dispatch)} className={`${menu === 6 && 'active'}`}>
                            <ShirtIcon />
                            <ShirtOutlinedIcon />
                        </Link>
                    </div>
                    <div className='mi zscale-effect'>
                        <Link to={meUrl} onClick={() => setMenu(meUrl, dispatch)} className={`${menu === 2 && 'active'}`}>
                            <AccountIcon />
                            <AccountOutlinedIcon />
                        </Link>
                    </div>
                    <div className='mi zscale-effect'>
                        <Link to="/notifications" onClick={() => { resetTotalNotification(); setMenu('/notifications', dispatch) }} className={`${menu === 4 && 'active'}`}>

                            {
                                globalState.total_notification > 0
                                &&
                                <div className={`${!globalState.use_total_notification_from_state ? 'notification-num' : 'notification-nonum'}`}>{renderTotalNotification()}</div>
                            }
                            <BellIcon />
                            <BellOutlinedIcon />
                        </Link>
                    </div>
                    <div className='mi zscale-effect'>
                        <Link to="/settings" onClick={() => setMenu('/settings', dispatch)} className={`${menu === 5 && 'active'}`}>
                            <SettingOutlinedIcon />
                            <SettingIcon />
                        </Link>

                    </div>
                </nav>
            </div>
            <SearchBoxPanel id='searchModal' isScroll={true} />
            <InBagPanel isShow={localState.showInBagDetail && window.location.hash.indexOf('#bag') !== -1} onBack={closeShowInBagDetailHandler} />
        </>
    )
}

export default MenuComponent;