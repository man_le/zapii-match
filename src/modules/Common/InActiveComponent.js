import React, {useRef} from 'react';
import { CON_TYPE, DB, fireStore, realTime } from '../../utils/DBUtils';
import md5 from 'md5';
import {isEmpty} from 'lodash';
import R from './../../locale/R';
import './stylesheet.scss';
import { showNotification } from '../../utils/NotificationUtils';

const Comp = ({ user }) => {

    const codeRef = useRef(null);

    const submitHandler = async () =>{
        const value = codeRef.current.value;
        if(!isEmpty(value)){
            const encrypValue = md5(value);
            if(encrypValue === user.extData[0].vcode){
                await fireStore.update(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, user.extData[0]._id], {'inactive':false}, false);
                window.location.reload(); 
            }else{
                showNotification('danger', R.access.wrong_vcode);
            }
        }
    }

    const resendHandler = async () =>{
        await realTime.update(
            'user_need_active',
            { [user.extData[0]._id]: user.email },
            null,
            user.extData[0]._id);

        window.location.reload(); 
    }

    return (
        <div className='zmain full'>
            <div className='row bar'>
                <div className='col-md-6 inactive-group'>
                    <div className='zti'>
                        {R.inactive.inactive}
                    </div>
                    <div>
                        {R.inactive.msg1}<strong>{user.email}</strong>{R.inactive.msg2}
                    </div>
                    <div className='zcode-group'>
                        <div className='zt'>ZAPII - </div>
                        <input ref={codeRef} type='text' maxLength="6" className='zcode' />
                    </div>
                    <div>
                        <button onClick={submitHandler} type='button' className='zbtn noselect zpointer'>{R.inactive.complete}</button>
                    </div>
                    <div>
                        <span onClick={resendHandler} className='resend noselect'>{R.inactive.resend}</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Comp;