import React from 'react';
import './stylesheet.scss';
import short from 'short-uuid';
import {isEmpty} from 'lodash';
import Avatar from 'react-avatar';
import { Link } from 'react-router-dom';
import MoreActionComponent from './MoreActionComponent';
import UserItemLoader from './UserItemLoader';

const UserItem = ({ type, modalId, userData, description, content_id, actionData, hideActionMenu, extClass }) => {

    const translator = short();

    if(isEmpty(userData)) return <UserItemLoader/>

    return (

        <div className={`user-item-group ${extClass && extClass}`}>
            <div className='i-av-g'>
                <Link onClick={()=>{modalId && document.getElementById(`${modalId}`).click()}} to={`/${translator.fromUUID(userData._id)}`}>
                    <Avatar className='i-av of_cover' src={userData.avatar} name={userData.name} size='3rem' round={true}/>
                </Link>
            </div>
            <div className='i-txt-g'>
                <Link onClick={()=>{modalId && document.getElementById(`${modalId}`).click()}} to={`/${!content_id ? translator.fromUUID(userData._id) : (type!=='following' || !type?'status/':'')+translator.fromUUID(content_id)}`}>
                    <div className='i-txt-1 noselect'><strong>{userData.name}</strong> {userData.label}</div>
                    {
                        description && <div className='i-txt-2 noselect'>{description}</div>
                    }
                </Link>
            </div>
            {
                !hideActionMenu
                &&
                <MoreActionComponent data={actionData} />
            }

        </div>
    )
}

export default UserItem;
