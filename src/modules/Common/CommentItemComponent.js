import * as firebase from "firebase/app";
import { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useContext, useEffect, useState } from 'react';
import NumericLabel from 'react-pretty-numbers';
import ReactHtmlParser from 'react-html-parser';
import Autolinker from 'autolinker';
import { Link } from 'react-router-dom';
import Avatar from 'react-avatar';
import short from 'short-uuid';
import type from '../../reducers/type';
import { CON_TYPE, DB, fireStore, realTime } from '../../utils/DBUtils';
import { buildLikeCommentNotification_nonRealTime } from '../../utils/NotificationUtils';
import { formatMoment } from '../../utils/StringUtils';
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from './../../locale/R';
import './stylesheet.scss';
import TimelineItemLoader from './TimelineItemLoader';

const CommentItemComponent = ({ commentObj, replyHandler, realTime_id, removeCommentHandler, loggedUser, isShow, statusType }) => {

    var translator = short();

    const [localState, setLocalState] = useState({
        userInfo: {},
        replyUserInfo: {},
        likeUserIds: [],
        likeUsers: [],
        isLike: null,
        removed: false
    });

    const [globalState, dispatch] = useContext(GlobalStateContext);

    const fetchData = async () => {
        let { newLikes_comment_userIds, newLikes_comment_users } = globalState;

        const userInfo = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, commentObj.user_id]);

        let replyUserInfo = null;
        if (!isEmpty(commentObj.reply_user_id) && commentObj.reply_user_id !== 'all') {
            replyUserInfo = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, commentObj.reply_user_id]);
        }

        // Get like user info
        const likeUserIds = commentObj.likes || []
        let likeUsers = [];
        if (!isEmpty(likeUserIds)) {

            // Seperate big array to multiple array with 10 items inside
            let maxLen = 100;
            if (likeUserIds.length < 100) maxLen = likeUserIds.length;
            let arrayMax10 = [];
            let step = 0;
            for (let i = 0; i <= maxLen / 10; i++) {
                arrayMax10.push(likeUserIds.slice(step, step + 10));
                step += 10;
            }
            for (let i = 0; i < arrayMax10.length; i++) {
                if (!isEmpty(arrayMax10[i]) && arrayMax10[i].length <= 10) {
                    const result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.IN, arrayMax10[i]]);
                    if(!isEmpty(result)) likeUsers.push(...result);
                }
            }
            // End seperating

        }

        if (!isEmpty(userInfo)) setLocalState({ userInfo: userInfo[0], likeUsers, likeUserIds, replyUserInfo: !isEmpty(replyUserInfo) && replyUserInfo[0] });

        // Find user like a comment in Realtime db
        realTime.find(
            DB.RT_NOTIFICATION.name(),
            snap => {
                const buildNewUserIdsLikes = [];
                const buildNewUserLikes = [];
                if (!isEmpty(snap.val())) {
                    for (let [key, value] of Object.entries(snap.val())) {
                        if (likeUserIds.indexOf(key) === -1) {
                            buildNewUserIdsLikes.push(key);
                            buildNewUserLikes.push(value);

                            // Show notification
                            // buildLikeCommentNotification(value, commentObj, loggedUser, store, dispatch, globalState);
                        }
                    }
                    newLikes_comment_userIds[commentObj._id_gen] = buildNewUserIdsLikes;
                    newLikes_comment_users[commentObj._id_gen] = buildNewUserLikes;
                    dispatch({ type: type.BUILD_NEW_LIKE_A_COMMENT, newLikes_comment_userIds, newLikes_comment_users });

                } else {
                    dispatch({ type: type.BUILD_NEW_LIKE_A_COMMENT, newLikes_comment_userIds: [], newLikes_comment_users: [] });
                }
            },
            `${commentObj._id_gen}_like_comment`);
    }

    useEffect(() => {
        fetchData();
    }, [])

    const likeModalHandler = async () => {
        dispatch({ type: type.LIKE_COMMENT_MODAL_LOADING });
        dispatch({ type: type.LIKE_COMMENT_MODAL, commentLikeUsers: localState.likeUsers, currentComment_id: commentObj._id_gen });
    }

    const likeHandler = async () => {
        let { likeUserIds, likeUsers } = localState;

        // Like
        if (isEmpty(likeUserIds) || likeUserIds.indexOf(loggedUser._id) === -1) {
            likeUserIds.push(loggedUser._id);
            likeUsers.push(loggedUser);
            setLocalState({ ...localState, isLike: true });
            const data = {
                likes: firebase.firestore.FieldValue.arrayUnion(loggedUser._id),
            }
            if (isEmpty(commentObj._id_gen)) {
                await fireStore.updateMultiConditions(
                    DB.COMMENT.name(),
                    data, true,
                    [DB.COMMENT.ID, CON_TYPE.EQ, commentObj._id_gen],
                );
            } else {
                await fireStore.updateMultiConditions(
                    DB.COMMENT.name(),
                    data, true,
                    [DB.COMMENT.ID_GEN, CON_TYPE.EQ, commentObj._id_gen],
                );
            }

            // Add to realtime database
            realTime.update(
                DB.RT_NOTIFICATION.name(),
                { [loggedUser._id]: loggedUser },
                null,
                `${commentObj._id_gen}_like_comment`);

            // Update realtime like for show notification on other page
            const dataForNotiInfo = {
                [loggedUser._id]: {
                    user_id: loggedUser._id,
                    status_id: commentObj.status_id,
                    comment_id_gen: commentObj._id_gen
                }
            }
            realTime.update(DB.RT_NOTIFICATION_INFO.name(), dataForNotiInfo, null, `${commentObj.user_id}_like_comment_notification`);

            // Add notification info
            buildLikeCommentNotification_nonRealTime(commentObj, loggedUser);

            // Un like
        } else {
            likeUserIds.splice(likeUserIds.indexOf(loggedUser._id), 1);
            const indexUserWillRemoved = likeUsers.findIndex(e => e._id === loggedUser._id);
            likeUsers.splice(indexUserWillRemoved, 1);

            setLocalState({ ...localState, isLike: false });
            const data = {
                likes: firebase.firestore.FieldValue.arrayRemove(loggedUser._id),
            }
            await fireStore.updateMultiConditions(
                DB.COMMENT.name(),
                data, true,
                [DB.COMMENT.ID_GEN, CON_TYPE.EQ, commentObj._id_gen],
            );

            // Add to realtime database
            realTime.update(
                DB.RT_NOTIFICATION.name(),
                { [loggedUser._id]: null },
                null,
                `${commentObj._id_gen}_like_comment`);
        }
    }

    const likeActiveHandler = () => {
        const { likeUserIds, isLike } = localState;
        if (isLike == null && !isEmpty(likeUserIds) && !isEmpty(userInfo) && likeUserIds.indexOf(loggedUser._id) !== -1) {
            return 'active';
        } else if (isLike === true) return 'active';
        return '';
    }

    const renderTotalLike = () => {
        const { likeUserIds } = localState;
        const { newLikes_comment_userIds } = globalState;
        let count = 0;
        if (!isEmpty(likeUserIds)) {
            count += likeUserIds.length;
        }
        if (!isEmpty(newLikes_comment_userIds) && !isEmpty(newLikes_comment_userIds[commentObj._id_gen])) {
            count += newLikes_comment_userIds[commentObj._id_gen].length;
        }
        return count;
    }

    const deleteHandler = async () => {
        if (isEmpty(commentObj._id_gen)) {
            fireStore.remove(DB.COMMENT.name(),
                [DB.COMMENT.ID_GEN, CON_TYPE.EQ, commentObj._id_gen],
            );
        } else {
            fireStore.remove(DB.COMMENT.name(),
                [DB.COMMENT.ID_GEN, CON_TYPE.EQ, commentObj._id_gen],
            );
        }

        // Increment total comment
        const dataQuiz = {
            totalComment: firebase.firestore.FieldValue.increment(-1),
        }
        removeCommentHandler(realTime_id);

        const quizResult = await fireStore.find(DB.STATUS.name(), [DB.STATUS.ID, CON_TYPE.EQ, commentObj.status_id]);
        if (!isEmpty(quizResult) && quizResult[0].totalComment > 0) {
            fireStore.updateMultiConditions(
                DB.STATUS.name(),
                dataQuiz, true,
                [DB.STATUS.ID, CON_TYPE.EQ, commentObj.status_id],
            );
        }


        setLocalState({ ...localState, removed: true });
    }

    const { userInfo, removed } = localState;
    if (isEmpty(userInfo)) return <TimelineItemLoader />;
    else if (removed || !isShow) return null;

    return (
        <div id={`comment_${commentObj._id_gen}`} className='avatar-group'>
            <Link to={`/${translator.fromUUID(userInfo._id)}`}>
                <Avatar className='avatar of_cover' src={userInfo.avatar} name={userInfo.name} size='2rem' round={true} />
            </Link>
            <div className='name-group'>
                <div>
                    <Link to={`/${translator.fromUUID(userInfo._id)}`} className='n'>{userInfo.name}</Link>
                    <div className='d'>{commentObj.createdDate !== 'now' ? formatMoment(moment, commentObj.createdDate) : R.common.now}</div>
                    <div className='content'>
                        {
                            !isEmpty(localState.replyUserInfo) &&
                            (
                                commentObj.reply_user_id !== commentObj.user_id ||
                                commentObj.reply_for_myself === false
                            )
                            &&
                            <Link to={`/${translator.fromUUID(localState.replyUserInfo._id)}`} className='rename'>{`@${localState.replyUserInfo.name}`}</Link>
                        }
                        {
                            commentObj.content.split("\n").map((item, i) => {
                                return (
                                    <span key={`${commentObj._id}_${i}`}>
                                        {ReactHtmlParser(Autolinker.link(item))}
                                        <br />
                                    </span>
                                )
                            })
                        }
                    </div>
                    <div className='c-bt-gr'>
                        <div className='favorite-group'>
                            <i onClick={likeHandler} className={`noselect material-icons favorite ${likeActiveHandler()}`}>favorite</i>
                            <div onClick={likeModalHandler} className='nlike' data-toggle="modal" data-target={`#comment_likeModal` + (statusType?'_'+statusType:'')}><NumericLabel>{renderTotalLike()}</NumericLabel></div>
                        </div>
                        <div onClick={() => replyHandler(userInfo)} className='favorite-group'>
                            <i className={`noselect material-icons reply`}>reply</i>
                            <div className='nlike'>{R.common.reply}</div>
                        </div>

                        {
                            commentObj.user_id === loggedUser._id
                            &&
                            <div onClick={deleteHandler} className='favorite-group'>
                                <i className={`noselect material-icons more`}>delete</i>
                            </div>
                        }

                    </div>
                </div>
            </div>
        </div>
    )
}

export default CommentItemComponent;