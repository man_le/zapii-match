import React from 'react';
import Resizer from 'react-image-file-resizer';
import { showNotification } from './../../utils/NotificationUtils';
import R from './../../locale/R';

const readURL = (onUploadSuccess, input, name) => {
  if (input.target.files && input.target.files[0]) {

    if (input.target.files[0].size > 4 * 1000000) {
      showNotification('danger', R.newquiz.max4mb);
      return;
    }

    // Resize image
    let file = input.target.files[0];
    Resizer.imageFileResizer(
      file,
      732,
      488,
      'JPEG',
      100,
      0,
      uri => {
        const reader = new FileReader();
        reader.onload = async (e) => {
          onUploadSuccess(uri);
        };
        reader.readAsArrayBuffer(file);
      },
      'blob'
    );
  }
}

const UploadFileComponent = ({ id, onUploadSuccess, name, accept, icon, className, component }) => {
  return (
    <>
      <input className='hidden' onChange={input => readURL(onUploadSuccess, input, name)} type='file' id={id} accept={accept} />
      <label className={className || 'in'} htmlFor={id}>
        {component}
        {/* <i className='material-icons-outlined'>{icon || 'image'}</i> */}
      </label>
    </>
  )
}

export default UploadFileComponent;