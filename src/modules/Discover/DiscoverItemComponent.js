import { isEmpty } from 'lodash';
import React, { useContext, useState } from 'react';
import CurrencyFormat from 'react-currency-format';
import { Link } from 'react-router-dom';
import short from 'short-uuid';
import type from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import BagIcon from '../Common/Icon/BagIcon';
import BookmarkIcon from '../Common/Icon/BookmarkIcon';
import R from './../../locale/R';
import './stylesheet.scss';

const DiscoverItemComponent = ({ loggedUser, data, dispatch, globalState }) => {
    const [localState, setLocalState] = useState({
        userInfo: null,
        init: false,
        removed: false,
        bookmarked: false,
        inDesigned: false,
        showDetail: false,
    });
    const [user, setUser] = useContext(RouteContext);

    let translator = short();

    const bookmarkHandler = async () => {
        let { itemsInBookmark } = globalState;

        const bookmarkData = {
            [DB.BOOKMARK.ID_GEN]: `${data._id}$${loggedUser._id}`,
            [DB.BOOKMARK.CLOTHES_ID]: data._id,
            image: isEmpty(data.image_detail) ? data.image : data.image_detail[0],
            name: data.name,
            price: data.new_price,
            mix_type: data.mix_type,
            filter_type: data.filter_type || '',
            size: data.size || [],
            [DB.BOOKMARK.USER_ID]: loggedUser._id
        };

        if (itemsInBookmark.indexOf(data._id + '$' + loggedUser._id) === -1) {

            itemsInBookmark.push(data._id + '$' + loggedUser._id);
            dispatch({ type: type.CHANGE_ITEM_IN_BOOKMARK, itemsInBookmark });

            await fireStore.updateMultiConditions(
                DB.BOOKMARK.name(),
                bookmarkData,
                true,
                [DB.BOOKMARK.CLOTHES_ID, CON_TYPE.EQ, data._id],
                [DB.BOOKMARK.USER_ID, CON_TYPE.EQ, loggedUser._id]
            );

        } else {

            itemsInBookmark = itemsInBookmark.filter(e => e !== data._id + '$' + loggedUser._id);
            dispatch({ type: type.CHANGE_ITEM_IN_BOOKMARK, itemsInBookmark });

            await fireStore.remove(
                DB.BOOKMARK.name(),
                [DB.BOOKMARK.CLOTHES_ID, CON_TYPE.EQ, data._id],
                [DB.BOOKMARK.USER_ID, CON_TYPE.EQ, loggedUser._id]
            )
        }
    }

    const preGoToURLHandler = () => {
        dispatch({ type: type.CHANGE_ROUTER, currentRouter: window.location.pathname });
    }

    const { itemsInBag, itemsInBookmark, itemsInDesign } = globalState;

    return (
        <div className='item'>
            <Link onClick={preGoToURLHandler} to={`/${window.innerWidth<=991?'detail_mobile':'detail'}/product/${translator.fromUUID(data._id)}`}>
                <img className='preview noselect' src={!isEmpty(data.image_detail) ? data.image_detail[0] : data.image} />
                <div className='item-name'>
                    {data.name}
                </div>
                <div className='price'>
                    <div className='old'>
                        <CurrencyFormat value={data.old_price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                    </div>
                    <div className='new'>
                        <CurrencyFormat value={data.new_price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                    </div>
                </div>
            </Link>

            {/* Desktop */}
            <div className='action-group noselect desk'>
                <Link onClick={preGoToURLHandler} to={`/detail/product/${translator.fromUUID(data._id)}`}>
                    <div title={R.discover.detail}>
                        <BagIcon isSelected={itemsInBag.indexOf(data._id + '$' + loggedUser._id) !== -1 || false} className='zscale-effect' width='20' height='20' style={{ marginTop: '0.15rem' }} color='#262626' />
                    </div>
                </Link>
                <div onClick={bookmarkHandler} title={R.discover.addBookmarked}>
                    <BookmarkIcon isSelected={itemsInBookmark.indexOf(data._id + '$' + loggedUser._id) !== -1 || false} className='zscale-effect' width='18' height='18' style={{ marginTop: '0.3rem' }} />
                </div>
            </div>
            {/* End Desktop */}

            {/* Mobile */}
            <div className='action-group noselect mobile'>
                <Link onClick={preGoToURLHandler} to={`/detail_mobile/product/${translator.fromUUID(data._id)}`}>
                    <div>
                        <BagIcon isSelected={itemsInBag.indexOf(data._id + '$' + loggedUser._id) !== -1 || false} className='zscale-effect' width='20' height='20' style={{ marginTop: '0.15rem' }} color='#262626' />
                    </div>
                </Link>
                <div onClick={bookmarkHandler}>
                    <BookmarkIcon isSelected={itemsInBookmark.indexOf(data._id + '$' + loggedUser._id) !== -1 || false} className='zscale-effect' width='18' height='18' style={{ marginTop: '0.3rem' }} />
                </div>
            </div>
            {/* End Mobile */}

        </div>

    )
}

export default DiscoverItemComponent