import * as firebase from "firebase/app";
import { isEmpty, isUndefined } from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import CurrencyFormat from 'react-currency-format';
import { withRouter } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import type from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import { productHaveItems } from '../../utils/JSONUtils';
import DataIsEmpty from '../Common/DataIsEmpty';
import LeftRightContentComponent from '../Common/LeftRightContentComponent';
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from './../../locale/R';
import emptyCartImg from './img/emptycart.png';
import LeftComponent from './LeftComponent';
import './stylesheet.scss';

const InBagCountComponent = ({ history }) => {

    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const [localState, setLocalState] = useState({
        init: false,
        data: [],
        loading: false,
    });

    const fetchData = async () => {
        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            setLocalState({ ...localState, loading: true });

            let userInfo = user.extData[0];

            const result = await fireStore.find(
                DB.IN_BAG.name(),
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, userInfo._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
            )

            if (!isEmpty(result)) {
                const items = seperateProductBySize(result);
                setLocalState({ ...localState, data: items, init: true, loading: false });
            } else {
                setLocalState({ ...localState, data: [], init: true, loading: false });
            }
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [user]);



    const upHandler = async (item) => {

        if (localState.loading) return;

        let newData = {
            [item.key]: firebase.firestore.FieldValue.increment(1),
        };
        if (item[item.key] >= 10) {
            newData = {
                [item.key]: 10
            }
        }

        fireStore.updateMultiConditions(
            DB.IN_BAG.name(),
            newData,
            false,
            [DB.IN_BAG.ID, CON_TYPE.EQ, item._id],
            [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
            [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
        );

        let { data } = localState;
        let dataItem = data.find(e => e._id === item._id && e.key === item.key);
        dataItem[item.key] = Number(dataItem[item.key] < 10) ? Number(dataItem[item.key]) + 1 : 10;
        setLocalState({ ...localState, data })
    }

    const downHandler = async (item) => {

        if (localState.loading) return;

        let newData = {
            [item.key]: firebase.firestore.FieldValue.increment(-1),
        };
        if (item[item.key] <= 0) {
            newData = {
                [item.key]: 0
            }
        }

        // If have no item with current size AND no item with another size, delete this itsm in db
        if (item[item.key] - 1 <= 0) {
            const isExist = productHaveItems(item);
            if (!isExist) {
                fireStore.remove(DB.IN_BAG.name(), [DB.IN_BAG.ID, CON_TYPE.EQ, item._id],
                    [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                    [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'])
            }
        } else {
            fireStore.updateMultiConditions(
                DB.IN_BAG.name(),
                newData,
                false,
                [DB.IN_BAG.ID, CON_TYPE.EQ, item._id],
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
            );
        }


        let { data } = localState;
        let dataItem = data.find(e => e._id === item._id && e.key === item.key)
        dataItem[item.key] = Number(dataItem[item.key] > 0) ? Number(dataItem[item.key]) - 1 : 0;
        setLocalState({ ...localState, data })
    }

    const nextPanelHandler = () => {
        if (isEmpty(data) || buildTotalPrice() === 0) return;
        dispatch({ type: type.CHANGE_ROUTER, currentRouter: `/bag/list`, currentRouterIndex: 6 });

        if (isEmpty(globalState.order_id)) {
            dispatch({ type: type.BUILD_ORDER_ID });
        }

        history.push('/bag/checkout');
    }

    const renderNum = (num) => {
        if (num >= 10) return 10;
        else if (num <= 0) return 0;
        else return num;
    }

    const seperateProductBySize = (items) => {
        let result = [];
        for (let i = 0; i < items.length; i++) {

            if (!isUndefined(items[i].S)) {
                let itemClone = { ...items[i] };
                delete itemClone.M;
                delete itemClone.L;
                delete itemClone.XL;
                itemClone['key'] = 'S';
                result.push(itemClone);
            }
            if (!isUndefined(items[i].M)) {
                let itemClone = { ...items[i] };
                delete itemClone.S;
                delete itemClone.L;
                delete itemClone.XL;
                itemClone['key'] = 'M';
                result.push(itemClone);
            }
            if (!isUndefined(items[i].L)) {
                let itemClone = { ...items[i] };
                delete itemClone.S;
                delete itemClone.M;
                delete itemClone.XL;
                itemClone['key'] = 'L';
                result.push(itemClone);
            }
            if (!isUndefined(items[i].XL)) {
                let itemClone = { ...items[i] };
                delete itemClone.S;
                delete itemClone.M;
                delete itemClone.L;
                itemClone['key'] = 'XL';
                result.push(itemClone);
            }

            // Size of shoes
            for (let sz = 32; sz <= 42; sz++) {
                if (!isUndefined(items[i][sz])) {
                    let itemClone = { ...items[i] };

                    // Remove another size
                    for (let szremove = 32; szremove <= 42; szremove++) {
                        if (szremove === sz) continue;
                        delete itemClone[szremove];
                    }

                    itemClone['key'] = sz.toString();
                    result.push(itemClone);
                }
            }
        }
        return result;
    }

    const buildTotalPrice = () => {
        const { data } = localState;
        let result = 0;
        for (let i = 0; i < data.length; i++) {
            let total = data[i][data[i].key];
            if (total < 0) total = 0;
            if (total > 10) total = 10;
            const value = data[i].price * total;
            result += value;
        }
        if (result <= 0) result = 0;
        return result;
    }

    const { data, init, loading } = localState;

    if (loading && !init) {
        return (
            <div className={`zloading-item`}><BounceLoader
                sizeUnit={"px"}
                size={50}
                loading={true}
                color='#777'
            /></div>
        )
    } else if (isEmpty(data) && init) {
        return (
            <div className='zmain full product-detail-page-group'>
                <div className='row bar'>
                    <div className='col-md-6 '>
                        <DataIsEmpty />
                    </div>
                </div>
            </div>
        )
    }

    return (
        <div className='zmain full product-count-page-group'>
            <div className='row bar'>

                {
                    buildTotalPrice() === 0
                    &&
                    <div className='col-md-6 zcenter'>
                        <div>
                            <img src={emptyCartImg} className='etp' />
                            <div className='text-align-center ett'>{R.common.empty_bag}</div>
                        </div>

                    </div>
                }

                {
                    buildTotalPrice() > 0
                    &&
                    <div className='col-md-6 cmenu'>
                        <div className='left'>

                        </div>
                        <div className='center'>
                            {R.bag.title}
                        </div>
                        <div onClick={nextPanelHandler} className={`right noselect zpointer ${isEmpty(data) || buildTotalPrice() === 0 ? 'zdisabled' : 'zscale-effect2'}`}>
                            <i className='material-icons'>chevron_right</i>
                        </div>
                    </div>

                }

                {
                    buildTotalPrice() > 0
                    &&
                    <div className='col-md-6 zbor alontop'>
                        <div className='gr1'>
                            {
                                data.map(e =>
                                    renderNum(e[e.key]) > 0
                                    &&
                                    <div key={`items_order_${e._id}_${e.key}`} className='pro-item noselect'>
                                        <div className='pro-preview'>
                                            <img src={e.image} />
                                        </div>
                                        <div className='pro-name'>
                                            {`${e.name} (${e.key})`}
                                        </div>

                                        <div className='price-count noselect'>
                                            <div className='pro-count'>
                                                <div onClick={() => downHandler(e)} className='zpointer up zscale-effect'>
                                                    -
                                            </div>
                                                <div className='num'>
                                                    {renderNum(e[e.key])}
                                                </div>
                                                <div onClick={() => upHandler(e)} className='zpointer down zscale-effect'>
                                                    +
                                            </div>
                                            </div>
                                            <div className='pro-price'>
                                                <CurrencyFormat value={e.price * renderNum(e[e.key])} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                            </div>
                                        </div>
                                    </div>
                                )
                            }

                            <div className='total-price-group'>
                                {
                                    !isEmpty(data) && buildTotalPrice() > 0
                                    &&
                                    <div className='total noselect'>
                                        <div className='l'>
                                            {R.bag.total}
                                        </div>
                                        <div className='r'>
                                            <CurrencyFormat value={buildTotalPrice()} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                        </div>
                                    </div>
                                }
                                <div onClick={nextPanelHandler} className={`g ${isEmpty(data) || buildTotalPrice() === 0 ? 'zdisabled' : ''}`}>
                                    <div className={`ins noselect zpointer ${isEmpty(data) || buildTotalPrice() === 0 ? '' : 'zscale-effect2'}`}>
                                        <div className='l'>
                                            {
                                                R.bag.next
                                            }
                                        </div>
                                        <i className='material-icons'>navigate_next</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                }

            </div>

            <LeftRightContentComponent
                LeftComponent={<LeftComponent />}
            />

        </div>
    )
}

export default withRouter(InBagCountComponent);