import { isEmpty, isUndefined, isFunction } from 'lodash';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { blobToDataURL } from 'blob-util'
import { BounceLoader } from 'react-spinners';
import * as firebase from "firebase/app";
import short from 'short-uuid';
import Avatar from 'react-avatar';
import ReactHtmlParser from 'react-html-parser';
import Autolinker from 'autolinker';
import uuid from 'uuid';
import moment from 'moment';
import { RouteContext } from '../../routes/routes';
import { DB, fireStore, realTime, CON_TYPE } from '../../utils/DBUtils';
import { showNotification, buildShareStatusNotification_nonRealTime } from '../../utils/NotificationUtils';
import BagIcon from '../Common/Icon/BagIcon';
import HatIcon from '../Common/Icon/HatIcon';
import ImageIcon from '../Common/Icon/ImageIcon';
import ShirtColorIcon from '../Common/Icon/ShirtColorIcon';
import ShirtIcon from '../Common/Icon/ShirtIcon';
import ShoesIcon from '../Common/Icon/ShoesIcon';
import TrousersIcon from '../Common/Icon/TrousersIcon';
import UploadFileComponent from '../Common/UploadFileComponent';
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from './../../locale/R';
import { type, uploadFile } from './../../utils/StorageUtils';
import './stylesheet.scss';
import { formatMoment } from '../../utils/StringUtils';



const NewStatusComponent = ({ viewFromHomePage, history, question, fromShareModal }) => {

    const [localState, setLocalState] = useState({
        clotherSetGroupActive: true,
        attachedFileActive: false,
        submitDisabled: true
    });

    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const desRef = useRef(null);

    const showHideClotherSetGroupHandler = () => {
        const { clotherSetGroupActive } = localState;
        let submitDisabled = clotherSetGroupActive;
        if (!isEmpty(desRef.current.value) && !isEmpty(desRef.current.value.trim())) submitDisabled = false;
        setLocalState({ ...localState, clotherSetGroupActive: !clotherSetGroupActive, submitDisabled });
    }

    const desHandler = () => {
        const value = desRef.current.value;
        if (localState.clotherSetGroupActive === false) {
            if (isEmpty(value) || isEmpty(value.trim())) {
                setLocalState({ ...localState, submitDisabled: true });
            } else {
                setLocalState({ ...localState, submitDisabled: false, description: value.substring(0, 3000) });
            }
        } else {
            setLocalState({ ...localState, submitDisabled: false, description: value.substring(0, 3000) });
        }
    }

    const formatDataBeforeSave = async (status_id) => {
        const { blobFileAttached, clotherSetGroupActive, attachedFileActive } = localState;
        const { setClothes } = globalState;

        // Build url attached
        let url = '';
        if (attachedFileActive) {
            url = await uploadFile(type.FILE + '/status/' + user.extData[0]._id, blobFileAttached, `statusFile_${status_id}`);
        }

        // Build description
        let description = '';
        if(localState.description){
            description = localState.description.substring(0, 3000) || '';
        }
        let attachedLink = '';
        if (!isEmpty(description) && (description.indexOf('http://') !== -1 || description.indexOf('https://') !== -1)) {
            let indexStartOfHttpString = description.indexOf('http://');
            if (indexStartOfHttpString === -1) indexStartOfHttpString = description.indexOf('https://');

            attachedLink = description.substring(indexStartOfHttpString, description.length).split(/\s+/)[0];
        }

        let statusType = 'status';
        if (!fromShareModal || isEmpty(setClothes)) statusType = 'status';
        else if (!isEmpty(setClothes)) statusType = 'set';

        return {
            _id: status_id,
            fileAttached: url,
            attachedLink,
            clotherSetGroupActive,
            attachedFileActive,
            description,
            [DB.STATUS.USER_ID]: user.extData[0]._id,
            type: statusType,
            setClothes: !fromShareModal ? [] : setClothes
        };
    }

    const submitHandler = async () => {
        const { clotherSetGroupActive } = localState;
        const { setClothesOwner, ownerStatusDate, ownerDescription, statusId_Owner, statusId, ownerFileAttached } = globalState;

        if (isEmpty(desRef.current.value) && !clotherSetGroupActive) return;

        if (!isEmpty(user.extData)) {

            setLocalState({ ...localState, loading: true });
            let _id = uuid.v4();
            const data = await formatDataBeforeSave(_id);

            if (!isEmpty(setClothesOwner) && fromShareModal) {
                data.setClothesOwner = setClothesOwner._id;
                data.ownerStatusDate = ownerStatusDate;
                data.ownerDescription = ownerDescription;
                data.statusId_Owner = statusId_Owner;
                data.statusId = statusId;
                data.ownerFileAttached = ownerFileAttached;
            }

            _id = await fireStore.insert(DB.STATUS.name(), data);

            // Create successfully
            if (!isEmpty(_id)) {

                // Update userid shared on database
                const data = {
                    shares: firebase.firestore.FieldValue.arrayUnion(user.extData[0]._id),
                }
                // For original status
                fireStore.updateMultiConditions(
                    DB.STATUS.name(),
                    data, true,
                    [DB.STATUS.ID, CON_TYPE.EQ, statusId_Owner],
                );
                // For current status
                fireStore.updateMultiConditions(
                    DB.STATUS.name(),
                    data, true,
                    [DB.STATUS.ID, CON_TYPE.EQ, statusId],
                );

                // START - Notification Handler
                // Add to realtime database content
                realTime.update(
                    DB.RT_NOTIFICATION.name(),
                    { [user.extData[0]._id]: user.extData[0] },
                    null,
                    `${statusId_Owner}_share`);

                // Update realtime share-quiz for show notification panel on other page
                const dataForNotiInfo = {
                    [user.extData[0]._id]: {
                        user_id: user.extData[0]._id,
                        status_id: statusId_Owner,
                    }
                }
                if (!isEmpty(setClothesOwner) && fromShareModal) {
                    realTime.update(DB.RT_NOTIFICATION_INFO.name(), dataForNotiInfo, null, `${setClothesOwner._id}_share_status_notification`);

                    // Add notification info
                    const newStatusData = {
                        _id: statusId_Owner,
                        user_id: setClothesOwner._id,
                    }
                    buildShareStatusNotification_nonRealTime(newStatusData, user.extData[0]);
                    // END - Notification Handler
                }


                // Close modal if its open
                if (fromShareModal) {
                    try {
                        let idElement = document.getElementById(`close-id-share_info_modal`);
                        if (!idElement) idElement = document.getElementById(`close-id-share_info_modal_from_timeline`);
                        idElement.click();
                    } catch (err) { }
                }

                desRef.current.value = '';

                setLocalState({ ...localState, loading: false });
                history.push('/me');
                return;

            } else showNotification('danger', R.newquiz.quiz_cannot_created);
        } else {
            showNotification('danger', R.newquiz.quiz_cannot_created);
        }

    }

    const closeAttachedHandler = () => {
        document.getElementById('uploadStatusFile').value = null;
        setLocalState({ ...localState, attachedFileActive: false, blobFileAttached: null });
    }


    const uploadSuccessHandler = blobFile => {
        blobToDataURL(blobFile).then(base64String => {
            let elem = document.getElementById("image-attached-preview");
            if(elem) elem.src = base64String;
        });
        setLocalState({ ...localState, blobFileAttached: blobFile, attachedFileActive: true });
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            if (!isEmpty(globalState.setClothes) && fromShareModal)
                setLocalState({ ...localState, clotherSetGroupActive: true, submitDisabled: false })
            loadCompleted = true;
        }

        return () => loadCompleted == true;

    }, [user, globalState.setClothes]);

    const renderOwnerDescription = () => {
        const { ownerDescription } = globalState;

        const split = ownerDescription.split('\n');
        const data = []
        for (let i = 0; i < split.length; i++) {
            data.push(<span key={`des_v_${i}`}>{ReactHtmlParser(Autolinker.link(split[i]))}<br /></span>);
        }
        if (!isEmpty(data)) return data;
        else return ownerDescription;
    }

    const { setClothes, setClothesOwner, ownerFileAttached } = globalState;

    return (
        <div className={`zmain full ${viewFromHomePage ? 'nst-from-homepage' : ''}`} style={{ paddingTop: 'unset' }}>
            <div className={!fromShareModal ? 'row col-md-6 nf2' : ''}>
                <div className={!fromShareModal ? 'col-md-12 nsg' : ''}>
                    <div className='zbod new-status-group' style={fromShareModal ? { borderRadius: 'unset', border: 'unset' } : {}}>
                        <div className='zitem br'>

                            <div className='textarea-group'>
                                <textarea id={`des_new_status_${fromShareModal ? '_share' : ''}`} maxLength={3000} className='zdes' ref={desRef} onKeyUp={desHandler} placeholder={isEmpty(question) ? R.newquiz.in_mind : R.newquiz.in_your_ans} />
                            </div>

                            {
                                localState.attachedFileActive
                                &&
                                <div className='attached-status'>
                                    <div className='t noselect'>
                                        <div className='status-group'>
                                            <div className='t2'>
                                                {R.newquiz.attached}
                                            </div>
                                            <div>
                                                <i className='material-icons noselect'>image</i>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='i' onClick={closeAttachedHandler}>
                                        <i className='material-icons noselect'>close</i>
                                    </div>
                                </div>
                            }
                        </div>
                        <div className='zitem tool-group' style={localState.attachedFileActive ? (window.location.pathname === '/status' && window.innerWidth <= 991) ? { marginTop: '4rem' } : { marginTop: '1rem' } : {}}>
                            <div className='tool'>

                                <UploadFileComponent
                                    id='uploadStatusFile'
                                    onUploadSuccess={blobFile => uploadSuccessHandler(blobFile)}
                                    name='tesst' accept='image/*'
                                    className='in'
                                    component={
                                        <div className='i noselect'>
                                            <ImageIcon style={{ stroke: '#ccc', strokeWidth: '8px' }} color='#ccc' />
                                            <div className='di'>
                                                {R.newquiz.image}
                                            </div>
                                        </div>
                                    }
                                />

                                {
                                    fromShareModal
                                    &&
                                    <div onClick={showHideClotherSetGroupHandler} className={`i noselect ${localState.clotherSetGroupActive ? 'active' : ''}`}>
                                        <ShirtColorIcon style={{ stroke: '#ccc', strokeWidth: '8px' }} color='#ccc' />
                                        <div className='di'>
                                            {R.newquiz.mixed}
                                        </div>
                                    </div>
                                }

                            </div>
                            {
                                (isUndefined(fromShareModal) || (!localState.clotherSetGroupActive && fromShareModal))
                                &&
                                <div className={`post ${isUndefined(fromShareModal) ? 'notsharemodel' : ''}`}>
                                    <button onClick={submitHandler} disabled={globalState.loading || localState.loading || localState.submitDisabled} type='button' className='btn btn-danger'>
                                        <div className='rs'>
                                            <i className='material-icons'>send</i>
                                        </div>
                                    </button>
                                </div>
                            }
                        </div>
                        <div className={`setc-group ${fromShareModal && localState.clotherSetGroupActive ? '' : 'hidden'}`}>
                            {
                                globalState.loading
                                    ?
                                    <div className={`zcenter`} style={{ marginTop: '0.75rem', marginBottom: '1.5rem' }}>
                                        <BounceLoader
                                            sizeUnit={"px"}
                                            size={50}
                                            loading={true}
                                            color='#777'
                                        />
                                    </div>
                                    :
                                    <>
                                        <div className='q-t-group'>
                                            <div className='qt'>
                                            </div>
                                            <div onClick={showHideClotherSetGroupHandler} className='qx noselect'>
                                                <i className='material-icons'>close</i>
                                            </div>
                                        </div>
                                        {
                                            !isEmpty(globalState.setClothesOwner)
                                            &&
                                            <div className='q-t-owner'>
                                                <div className='ln1'>
                                                    <div>
                                                        {R.timeline.setBy}
                                                    </div>
                                                    <div className='ln1_1'>
                                                        <div className='avt_1'>
                                                            <Avatar className='of_cover' src={globalState.setClothesOwner.avatar} name={globalState.setClothesOwner.name} size='1rem' round={true} />
                                                        </div>
                                                        <div className='n1_1 noselect'>
                                                            {globalState.setClothesOwner.name}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='ln2'>
                                                    {R.timeline.setAt} {formatMoment(moment, globalState.ownerStatusDate)}
                                                </div>
                                                <div className='ln3'>
                                                    {renderOwnerDescription()}
                                                </div>
                                            </div>
                                        }

                                        {
                                            isEmpty(globalState.setClothesOwner)
                                                ?
                                                <div className='qs-c-group'>
                                                    <div className={`ql ${!localState.blobFileAttached ? 'center' : ''}`}>
                                                        {
                                                            setClothes && setClothes.image &&
                                                            <img src={setClothes.image} />
                                                        }
                                                    </div>
                                                    {
                                                        localState.blobFileAttached
                                                        &&
                                                        <div className='qr'>
                                                            <img id='image-attached-preview' />
                                                        </div>
                                                    }
                                                </div>
                                                :
                                                <div className='qs-c-group'>
                                                    <div className={`ql ${!ownerFileAttached ? 'center' : ''}`}>
                                                        {
                                                            setClothes && setClothes.image &&
                                                            <img src={setClothes.image} />
                                                        }
                                                    </div>
                                                    {
                                                        ownerFileAttached
                                                        &&
                                                        <div className='qr'>
                                                            <img id='owner-image-attached-preview' src={ownerFileAttached} />
                                                        </div>
                                                    }
                                                </div>
                                        }

                                    </>
                            }
                        </div>
                        {
                            localState.clotherSetGroupActive && fromShareModal
                            &&
                            <div className='zitem tool-group'>
                                <div className='post'>
                                    <button onClick={submitHandler} disabled={globalState.loading || localState.loading || localState.submitDisabled} type='button' className='btn btn-danger'>
                                        <div className='rs'>
                                            <i className='material-icons'>send</i>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        }

                        <button onClick={submitHandler} disabled={globalState.loading || localState.loading || localState.submitDisabled} type='button' className={`post-header btn-link`}>
                            {R.newquiz.postnew}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withRouter(NewStatusComponent);