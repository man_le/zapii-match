import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import Avatar from 'react-avatar';
import NumericLabel from 'react-pretty-numbers';
import { withRouter } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import short from 'short-uuid';
import { updateFollower, updateFollowing } from '../../reducers';
import reducerType from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import { CON_TYPE, DB, fireStore, realTime } from '../../utils/DBUtils';
import { buildFollowingNotification_nonRealTime } from '../../utils/NotificationUtils';
import DataIsEmpty from '../Common/DataIsEmpty';
import AccountIcon from '../Common/Icon/AccountIcon';
import AccountOutlinedIcon from '../Common/Icon/AccountOutlinedIcon';
import ClothesIcon from '../Common/Icon/ClothesIcon';
import SendIcon from '../Common/Icon/SendIcon';
import ModalComponent from '../Common/ModalComponent';
import PageNotFound from '../Common/PageNotFound';
import UserItem from '../Common/UserItem';
import { GlobalStateContext } from '../Layout/HomeLayout';
import TimelineComponent from '../Status/TimelineComponent';
import R from './../../locale/R';
import AboutMeComponent from './AboutMeComponent';
import SetClotherComponent from './SetClother/SetClotherComponent';
import DefaultCovert from './imgs/default_cover.jpg';
import './stylesheet.scss';

const Comp = ({ match, history, id }) => {
    const { params: { userId, tabId } } = match;
    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);

    const [localState, setLocalState] = useState({
        user: {},
        id: null,
        loadCompleted: false,
        type: !isEmpty(tabId) ? Number(tabId) : 1,
        following: [],  // User following
        follower: [],  // User Follower
        followingResult: [], followerResult: [],
    });
    let translator = short();


    const menuHandler = (type) => {
        dispatch({ type: reducerType.CHANGE_ROUTER, currentRouter: `/${translator.fromUUID(localState.user._id)}/t/${type}` });
        setLocalState({ ...localState, type });
    }

    const fetchData = async () => {

        let result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, id]);
        let userResult = null;
        if (!isEmpty(result)) {
            userResult = result[0];
        }

        let followingResult = [];
        let followerResult = [];

        if (!isEmpty(userResult)) {
            followingResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, userResult._id]);
            followerResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, userResult._id]);
        }

        // Get Following Users
        const followingUserIds = [];
        followingResult.map(e => {
            if (followingUserIds.indexOf(e.userid_To) === -1) followingUserIds.push(e.userid_To)
        });
        const folowingUsers = await updateFollowing(dispatch, { followingUserIds, followingResult });

        // Get Follower Users
        const followerUserIds = [];
        followerResult.map(e => {
            if (followerUserIds.indexOf(e.userid_From) === -1) followerUserIds.push(e.userid_From)
        });
        const followerUsers = await updateFollower(dispatch, { followingUserIds, followerUserIds, followerResult });

        // Adapt to show button Follow/Un-Follow in Follower Modal
        if (!isEmpty(followerUsers)) {
            followerUsers.some((e, i) => {
                if (followingUserIds.indexOf(e._id) !== -1) {
                    followerUsers[i].following = true;
                    return true;
                }
            })
        }

        // Get Following Users
        const loggedUserFollowingUserIds = [];
        if (!isEmpty(user) && !isEmpty(user.extData)) {
            let followingResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id]);
            followingResult.map(e => {
                if (loggedUserFollowingUserIds.indexOf(e.userid_To) === -1) loggedUserFollowingUserIds.push(e.userid_To)
            });
        }

        setLocalState({
            ...localState,
            user: userResult,
            loadCompleted: true,
            following: folowingUsers,
            follower: followerUsers,
            followerResult,
            followingResult,
            followingUserIds: loggedUserFollowingUserIds,
            key: Math.random()
        });
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;

    }, [id]);


    // Handle Follow button
    const followingHandler = async (localState, setLocalState, isUnFollowing) => {

        setLocalState({ ...localState, submitting: true });

        if (isUnFollowing) {
            await fireStore.remove(DB.FOLLOWING.name(),
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id],
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, id]
            );
        } else {
            const data = {
                [DB.FOLLOWING.USER_ID_FROM]: user.extData[0]._id,
                [DB.FOLLOWING.USER_ID_TO]: id,
            }
            await fireStore.updateMultiConditions(
                DB.FOLLOWING.name(), data, true,
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, id],
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id],
            );


            // Update realtime following for show notification on other page
            realTime.update(DB.RT_NOTIFICATION_INFO.name(), data, null, `${id}_following_notification`, user.extData[0]._id);

            // Add notification info
            buildFollowingNotification_nonRealTime(id, user.extData[0]);

        }

        // Get Followers info
        let followerResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, id]);
        const followerUserIds = [];
        followerResult.map(e => {
            if (followerUserIds.indexOf(e.userid_From) === -1) followerUserIds.push(e.userid_From)
        });
        let followerUsers = [];
        if (!isEmpty(followerUserIds)) {

            // Seperate big array to multiple array with 10 items inside
            let maxLen = 100;
            if (followerUserIds.length < 100) maxLen = followerUserIds.length;
            let arrayMax10 = [];
            let step = 0;
            for (let i = 0; i <= maxLen / 10; i++) {
                arrayMax10.push(followerUserIds.slice(step, step + 10));
                step += 10;
            }
            for (let i = 0; i < arrayMax10.length; i++) {
                if (!isEmpty(arrayMax10[i]) && arrayMax10[i].length <= 10) {
                    const _result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.IN, arrayMax10[i]]);
                    if (!isEmpty(_result)) followerUsers.push(..._result);
                }
            }
            // End seperating
        }

        // Get Following Users
        let followingResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, id]);
        const followingUserIds = []
        followingResult.map(e => {
            if (followingUserIds.indexOf(e.userid_To) === -1) followingUserIds.push(e.userid_To)
        });
        let folowingUsers = [];
        if (!isEmpty(followingUserIds)) {

            // Seperate big array to multiple array with 10 items inside
            let maxLen = 100;
            if (followingUserIds.length < 100) maxLen = followingUserIds.length;
            let arrayMax10 = [];
            let step = 0;
            for (let i = 0; i <= maxLen / 10; i++) {
                arrayMax10.push(followingUserIds.slice(step, step + 10));
                step += 10;
            }
            for (let i = 0; i < arrayMax10.length; i++) {
                if (!isEmpty(arrayMax10[i]) && arrayMax10[i].length <= 10) {
                    const _result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.IN, arrayMax10[i]]);
                    if (!isEmpty(_result)) folowingUsers.push(..._result);
                }
            }
            // End seperating
        }

        // Adapt to show button Follow/Un-Follow in Follower Modal
        if (!isEmpty(followerUsers)) {
            followerUsers.some((e, i) => {
                if (followingUserIds.indexOf(e._id) !== -1) {
                    followerUsers[i].following = true;
                    return true;
                }
            })
        }
        setLocalState({
            ...localState,
            followingResult: followingResult || [],
            following: folowingUsers,
            followerResult: followerResult || [],
            follower: followerUsers,
            submitting: false
        });
    }

    // Handle Follow from Modal
    const followingModalHandler = async (isUnFollowing, followUserId) => {
        if (isUnFollowing) {
            await fireStore.remove(DB.FOLLOWING.name(),
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id],
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, followUserId]
            );
        } else {
            const data = {
                [DB.FOLLOWING.USER_ID_FROM]: user.extData[0]._id,
                [DB.FOLLOWING.USER_ID_TO]: followUserId,
            }
            await fireStore.updateMultiConditions(
                DB.FOLLOWING.name(),
                data, true,
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id],
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, followUserId]
            );

            // Update realtime following for show notification on other page
            realTime.update(DB.RT_NOTIFICATION_INFO.name(), data, null, `${followUserId}_following_notification`, user.extData[0]._id);

            // Add notification info
            buildFollowingNotification_nonRealTime(followUserId, user.extData[0]);

        }

        // Get Following Users
        let followingResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id]);
        const followingUserIds = [];
        followingResult.map(e => {
            if (followingUserIds.indexOf(e.userid_To) === -1) followingUserIds.push(e.userid_To)
        });

        setLocalState({ ...localState, followingUserIds: followingUserIds });
    }

    const renderTimeLine = () => {
        const { type } = localState;
        switch (type) {
            case 1:
                return <TimelineComponent isViewOtherProfile={true} otherProfileUser_id={localState.user._id} fromProfile={true} key={localState.key} hideNewStatus={true} history={history}/>
            case 2:
                return <SetClotherComponent fromOtherProfile={true} otherProfileUser_id={localState.user._id} />
            case 3:
                return <AboutMeComponent key={localState.key} user={localState.user} fromOtherProfile={true} />;
        }
    }

    if (!localState.loadCompleted) return (
        <div className={`zloading-item`}>
            <BounceLoader
                sizeUnit={"px"}
                size={50}
                loading={true}
                color='#777'
            /></div>);
    else if (isEmpty(localState.user)) return <PageNotFound />;

    // Disable follow button if I following user
    let isDisabledFollow = false;

    if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(localState.followerResult)) {
        isDisabledFollow = !isEmpty(localState.followerResult.filter(e => e[DB.FOLLOWING.USER_ID_FROM] === user.extData[0]._id));
    }

    return (
        <div style={{ minHeight: '39rem' }}>
            <div className='row col-md-6 profile-info'>

                <div className='row col-md-6 zcover-g'>
                    <img src={localState.user.cover || DefaultCovert} />
                    <div className='zcover-line'></div>
                </div>

                <div className='col-md-3 zavatar-g'>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <Avatar className='of_cover' src={localState.user.avatar} name={localState.user.name} size='7rem' round={true} />
                    </div>
                </div>

            </div>


            <div className='row col-md-6 uname-g'>
                <div className='fl'>
                    <div className="zlable" data-toggle="modal" data-target="#followId">
                        <strong><NumericLabel>{globalState.following ? globalState.following.length : 0}</NumericLabel></strong> {R.profile.following}
                    </div>
                </div>
                <div className='zname zellipis'>
                    {localState.user.name}
                    <div className='flbt'>
                        {
                            userId !== translator.fromUUID(user.extData[0]._id)
                            &&
                            <button disabled={localState.submitting} onClick={() => followingHandler(localState, setLocalState, isDisabledFollow ? true : false)} type='button' className={`btn btn-sm btn-danger zflex`}>
                                <div className='material-g'>
                                    <i className='material-icons'>perm_identity</i>
                                </div>
                                <div className='znn'>{isDisabledFollow ? R.timeline.following : R.timeline.follow}</div>
                            </button>
                        }
                    </div>
                </div>
                <div className='fl'>
                    <div className="zlable" data-toggle="modal" data-target="#followerId">
                        <strong><NumericLabel>{globalState.follower ? globalState.follower.length : 0}</NumericLabel></strong> {R.profile.follower}
                    </div>
                </div>
            </div>

            <div className='row col-md-6 uname-g-mobile'>
                <div className='zname zellipis'>
                    {localState.user.name}
                </div>
                <div className='fl'>
                    <div className="zlable" data-toggle="modal" data-target="#followId">
                        <strong><NumericLabel>{globalState.following ? globalState.following.length : 0}</NumericLabel></strong> {R.profile.following}
                    </div>
                </div>
                <div className='fl'>
                    <div className="zlable" data-toggle="modal" data-target="#followerId">
                        <strong><NumericLabel>{globalState.follower ? globalState.follower.length : 0}</NumericLabel></strong> {R.profile.follower}
                    </div>
                </div>
                <div className='fl'>
                    {
                        userId !== translator.fromUUID(user.extData[0]._id)
                        &&
                        <button disabled={localState.submitting} onClick={() => followingHandler(localState, setLocalState, isDisabledFollow ? true : false)} type='button' className={`btn btn-sm btn-danger zflex`}>
                            <div className='material-g'>
                                <i className='material-icons'>perm_identity</i>
                            </div>
                            <div className='znn'>{isDisabledFollow ? R.timeline.following : R.timeline.follow}</div>
                        </button>
                    }
                </div>
            </div>



            <div className='row tag-group' style={{marginBottom:'0.75rem'}}>
                <div className='col-md-6 ztag'>
                    <div className='ztag-item'>
                        <ul className='g'>
                            <li onClick={() => menuHandler(1)} className={`noselect ${localState.type === 1 ? 'active' : ''}`}>
                                <SendIcon isSelected={localState.type === 1} width='27' height='27' />
                                <div>{R.profile.created}</div>
                            </li>
                            <li onClick={() => menuHandler(2)} className={`noselect ${localState.type === 2 ? 'active' : ''}`}>
                                <ClothesIcon isSelected={localState.type === 2} style={{ marginTop: '-0.15rem' }} />
                                <div>{R.profile.setClo}</div>
                            </li>
                            <li onClick={() => menuHandler(3)} className={`noselect ${localState.type === 2 ? 'active' : ''}`}>
                                {
                                    localState.type === 3
                                        ?
                                        <AccountIcon />
                                        :
                                        <AccountOutlinedIcon style={{ stroke: '#fff', strokeWidth: '7px' }} />
                                }
                                <div>{R.profile.about}</div>
                            </li>
                        </ul>

                        <ul className='g-mobile'>
                            <li onClick={() => menuHandler(1)} className={`noselect ${localState.type === 1 ? 'active' : ''}`}>
                                <SendIcon isSelected={localState.type === 1} width='27' height='27' />
                            </li>
                            <li onClick={() => menuHandler(2)} className={`noselect ${localState.type === 2 ? 'active' : ''}`}>
                                <ClothesIcon isSelected={localState.type === 2} style={{ marginTop: '-0.15rem' }} />
                            </li>
                            <li onClick={() => menuHandler(3)} className={`noselect ${localState.type === 3 ? 'active' : ''}`}>
                                {
                                    localState.type === 3
                                        ?
                                        <AccountIcon />
                                        :
                                        <AccountOutlinedIcon style={{ stroke: '#fff', strokeWidth: '7px' }} />
                                }
                            </li>
                        </ul>

                    </div>
                </div>
            </div>

            <div className={localState.type === 1 ? 'novScroll' : ''}>
                {renderTimeLine()}
            </div>

            <ModalComponent id='followId' title={`${R.timeline.following} (${localState.following ? localState.following.length : 0})`}>
                <div>
                    {
                        !isEmpty(localState.following)
                            ?
                            localState.following.map((e, i) =>
                                <UserItem
                                    modalId='followId'
                                    key={i} userData={e}
                                    hideActionMenu={e._id === user.extData[0]._id}
                                    description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                    actionData={[
                                        {
                                            label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                            onClickHandler: () => followingModalHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                        }
                                    ]}
                                />
                            )
                            :
                            <DataIsEmpty hideIcon={true} label={R.common.no_following} color='#eee' />
                    }

                </div>
            </ModalComponent>

            <ModalComponent id='followerId' title={`${R.timeline.follower} (${localState.follower ? localState.follower.length : 0})`}>
                <div>
                    {
                        !isEmpty(localState.follower)
                            ?
                            localState.follower.map((e, i) =>
                                <UserItem modalId='followerId' key={i} userData={e}
                                    hideActionMenu={e._id === user.extData[0]._id}
                                    description={localState.followingUserIds.indexOf(e._id) !== -1 && R.timeline.following}
                                    actionData={[
                                        {
                                            label: localState.followingUserIds.indexOf(e._id) !== -1 ? R.timeline.un_follow : R.timeline.follow,
                                            onClickHandler: () => followingModalHandler(localState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                        }
                                    ]}
                                />
                            )
                            :
                            <DataIsEmpty hideIcon={true} label={R.common.no_follower} color='#eee' />
                    }

                </div>
            </ModalComponent>
        </div>
    )
}

export default withRouter(Comp);