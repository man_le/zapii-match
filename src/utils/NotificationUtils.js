import * as firebase from "firebase/app";
import { store } from 'react-notifications-component';
import { CON_TYPE, DB, fireStore } from './DBUtils';

export const showNotification = (type, msg, isContent) =>{
    let notificationValue = {
        message: msg,
        type: type,
        insert: "top",
        container: "top-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
            duration: 3000
        }
    }
    if(isContent){
        notificationValue.content=msg;
    }
    if(window.innerWidth <= 991 && type==='success')return;
    
    const notificationItems = document.getElementsByClassName('notification-parent');
    if(!notificationItems || notificationItems.length===0){
        store.addNotification(notificationValue);
    }
}

export const buildLikeStatusNotification_nonRealTime = (quiz, loggedUser) => {

    // You like
    if (loggedUser._id === quiz.user_id) return;

    // Save notification
    const data = {
        [DB.NOTIFICATION.USER_ID]: quiz.user_id, // statu owner
        [DB.NOTIFICATION.USER_ID_DID]: loggedUser._id,
        createdByMe: loggedUser._id === quiz.user_id,
        [DB.NOTIFICATION.OBJECT_ID]: quiz._id,
        [DB.NOTIFICATION.OBJECT_TYPE]: 'like_status',
        [DB.NOTIFICATION.IS_READ]: false
    }
    fireStore.updateMultiConditions(
        DB.NOTIFICATION.name(),
        data, true,
        [DB.NOTIFICATION.USER_ID, CON_TYPE.EQ, quiz.user_id],
        [DB.NOTIFICATION.USER_ID_DID, CON_TYPE.EQ, loggedUser._id],
        [DB.NOTIFICATION.OBJECT_ID, CON_TYPE.EQ, quiz._id],
        [DB.NOTIFICATION.OBJECT_TYPE, CON_TYPE.EQ, 'like_status'],
    );

    // Increment total notification
    const totalNotification = {
        total_notification: firebase.firestore.FieldValue.increment(1),
    }
    fireStore.updateMultiConditions(
        DB.USER.name(),
        totalNotification, false,
        [DB.USER.ID, CON_TYPE.EQ, quiz.user_id],
    );
}

export const buildLikeCommentNotification_nonRealTime = async (commentObj, loggedUser) => {

    // If you like for yourself
    if (commentObj.user_id === loggedUser._id) return;

    // Save notification
    const data = {
        [DB.NOTIFICATION.USER_ID]: commentObj.user_id, // comment owner
        [DB.NOTIFICATION.USER_ID_DID]: loggedUser._id,
        createdByMe: loggedUser._id === commentObj.user_id,
        [DB.NOTIFICATION.OBJECT_ID]: commentObj.status_id,
        [DB.NOTIFICATION.OBJECT_TYPE]: 'like_comment',
        [DB.NOTIFICATION.IS_READ]: false
    }
    fireStore.updateMultiConditions(
        DB.NOTIFICATION.name(),
        data, true,
        [DB.NOTIFICATION.USER_ID, CON_TYPE.EQ, commentObj.user_id],
        [DB.NOTIFICATION.USER_ID_DID, CON_TYPE.EQ, loggedUser._id],
        [DB.NOTIFICATION.OBJECT_ID, CON_TYPE.EQ, commentObj._id],
        [DB.NOTIFICATION.OBJECT_TYPE, CON_TYPE.EQ, 'like_comment'],
    );

    // Increment total notification
    const totalNotification = {
        total_notification: firebase.firestore.FieldValue.increment(1),
    }
    fireStore.updateMultiConditions(
        DB.USER.name(),
        totalNotification, false,
        [DB.USER.ID, CON_TYPE.EQ, commentObj.user_id],
    );
}

export const buildCommentNotification_nonRealTime = async (commentObj, quiz, loggedUser) => {

    if (commentObj.user_id === quiz.user_id) return;

    let isReply = false;
    if (commentObj.reply_user_id !== commentObj.user_id &&
        commentObj.reply_user_id !== loggedUser._id) {
        isReply = true;
    }

    // Save notification
    const data = {
        [DB.NOTIFICATION.USER_ID]: !isReply ? quiz.user_id : commentObj.reply_user_id,
        [DB.NOTIFICATION.USER_ID_DID]: commentObj.user_id,
        createdByMe: commentObj.user_id === quiz.user_id,
        [DB.NOTIFICATION.OBJECT_ID]: quiz._id,
        [DB.NOTIFICATION.OBJECT_TYPE]: !isReply ? 'comment_quiz' : 'reply_comment',
        [DB.NOTIFICATION.IS_READ]: false
    }
    fireStore.insert(
        DB.NOTIFICATION.name(),
        data
    );

    // Increment total notification
    const totalNotification = {
        total_notification: firebase.firestore.FieldValue.increment(1),
    }
    fireStore.updateMultiConditions(
        DB.USER.name(),
        totalNotification, false,
        [DB.USER.ID, CON_TYPE.EQ, !isReply ? quiz.user_id : commentObj.reply_user_id],
    );
}

export const buildFollowingNotification_nonRealTime = (user_id, loggedUser) => {

    // You like
    if (loggedUser._id === user_id) return;

    // Save notification
    const data = {
        [DB.NOTIFICATION.USER_ID]: user_id, // owner
        [DB.NOTIFICATION.USER_ID_DID]: loggedUser._id,
        createdByMe: loggedUser._id === user_id,
        [DB.NOTIFICATION.OBJECT_ID]: loggedUser._id,
        [DB.NOTIFICATION.OBJECT_TYPE]: 'following',
        [DB.NOTIFICATION.IS_READ]: false
    }
    fireStore.updateMultiConditions(
        DB.NOTIFICATION.name(),
        data, true,
        [DB.NOTIFICATION.USER_ID, CON_TYPE.EQ, user_id],
        [DB.NOTIFICATION.USER_ID_DID, CON_TYPE.EQ, loggedUser._id],
        [DB.NOTIFICATION.OBJECT_ID, CON_TYPE.EQ, loggedUser._id],
        [DB.NOTIFICATION.OBJECT_TYPE, CON_TYPE.EQ, 'following'],
    );

    // Increment total notification
    const totalNotification = {
        total_notification: firebase.firestore.FieldValue.increment(1),
    }
    fireStore.updateMultiConditions(
        DB.USER.name(),
        totalNotification, false,
        [DB.USER.ID, CON_TYPE.EQ, user_id],
    );
}

export const buildShareStatusNotification_nonRealTime = (status, loggedUser) => {

    // You share
    if (loggedUser._id === status.user_id) return;

    // Save notification
    const data = {
        [DB.NOTIFICATION.USER_ID]: status.user_id, // status owner
        [DB.NOTIFICATION.USER_ID_DID]: loggedUser._id,
        createdByMe: loggedUser._id === status.user_id,
        [DB.NOTIFICATION.OBJECT_ID]: status._id,
        [DB.NOTIFICATION.OBJECT_TYPE]: 'share_status',
        [DB.NOTIFICATION.IS_READ]: false
    }
    fireStore.updateMultiConditions(
        DB.NOTIFICATION.name(),
        data, true,
        [DB.NOTIFICATION.USER_ID, CON_TYPE.EQ, status.user_id],
        [DB.NOTIFICATION.USER_ID_DID, CON_TYPE.EQ, loggedUser._id],
        [DB.NOTIFICATION.OBJECT_ID, CON_TYPE.EQ, status._id],
        [DB.NOTIFICATION.OBJECT_TYPE, CON_TYPE.EQ, 'share_status'],
    );

    // Increment total notification
    const totalNotification = {
        total_notification: firebase.firestore.FieldValue.increment(1),
    }
    fireStore.updateMultiConditions(
        DB.USER.name(),
        totalNotification, false,
        [DB.USER.ID, CON_TYPE.EQ, status.user_id],
    );
}

export const buildScoreStatusNotification_nonRealTime = (status, loggedUser) => {

    // Save notification
    const data = {
        [DB.NOTIFICATION.USER_ID]: status.user_id,
        [DB.NOTIFICATION.USER_ID_DID]: loggedUser._id,
        createdByMe: loggedUser._id === status.user_id,
        [DB.NOTIFICATION.OBJECT_ID]: status._id,
        [DB.NOTIFICATION.OBJECT_TYPE]: 'score_status',
        [DB.NOTIFICATION.IS_READ]: false
    }
    fireStore.updateMultiConditions(
        DB.NOTIFICATION.name(),
        data, true,
        [DB.NOTIFICATION.USER_ID, CON_TYPE.EQ, status.user_id],
        [DB.NOTIFICATION.USER_ID_DID, CON_TYPE.EQ, loggedUser._id],
        [DB.NOTIFICATION.OBJECT_ID, CON_TYPE.EQ, status._id],
        [DB.NOTIFICATION.OBJECT_TYPE, CON_TYPE.EQ, 'score_status'],
    );

    // Increment total notification
    const totalNotification = {
        total_notification: firebase.firestore.FieldValue.increment(1),
    }
    fireStore.updateMultiConditions(
        DB.USER.name(),
        totalNotification, false,
        [DB.USER.ID, CON_TYPE.EQ, status.user_id],
    );
}