import React from 'react';
import { Link } from 'react-router-dom';
import LogoImg from './img/zapiilogo.png';
import './stylesheet_terms.scss';

const Comp = () => {
    return (
        <div className='container terms-group'>
            <div className='row'>
                <div className='col-md-6'>
                    <div className='ztop'>
                        <img className='top-logo' src={LogoImg} />
                        {
                            window.location.pathname.indexOf('_terms') !== -1
                                ?
                                <Link to='/' className='top-link'>
                                    Trang Chủ
                                </Link>
                                :
                                <Link to='/login' className='top-link'>
                                    Đăng nhập
                                </Link>
                        }
                    </div>
                    <div className='zdetail'>
                        <div className='hd'>
                            Điều khoản sử dụng
                        </div>
                        <div className='text-gradiant-red tt l'>Dịch vụ của Zapii</div>
                        <div className='l'>Sứ mệnh của chúng tôi là mang lại cho mọi người một không gian, nơi bạn có thể chia sẻ những cảm nghĩ của mình về các chủ đề trong cuộc sống cũng như thể hiện cá tính của bản thân thông qua gu thời trang.</div>
                        <div className='l'>Bạn có thể tự do sáng tạo một phong cách thời trang riêng biệt thông qua tính năng "Phối Đồ", sau đó lưu vào bộ sưu tập, chia sẻ để mọi người có thể tham khảo và bình luận, hoặc đặt hàng, chúng tôi sẽ giao nó đến cho bạn.</div>
                        <div className='l'>Mỗi thành viên sẽ được mua sắm với mức giá ưu đãi riêng tùy vào số điểm bạn tích lũy. Điểm tích lũy sẽ được cộng 30 khi chia sẻ của bạn được trên 30 lượt thích, mỗi điễm sẽ giảm tương ứng <strong>1,000VNĐ</strong> khi mua sắm (xin lưu ý là điểm này không có khả năng quy đổi ra tiền mặt).</div>

                        <div className='text-gradiant-green tt l'>Luôn luôn cải tiến và hoàn thiện sản phẩm</div>
                        <div className='l'>Chúng tôi tiến hành nghiên cứu và cộng tác với những bên khác để cải tiến sản phẩm của mình. Một trong những biện pháp của chúng tôi là phân tích dữ liệu mình có và hiểu cách mọi người sử dụng sản phẩm của mình. Chúng tôi rất vui khi được nghe ý kiến đóng góp của các bạn từ feedback@zapii.me.</div>

                        <div className='text-gradiant-green tt l'>Chính sách đổi trả hàng</div>
                        <div className='l'>Chúng tôi hợp tác với các cửa hàng để phục vụ cho việc mua sắm trên ứng dụng, dưới đây là quy định đối với các sản phẩm được phép đổi trả:</div>
                        <div style={{ marginLeft: '1.5rem' }}>
                            <ul>
                                <li>Thời gian đổi hàng trong vòng 30 ngày khi xuất hóa đơn</li>
                                <li>Không chấp nhận việc trả hàng để lấy lại tiền mặt trong bất cứ trường hợp nào</li>
                                <li>Việc đổi trả chỉ được thực hiện khi sản phẩm bị lỗi hoặc những sự cố phát sinh do lỗi từ phía Zapii</li>
                                <li>Chỉ chấp nhận việc đổi hàng khi sản phẩm chưa qua sử dụng, còn nhãn mác và hóa đơn mua hàng.</li>
                            </ul>
                        </div>
                        <div className='l'><strong>Lưu ý:</strong></div>
                        <div className='l'>Zapii không đảm bảo mặt hàng quý khách muốn đổi trả sẽ còn hàng, nếu trong trường hợp sản phẩm quý khách muốn đổi đã hết hàng, xin quý khách chọn sản phẩm khác bằng giá hoặc nếu cao hơn sẽ bù chênh lệch.</div>

                        <div className='text-gradiant-green tt l'>Chính sách bảo hành</div>
                        <div className='l'>Zapii sẽ liên hệ với các đối tác để thực hiện việc bảo hành miễn phí sản phẩm trong suốt thời gian khách hàng sử dụng.</div>
                        <div className='l'><strong>Những trường hợp Zapii từ chối bảo hành:</strong></div>
                        <div style={{ marginLeft: '1.5rem' }}>
                            <ul>
                                <li>Sản phẩm đã được sửa chữa, thay đổi không phải do Zapii thực hiện</li>
                                <li>Sản phẩm bị hư không phải do lỗi sản xuất: trầy xước, mòn đế, nóng chảy, cháy, thú vật cắn, cắt...</li>
                                <li>Sản phẩm bị hao mòn tự nhiên trong quá trình sử dụng</li>
                                <li>Sản phẩm PU tự hủy qua thời gian sử dụng, sản phẩm bị biến dạng hoặc không có thông tin bảo hành.</li>
                            </ul>
                        </div>

                        <div className='l'><strong>Quy trình bảo hành:</strong></div>
                        <div style={{ marginLeft: '1.5rem' }}>
                            <ul>
                                <li>Zapii tiếp nhận sản phẩm bảo hành tại địa chỉ : 174/33/5 Nguyễn Tư Giãn, Phường 12, Quận Gò Vấp, TPHCM</li>
                                <li>Zapii tiến hành liên hệ các bên để thực hiện bảo hành sản phẩm</li>
                                <li>Sau khi bảo hành, nhân viên Zapii sẽ liên hệ và hẹn trả sản phẩm đã bảo hành cho khách hàng.</li>
                            </ul>
                        </div>

                        <div className='text-gradiant-blue tt l'>Chúng tôi cung cấp những dịch vụ này đến bạn để thúc đẩy sứ mệnh của mình. Đổi lại, chúng tôi cần bạn thực hiện các cam kết sau:</div>
                        <div className='l'>Bạn không thể sử dụng Zapii nếu</div>
                        <div style={{ marginLeft: '1.5rem' }}>
                            <ul>
                                <li>Bạn chưa đủ 12 tuổi trở lên</li>
                                <li>Bạn từng bị kết án tội phạm tình dục</li>
                                <li>Bạn sử dụng các phương pháp tạo tài khoản giả mạo để tích điểm trên Zapii</li>
                                <li>Bạn bị cấm nhận các sản phẩm, dịch vụ hoặc phần mềm của chúng tôi theo luật áp dụng</li>
                                <li>Trước đây, chúng tôi đã vô hiệu hóa tài khoản của bạn vì vi phạm các điều khoản hoặc chính sách của chúng tôi</li>
                            </ul>
                        </div>

                        <div className='text-gradiant-red tt l'>Các quyền bạn cấp cho chúng tôi</div>
                        <div className='l'>Chúng tôi liên tục kiểm soát và loại bỏ ngay lập tức các thông tin có tính:</div>
                        <div style={{ marginLeft: '1.5rem' }}>
                            <ul>
                                <li>Xâm phạm quyền tự do cá nhân của người khác, làm nhục, phỉ báng, bôi nhọ người khác, hoặc có các hành động gây phương hại hay gây bất lợi cho người khác</li>
                                <li>Truyền bá và phân phối thông tin cá nhân của bên thứ ba mà không được sự chấp thuận của họ</li>
                                <li>Truyền đi những tập tin virus gây hư hại hoạt động của các máy tính khác</li>
                                <li>Sử dụng các loại robot, nhện máy (spiders) hoặc bất kỳ thiết bị tự động nào với mục đích theo dõi và thu thập tài liệu của website cho bất kỳ mục đích tái sử dụng mà không được sự cho phép trước bằng văn bản</li>
                                <li>Sử dụng bất kỳ thiết bị, phần mềm hoặc tiến trình nào nhằm xâm phạm hoặc cố ý xâm phạm đến hoạt động của website</li>
                                <li>Bất kỳ hành động nào không hợp pháp hoặc bị cấm bởi các bộ luật tương ứng</li>
                                <li>Bất kỳ hành động nào mà chúng tôi cho rằng không thích hợp.</li>
                            </ul>
                        </div>

                        <div className='l2'>
                            Nhằm mục đích hỗ trợ cộng đồng, chúng tôi khuyến khích bạn báo cáo nội dung hoặc hành vi mà bạn cho rằng vi phạm quyền của mình (bao gồm quyền sở hữu trí tuệ) hay các điều khoản và chính sách của chúng tôi.
                        </div>
                        <div className='l'>
                            Chúng tôi muốn Zapii trở thành nơi mà mọi người cảm thấy được chào đón và an toàn để thể hiện bản thân, cũng như chia sẻ suy nghĩ và ý tưởng của mình.
                        </div>
                        <div className='l'>
                            Nếu chúng tôi xác định bạn đã vi phạm các điều khoản hoặc chính sách của mình, chúng tôi có thể áp dụng các biện pháp đối với tài khoản của bạn để bảo vệ cộng đồng và các dịch vụ của mình, bao gồm tạm ngừng quyền truy cập vào tài khoản hoặc vô hiệu hóa tài khoản của bạn. Chúng tôi cũng có thể tạm ngừng hoặc vô hiệu hóa tài khoản của bạn nếu bạn khiến chúng tôi gặp rủi ro hoặc rắc rối liên quɑn đến pháp luật hoặc khi pháp luật cho phép hoặc yêu cầu chúng tôi thực hiện hành động này. Khi thích hợp, chúng tôi sẽ thông báo cho bạn về tài khoản vào lần tiếp theo bạn truy cập tài khoản của mình.
                        </div>

                        <div className='text-gradiant-red tt l2'>Giới hạn trách nhiệm pháp lý</div>
                        <div className='l'>
                            Chúng tôi nỗ lực cung cấp Sản phẩm tốt nhất có thể và định ra các nguyên tắc rõ ràng cho tất cả những người sử dụng. Tuy thường xuyên kiểm tra và cập nhật nhưng chúng tôi không đảm bảo rằng Sản phẩm luôn an toàn, bảo mật, không có lỗi hoặc Sản phẩm sẽ hoạt động mà không bị gián đoạn, chậm trễ hoặc thiếu sót. Trong phạm vi luật pháp cho phép, chúng tôi cũng TỪ CHỐI MỌI BẢO ĐẢM VỀ QUYỀN SỞ HỮU VÀ KHÔNG VI PHẠM. Chúng tôi không kiểm soát hoặc chi phối hành động hay phát ngôn của mọi người và những đối tượng khác. Chúng tôi cũng không chịu trách nhiệm về hành động hoặc cách ứng xử của họ hoặc mọi nội dung mà họ chia sẻ (bao gồm nội dung phản cảm, không phù hợp, tục tĩu, bất hợp pháp và các nội dung có thể bị phản đối khác).
                        </div>
                        <div className='l'>
                            Chúng tôi không thể dự đoán thời điểm phát sinh sự cố về Sản phẩm của mình. Vì vậy, trách nhiệm pháp lý của chúng tôi chỉ giới hạn trong phạm vi tối đa mà luật áp dụng cho phép và trong mọi trường hợp, chúng tôi sẽ không chịu trách nhiệm với bạn về bất kỳ tổn thất nào về lợi nhuận, doanh thu, thông tin, dữ liệu hoặc về thiệt hại mang tính hệ quả, đặc biệt, gián tiếp, răn đe, trừng phạt hoặc ngẫu nhiên phát sinh từ hoặc liên quan đến các Điều khoản này hoặc Sản phẩm của Zapii, ngay cả khi chúng tôi đã được thông báo về khả năng xảy ra các thiệt hại đó.
                        </div>

                    </div>
                </div>


            </div>

            <div className='row'>
                <div className='col-md-6 zcontact'>
                    <div className='it'>
                        Cập nhật sau cùng ngày 14/05/2020
                    </div>
                    <div className='it'>
                        <a href="feedback@zapii.me">feedback@zapii.me</a>
                    </div>
                    <div className='it'>
                        ©Zapii.me {new Date().getFullYear()}
                    </div>

                </div>
            </div>
        </div>
    )
}

export default Comp;