import React, { useContext } from 'react';
import '../constants/stylesheet.css';
import R from './../../../locale/R';
import LogoutIcon from '../../Common/Icon/LogoutIcon';
import { signOut } from './../../../utils/AuthUtils'
import { RouteContext } from '../../../routes/routes';

const AccountContainer = () => {
    const [user, setUser] = useContext(RouteContext);
    return (
        <div>
            <div className="container-fluid">

                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">{R.account.title}</h1>
                </div>

                <div className="row new">
                    <div className="col-xl-6 col-lg-6 title">
                        Name
                        </div>
                </div>
                <div className="row">
                    <div className="col-xl-6 col-lg-6 value">
                        {user.displayName}
                    </div>
                </div>

                <div className="row new">
                    <div className="col-xl-6 col-lg-6 title">
                        Email
                        </div>
                </div>
                <div className="row">
                    <div className="col-xl-6 col-lg-6 value">
                        {user.email}
                    </div>
                </div>

                <div className="row new">
                    <div className="col-xl-6 col-lg-6 title">
                        Account type
                        </div>
                </div>
                <div className="row">
                    <div className="col-xl-6 col-lg-6 value">
                        <button className="btn btn-primary btn-sm">Vip</button>
                    </div>
                </div>

                <div className="row end">
                    <div className="col-xl-6 col-lg-6 value">
                        <span onClick={signOut} className="button disable-selection">
                            <LogoutIcon color='#224abe' width={18} height={18} />
                            <span style={{ paddingLeft: '0.25rem' }}>Log out</span>
                        </span>
                    </div>
                </div>


            </div>

        </div>
    );
}

export default AccountContainer;
