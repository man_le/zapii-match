
import React from 'react';
import CheckboxOutlinedIcon from '../Common/Icon/CheckboxOutlinedIcon';
import R from '../../locale/R';
import './stylesheet.scss';

const InBagOrderCompletePanel = ({globalState, onBack})=>{
    return (
        <div className='order-complete-panel'>
            <div onClick={onBack} className='gr'>
                <CheckboxOutlinedIcon width='60' height='60'/>
                <div className='it'>
                   {R.bag.success}
                </div>
                <div className='it'>
                    {R.bag.order_id} <span>#{globalState.order_id}</span>
                </div>
            </div>
            
        </div>
    )
}
export default InBagOrderCompletePanel;