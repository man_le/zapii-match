import React from 'react';
import './stylesheet.scss';

const ProgressBar = ({ percent }) => {

    return (
        <div className='progressbar-group'>
            <div className='i-txt-g zprogress-bar'>
                <div className='blackbg' style={{ '--percent': `${percent}%` }}></div>
                <div className='whitebg'></div>
                <div className='makered' style={{ '--percent': `${percent}%` }}></div>
                <div className='ztext'>
                    <div className='t'>
                        {percent + '%'}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProgressBar;
