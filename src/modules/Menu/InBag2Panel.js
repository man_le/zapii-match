import { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useContext, useEffect, useRef, useState } from 'react';
import * as firebase from "firebase/app";
import CurrencyFormat from 'react-currency-format';
import format from 'string-template';
import R from '../../locale/R';
import type from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import { GlobalStateContext } from '../Layout/HomeLayout';
import LoadingAllPageComponent from './../Common/LoadingAllPageComponent';
import { showNotification } from './../../utils/NotificationUtils';
import InBagAddressPanel from './InBagAddressPanel';
import InBagOrderCompletePanel from './InBagOrderCompletePanel';
import './stylesheet.scss';
import CoverAllPage from '../Common/CoverAllPage';

const InBag2Panel = ({ onBack, isShow, totalPrice }) => {

    const [localState, setLocalState] = useState({
        paytype: 'cod',
        showAddressBook: false,
        addressSelected: null,
        loading: false,
        submitting: false,
        totalDiscount: 0,
        messageDiscount: null,
        isUseVouncher: false,
        showCompletePage: false,
        vouncherResult: null
    });
    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const vouncherRef = useRef(null);
    const noteRef = useRef(null);

    const closeModalHandler = () => {
        onBack();
    }

    const buildData = async () => {

        setLocalState({ ...localState, loading: true });

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {
            let addressSelected = await fireStore.find(DB.ADDRESS.name(), [DB.ADDRESS.USER_ID, CON_TYPE.EQ, user.extData[0]._id], ['isDefault', CON_TYPE.EQ, true]);
            if (!isEmpty(addressSelected)) {
                addressSelected = addressSelected[0];
                setLocalState({ ...localState, addressSelected, loading: false });
            } else {
                setLocalState({ ...localState, addressSelected: null, loading: false });
            }
        } else {
            setLocalState({ ...localState, loading: false });
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            buildData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [
        user,
        globalState.isUpdateInBag,
        globalState.isUpdateAddress,
        globalState.order_id
    ]);

    const payTypeHandler = type => {
        setLocalState({ ...localState, paytype: type });
    }

    const editAddressHandler = () => {
        setLocalState({ ...localState, showAddressBook: true });
    }

    const onBackInAddress = () => {
        const { loading } = localState;
        if (!loading) setLocalState({ ...localState, showAddressBook: false });
    }

    const calDiscount = (vouncherObj) => {
        const { discountType, percentDiscountValue, maxDiscount } = vouncherObj
        let totalDiscount = 0;
        if (discountType === 'percent') {
            totalDiscount = ((totalPrice * percentDiscountValue) / 100);
            if (totalDiscount > maxDiscount) totalDiscount = maxDiscount;
            if (totalDiscount <= 0) totalDiscount = 0;
        } else if (discountType === 'value') {
            totalDiscount = maxDiscount;
            if (totalDiscount <= 0) totalDiscount = 0;
        }

        return totalDiscount;
    }

    const vouncherRemoveHandler = () => {
        setLocalState({ ...localState, totalDiscount: 0, messageDiscount: null, isUseVouncher: false, vouncherResult:null });
    }

    const vouncherSubmitHandler = async () => {

        const vouncher = vouncherRef.current.value;
        if (localState.loading || isEmpty(vouncher)) return;

        setLocalState({ ...localState, loading: true });

        let vouncherResult = null;

        // Vouncher from Score Accumulate
        if (vouncher.indexOf('ZA#') !== -1) {

            let scoreAcc = await fireStore.find(
                DB.SCORE_ACCUMULATE.name(), [DB.SCORE_ACCUMULATE.USER_ID, CON_TYPE.EQ, user.extData[0]._id]
            )
            if (!isEmpty(scoreAcc)) {
                let discountValue = vouncher.replace('ZA#', '');
                if (isEmpty(discountValue)) discountValue = 0;
                else discountValue = Number(discountValue);

                scoreAcc = scoreAcc[0];
                if (discountValue <= Number(scoreAcc.score)) {

                    // START - Calc discount value
                    // 0-200k: 15k
                    // 200k-500k: 25k
                    // 500k-1000k: 45k
                    // >1000k: 65k
                    if (totalPrice < 200000 && discountValue > 15) {
                        showNotification('danger', R.common.error_msg_maximum_za_code_15);
                        setLocalState({ ...localState, loading: false });
                        return;
                    } else if (totalPrice >= 200000 && totalPrice < 500000 && discountValue > 25) {
                        showNotification('danger', R.common.error_msg_maximum_za_code_25);
                        setLocalState({ ...localState, loading: false });
                        return;
                    } else if (totalPrice >= 500000 && totalPrice < 1000000 && discountValue > 45) {
                        showNotification('danger', R.common.error_msg_maximum_za_code_45);
                        setLocalState({ ...localState, loading: false });
                        return;
                    } else if (totalPrice >= 1000000 && discountValue > 65) {
                        showNotification('danger', R.common.error_msg_maximum_za_code_65);
                        setLocalState({ ...localState, loading: false });
                        return;
                    }
                    //// END - Calc discount value

                    vouncherResult = [{
                        ...scoreAcc,
                        numberValidDate: 0,
                        discountType: 'value',
                        percentDiscountValue: 0,
                        maxDiscount: discountValue * 1000,
                        isUseAccumulate: true
                    }]
                }
            }

        } else {

            // Check vouncher is used or not
            const vouncherUsedResult = await fireStore.find(
                DB.VOUNCHER_USED.name(),
                [DB.VOUNCHER_USED.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                [DB.VOUNCHER_USED.CODE, CON_TYPE.EQ, vouncher]
            )
            if (!isEmpty(vouncherUsedResult)) {
                showNotification('danger', R.bag.error_used_vouncher);
                setLocalState({ ...localState, loading: false });
                return;
            }

            // Vouncher for all
            vouncherResult = await fireStore.find(
                DB.VOUNCHER.name(),
                [DB.VOUNCHER.CODE, CON_TYPE.EQ, vouncher],
                [DB.VOUNCHER.TYPE, CON_TYPE.EQ, 'all']
            );

            // Vouncher for specific user
            if (isEmpty(vouncherResult)) {
                vouncherResult = await fireStore.find(
                    DB.VOUNCHER.name(),
                    [DB.VOUNCHER.CODE, CON_TYPE.EQ, vouncher],
                    [DB.VOUNCHER.TYPE, CON_TYPE.EQ, user.extData[0]._id]
                );
            }
        }

        if (!isEmpty(vouncherResult)) {
            vouncherResult = vouncherResult[0];

            // Check vouncher deadline
            let dateAfterAdd = moment(vouncherResult.createdDate).add(vouncherResult.numberValidDate, 'd').valueOf();
            let currentDate = moment().valueOf();

            let totalMoneyDiscount = 0;
            if (currentDate <= dateAfterAdd || vouncherResult.isUseAccumulate) {
                totalMoneyDiscount = calDiscount(vouncherResult);

                let messageDiscount = null;

                const formatter = new Intl.NumberFormat('en-US', {
                    currency: 'USD',
                    style: 'currency',
                    minimumFractionDigits: 0
                })

                if (vouncherResult.discountType === 'percent') {
                    messageDiscount = format(R.bag.apply_vouncher, { code: vouncher, percent: vouncherResult.percentDiscountValue, max: formatter.format(vouncherResult.maxDiscount).replace('$', '') + 'đ' });
                } else {
                    messageDiscount = format(R.bag.apply_vouncher2, {
                        code: vouncher,
                        max: formatter.format(vouncherResult.maxDiscount).replace('$', '') + 'đ'
                    });
                }

                setLocalState({ ...localState, vouncherResult, totalDiscount: totalMoneyDiscount, messageDiscount, isUseVouncher: true, loading: false });
            } else {
                showNotification('danger', R.bag.error_deadline_vouncher);
                setLocalState({ ...localState, loading: false });
            }
        } else {
            showNotification('danger', R.bag.error_wrong_vouncher);
            setLocalState({ ...localState, loading: false });
        }
    }

    const closeSuccessModalHandler = () => {
        setLocalState({ ...localState, isUseVouncher: false, totalDiscount: 0, messageDiscount: null, showCompletePage: false });
        vouncherRef.current.value = '';

        dispatch({ type: type.RESET_ORDER_ID });
        dispatch({ type: type.UPDATE_IN_BAG });
        dispatch({ type: type.CHANGE_ITEM_IN_BAG, itemsInBag: [] });

        window.location.replace("#");
    }

    const submitOrderHandler = async () => {

        if (isEmpty(localState.addressSelected)) {
            showNotification('danger', R.addressBook.error_no_address);
            return;
        }

        if (localState.submitting) return;

        const { addressSelected } = localState;

        setLocalState({ ...localState, submitting: true });

        const newData = {
            [DB.ORDER.STATUS]: 'pending',
            vouncher: vouncherRef.current.value,
            isCancel: false,
            [DB.ORDER.NOTE]: noteRef.current.value,
            [DB.ORDER.USER_ID]: user.extData[0]._id,
            [DB.ORDER.ORDER_ID]: globalState.order_id,
            [DB.ORDER.BEFORE_PRICE]: totalPrice,
            [DB.ORDER.ADDRESS_VALUE]: addressSelected.name + '#' + addressSelected.phone + '#' + addressSelected.address,
            [DB.ORDER.ADDRESS_ID]: addressSelected._id,
            [DB.ORDER.TOTAL_PRICE]: totalPrice - localState.totalDiscount,
            [DB.ORDER.DISCOUNT_MESSAGE]: localState.messageDiscount
        }
        const _id = await fireStore.insert(
            DB.ORDER.name(),
            newData
        );

        // Update in bag
        if (!isEmpty(_id)) {

            // Update order_id
            await fireStore.updateMultiConditions(
                DB.IN_BAG.name(),
                { [DB.IN_BAG.ORDER_ID]: _id },
                false,
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
            )

            // Update vouncher is used
            const vouncher = vouncherRef.current.value;
            if (vouncher.indexOf('ZA#') !== -1  && localState.isUseVouncher) {
                let score = vouncher.replace('ZA#', '');
                try {
                    score = Number(score) * -1;
                } catch (err) {
                    score = 0;
                }
                const newData = {
                    [DB.SCORE_ACCUMULATE.SCORE]: firebase.firestore.FieldValue.increment(score),
                }
                await fireStore.updateMultiConditions(
                    DB.SCORE_ACCUMULATE.name(),
                    newData,
                    false,
                    [DB.SCORE_ACCUMULATE.USER_ID, CON_TYPE.EQ, user.extData[0]._id]
                );
            } else {
                if (!isEmpty(localState.vouncherResult)) {
                    const vouncherUsedData = {
                        [DB.VOUNCHER_USED.CODE]: vouncher,
                        [DB.VOUNCHER_USED.USER_ID]: user.extData[0]._id,
                        [DB.VOUNCHER_USED.VOUNCHER_ID]: localState.vouncherResult._id
                    }
                    await fireStore.updateMultiConditions(
                        DB.VOUNCHER_USED.name(),
                        vouncherUsedData,
                        true,
                        [DB.VOUNCHER_USED.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                        [DB.VOUNCHER_USED.VOUNCHER_ID, CON_TYPE.EQ, localState.vouncherResult._id]
                    )
                }
            }

        }
        setLocalState({ ...localState, loading: false, showCompletePage: true, submitting: false });
    }

    const { addressSelected } = localState;

    return (
        <div className="inBag page2 pdetail" style={isShow && !isEmpty(globalState.order_id) ? { display: '' } : { display: 'none' }}>
            <div className="zheader noselect">
                <button onClick={closeModalHandler} type="button" className="close zscale-effect">
                    <i className="material-icons">keyboard_backspace</i>
                </button>
                <div className='ztitle'>
                    {R.bag.checkout}
                </div>
            </div>
            <div className="zbody">
                <div className='checkout-address-group'>
                    <div className='p2g2'>
                        <div className='lbl'>
                            {R.bag.order_id}
                        </div>
                        <div className='order_id'>
                            #{globalState.order_id}
                        </div>
                    </div>
                    <div className='p2g'>
                        <div className='lbl'>
                            {R.bag.address}
                        </div>
                        <div className='address-g'>
                            {
                                !isEmpty(localState.addressSelected)
                                    ?
                                    <div className='sel'>
                                        <div className='name-phone'>
                                            {addressSelected.name} - {addressSelected.phone}
                                        </div>
                                        <div className='address-detail'>
                                            {addressSelected.address}
                                        </div>
                                    </div>
                                    :
                                    <div className='no-data'>{R.addressBook.error_no_address}</div>
                            }

                            <div onClick={editAddressHandler} className='ig'>
                                <i className='material-icons'>edit</i>
                            </div>
                        </div>
                    </div>
                    <div className='p2g'>
                        <div className='lbl'>
                            {R.bag.checkout}
                        </div>
                        <div className='p-group'>
                            <div onClick={() => payTypeHandler('cod')} className={`pi`}>
                                <div className={`zckbox ${localState.paytype === 'cod' ? 'active' : ''}`}>
                                    <i className='material-icons'>check</i>
                                </div>
                                <div className='t'>
                                    {R.bag.cod}
                                </div>
                            </div>
                            <div onClick={() => payTypeHandler('account')} data-toggle={"modal"} data-target={`#pay_account_modal`} className={`pi`}>
                                <div className={`zckbox ${localState.paytype === 'account' ? 'active' : ''}`}>
                                    <i className='material-icons'>check</i>
                                </div>
                                <div className='t'>
                                    {R.bag.account}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='p2g'>
                        <div className='lbl'>
                            {R.bag.vouncher}
                        </div>
                        <div className='inv'>
                            <input className={localState.isUseVouncher ? 'zdisabled' : ''} disabled={localState.isUseVouncher} ref={vouncherRef} maxLength='10' type='text' placeholder={R.bag.input_code} />
                            {
                                localState.isUseVouncher
                                    ?
                                    <div onClick={vouncherRemoveHandler} className='bn'>
                                        <i className='material-icons'>close</i>
                                    </div>
                                    :
                                    <div onClick={vouncherSubmitHandler} className='bn'>
                                        <i className='material-icons'>check</i>
                                    </div>
                            }

                        </div>
                    </div>
                    <div className='p2g'>
                        <div className='lbl'>
                            {R.bag.note}
                        </div>
                        <div className='note-g'>
                            <textarea ref={noteRef} maxLength='250' placeholder={R.bag.note_placeholder} ></textarea>
                        </div>
                    </div>
                </div>

                <div className='fixed-bar' style={localState.isUseVouncher ? { height: '9rem' } : {}}>
                    <div className='total'>
                        <div className='l'>
                            {R.bag.total}
                        </div>
                        <div className='r' style={localState.isUseVouncher ? { color: '#777' } : {}}>
                            <CurrencyFormat className='complete-order-total-price' value={totalPrice} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                        </div>
                    </div>
                    <div className='total vouncher'>
                        <div className='l' style={{ fontSize: '0.95rem' }}>
                            {localState.messageDiscount}
                        </div>
                    </div>
                    <div className='total'>
                        <div className='l'>
                            {R.bag.left}
                        </div>
                        <div className='r'>
                            <CurrencyFormat value={totalPrice - localState.totalDiscount} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                        </div>
                    </div>
                    <div className='g'>
                        <div onClick={submitOrderHandler} className='ins noselect zpointer'>
                            <div className='l'>
                                {
                                    R.bag.submit
                                }
                            </div>
                            <i className='material-icons'>navigate_next</i>
                        </div>
                    </div>
                </div>

            </div>
            <InBagAddressPanel onBack={onBackInAddress} isShow={localState.showAddressBook} />
            {
                localState.showCompletePage
                &&
                <InBagOrderCompletePanel globalState={globalState} onBack={closeSuccessModalHandler} />
            }

            {
                localState.submitting && <LoadingAllPageComponent />
            }
            <CoverAllPage />
        </div>
    )
}

export default InBag2Panel;