import htmlToImage from 'html-to-image';
import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useRef, useState } from 'react';
import CurrencyFormat from 'react-currency-format';
import { Link } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import short from 'short-uuid';
import uuid from 'uuid';
import R from '../../../locale/R';
import type from '../../../reducers/type';
import { RouteContext } from '../../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../../utils/DBUtils';
import { buildProductFilterTypes } from '../../../utils/JSONUtils';
import { showNotification } from '../../../utils/NotificationUtils';
import DataIsEmpty from '../../Common/DataIsEmpty';
import ClothesIcon from '../../Common/Icon/ClothesIcon';
import ShirtIcon from '../../Common/Icon/ShirtIcon';
import ShirtOutlinedIcon from '../../Common/Icon/ShirtOutlinedIcon';
import MoreActionComponent from '../../Common/MoreActionComponent';
import { GlobalStateContext } from '../../Layout/HomeLayout';
import LeftRightContentComponent from '../../Common/LeftRightContentComponent';
import LeftComponent from './LeftComponent';
import { dragElement } from './../../../utils/ImageUtils';
import { type as storageType, uploadFile } from './../../../utils/StorageUtils';
import DesignTabProgressComponent from './DesignTabProgressComponent';
import './stylesheet.scss';


const DesignComponent = ({ history, mix_id }) => {

    const translator = short();

    let mix_id_build = mix_id;
    if (mix_id !== 'new') {
        try {
            mix_id_build = translator.toUUID(mix_id);
        } catch (err) {
            mix_id_build = 'new'
        }
    }

    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);
    const nameOfSetRef = useRef(null);

    const [localState, setLocalState] = useState({
        data: {},
        collectIndex: 1,
        loading: true,
        submitting: false,
        itemsInDesign: [],
        initItemInDesign: false,

        itemsToMix: [],

        isCreateNew: true,
        setName: null,
        setImage: null,

        initFirstTime: false,
        isActiveImgPreview: true,
        activeImgPreview: '',
        sexType: 'women',
        filterType: 'all',
        sortValue: 'createdDate',
        sortType: 'desc',
        more: false,
        lastVisible: null
    })

    const changeCollectIndex = index => {
        setLocalState({ ...localState, collectIndex: index });
    }

    const fetchData = async (sexTypeParam, filterTypeParam, sortValueParam, sortTypeParam) => {

        let { sexType, sortType, sortValue, filterType, lastVisible, itemsInDesign, itemsToMix, isCreateNew, setName, setImage } = localState;

        filterType = filterTypeParam || filterType;
        sexType = sexTypeParam || sexType;
        sortValue = sortValueParam || sortValue;
        sortType = sortTypeParam || sortType;

        if (sexTypeParam || filterTypeParam || sortValueParam) {
            itemsInDesign = [];
            lastVisible = null;
        }

        // Get clothes set info
        if (mix_id_build !== 'new') {
            let setResult = await fireStore.find(
                DB.CLOTHES_SET.name(),
                [DB.CLOTHES_SET.ID, CON_TYPE.EQ, mix_id_build]
            );

            if (!isEmpty(setResult)) {
                setResult = setResult[0];
                itemsToMix = setResult.itemsToMix;
                isCreateNew = false;
                setName = setResult.name;
                setImage = setResult.image;
            }
        } else {
            isCreateNew = true;
            setName = '';
            setImage = null;
        }

        // Get items in design
        let condition2 = null;
        if (filterType !== 'all') {
            condition2 = [DB.CLOTHES.FILTER_TYPE, CON_TYPE.EQ, filterType];
        }
        const items = await fireStore.findLimitMultiConditions(
            DB.CLOTHES.name(),
            10, lastVisible, null, sortValue, sortType,
            [DB.CLOTHES.TYPE, CON_TYPE.EQ, sexType],
            [DB.CLOTHES.OUT_OF_STOCK, CON_TYPE.EQ, false],
            condition2
        );
        let more = true;
        if (isEmpty(items) || isEmpty(items.data)) {
            more = false;
        } else {
            for (let i = 0; i < items.data.length; i++) {
                if (itemsInDesign.filter(e => e._id !== items.data[i]._id)) itemsInDesign.push(items.data[i]);
            }
        }

        setLocalState({
            ...localState,
            loading: false,
            itemsInDesign,
            itemsToMix,
            lastVisible: items.lastVisible,
            initItemInDesign: true,
            initFirstTime: true,
            sexType: sexTypeParam || sexType,
            filterType: filterTypeParam || filterType,
            sortType: sortTypeParam || sortType,
            sortValue: sortValueParam || sortValue,
            more,
            isCreateNew,
            setName,
            setImage
        });
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => {
            return loadCompleted == true;
        }
    }, [
        // user,
    ]);

    const buildTotalPrice = () => {
        let total = 0;
        const {
            itemsToMix
        } = localState;

        for (let i = 0; i < itemsToMix.length; i++) {
            if (itemsToMix[i]) {
                total += Number(itemsToMix[i].new_price);
            }
        }
        return total;
    }

    const renderFilterType = type => {
        let key = ''
        if (type === 'short') {
            if (localState.clothes_type === 'man') {
                key = 'short_man';
            } else {
                key = 'short';
            }
        } else {
            key = type;
        }
        return key;
    }

    const changeClothesTypeHandler = e => {
        fetchData(e.target.value, null, null, null);
    }

    const changeFilterTypeHandler = e => {
        fetchData(null, e.target.value, null, null);
    }

    const changeSortHandler = e => {
        const value = e.target.value;
        if (value === 'new') {
            fetchData(null, null, 'updatedDate', 'desc');
        } else if (value === 'price_az') {
            fetchData(null, null, 'new_price', 'asc');
        } else if (value === 'price_za') {
            fetchData(null, null, 'new_price', 'desc');
        }
    }

    const mixItemScrollHandler = e => {
        var d = document.getElementById('list_clothes_collect');
        if ((e.target.scrollTop + d.offsetHeight) - 66 >= e.target.scrollHeight) {
            fetchData()
        }
    }

    const addItemToMixHandler = item => {
        let { itemsToMix } = localState;
        if (isEmpty(itemsToMix.find(e => e && e._id === item._id))) itemsToMix.push(item);
        setLocalState({ ...localState, itemsToMix, isCreateNew: true });


        setTimeout(() => {
            const itemsPreview = document.getElementsByClassName('img-preview');
            for (let i = 0; i < itemsPreview.length; i++) {
                const itemsPreViewId = itemsPreview[i].id;
                dragElement(itemsPreview[i], itemsPreViewId);
            }
        }, 500);
    }

    const removeFromInMixHandler = _id => {
        let { itemsToMix } = localState;
        const index = itemsToMix.findIndex(e => e && e._id === _id);
        if (index !== -1) {
            itemsToMix[index] = null;
        }
        setLocalState({ ...localState, itemsToMix, isCreateNew: true });

        setTimeout(() => {
            const itemsPreview = document.getElementsByClassName('img-preview');
            for (let i = 0; i < itemsPreview.length; i++) {
                const itemsPreViewId = itemsPreview[i].id;
                dragElement(itemsPreview[i], itemsPreViewId);
            }
        }, 500);
    }

    const renderContent = () => {
        const { itemsInDesign, collectIndex, initItemInDesign } = localState;

        // Render Item Design
        if (collectIndex === 1) {

            let ResultComp = null;

            if (isEmpty(itemsInDesign)) {
                if (!initItemInDesign) ResultComp = (
                    <div className={`zcenter`}>
                        <BounceLoader
                            sizeUnit={"px"}
                            size={50}
                            loading={true}
                            color='#777'
                        />
                    </div>
                )
                else ResultComp = <DataIsEmpty className='zmargin_center' />
            }
            return (
                <div>
                    <div className='collect-group-search'>
                        <div className='type-group'>
                            <div className='lbl'>
                                {R.mix.sex}
                            </div>
                            <div className='val'>
                                <select name="type" onChange={changeClothesTypeHandler}>
                                    <option value="women">{R.mix.women}</option>
                                    <option value="man">{R.mix.man}</option>
                                </select>
                            </div>
                            <i className='material-icons'>chevron_right</i>
                        </div>
                        <div className='type-group'>
                            <div className='lbl'>
                                {R.mix.type}
                            </div>
                            <div className='val'>
                                <select name="filter" onChange={changeFilterTypeHandler}>
                                    {
                                        buildProductFilterTypes(localState.clothes_type).map((e, i) =>
                                            <option key={'clothes_type_' + i} value={e}>
                                                {R.product[renderFilterType(e)]}
                                            </option>
                                        )
                                    }
                                </select>
                            </div>
                            <i className='material-icons'>chevron_right</i>
                        </div>
                        <div className='type-group'>
                            <div className='lbl'>
                                {R.mix.sort}
                            </div>
                            <div className='val'>
                                <select name="sort" onChange={changeSortHandler}>
                                    <option value={'new'}>{R.discover.new}</option>
                                    <option value={'price_az'}>{R.discover.price_az}</option>
                                    <option value='price_za'>{R.discover.price_za}</option>
                                </select>
                            </div>
                            <i className='material-icons'>chevron_right</i>
                        </div>
                        <div className='sb-group'>
                            <input type='text' />
                        </div>
                    </div>

                    {
                        ResultComp !== null
                            ?
                            ResultComp
                            :
                            <div className='items-mix-result'>
                                <div className='mix-item-group'>
                                    {
                                        itemsInDesign.map(e =>
                                            <div key={'pc-clo_' + e._id} className='zit'>
                                                <div className='tb'>
                                                    <i onClick={() => addItemToMixHandler(e)} className='material-icons'>add</i>
                                                    <i onClick={() => gotoDetail(e._id)} className='material-icons s'>visibility</i>
                                                </div>
                                                <img onClick={() => addItemToMixHandler(e)} src={e.image} />
                                                <div className='price'>
                                                    {e.new_price}
                                                </div>
                                            </div>
                                        )
                                    }
                                </div>
                            </div>
                    }

                </div>
            )

        } else {
            return (
                <DesignTabProgressComponent
                    removeFromInMixHandler={removeFromInMixHandler}
                    history={history}
                    itemsInMix={localState.itemsToMix}
                />
            )
        }

    }

    const gotoURLHandler = url => {

        // Add items to global state
        const itemsInMix_bag = [];
        const itemsInMix_shirt = [];
        const itemsInMix_hat = [];
        const itemsInMix_shoes = [];
        const itemsInMix_trousers = [];

        const { itemsToMix } = localState;
        for (let i = 0; i < itemsToMix.length; i++) {
            const item = itemsToMix[i];
            if (!item) continue;
            if (item.mix_type === 'hat' && isEmpty(itemsInMix_hat.filter(e => e._id === item._id))) itemsInMix_hat.push(item);
            else if (item.mix_type === 'shirt' && isEmpty(itemsInMix_shirt.filter(e => e._id === item._id))) itemsInMix_shirt.push(item);
            else if (item.mix_type === 'trousers' && isEmpty(itemsInMix_trousers.filter(e => e._id === item._id))) itemsInMix_trousers.push(item);
            else if (item.mix_type === 'shoes' && isEmpty(itemsInMix_shoes.filter(e => e._id === item._id))) itemsInMix_shoes.push(item);
            else if (item.mix_type === 'bag' && isEmpty(itemsInMix_bag.filter(e => e._id === item._id))) itemsInMix_bag.push(item);
        }
        dispatch({
            type: type.UPDATE_ITEMS_DESIGN,
            itemsInMix_bag,
            itemsInMix_shirt,
            itemsInMix_hat,
            itemsInMix_shoes,
            itemsInMix_trousers
        });


        dispatch({ type: type.CHANGE_ROUTER, currentRouter: window.location.pathname });
        history.push(url);
    }

    const gotoDetail = _id => {
        dispatch({ type: type.CHANGE_ROUTER, currentRouter: window.location.pathname });
        history.push('/detail/product/' + translator.fromUUID(_id));
    }

    const editHandler = () => {
        setTimeout(() => {
            const itemsPreview = document.getElementsByClassName('img-preview');
            for (let i = 0; i < itemsPreview.length; i++) {
                const itemsPreViewId = itemsPreview[i].id;
                dragElement(itemsPreview[i], itemsPreViewId);
            }
        }, 5000);

        setLocalState({ ...localState, isCreateNew: true });
    }

    const saveClotherSetHandler = async () => {
        const { submitting, itemsToMix } = localState;

        if (submitting) return;
        setLocalState({ ...localState, submitting: true });

        const value = nameOfSetRef.current.value;
        if (isEmpty(value)) {
            showNotification('danger', R.mix.error_miss_name);
            setLocalState({ ...localState, submitting: false });
            return;
        }

        var node = document.getElementById('id-imgg');

        const elements = document.getElementsByClassName('img-preview');
        for (let i = 0; i < elements.length; i++) {
            elements[i].classList.remove('active');
        }

        htmlToImage.toBlob(node)
            .then(async (blob) => {

                // Build list of clothes id
                const arrayClothesId = [];
                for (let i = 0; i < itemsToMix.length; i++) {
                    const item = itemsToMix[i];
                    if (!item) continue;
                    if (arrayClothesId.indexOf(item._id) === -1) {
                        arrayClothesId.push(item._id);
                    }
                }

                const newData = {
                    name: value,
                    itemsToMix,
                    [DB.CLOTHES_SET.ITEMS]: arrayClothesId,
                    user_id: user.extData[0]._id
                }

                let url = null;
                if (mix_id_build === 'new') {

                    // Save image
                    const _id = uuid.v4();
                    url = await uploadFile(storageType.FILE + '/set/' + user.extData[0]._id, blob, `setFile_${_id}`);
                    newData.image = url;
                    newData._id = _id;

                    await fireStore.insert(DB.CLOTHES_SET.name(), newData);
                    showNotification('success', R.mix.success);

                } else {
                    // Save image
                    url = await uploadFile(storageType.FILE + '/set/' + user.extData[0]._id, blob, `setFile_${mix_id_build}`);
                    newData.image = url;

                    await fireStore.updateMultiConditions(DB.CLOTHES_SET.name(), newData, true,
                        [DB.CLOTHES_SET.ID, CON_TYPE.EQ, mix_id_build],
                        [DB.CLOTHES_SET.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                    );
                    showNotification('success', R.mix.success_up);
                }

                history.push(`/${translator.fromUUID(user.extData[0]._id)}/t/2`);
                setLocalState({ ...localState, submitting: false, itemsToMix: [] });

            })
            .catch(function (error) {
                console.error('oops, something went wrong!', error);
            });
    }

    const activeImgPreviewHandler = (idImg) => {
        setLocalState({ ...localState, isActiveImgPreview: true, activeImgPreview: idImg });
    }

    const inActiveImgPrevewHandler = (e) => {
        if (e.target.className === 'imgg') setLocalState({ ...localState, isActiveImgPreview: false });
    }

    const buildSizeItems = () => {
        return localState.itemsToMix.filter(e => e).length;
    }

    const {
    } = localState;

    return (
        <div className='zmain full design-page-group noselect'>
            <div className='row bar'>

                <div className='col-md-6 cmenu'>
                    <div className='left'>
                    
                    </div>
                    <div className='center'>
                        {R.mix.title}
                    </div>
                    <div className='right'>

                    </div>
                </div>

                <div id='id-design-page' className='col-md-6 zbor' style={{ paddingLeft: 'unset', zIndex:'9' }}>
                    <div className='gr1'>
                        <div className='ds'>
                            <div id='id-imgg' className='imgg' onClick={e => inActiveImgPrevewHandler(e)}>
                                <div style={!localState.isCreateNew ? { display: 'none' } : {}} >
                                    {
                                        localState.itemsToMix.map((e, i) =>
                                            e
                                            &&
                                            <div key={`img-${e._id}_${i}`} onClick={() => activeImgPreviewHandler(`img-${e._id}_${i}`)} id={`img-${e._id}_${i}_${mix_id}`} className={`img-preview resize poabs ${localState.isActiveImgPreview && localState.activeImgPreview === `img-${e._id}_${i}` ? 'active' : ''}`}>
                                                <div onClick={() => removeFromInMixHandler(e._id)} className='close-group'>
                                                    <i className='material-icons'>close</i>
                                                </div>
                                                <img id={`img-${e._id}_${i}_${mix_id}-` + 'move'} src={e.image} />
                                            </div>
                                        )
                                    }
                                </div>

                                <div style={localState.isCreateNew ? { display: 'none' } : {}}>
                                    <img className='viewb' src={localState.setImage} />
                                    <div onClick={editHandler} className='editViewb'>
                                        {R.mix.edit}
                                    </div>
                                </div>

                            </div>
                            <div>
                                <div className='set-total-price'>
                                    <div className='setl'>
                                        {R.mix.total}
                                    </div>
                                    <div className='setv'>
                                        <CurrencyFormat value={buildTotalPrice()} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                    </div>
                                </div>
                                {
                                    buildTotalPrice() > 0
                                    &&
                                    <div className='name-mix-group'>
                                        <input ref={nameOfSetRef} type='text' className='txt' placeholder={R.mix.save} defaultValue={localState.setName} />
                                        <button disabled={localState.submitting} onClick={saveClotherSetHandler} type='button'>
                                            <i className='material-icons zscale-effect'>check</i>
                                        </button>
                                        <MoreActionComponent className='more' icon='more_vert' data={
                                            [
                                                {
                                                    label: R.mix.addAll,
                                                    onClickHandler: () => gotoURLHandler('/mix/addAll')
                                                },
                                            ]
                                        } />
                                    </div>
                                }
                            </div>
                        </div>
                        <div id='list_clothes_collect' className='collect' style={{ height: window.innerHeight - 150 }}>
                            {
                                <div className='mix-bar'>
                                    <div onClick={() => changeCollectIndex(0)} className={`ml zscale-effect2 ${localState.collectIndex === 0 ? 'active' : ''}`}>
                                        {
                                            localState.collectIndex === 0
                                                ?
                                                <ShirtIcon width='23' height='23' style={{ marginTop: '-0.15rem' }} />
                                                :
                                                <ShirtOutlinedIcon width='23' height='23' style={{ marginTop: '-0.15rem' }} />
                                        }
                                        {
                                            localState.collectIndex === 0
                                            &&
                                            <div className='t'>
                                                {`${R.mix.title2} (${buildSizeItems()})`}
                                            </div>
                                        }
                                    </div>
                                    <div onClick={() => changeCollectIndex(1)} className={`ml zscale-effect2 ${localState.collectIndex === 1 ? 'active' : ''}`}>
                                        <ClothesIcon isSelected={localState.collectIndex === 1 ? true : false} width='25' height='25' style={{ marginTop: '-0.15rem' }} />
                                        {
                                            localState.collectIndex === 1
                                            &&
                                            <div className='t'>
                                                {`${R.mix.collect} (${localState.itemsInDesign ? localState.itemsInDesign.length : 0})`}
                                            </div>
                                        }
                                    </div>
                                </div>
                            }
                            <div className='mix-content' onScroll={mixItemScrollHandler} style={{ height: window.innerHeight - 200 }}>
                                {renderContent()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <LeftRightContentComponent
                LeftComponent={<LeftComponent changeStatusType={()=>{}} statusType={1} />}
            />

        </div>
    )
}

export default DesignComponent;