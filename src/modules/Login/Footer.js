import cookies from 'js-cookie';
import { isEmpty } from 'lodash';
import React from 'react';
import { Link } from 'react-router-dom';
import R from './../../locale/R';
import './stylesheet.scss';

const changeLang = (lang) => {
    cookies.set('z_lang', lang);
    window.location.reload();
}

const renderLangLabel = () => {
    let lang = cookies.get('z_lang');
    if (isEmpty(lang)) {
        return R.access.select_lang;
    } else if (lang === 'vi') {
        return R.access.vi;
    } else {
        return R.access.en;
    }
}

const Footer = () => {
    return (
        <>
            <div className='login-footer pc'>

                <Link to='/about' className='zintro'>
                    {R.access.intro}
                </Link>

                <Link to='/terms' className='zintro'>
                    {R.access.term}
                </Link>

                <Link to='/privacy' className='zintro'>
                    {R.access.policy}
                </Link>

                <Link to='/qa' className='zintro'>
                    {R.access.qa}
                </Link>

                <div className="btn-group select-lang item-footer">
                    <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {renderLangLabel()}
                    </span>
                    <div style={{ fontSize: '0.9rem' }} className="dropdown-menu z">
                        <span className="dropdown-item zdisabled">{R.access.en}</span>
                        <span className="dropdown-item" onClick={() => changeLang('vi')}>{R.access.vi}</span>
                    </div>
                </div>

                <div className='copyright'>
                    ©Zapii.me {new Date().getFullYear()}
            </div>
            </div>

            <div className='login-footer mobile'>

                <div className='lfi' style={{paddingBottom:'0.25rem'}}>
                    <Link to='/about' className='zintro'>
                        {R.access.intro}
                    </Link>
                    <div className="btn-group select-lang item-footer">
                        <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {renderLangLabel()}
                        </span>
                        <div style={{ fontSize: '0.9rem' }} className="dropdown-menu z">
                            <span className="dropdown-item zdisabled">{R.access.en}</span>
                            <span className="dropdown-item" onClick={() => changeLang('vi')}>{R.access.vi}</span>
                        </div>
                    </div>
                </div>

                <div className='lfi'>
                    <Link to='/terms' className='zintro'>
                        {R.access.term}
                    </Link>

                    <Link to='/privacy' className='zintro'>
                        {R.access.policy}
                    </Link>
                </div>
                <div className='lfi'>
                    <Link to='/qa' className='qa'>
                        {R.access.qa}
                    </Link>
                </div>
                <div className='lfi'>
                    <div className='copyright'>
                        ©Zapii.me {new Date().getFullYear()}
                    </div>
                </div>
            </div>
        </>
    )
}

export default Footer;