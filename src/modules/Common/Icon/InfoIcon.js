import React from 'react';
const InfoIcon = () => {
    return (
        <svg width="32px" height="32px" viewBox="0 0 64 64" enable-background="new 0 0 64 64">
   <path fill="none" stroke="#a6a6a6" stroke-width="3" stroke-miterlimit="10" d="M53.92,10.081c12.107,12.105,12.107,31.732,0,43.838
       c-12.106,12.108-31.734,12.108-43.84,0c-12.107-12.105-12.107-31.732,0-43.838C22.186-2.027,41.813-2.027,53.92,10.081z"/>
   <line stroke="#a6a6a6" stroke-width="3" stroke-miterlimit="10" x1="32" y1="47" x2="32" y2="25"/>
   <line stroke="#a6a6a6" stroke-width="3" stroke-miterlimit="10" x1="32" y1="21" x2="32" y2="17"/>
   </svg>
    )
}

export default InfoIcon;
