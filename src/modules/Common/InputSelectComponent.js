import React, { useState, useEffect } from 'react';
import Select from 'react-select'

const InputSelectComponent = ({ value, options, className, isSearchable, placeholder, onChange  }) => {

    // TEST OPTIONS
    const options1 = [
        { value: 'chocolate', label: <div className='g'><div className='icon'>😜</div><div className='t'>XXX</div></div> },
        { value: 'strawberry', label: 'Strawberry' },
        { value: 'vanilla', label: 'Vanilla' }
    ]

    const colourStyles = {
        control: styles => ({ ...styles, backgroundColor: 'white' }),
        option: (styles, { data, isDisabled, isFocused, isSelected }) => {
            return {
                ...styles,
                cursor: 'cursor',
                backgroundColor: isSelected ? '#262626' : '#fff',
                marginBottom:'0.15rem',

                ':hover': {
                    ...styles,
                    color: '#fff',
                    cursor: 'pointer',
                    backgroundColor: '#262626'
                },
            };
        },
    };

    return (
        <Select
            className= {className && className}
            placeholder={placeholder}
            options={options}
            isSearchable={ isSearchable || false }
            theme={theme => ({
                ...theme,
                colors: {
                    ...theme.colors,
                    primary25: '#fff',
                    primary: '#ccc',
                }
            })}
            styles={colourStyles}
            onChange={onChange}
            value={options.filter(e=>e.value===value)}
        />
    )
}

export default InputSelectComponent;