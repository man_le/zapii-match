import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/storage";
import {isEmpty} from 'lodash';

var storageRef = firebase.storage().ref();

/**
 * @param {*} type 
 * @param {*} base64 
 * @param {*} name 
 */
const upload = async (type, base64, name)=>{
    let url = null;
    try{
        const ref = await storageRef.child(`${type}/${name}`);
        const doc = await ref.putString(base64, 'data_url');
        if(doc.state === 'success'){
            url = await ref.getDownloadURL()
        }
    }catch(err){
        console.error(err);
    }
    return url;
}

/**
 * @param {*} type 
 * @param {*} base64 
 * @param {*} name 
 */
const uploadFile = async (type, file, name)=>{
    let url = null;
    try{
        const ref = await storageRef.child(`${type}/${name}`);
        const doc = await ref.put(file);
        if(doc.state === 'success'){
            url = await ref.getDownloadURL()
        }
    }catch(err){
        console.error(err);
    }
    return url;
}

/**
 * @param {*} type 
 * @param {*} base64 
 * @param {*} name 
 */
const deleteFile = async (type, name)=>{
    let success = false;
    try{
        const ref = await storageRef.child(`${type}/${name}`);
        const doc = await ref.delete();
        success = true;
    }catch(err){
        console.error(err);
    }
    return success;
}

const type = {
    'AVATAR':'avatar',
    'COVER':'cover',
    'FILE': 'file',
}

export {
    upload,
    uploadFile,
    deleteFile,
    type
}