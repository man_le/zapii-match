import React from 'react';

const TabComponent = ({ tabTitles, tabContents }) => {
    return (
        <div className="ztab-group">
            <ul className="nav nav-pills nav-pills-icons nav-pills-danger" role="tablist">
                {
                    tabTitles.map(e => e)
                }
            </ul>
            <div className="tab-content tab-space">
                {
                    tabContents.map(e=>e)
                }
            </div>
        </div>
    )
}

export default TabComponent;