import React from 'react';
import Resizer from 'react-image-file-resizer';

const readURL = (input) => {
  if (input.target.files && input.target.files[0]) {

    // Resize image
    let file = input.target.files[0];
    Resizer.imageFileResizer(
      file,
      300,
      300,
      'JPEG',
      100,
      0,
      uri => {
        const reader = new FileReader();
        reader.onload = async (e) => {
          document.getElementById('base64AvatarRaw').innerHTML = uri;
          document.getElementById('imagePreview').style.backgroundImage = 'url(' + uri + ')';
        };
        reader.readAsArrayBuffer(file);
      },
      'base64'
    );
  }
}

const UploadAvatarComponent = ({ defaultUrl }) => {
  return (
    <div className="avatar-upload">
      <div className="avatar-edit">
        <input onChange={input => readURL(input)} type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
        <label htmlFor="imageUpload"></label>
      </div>
      <div className="avatar-preview">
        <div id="imagePreview" style={{ backgroundImage: `url('${defaultUrl}')` }}>
        </div>

        <div className='hidden' id='base64AvatarRaw'>
          {defaultUrl}
        </div>

      </div>
    </div>
  )
}

export default UploadAvatarComponent;