import React from 'react';
import ContentLoader from 'react-content-loader';
import './stylesheet.scss';

const TimelineItemLoader = () => {
    return (
        <div className='time-line-item-loader'>
            <ContentLoader
                speed={2}
                width={400}
                height={28}
                viewBox="0 0 400 28"
                backgroundColor="#f3f3f3"
                secondaryColor="#ecebeb"
            >
                <rect x="10" y="2" rx="3" ry="3" width="398" height="6" />
                <rect x="10" y="19" rx="3" ry="3" width="222" height="6" />
                <rect x="320" y="22" rx="0" ry="0" width="80" height="6" />
            </ContentLoader>
        </div>
    )
}

export default TimelineItemLoader;
