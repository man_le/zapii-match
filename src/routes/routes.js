import React, { useState } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import DesignComponent from '../modules/Design/DesignComponent';
import SetInBagComponent from '../modules/Design/SetInBag/SetInBagComponent';
import DetailProductByIdPanel from '../modules/Discover/DetailProductByIdPanel';
import EditAccountComponent from '../modules/EditAccount/EditAccountComponent';
import EditNotificationComponent from '../modules/EditNotification/EditNotificationComponent';
import EditProfileComponent from '../modules/EditProfile/EditProfileComponent';
import InBagCheckoutComponent from '../modules/InBag/InBagCheckoutComponent';
import InBagCountComponent from '../modules/InBag/InBagCountComponent';
import ForgotPass from '../modules/Login/ForgotPass';
import Login from '../modules/Login/Login';
import SignUp from '../modules/Login/SignUp';
import AboutComponent from '../modules/Login/Static/AboutComponent';
import PolicyComponent from '../modules/Login/Static/PolicyComponent';
import TermsComponent from '../modules/Login/Static/TermsComponent';
import QAComponent from '../modules/Login/Static/QAComponent';
import NotificationsComponent from '../modules/Notifications/NotificationsComponent';
import ProductDetailComponent from '../modules/ProductDetail/ProductDetailComponent';
import ProfilePageComponent from '../modules/ProfilePage/ProfilePageComponent';
import SettingComponent from '../modules/Setting/SettingComponent';
import LoginRoute from './LoginRoute';
import PrivateRoute from './PrivateRoute';
import TimelineByIdComponent from '../modules/Status/TimelineByIdComponent';
import OrderDetailPageComponent from '../modules/ProfilePage/ListOrderHistory/OrderDetailPageComponent';

const initState = {};

export const RouteContext = React.createContext([{}, () => { }]);

export default ({ loggedIn }) => {
  const [user, setUser] = useState(initState);

  return (
    <RouteContext.Provider value={[user, setUser]}>
      <Router>
        <Switch>
          <LoginRoute loggedIn={loggedIn} exact path="/login" component={Login} />
          <LoginRoute loggedIn={loggedIn} exact path="/reg" component={SignUp} />
          <LoginRoute loggedIn={loggedIn} exact path="/forgot" component={ForgotPass} />

          <LoginRoute loggedIn={loggedIn} exact path="/about" component={AboutComponent} />
          <LoginRoute loggedIn={loggedIn} exact path="/terms" component={TermsComponent} />
          <LoginRoute loggedIn={loggedIn} exact path="/privacy" component={PolicyComponent} />
          <LoginRoute loggedIn={loggedIn} exact path="/qa" component={QAComponent} />

          <PrivateRoute loggedIn={loggedIn} exact path="/_about" component={AboutComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path="/_terms" component={TermsComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path="/_privacy" component={PolicyComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path="/_qa" component={QAComponent} />
          
          <PrivateRoute loggedIn={loggedIn} exact path='/' component={null} />
          <PrivateRoute loggedIn={loggedIn} exact path='/status' component={null} />
          <PrivateRoute loggedIn={loggedIn} exact path='/set' component={null} />
          
          <PrivateRoute loggedIn={loggedIn} exact path='/:userId' component={ProfilePageComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/:userId/t/:tabId' component={ProfilePageComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/:userId/order/:orderCode' component={OrderDetailPageComponent} />
          
          <PrivateRoute loggedIn={loggedIn} exact path='/setting/profile' component={EditProfileComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/setting/account' component={EditAccountComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/setting/notification' component={EditNotificationComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/status/:status_id' component={TimelineByIdComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/notification' component={NotificationsComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/settings' component={SettingComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/discover' component={null} />

          <PrivateRoute loggedIn={loggedIn} exact path='/detail/product/:clothes_id' component={ProductDetailComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/detail_mobile/product/:clothes_id' component={DetailProductByIdPanel} />
          <PrivateRoute loggedIn={loggedIn} exact path='/bag/list' component={InBagCountComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/bag/checkout' component={InBagCheckoutComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/mix/:mix_id' component={null} />
          <PrivateRoute loggedIn={loggedIn} exact path='/_mix/:mix_id' component={DesignComponent} />
          <PrivateRoute loggedIn={loggedIn} exact path='/mix/addAll' component={SetInBagComponent} />

        </Switch>
      </Router>
    </RouteContext.Provider>
  );
}