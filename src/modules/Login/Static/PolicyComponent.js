import React from 'react';
import { Link } from 'react-router-dom';
import LogoImg from './img/zapiilogo.png';
import './stylesheet_terms.scss';

const Comp = () => {
    return (
        <div className='container terms-group'>
            <div className='row'>
                <div className='col-md-6'>
                    <div className='ztop'>
                        <img className='top-logo' src={LogoImg} />
                        {
                            window.location.pathname.indexOf('_privacy') !== -1
                                ?
                                <Link to='/' className='top-link'>
                                    Trang Chủ
                                </Link>
                                :
                                <Link to='/login' className='top-link'>
                                    Đăng nhập
                                </Link>
                        }
                    </div>
                    <div className='zdetail'>
                        <div className='hd'>
                            Chính sách riêng tư
                        </div>
                        <div className='text-gradiant-red tt l bl'>Chúng tôi thu thập những loại thông tin nào:</div>
                        <div className='l'>Để cung cấp dịch vụ, đồng thời giúp bạn có những trải nghiệm tốt hơn với Zapii, chúng tôi phải thu thập và xử lý các thông tin về bạn, dưới đây là các thông tin mà chúng tôi sẽ thu thập:</div>

                        <div className='l'><span className='bl text-gradiant-green'>- Thông tin mà bạn cung cấp:</span> Chúng tôi thu thập nội dung, thông tin liên lạc và các thông tin khác mà bạn cung cấp khi sử dụng Sản phẩm của chúng tôi, bao gồm các thông tin khi bạn đăng ký một tài khoản, tạo hoặc chia sẻ nội dung với người khác.</div>
                        <div className='l'><span className='bl text-gradiant-green'>- Sự kết nối:</span> Chúng tôi thu thập thông tin về những người, trang, tài khoản, hashtag và nhóm mà bạn kết nối cũng như cách bạn tương tác với họ trên sản phẩm của chúng tôi, chẳng hạn như những người bạn liên lạc nhiều nhất hoặc các nhóm bạn tham gia.</div>
                        <div className='l'><span className='bl text-gradiant-green'>- Cách sử dụng của bạn:</span> Chúng tôi thu thập thông tin về cách bạn sử dụng Sản phẩm của chúng tôi, chẳng hạn như loại nội dung bạn xem hoặc tương tác; các tính năng bạn sử dụng; hành động bạn thực hiện; những người hoặc tài khoản bạn tương tác; và thời gian, tần suất cũng như khoảng thời gian hoạt động của bạn.</div>
                        <div className='l'><span className='bl text-gradiant-green'>- Thông tin về thiết bị:</span> Chúng tôi thu thập thông tin về thiết bị mà bạn dùng để sử dụng Zapii, các thông tin này bao gồm:</div>
                        <div style={{ marginLeft: '1.5rem' }}>
                            <ul>
                                <li><strong>Các thuộc tính thiết bị:</strong> những thông tin như hệ điều hành, phiên bản phần cứng và phần mềm, mức pin, cường độ tín hiệu, dung lượng bộ nhớ trống, plugin, loại trình duyệt, tên cũng như loại của tệp và ứng dụng.</li>
                                <li><strong>Hoạt động trên thiết bị:</strong> thông tin về các hoạt động và hành vi được thực hiện trên thiết bị, chẳng hạn như liệu một cửa sổ ở nền trước hay nền sau hoặc các hoạt động của chuột (thông tin này có thể giúp phân biệt người thật với các bot).
                                </li>
                                <li><strong>Tín hiệu thiết bị:</strong> Tín hiệu Bluetooth và thông tin về các cột phát sóng, đèn hiệu và điểm truy cập Wi-Fi ở gần.</li>
                                <li><strong>Dữ liệu từ cài đặt thiết bị:</strong> thông tin bạn cho phép chúng tôi nhận thông qua cài đặt thiết bị mà bạn bật, chẳng hạn như quyền truy cập vào vị trí GPS, máy ảnh hoặc ảnh của bạn.</li>
                                <li><strong>Mạng và kết nối:</strong> thông tin như tên của nhà mạng di động hoặc ISP, ngôn ngữ, múi giờ, số điện thoại di động, địa chỉ IP, tốc độ kết nối và trong một số trường hợp là thông tin về các thiết bị khác ở gần hoặc trên mạng của bạn, để chúng tôi có thể thực hiện những việc như giúp bạn truyền video từ điện thoại đến TV.</li>
                                <li><strong>Dữ liệu cookie:</strong> dữ liệu từ các cookie được lưu trữ trên thiết bị của bạn, bao gồm cả cài đặt và ID cookie.</li>
                            </ul>
                        </div>

                        <div className='text-gradiant-red tt l bl'>Chúng tôi sử dụng thông tin này như thế nào:</div>
                        <div className='l'>Chúng tôi sử dụng thông tin này để cung cấp và hỗ trợ bạn trong quá trình sử dụng sản phẩm. Dưới đây là những điều chúng tôi làm  với thông tin của bạn:</div>
                        <div className='l'><span className='bl text-gradiant-green'>Cung cấp, cá nhân hóa: </span> Để tạo sản phẩm được cá nhân hóa độc nhất cho bạn và phù hợp với bạn, chúng tôi sử dụng các kết nối, tùy chọn, sở thích và hoạt động của bạn dựa trên dữ liệu chúng tôi thu thập và tìm hiểu từ bạn cùng những người khác; cách bạn sử dụng và tương tác với Sản phẩm của chúng tôi; những người, địa điểm hoặc nội dung bạn đã kết nối và quan tâm.</div>
                        <div className='l'><span className='bl text-gradiant-green'>Nghiên cứu và phát triển sản phẩm: </span> Chúng tôi sử dụng thông tin mình có để phát triển, thử nghiệm và cải thiện Sản phẩm, chẳng hạn như bằng cách tiến hành khảo sát và nghiên cứu, cũng như thử nghiệm và khắc phục sự cố các tính năng, sản phẩm mới.</div>
                        <div className='l'><span className='bl text-gradiant-green'>Quảng cáo:</span> Chúng tôi sử dụng những thông tin đã có về bạn, bao gồm thông tin về sở thích, hành động và kết nối, để lựa chọn và cá nhân hóa quảng cáo, ưu đãi cũng như nội dung được tài trợ khác mà chúng tôi hiển thị cho bạn.</div>
                        <div className='l'><span className='bl text-gradiant-green'>Cải thiện độ an toàn, tính toàn vẹn và bảo mật: </span> Chúng tôi sử dụng thông tin mình có để xác minh tài khoản và hoạt động, chống hành vi có hại, phát hiện và ngăn chặn spam cũng như các trải nghiệm tiêu cực khác, duy trì tính toàn vẹn, đồng thời cải thiện độ an toàn và tính bảo mật cho Zapii.</div>
                        <div className='l'><span className='bl text-gradiant-green'>Liên lạc với bạn: </span> Chúng tôi sử dụng thông tin mình có để gửi cho bạn các thông tin tiếp thị, thông báo cho bạn về Sản phẩm của chúng tôi, cũng như cho bạn biết về các chính sách và điều khoản của chúng tôi. Chúng tôi cũng sử dụng thông tin của bạn để trả lời bạn khi bạn liên hệ với chúng tôi.</div>

                    </div>
                </div>
            </div>


            <div className='row'>
                <div className='col-md-6 zcontact'>
                    <div className='it'>
                        Cập nhật sau cùng ngày 14/05/2020
                    </div>
                    <div className='it'>
                        <a href="feedback@zapii.me">feedback@zapii.me</a>
                    </div>
                    <div className='it'>
                        ©Zapii.me {new Date().getFullYear()}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Comp;