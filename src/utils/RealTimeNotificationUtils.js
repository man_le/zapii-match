import { isEmpty } from 'lodash';
import React from 'react';
import format from 'string-template';
import UserItem from "../modules/Common/UserItem";
import type from '../reducers/type';
import R from './../locale/R';
import { CON_TYPE, DB, fireStore } from './DBUtils';
import { showNotification } from './NotificationUtils';

export const buildLikeStatusNotification = async (quiz, loggedUser, store, dispatch, total_notification) => {
    const { innerWidth: width, innerHeight: height } = window;

    // You like
    if (loggedUser._id === quiz.user_id) return;

    let userInfo = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, quiz.user_id]);

    if (width > 768 && !isEmpty(userInfo)) {
        userInfo = userInfo[0];

        showNotification('success',
            <UserItem
                userData={{ ...userInfo, label: R.notification.like_status }}
                description={R.common.now2}
                content_id={quiz._id}
                hideActionMenu={true}
                extClass='user-item-notify'
            />,
            true)
    }

    dispatch({ type: type.INCREASE_TOTAL_NOTIFICATION, total_notification });
}

export const buildLikeCommentNotification = async (commentObj, loggedUser, store, dispatch, total_notification) => {
    const { innerWidth: width, innerHeight: height } = window;

    // If you like yourself
    if (commentObj.user_id === loggedUser._id) return;

    let userInfo = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, commentObj.user_id]);

    if (width > 768 && !isEmpty(userInfo)) {
        userInfo = userInfo[0];

        showNotification('success',
            <UserItem
                userData={{ ...userInfo, label: R.notification.like_comment }}
                description={R.common.now2}
                content_id={commentObj.status_id}
                hideActionMenu={true}
                extClass='user-item-notify'
            />,
            true)
    }

    dispatch({ type: type.INCREASE_TOTAL_NOTIFICATION, total_notification });
}

export const buildCommentNotification = async (commentObj, store, loggedUser, dispatch, total_notification) => {
    const { innerWidth: width, innerHeight: height } = window;

    if (loggedUser._id === commentObj.user_id) return;

    // If comment is reply to you OR comment from the status created by you -- Handle
    if (
        (
            commentObj.reply_user_id !== commentObj.user_id &&
            commentObj.reply_user_id === loggedUser._id
        )
        ||
        (
            commentObj.status_owner === loggedUser._id
        )
    ) {

        // Reply for me
        let isReply = false;
        if (commentObj.reply_user_id !== commentObj.user_id &&
            commentObj.reply_user_id === loggedUser._id) {
            isReply = true;
        }

        let userInfo = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, commentObj.user_id]);

        if (width > 768 && !isEmpty(userInfo)) {
            userInfo = userInfo[0];
            showNotification('success',
                <UserItem
                    userData={{ ...userInfo, label: !isReply ? R.notification.comment_quiz : R.notification.reply_comment }}
                    description={R.common.now2}
                    content_id={commentObj.status_id}
                    hideActionMenu={true}
                    extClass='user-item-notify'
                />,
                true)
        }
        dispatch({ type: type.INCREASE_TOTAL_NOTIFICATION, total_notification });
    }

}

export const buildFollowingNotification = async (value, loggedUser, store, dispatch, total_notification) => {
    const { innerWidth: width, innerHeight: height } = window;

    if (value.userid_From === loggedUser._id) return;

    let userInfo = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, value.userid_From]);

    if (width > 768 && !isEmpty(userInfo)) {
        userInfo = userInfo[0];

        showNotification('success',
            <UserItem
                userData={{ ...userInfo, label: R.notification.following }}
                description={R.common.now2}
                content_id={value.userid_From}
                hideActionMenu={true}
                extClass='user-item-notify'
                type='following'
            />,
            true)
    }
    dispatch({ type: type.INCREASE_TOTAL_NOTIFICATION, total_notification });
}

export const buildShareStatusNotification = async (status, loggedUser, store, dispatch, total_notification) => {
    const { innerWidth: width, innerHeight: height } = window;

    // You like
    if (loggedUser._id === status.user_id) return;

    let userInfo = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, status.user_id]);
    if (width > 768 && !isEmpty(userInfo)) {
        userInfo = userInfo[0];

        showNotification('success',
            <UserItem
                userData={{ ...userInfo, label: R.notification.share_status }}
                description={R.common.now2}
                content_id={status._id}
                hideActionMenu={true}
                extClass='user-item-notify'
            />,
            true);
    }
    dispatch({ type: type.INCREASE_TOTAL_NOTIFICATION, total_notification });
}

export const buildScoreStatusNotification = async (status, loggedUser, store, dispatch, total_notification) => {
    const { innerWidth: width, innerHeight: height } = window;

    let userInfo = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, status.user_id]);

    if (width > 768 && !isEmpty(userInfo)) {
        showNotification('success', format(R.notification.addScore, { score: 1 }));
    }

    dispatch({ type: type.INCREASE_TOTAL_NOTIFICATION, total_notification });
}

export const buildOrderStatusNotification = async (status, loggedUser, dispatch, total_notification) => {
    const { innerWidth: width, innerHeight: height } = window;

    let userInfo = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, status.user_id]);

    if (width > 768 && !isEmpty(userInfo)) {
        switch (status.statusValue) {
            case 'received': showNotification('success', format(R.notification.receivedOrder, { orderCode: status.order_code }));
                break;
            case 'sending': showNotification('success', format(R.notification.sendingOrder, { orderCode: status.order_code }));
                break;
            case 'sent': showNotification('success', format(R.notification.sentOrder, { orderCode: status.order_code }));
                break;
            case 'cancel': showNotification('success', format(R.notification.cancelOrder, { orderCode: status.order_code }));
                break;
        }
    }

    dispatch({ type: type.INCREASE_TOTAL_NOTIFICATION, total_notification });
}