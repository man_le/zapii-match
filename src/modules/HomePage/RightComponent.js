import React, { useEffect, useState, useContext } from 'react';
import { isEmpty } from 'lodash';
import { Link, withRouter } from 'react-router-dom';
import type from '../../reducers/type';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import { GlobalStateContext } from '../Layout/HomeLayout';

const Comp = ({history}) => {

    const [localState, setLocalState] = useState({ ads: [] });
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const fetchData = async () => {
        if(isEmpty(globalState.ads)){
            const result = await fireStore.findLimitStatic(
                DB.ADS.name(), 1000, 
                [DB.ADS.CREATED_DATE, CON_TYPE.GR, 0],
                [DB.ADS.SHOW, CON_TYPE.EQ, true],
                );
            if (!isEmpty(result)) {
                dispatch({type:type.BUILD_ADS, ads: result});
                setLocalState({ ...localState, ads: result });
            }
        }else{
            setLocalState({...localState, ads: globalState.ads});
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, []);

    const gotoHandler = e=>{
        if(e.isZapiiLink){
            history.push(e.link);
        }else{
            window.open(e.link, '_blank');
        }
    }

    return (
        <div className='right-content'>
            {
                localState.ads.map(e =>
                    <img onClick={()=>gotoHandler(e)} key={e._id} className='ads' src={e.image} />
                )
            }
            
        </div>
    )
}

export default withRouter(Comp);
