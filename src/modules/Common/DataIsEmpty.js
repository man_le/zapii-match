import React from 'react';
import R from './../../locale/R';

const DataIsEmpty = ({ className, hideIcon, label, color}) => {
    return (
        <div style={color?{color}:{}} className={`data-is-empty ${className && className}`}>
            {!hideIcon && <i className='material-icons'>error</i>}
            <div className='t'>{!label ? R.common.no_data : label}</div>
        </div>
    )
}

export default DataIsEmpty;