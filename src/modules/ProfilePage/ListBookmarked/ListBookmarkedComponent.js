import React, { useEffect, useState, useContext } from 'react';
import { BounceLoader } from 'react-spinners';
import { Link, withRouter } from 'react-router-dom';
import CurrencyFormat from 'react-currency-format';
import short from 'short-uuid';
import { GlobalStateContext } from '../../Layout/HomeLayout';
import InfiniteScroll from "react-infinite-scroll-component";
import { RouteContext } from '../../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../../utils/DBUtils';
import type from '../../../reducers/type';
import './stylesheet.scss';
import { isEmpty } from 'lodash';
import DataIsEmpty from '../../Common/DataIsEmpty';
import SetClotherIcon from '../../Common/Icon/SetClotherIcon';
import MoreActionComponent from '../../Common/MoreActionComponent';
import R from '../../../locale/R';
import BookmarkIcon from '../../Common/Icon/BookmarkIcon';

const SetClotherComponent = ({ history }) => {

    const [localState, setLocalState] = useState({
        data: [],
        lastVisible: null,
        init: false,
        loading: false
    });

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);

    const fetchData = async () => {
        const { data, lastVisible } = localState;

        setLocalState({ ...localState, loading: true });

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {
            const userInfo = user.extData[0];

            const result = await fireStore.findLimit(
                DB.BOOKMARK.name(),
                [DB.BOOKMARK.USER_ID, CON_TYPE.EQ, userInfo._id],
                DB.BOOKMARK.CREATED_DATE,
                6,
                lastVisible,
                null
            );

            let more = true;
            if (isEmpty(result) || isEmpty(result.data)) {
                more = false;
            } else if (!isEmpty(result) && !isEmpty(result.data)) {
                result.data.forEach(e => {
                    if (isEmpty(data.filter(ei => ei._id === e._id))) data.push(e);
                })
            }

            setLocalState({
                ...localState,
                lastVisible: result ? result.lastVisible : null,
                data,
                more,
                init: true,
                loading: false
            });
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [user]);

    const fetchMoreData = () => {
        fetchData();
    }

    const gotoURL = _id => {
        dispatch({ type: type.CHANGE_ROUTER, currentRouter: '/me' });
        const w = window.innerWidth;
        if (w <= 991) {
            history.push('/detail_mobile/product/' + short().fromUUID(_id));
        } else {
            history.push('/detail/product/' + short().fromUUID(_id));
        }
    }

    const deleteHandler = async _id => {
        let { data } = localState;

        await fireStore.remove(DB.BOOKMARK.name(), [DB.BOOKMARK.ID, CON_TYPE.EQ, _id]);
        data = data.filter(e => e._id !== _id);

        setLocalState({ ...localState, data })
    }

    const { data, init, more } = localState;

    if (isEmpty(data)) {

        if (!init) return (
            <div className={`zcenter`} style={{ marginTop: '0.75rem' }}>
                <BounceLoader
                    sizeUnit={"px"}
                    size={50}
                    loading={true}
                    color='#777'
                />
            </div>
        )
        else return (
            <div className='set-bookmark-group'>
                <DataIsEmpty className='zmargin_center' />
            </div>

        )
    }

    return (

        <div className='full set-bookmark-group zmain noselect'>
            <div className='row bar'>
                <div className='col-md-6 zbor'>

                    <div className='snewg'>
                        <div className='gr'>
                            <BookmarkIcon width='22' height='22' />
                            <div className='t'>{R.profile.bookmarked}</div>
                        </div>
                    </div>

                    <InfiniteScroll
                        dataLength={data.length}
                        next={fetchMoreData}
                        hasMore={more}
                    >
                        {
                            data.map(e =>
                                <div key={`set_${e._id}`} className='ci'>
                                    <div className='ic'>
                                        <img src={e.image} />
                                    </div>
                                    <div className='cn'>
                                        <div className='na'>{e.name}</div>
                                        <div className='pr'>
                                            <CurrencyFormat value={e.price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                        </div>
                                    </div>
                                    <MoreActionComponent data={
                                        [
                                            {
                                                label: R.set.detail,
                                                onClickHandler: () => gotoURL(e.clothes_id)
                                            },
                                            {
                                                label: R.set.delete,
                                                onClickHandler: () => deleteHandler(e._id)
                                            },
                                        ]
                                    } />
                                </div>
                            )
                        }
                    </InfiniteScroll>
                </div>
            </div>
        </div>

    )
}

export default withRouter(SetClotherComponent);