import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import {isEmpty} from 'lodash';
import { isLoggedIn } from '../utils/AuthUtils';

export default ({ location, loggedIn, component: Component, ...rest }) => {

    let url = window.location.href;
    url = url.split('?continue=');
    if(url.length==2) url = url[1].replace(window.location.origin, '').replace('#','');
    if(isEmpty(url) || url.length<2) url = '/';

    return (
        <Route
            {...rest}
            render={props =>
                !isLoggedIn(loggedIn) ?  // is not authenticated
                    <Component {...props} />
                    :
                    <Redirect to={{ pathname: url, state: { from: props.location } }} />
            }
        />
    )
}
