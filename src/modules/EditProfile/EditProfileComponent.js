import { isEmpty } from 'lodash';
import React, { useContext, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import { RouteContext } from '../../routes/routes';
import TimelineItemLoader from '../Common/TimelineItemLoader';
import UploadAvatarComponent from '../Common/UploadAvatarComponent';
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from './../../locale/R';
import { CON_TYPE, DB, fireStore } from './../../utils/DBUtils';
import { type, upload } from './../../utils/StorageUtils';
import './stylesheet.scss';
import { buildKeyWords } from '../../utils/StringUtils';
import { showNotification } from '../../utils/NotificationUtils';
import UploadCoverComponent from '../Common/UploadCoverComponent';
import LeftComponent from './../EditNotification/LeftComponent';
import LeftRightContentComponent from '../Common/LeftRightContentComponent';

const saveHandler = async (param, user, setUser, localState, setLocalState) => {

    if (isEmpty(param)) return;

    let data = {
        _id: param._id,
        avatar: param.avatar,
        cover:param.cover,
        userUid: param.userUid,
        name: param.nameRef.current.value.substring(0,100),
        location: param.locationRef.current.value.substring(0,200),
        link: param.linkRef.current.value.substring(0,100),
        about: param.aboutRef.current.value.substring(0,3000),
        keywords: buildKeyWords(param.nameRef.current.value.substring(0,100))
    }

    setLocalState({ ...localState, isLoading: true });

    const avatarBase64 = document.getElementById('base64AvatarRaw').innerHTML;
    if (!isEmpty(avatarBase64) && avatarBase64.indexOf(';base64,') != -1) {
        let url = await upload(type.AVATAR, avatarBase64, `${param.userUid}.jpg`);
        if (isEmpty(url)) {
            showNotification('danger', R.setting.cannot_upload_image)
        } else {
            data = { ...data, avatar: url }
        }
    }

    const coverBase64 = document.getElementById('base64CoverRaw').innerHTML;
    if (!isEmpty(coverBase64) && coverBase64.indexOf(';base64,') != -1) {
        let url = await upload(type.COVER, coverBase64, `${param.userUid}.jpg`);
        if (isEmpty(url)) {
            showNotification('danger', R.setting.cannot_upload_image)
        } else {
            data = { ...data, cover: url }
        }
    }

    if (!isEmpty(data) && !isEmpty(data.userUid)) {
        await fireStore.update(DB.USER.name(), [DB.USER.USERUID, CON_TYPE.EQ, data.userUid], data, true);
        showNotification('success', R.setting.updated)
        user.extData = [data];
        setUser({ ...user });
    }

    setLocalState({ ...localState, isLoading: false });

}

const aboutChangeHandler = (aboutRef, setLocalState) => {
    setLocalState({ des: aboutRef.current.value });
}

const EditProfileComponent = () => {

    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const [localState, setLocalState] = useState({ isLoading: false, des: '' });

    const nameRef = useRef(null);
    const locationRef = useRef(null);
    const linkRef = useRef(null);
    const aboutRef = useRef(null);

    const data = {
        userUid: user.uid,
        nameRef,
        locationRef,
        linkRef,
        aboutRef,
    }

    let extData = {};
    if (!isEmpty(user.extData)) {
        extData = user.extData[0];
        data.avatar = extData.avatar;
        data.cover = extData.cover;
        data._id = extData._id;
    } else {
        return <TimelineItemLoader />;
    }

    return (
        <>
        <div className='container'>
            <div className='row zprofile-group'>

                <div className='col-md-6'>

                    <div className='t'>
                        {R.setting.profile}
                    </div>

                    <div className='zedit-item'>
                        <label className="bmd-label-floating">{R.setting.avatar}</label>
                        <div>
                            <UploadAvatarComponent defaultUrl={user.extData[0].avatar} />
                        </div>
                    </div>

                    <div className='zedit-item'>
                        <label className="bmd-label-floating">{R.setting.cover}</label>
                        <div>
                            <UploadCoverComponent defaultUrl={user.extData[0].cover} />
                        </div>
                    </div>

                    <div className="form-group zedit-item">
                        <label htmlFor="name" className="bmd-label-floating">{R.setting.name}</label>
                        <input maxLength={100} ref={nameRef} defaultValue={user.extData[0].name} type="text" className="form-control" id="name" />
                    </div>
                    <div className="form-group zedit-item">
                        <label htmlFor="location" className="bmd-label-floating">{R.setting.location}</label>
                        <input maxLength={200} ref={locationRef} defaultValue={user.extData[0].location} type="text" className="form-control" id="location" />
                    </div>
                    <div className="form-group zedit-item">
                        <label htmlFor="link" className="bmd-label-floating">{R.setting.link}</label>
                        <input maxLength={100} ref={linkRef} type="text" defaultValue={user.extData[0].link} className="form-control" id="link" />
                    </div>
                    <div className="form-group zedit-item">
                        <label htmlFor="des" className="bmd-label-floa]ting">{R.setting.about}</label>
                        <textarea maxLength={3000} ref={aboutRef} type="text" defaultValue={user.extData[0].about} onChange={() => aboutChangeHandler(aboutRef, setLocalState)} className="zdes form-control" name='des' id="des" />
                    </div>

                    <div className="zedit-item" style={{ display: 'flex', justifyContent: 'center' }}>
                        <Link to={globalState.currentRouter === window.location.pathname ? '/' : globalState.currentRouter}>
                            <button type='button' className="btn btn-default btn-cancel">{R.common.cancel}</button>
                        </Link>
                        <button type='button' disabled={localState.isLoading} onClick={() => saveHandler(data, user, setUser, localState, setLocalState)} className="btn btn-danger">{R.common.save}</button>
                    </div>
                </div>
            </div>
        </div>

        <LeftRightContentComponent
                LeftComponent={<LeftComponent />}
            />
        </>
    )
}

export default EditProfileComponent;