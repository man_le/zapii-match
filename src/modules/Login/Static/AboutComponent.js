import React from 'react';
import { Link } from 'react-router-dom';
import FashionImg from './img/fashion_1.png';
import LogoImg from './img/zapiilogo.png';
import ShoesImg from './img/shoes.png';
import MixImg from './img/mix.png';
import Mix2Img from './img/mix2.jpg';
import './stylesheet.scss';

const Comp = () => {
    return (
        <div className='container about-us-group'>
            <div className='row'>
                <div className='col-md-6'>
                    <div className='ztop'>
                        <img className='top-cover' src={FashionImg} />
                        <img className='top-logo' src={LogoImg} />
                        {
                            window.location.pathname.indexOf('_about') !== -1
                                ?
                                <Link to='/' className='top-link'>
                                    Trang Chủ
                                </Link>
                                :
                                <Link to='/login' className='top-link'>
                                    Đăng nhập
                                </Link>
                        }
                    </div>
                    <div className='zdetail'>
                        Chúng tôi mang đến cho bạn một không gian, nơi bạn có thể tự tin <span className='text-gradiant-red'>chia sẻ cá tính của bản thân thông qua gu thời trang.</span>
                    </div>
                </div>
            </div>

            <div className='row'>
                <div className='col-md-6 md1'>
                    <div className='zgr1'>
                        <div className='zdt1'>
                            Bạn cũng có thể <span className='text-gradiant-green'>chia sẻ những cảm nghĩ của mình</span> về các chủ đề trong cuộc sống, như là reivew một món ăn, cảm nhận về một địa điểm du lịch,...
                        </div>
                        <div>
                            <img className='top-cover' src={ShoesImg} />
                        </div>
                    </div>
                </div>
            </div>

            <div className='row'>
                <div className='col-md-6 md2'>
                    <div className='zgr2'>
                        <div>
                            <img className='top-cover2' src={MixImg} />
                        </div>
                        <div className='zdt2'>
                            Chúng tôi cũng <span className='text-gradiant-blue'>hỗ trợ mua sắm với giá ưu đãi riêng</span>, tùy vào số điểm bạn tích lũy. Điểm tích lũy sẽ được <span className='text-gradiant-red'>cộng 30</span> khi cảm nghĩ của bạn được <span className='text-gradiant-blue'>trên 30 lượt yêu thích.</span>
                        </div>
                    </div>
                </div>
            </div>

            <div className='row'>
                <div className='col-md-6 md3'>
                    <div className='zgr3'>
                        <div className='zdt3'>
                            Với tính năng <span className='text-gradiant-red'>Phối Đồ</span>, bạn có thể <span className='text-gradiant-green'>thực hiện việc kết hợp thời trang - phụ kiện độc đáo</span> cho riêng bản thân, <span className='text-gradiant-red'>chia sẻ</span> với mọi người, <span className='text-gradiant-red'>và tích điểm</span> để được giảm giá khi mua sắm.
                        </div>
                        <div>
                            <img className='top-cover3' src={Mix2Img} />
                        </div>
                    </div>
                </div>
            </div>

            <div className='row'>
                <div className='col-md-6 zcontact'>
                    <div className='it'>
                        Cập nhật sau cùng ngày 14/05/2020
                    </div>
                    <div className='it'>
                        <a href="feedback@zapii.me">feedback@zapii.me</a>
                    </div>
                    <div className='it'>
                        ©Zapii.me {new Date().getFullYear()}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Comp;