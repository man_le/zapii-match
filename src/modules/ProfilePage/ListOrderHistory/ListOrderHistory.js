import React, { useEffect, useState, useContext } from 'react';
import { BounceLoader } from 'react-spinners';
import { Link, withRouter } from 'react-router-dom';
import CurrencyFormat from 'react-currency-format';
import short from 'short-uuid';
import { GlobalStateContext } from '../../Layout/HomeLayout';
import InfiniteScroll from "react-infinite-scroll-component";
import { RouteContext } from '../../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../../utils/DBUtils';
import type from '../../../reducers/type';
import moment from 'moment';
import './stylesheet.scss';
import { isEmpty } from 'lodash';
import DataIsEmpty from '../../Common/DataIsEmpty';
import OrderCompleteIcon from '../../Common/Icon/OrderCompleteIcon';
import MoreActionComponent from '../../Common/MoreActionComponent';
import R from '../../../locale/R';
import OrderPendingIcon from '../../Common/Icon/OrderPendingIcon';
import OrderPackingIcon from '../../Common/Icon/OrderPackingIcon';
import OrderSendingIcon from '../../Common/Icon/OrderSendingIcon';
import OrderCancelIcon from '../../Common/Icon/OrderCancelIcon';
import BagIcon from '../../Common/Icon/BagIcon';
import OrderDetailComponent from './OrderDetailComponent';
import { formatMoment } from '../../../utils/StringUtils';

const Comp = ({ history }) => {

    const urlParams = new URLSearchParams(window.location.search);
    const order_id_param = urlParams.get('order');

    const [localState, setLocalState] = useState({
        data: [],
        lastVisible: null,
        init: false,
        loading: false,
        showOrderDetail: !isEmpty(order_id_param),
        currentOrder: null
    });

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);

    const fetchData = async () => {
        const { data, lastVisible } = localState;

        setLocalState({ ...localState, loading: true });

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {
            const userInfo = user.extData[0];

            const result = await fireStore.findLimit(
                DB.ORDER.name(),
                [DB.ORDER.USER_ID, CON_TYPE.EQ, userInfo._id],
                DB.ORDER.CREATED_DATE,
                12,
                lastVisible,
                null
            );

            let more = true;
            if (isEmpty(result) || isEmpty(result.data)) {
                more = false;
            } else if (!isEmpty(result) && !isEmpty(result.data)) {
                result.data.forEach(e => {
                    if (isEmpty(data.filter(ei => ei._id === e._id))) data.push(e);
                })
            }

            setLocalState({
                ...localState,
                lastVisible: result ? result.lastVisible : null,
                data,
                more,
                init: true,
                loading: false
            });
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [user]);

    const fetchMoreData = () => {
        fetchData();
    }

    const showOrderDetailHandler = order => {
        setLocalState({ ...localState, showOrderDetail: true, currentOrder: order })
    }

    const hideOrderDetailHandler = (currentOrder)=>{
        let {data} = localState;
        
        if(!isEmpty(currentOrder)){
            const index = data.findIndex(e=>e._id === currentOrder._id);
            data[index] = currentOrder;
        }

        setLocalState({ ...localState, showOrderDetail: false, data });
        history.push(window.location.pathname);
    }

    const renderStatus = (status, isCancel) => {

        if(isCancel){
            return (
                <>
                    <OrderCancelIcon width='35' height='35' style={{ marginTop: '-0.25rem' }} />
                    <div className='nai' style={{color:'#ef5350'}}>
                        {R.profile.cancel_order_status}
                    </div>
                </>
            )
        }

        if (status === 'pending') {
            return (
                <>
                    <OrderPendingIcon width='35' height='35' style={{ marginTop: '-0.25rem' }} />
                    <div className='nai'>
                        {R.profile.pending}
                    </div>
                </>
            )
        } else if (status === 'received') {
            return (
                <>
                    <OrderPackingIcon width='30' height='30' style={{ marginTop: '-0.35rem' }} />
                    <div className='nai'>
                        {R.profile.receiving}
                    </div>
                </>
            )
        } else if (status === 'sending') {
            return (
                <>
                    <OrderSendingIcon width='35' height='35' style={{ marginTop: '-0.5rem' }} />
                    <div className='nai'>
                        {R.profile.sending}
                    </div>
                </>
            )
        } else if (status === 'sent') {
            return (
                <>
                    <OrderCompleteIcon width='35' height='35' style={{ marginTop: '-0.25rem' }} />
                    <div className='nai' style={{color:'#28a745'}}>
                        {R.profile.sent}
                    </div>
                </>
            )
        }
    }

    const { data, init, more } = localState;

    if (isEmpty(data)) {

        if (!init) return (
            <div className={`zcenter`} style={{ marginTop: '0.75rem' }}>
                <BounceLoader
                    sizeUnit={"px"}
                    size={50}
                    loading={true}
                    color='#777'
                />
            </div>
        )
        else return (
            <div className='set-order-group'>
                <DataIsEmpty className='zmargin_center' />
            </div>

        )
    }

    return (
        <>
            <div className='full set-order-group zmain' style={!localState.showOrderDetail?{display:''}:{display:'none'}}>
                <div className='row bar'>
                    <div className='col-md-6 zbor'>

                        <div className='snewg'>
                            <div className='gr'>
                                <BagIcon width='25' height='25' style={{ marginTop: '-0.25rem' }} />
                                <div className='t'>{R.profile.orderStatus}</div>
                            </div>
                        </div>

                        <InfiniteScroll
                            dataLength={data.length}
                            next={fetchMoreData}
                            hasMore={more}
                        >
                            {
                                data.map(e =>
                                    <div key={`set_${e._id}`} className='ci'>
                                        <div onClick={() => showOrderDetailHandler(e)} className='ic'>
                                            #{e.order_id}
                                            <div className='icd'>
                                                {formatMoment(moment, e.createdDate)}
                                            </div>
                                        </div>
                                        <div className='cn'>
                                            <div className='na'>
                                                {renderStatus(e.status, e.isCancel)}
                                            </div>
                                            <div className='pr'>
                                                <CurrencyFormat className='order-total-price' value={e.totalPrice} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                            </div>
                                        </div>
                                        <MoreActionComponent data={
                                            [
                                                {
                                                    label: R.set.detail,
                                                    onClickHandler: () => showOrderDetailHandler(e)
                                                }
                                            ]
                                        } />
                                    </div>
                                )
                            }
                        </InfiniteScroll>
                    </div>
                </div>
            </div>
            {
                localState.showOrderDetail
                &&
                <OrderDetailComponent onBack={hideOrderDetailHandler} history={history} order={localState.currentOrder} order_id_query={order_id_param} />
            }
        </>

    )
}

export default withRouter(Comp);