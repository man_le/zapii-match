import { isEmpty, isUndefined } from 'lodash';
import moment from 'moment';
import * as firebase from "firebase/app";
import React, { useContext, useEffect, useRef, useState } from 'react';
import CurrencyFormat from 'react-currency-format';
import { Link, withRouter } from 'react-router-dom';
import short from 'short-uuid';
import { BounceLoader } from 'react-spinners';
import format from 'string-template';
import type from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import AddressBookComponent from '../AddressBook/AddressBookComponent';
import DataIsEmpty from '../Common/DataIsEmpty';
import ModalComponent from '../Common/ModalComponent';
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from './../../locale/R';
import { showNotification } from './../../utils/NotificationUtils';
import './stylesheet.scss';

const InBagCheckoutComponent = ({ history }) => {

    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);
    const vouncherRef = useRef(null);
    const noteRef = useRef(null);

    const [localState, setLocalState] = useState({
        init: false,
        data: [],
        loading: false,
        paytype: 'cod',
        isUseVouncher: false,
        totalDiscount: 0,
        messageDiscount: null,
        addressSelected: null,
        vouncherResult: null
    });

    const fetchData = async () => {
        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            setLocalState({ ...localState, loading: true });

            let userInfo = user.extData[0];
            const result = await fireStore.find(
                DB.IN_BAG.name(),
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, userInfo._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
            )
            if (!isEmpty(result)) {

                // Build address
                let addressSelected = await fireStore.find(DB.ADDRESS.name(), [DB.ADDRESS.USER_ID, CON_TYPE.EQ, user.extData[0]._id], ['isDefault', CON_TYPE.EQ, true]);
                if (!isEmpty(addressSelected)) {
                    addressSelected = addressSelected[0];
                }

                const items = seperateProductBySize(result);
                setLocalState({ ...localState, data: items, init: true, addressSelected, loading: false });
            } else {
                setLocalState({ ...localState, data: [], init: true, loading: false });
            }
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [
        user,
        globalState.isUpdateInBag,
        globalState.isUpdateAddress,
        globalState.order_id
    ]);

    const seperateProductBySize = (items) => {
        let result = [];
        for (let i = 0; i < items.length; i++) {

            if (!isUndefined(items[i].S)) {
                let itemClone = { ...items[i] };
                delete itemClone.M;
                delete itemClone.L;
                itemClone['key'] = 'S';
                result.push(itemClone);
            }
            if (!isUndefined(items[i].M)) {
                let itemClone = { ...items[i] };
                delete itemClone.S;
                delete itemClone.L;
                itemClone['key'] = 'M';
                result.push(itemClone);
            }
            if (!isUndefined(items[i].L)) {
                let itemClone = { ...items[i] };
                delete itemClone.S;
                delete itemClone.M;
                itemClone['key'] = 'L';
                result.push(itemClone);
            }
            if (!isUndefined(items[i].XL)) {
                let itemClone = { ...items[i] };
                delete itemClone.S;
                delete itemClone.M;
                delete itemClone.L;
                itemClone['key'] = 'XL';
                result.push(itemClone);
            }

            // Size of shoes
            for(let sz=32;sz<=42;sz++){
                if (!isUndefined(items[i][sz])) {
                    let itemClone = { ...items[i] };

                    // Remove another size
                    for(let szremove=32;szremove<=42;szremove++){
                        if(szremove===sz) continue;
                        delete itemClone[szremove];
                    }

                    itemClone['key'] = sz.toString();
                    result.push(itemClone);
                }
            }

        }
        return result;
    }

    const buildTotalPrice = () => {
        const { data } = localState;
        let result = 0;
        for (let i = 0; i < data.length; i++) {
            let total = data[i][data[i].key];
            if (total < 0) total = 0;
            const value = data[i].price * total;
            result += value;
        }
        if (result <= 0) result = 0;
        return Number(result);
    }

    const calDiscount = (vouncherObj) => {
        const { discountType, percentDiscountValue, maxDiscount } = vouncherObj
        let totalDiscount = 0;
        if (discountType === 'percent') {
            totalDiscount = ((buildTotalPrice() * percentDiscountValue) / 100);
            if (totalDiscount > maxDiscount) totalDiscount = maxDiscount;
            if (totalDiscount <= 0) totalDiscount = 0;
        } else if (discountType === 'value') {
            totalDiscount = maxDiscount;
            if (totalDiscount <= 0) totalDiscount = 0;
        }

        return totalDiscount;
    }

    const { data, init, loading } = localState;

    const payTypeHandler = type => {
        setLocalState({ ...localState, paytype: type });
    }

    const vouncherRemoveHandler = () => {
        setLocalState({ ...localState, totalDiscount: 0, messageDiscount: null, isUseVouncher: false, vouncherResult:null });
    }

    const vouncherSubmitHandler = async () => {

        const vouncher = vouncherRef.current.value;
        if (localState.loading || isEmpty(vouncher)) return;

        setLocalState({ ...localState, loading: true });

        let vouncherResult = null;

        // Vouncher from Score Accumulate
        if (vouncher.indexOf('ZA#') !== -1) {

            let scoreAcc = await fireStore.find(
                DB.SCORE_ACCUMULATE.name(), [DB.SCORE_ACCUMULATE.USER_ID, CON_TYPE.EQ, user.extData[0]._id]
            )
            if (!isEmpty(scoreAcc)) {
                let discountValue = vouncher.replace('ZA#', '');
                if (isEmpty(discountValue)) discountValue = 0;
                else discountValue = Number(discountValue);

                scoreAcc = scoreAcc[0];
                if (discountValue <= Number(scoreAcc.score)) {

                    // START - Calc discount value
                    // 0-200k: 15k
                    // 200k-500k: 25k
                    // 500k-1000k: 45k
                    // >1000k: 65k
                    const totalPrice = buildTotalPrice();
                    if (totalPrice < 200000 && discountValue > 15) {
                        showNotification('danger', R.common.error_msg_maximum_za_code_15);
                        setLocalState({ ...localState, loading: false });
                        return;
                    } else if (totalPrice >= 200000 && totalPrice < 500000 && discountValue > 25) {
                        showNotification('danger', R.common.error_msg_maximum_za_code_25);
                        setLocalState({ ...localState, loading: false });
                        return;
                    } else if (totalPrice >= 500000 && totalPrice < 1000000 && discountValue > 45) {
                        showNotification('danger', R.common.error_msg_maximum_za_code_45);
                        setLocalState({ ...localState, loading: false });
                        return;
                    } else if (totalPrice >= 1000000 && discountValue > 65) {
                        showNotification('danger', R.common.error_msg_maximum_za_code_65);
                        setLocalState({ ...localState, loading: false });
                        return;
                    }
                    //// END - Calc discount value

                    vouncherResult = [{
                        ...scoreAcc,
                        numberValidDate: 0,
                        discountType: 'value',
                        percentDiscountValue: 0,
                        maxDiscount: discountValue * 1000,
                        isUseAccumulate: true
                    }]
                }
            }

        } else {

            // Check vouncher is used or not
            const vouncherUsedResult = await fireStore.find(
                DB.VOUNCHER_USED.name(),
                [DB.VOUNCHER_USED.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                [DB.VOUNCHER_USED.CODE, CON_TYPE.EQ, vouncher]
            )
            if (!isEmpty(vouncherUsedResult)) {
                showNotification('danger', R.bag.error_used_vouncher);
                setLocalState({ ...localState, loading: false });
                return;
            }

            // Vouncher for all
            vouncherResult = await fireStore.find(
                DB.VOUNCHER.name(),
                [DB.VOUNCHER.CODE, CON_TYPE.EQ, vouncher],
                [DB.VOUNCHER.TYPE, CON_TYPE.EQ, 'all']
            );

            // Vouncher for specific user
            if (isEmpty(vouncherResult)) {
                vouncherResult = await fireStore.find(
                    DB.VOUNCHER.name(),
                    [DB.VOUNCHER.CODE, CON_TYPE.EQ, vouncher],
                    [DB.VOUNCHER.TYPE, CON_TYPE.EQ, user.extData[0]._id]
                );
            }
        }

        if (!isEmpty(vouncherResult)) {
            vouncherResult = vouncherResult[0];

            // Check vouncher deadline
            let dateAfterAdd = moment(vouncherResult.createdDate).add(vouncherResult.numberValidDate, 'd').valueOf();
            let currentDate = moment().valueOf();

            let totalMoneyDiscount = 0;
            if (currentDate <= dateAfterAdd || vouncherResult.isUseAccumulate) {
                totalMoneyDiscount = calDiscount(vouncherResult);

                let messageDiscount = null;

                const formatter = new Intl.NumberFormat('en-US', {
                    currency: 'USD',
                    style: 'currency',
                    minimumFractionDigits: 0
                })

                if (vouncherResult.discountType === 'percent') {
                    messageDiscount = format(R.bag.apply_vouncher, { code: vouncher, percent: vouncherResult.percentDiscountValue, max: formatter.format(vouncherResult.maxDiscount).replace('$', '') + 'đ' });
                } else {
                    messageDiscount = format(R.bag.apply_vouncher2, {
                        code: vouncher,
                        max: formatter.format(vouncherResult.maxDiscount).replace('$', '') + 'đ'
                    });
                }

                setLocalState({ ...localState, vouncherResult, totalDiscount: totalMoneyDiscount, messageDiscount, isUseVouncher: true, loading: false });
            } else {
                showNotification('danger', R.bag.error_deadline_vouncher);
                setLocalState({ ...localState, loading: false });
            }
        } else {
            showNotification('danger', R.bag.error_wrong_vouncher);
            setLocalState({ ...localState, loading: false });
        }
    }

    const submitOrderHandler = async () => {

        if (isEmpty(localState.addressSelected)) {
            showNotification('danger', R.addressBook.error_no_address);
            return;
        }

        if (localState.submitting) return;

        const { addressSelected } = localState;

        setLocalState({ ...localState, submitting: true });

        const newData = {
            [DB.ORDER.STATUS]: 'pending',
            vouncher: vouncherRef.current.value,
            isCancel: false,
            [DB.ORDER.NOTE]: noteRef.current.value,
            [DB.ORDER.USER_ID]: user.extData[0]._id,
            [DB.ORDER.ORDER_ID]: globalState.order_id,
            [DB.ORDER.BEFORE_PRICE]: buildTotalPrice(),
            [DB.ORDER.ADDRESS_VALUE]: addressSelected.name + '#' + addressSelected.phone + '#' + addressSelected.address,
            [DB.ORDER.ADDRESS_ID]: addressSelected._id,
            [DB.ORDER.TOTAL_PRICE]: buildTotalPrice() - localState.totalDiscount,
            [DB.ORDER.DISCOUNT_MESSAGE]: localState.messageDiscount
        }
        const _id = await fireStore.insert(
            DB.ORDER.name(),
            newData
        );

        // Update in bag
        if (!isEmpty(_id)) {

            // Update order_id
            await fireStore.updateMultiConditions(
                DB.IN_BAG.name(),
                { [DB.IN_BAG.ORDER_ID]: _id },
                false,
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
            )

            // Update vouncher is used
            const vouncher = vouncherRef.current.value;
            if (vouncher.indexOf('ZA#') !== -1 && localState.isUseVouncher) {
                let score = vouncher.replace('ZA#', '');
                try {
                    score = Number(score) * -1;
                } catch (err) {
                    score = 0;
                }
                const newData = {
                    [DB.SCORE_ACCUMULATE.SCORE]: firebase.firestore.FieldValue.increment(score),
                }
                await fireStore.updateMultiConditions(
                    DB.SCORE_ACCUMULATE.name(),
                    newData,
                    false,
                    [DB.SCORE_ACCUMULATE.USER_ID, CON_TYPE.EQ, user.extData[0]._id]
                );
            } else {
                if (!isEmpty(localState.vouncherResult)) {
                    const vouncherUsedData = {
                        [DB.VOUNCHER_USED.CODE]: vouncher,
                        [DB.VOUNCHER_USED.USER_ID]: user.extData[0]._id,
                        [DB.VOUNCHER_USED.VOUNCHER_ID]: localState.vouncherResult._id
                    }
                    await fireStore.updateMultiConditions(
                        DB.VOUNCHER_USED.name(),
                        vouncherUsedData,
                        true,
                        [DB.VOUNCHER_USED.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                        [DB.VOUNCHER_USED.VOUNCHER_ID, CON_TYPE.EQ, localState.vouncherResult._id]
                    )
                }
            }


        }
        setLocalState({ ...localState, loading: false, showCompletePage: true, submitting: false });

        const content = (
            <div className='successfully-content'>
                <div>{R.bag.success}</div>
                <div>{format(R.bag.success2, { code: globalState.order_id })}</div>
            </div>
        )
        showNotification('success', content, true);

        dispatch({ type: type.RESET_ORDER_ID });

        // Go to order status page
        let meUrl = '';
        if (!isEmpty(user) && !isEmpty(user.extData)) {
            const id = user.extData[0]._id;
            let translator = short();
            if (!isEmpty(id)) meUrl = `/${translator.fromUUID(id)}`;
        }

        history.push(`${meUrl}/t/7`);
    }


    if (loading && !init) {
        return (
            <div className={`zloading-item`}><BounceLoader
                sizeUnit={"px"}
                size={50}
                loading={true}
                color='#777'
            /></div>
        )
    } else if (isEmpty(data) && init) {
        return (
            <div className='zmain full product-detail-page-group'>
                <div className='row bar'>
                    <div className='col-md-6 '>
                        <DataIsEmpty />
                    </div>
                </div>
            </div>
        )
    }

    const { addressSelected } = localState;

    if (!loading && init) {
        if (isEmpty(globalState.order_id)) {
            history.push('/bag/list');
        }
    }

    return (
        <div className='zmain full product-count-page-group noselect'>
            <div className='row bar'>

                <div className='col-md-6 cmenu'>
                    <div className='left'>
                    
                    </div>
                    <div className='center'>
                        {R.bag.checkout}
                    </div>
                    <div className='right'>

                    </div>
                </div>

                <div className='col-md-6 zbor'>
                    <div className='gr1'>

                        <div className='order_info'>
                            <div className='order-id'>
                                <div className='l'>
                                    {R.bag.order_id}
                                </div>
                                <div className='t'>
                                    #{globalState.order_id}
                                </div>
                            </div>
                            <div className='group-item'>
                                <div className='lbl'>
                                    {R.bag.address} (<span className='edit' data-toggle="modal" data-target={`#address_book_modal`}>{R.bag.change}</span>)
                                </div>
                                <div className='address-g'>
                                    {
                                        !isEmpty(addressSelected)
                                            ?
                                            <div className='sel'>
                                                <div className='name-phone'>
                                                    {addressSelected.name} - {addressSelected.phone}
                                                </div>
                                                <div className='address-detail'>
                                                    {addressSelected.address}
                                                </div>
                                            </div>
                                            :
                                            <div className='no-data'>{R.addressBook.error_no_address}</div>
                                    }
                                </div>
                            </div>
                            <div className='group-item'>
                                <div className='lbl'>
                                    {R.bag.pay}
                                </div>
                                <div className='p-group'>
                                    <div onClick={() => payTypeHandler('cod')} className={`pi`}>
                                        <div className={`zpointer zckbox ${localState.paytype === 'cod' ? 'active' : ''}`}>
                                            <i className='material-icons'>check</i>
                                        </div>
                                        <div className='t'>
                                            {R.bag.cod}
                                        </div>
                                    </div>
                                    <div onClick={() => payTypeHandler('account')} data-toggle={"modal"} data-target={`#pay_account_modal`} className={`pi`}>
                                        <div className={`zpointer zckbox ${localState.paytype === 'account' ? 'active' : ''}`}>
                                            <i className='material-icons'>check</i>
                                        </div>
                                        <div className='t'>
                                            {R.bag.account}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='group-item'>
                                <div className='lbl'>
                                    {R.bag.vouncher}
                                </div>
                                <div className='inv'>
                                    <input className={localState.isUseVouncher ? 'zdisabled' : ''} disabled={localState.isUseVouncher} ref={vouncherRef} maxLength='10' type='text' placeholder={R.bag.input_code} />
                                    {
                                        localState.isUseVouncher
                                            ?
                                            <div onClick={vouncherRemoveHandler} className='bn zpointer noselect'>
                                                <i className='material-icons'>close</i>
                                            </div>
                                            :
                                            <div onClick={vouncherSubmitHandler} className='bn zpointer noselect'>
                                                <i className='material-icons'>check</i>
                                            </div>
                                    }
                                </div>
                            </div>
                            <div className='group-item'>
                                <div className='lbl'>
                                    {R.bag.note}
                                </div>
                                <div className='note-g'>
                                    <textarea ref={noteRef} maxLength='250' placeholder={R.bag.note_placeholder} ></textarea>
                                </div>
                            </div>
                        </div>

                        <div className='total-price-group'>
                            <div className='ztotal'>
                                <div className='l'>
                                    {R.bag.total}
                                </div>
                                <div className={localState.isUseVouncher ? 'r1' : 'r'}>
                                    <CurrencyFormat className='complete-order-total-price' value={buildTotalPrice()} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                </div>
                            </div>
                            {
                                !isEmpty(localState.messageDiscount)
                                &&
                                <div className='ztotal vouncher'>
                                    <div className='l' style={{ fontSize: '0.95rem' }}>
                                        {localState.messageDiscount}
                                    </div>
                                </div>
                            }
                            {
                                !isEmpty(localState.messageDiscount)
                                &&
                                <div className='ztotal'>
                                    <div className='l'>
                                        {R.bag.left}
                                    </div>
                                    <div className='r'>
                                        <CurrencyFormat value={buildTotalPrice() - localState.totalDiscount} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                    </div>
                                </div>
                            }

                            <div onClick={submitOrderHandler} className={`g ${localState.submitting || isEmpty(data) || buildTotalPrice() === 0 ? 'zdisabled' : ''}`}>
                                <div className={`ins noselect zpointer ${isEmpty(data) || buildTotalPrice() === 0 ? '' : 'zscale-effect2'}`}>
                                    <div className='l'>
                                        {
                                            R.bag.checkout
                                        }
                                    </div>
                                    <i className='material-icons'>navigate_next</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <ModalComponent id={'address_book_modal'} hideHeader={true} styleModalDialg={{maxWidth:'710px'}}>
                <AddressBookComponent />
            </ModalComponent>

        </div>
    )
}

export default withRouter(InBagCheckoutComponent);