import React, { useState, useEffect, useContext } from 'react';
import short from 'short-uuid';
import { isEmpty } from 'lodash';
import * as firebase from "firebase/app";
import ReactHtmlParser from 'react-html-parser';
import Autolinker from 'autolinker';
import CurrencyFormat from 'react-currency-format';
import { BounceLoader } from 'react-spinners';
import { Link, withRouter } from 'react-router-dom';
import { CON_TYPE, DB, fireStore, realTime } from './../../utils/DBUtils';
import { RouteContext } from '../../routes/routes';
import type from '../../reducers/type';
import BagIcon from '../Common/Icon/BagIcon';
import CloseIcon from '../Common/Icon/CloseIcon';
import BookmarkIcon from '../Common/Icon/BookmarkIcon';
import ClothesIcon from '../Common/Icon/ClothesIcon';
import DataIsEmpty from './../Common/DataIsEmpty';
import SizeComponent from './../SizeComponent/SizeComponent';
import { GlobalStateContext } from '../Layout/HomeLayout';
import LeftRightContentComponent from '../Common/LeftRightContentComponent';
import LeftComponent from './LeftComponent';
import R from './../../locale/R';
import './stylesheet.scss';
import { showNotification } from '../../utils/NotificationUtils';

const InBagCountComponent = ({ match, history }) => {
    const { params: { clothes_id } } = match;

    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const [localState, setLocalState] = useState({
        init: false,
        data: {},
        loading: false,
        size: 'M',

        image_detail_index:0
    });

    let id = null;
    try {
        id = short().toUUID(clothes_id);
    } catch (err) {
        id = null;
    }

    const sizeHandler = (type) => {
        setLocalState({ ...localState, size: type });
    }

    const inBagHandler = async (inBag, sizeType) => {

        // Add into bag
        if (inBag === false) {
            setLocalState({ ...localState, showDetail: false });

            const newData = {
                [DB.IN_BAG.ID_GEN]: `${id}$${user.extData[0]._id}`,
                [DB.IN_BAG.CLOTHES_ID]: id,
                [DB.IN_BAG.USER_ID]: user.extData[0]._id,
                [DB.IN_BAG.ORDER_ID]: 'new',
                name: data.name,
                image: data.image,
                price: data.new_price,
                [sizeType]: firebase.firestore.FieldValue.increment(1),
            };

            await fireStore.updateMultiConditions(
                DB.IN_BAG.name(),
                newData,
                true,
                [DB.IN_BAG.CLOTHES_ID, CON_TYPE.EQ, id],
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new']
            );

            // Remove in bag
        } else {
            setLocalState({ ...localState, showDetail: false });
            await fireStore.remove(
                DB.IN_BAG.name(),
                [DB.IN_BAG.CLOTHES_ID, CON_TYPE.EQ, id],
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
            )
        }

        dispatch({ type: type.UPDATE_IN_BAG });
    }

    const removeBagHandler = async () => {
        let { itemsInBag } = globalState;

        if (itemsInBag.indexOf(id + '$' + user.extData[0]._id) === -1) return;

        itemsInBag = itemsInBag.filter(e => e !== id + '$' + user.extData[0]._id);
        dispatch({ type: type.CHANGE_ITEM_IN_BAG, itemsInBag });

        inBagHandler(true);

        showNotification('success', R.discover.remove2bag);
    }

    const addBagHandler = async () => {
        let { itemsInBag } = globalState;
        if (itemsInBag.indexOf(id + '$' + user.extData[0]._id) === -1) itemsInBag.push(id + '$' + user.extData[0]._id);
        dispatch({ type: type.CHANGE_ITEM_IN_BAG, itemsInBag });

        inBagHandler(false, localState.size);

        showNotification('success', R.discover.add2bag);
    }

    const bookmarkHandler = async () => {
        let { itemsInBookmark } = globalState;
        let { data } = localState;

        const bookmarkData = {
            [DB.BOOKMARK.ID_GEN]: `${id}$${user.extData[0]._id}`,
            [DB.BOOKMARK.CLOTHES_ID]: id,
            image: data.image,
            name: data.name,
            price: data.new_price,
            mix_type: data.mix_type,
            filter_type: data.filter_type || '',
            size: data.size || [],
            [DB.BOOKMARK.USER_ID]: user.extData[0]._id
        };

        if (itemsInBookmark.indexOf(id + '$' + user.extData[0]._id) === -1) {

            itemsInBookmark.push(id + '$' + user.extData[0]._id);
            dispatch({ type: type.CHANGE_ITEM_IN_BOOKMARK, itemsInBookmark });

            await fireStore.updateMultiConditions(
                DB.BOOKMARK.name(),
                bookmarkData,
                true,
                [DB.BOOKMARK.CLOTHES_ID, CON_TYPE.EQ, id],
                [DB.BOOKMARK.USER_ID, CON_TYPE.EQ, user.extData[0]._id]
            );

        } else {

            itemsInBookmark = itemsInBookmark.filter(e => e !== id + '$' + user.extData[0]._id);
            dispatch({ type: type.CHANGE_ITEM_IN_BOOKMARK, itemsInBookmark });

            await fireStore.remove(
                DB.BOOKMARK.name(),
                [DB.BOOKMARK.CLOTHES_ID, CON_TYPE.EQ, id],
                [DB.BOOKMARK.USER_ID, CON_TYPE.EQ, user.extData[0]._id]
            )
        }
    }

    const preGoToURL = () => {
        dispatch({ type: type.CHANGE_ROUTER, currentRouter: `/detail/product/${clothes_id}`, currentRouterIndex: 6 });
    }

    const fetchData = async () => {
        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            setLocalState({ ...localState, loading: true });

            let userInfo = user.extData[0];

            let result = await fireStore.find(DB.CLOTHES.name(), [DB.CLOTHES.ID, CON_TYPE.EQ, id]);

            if (!isEmpty(result)) {
                result = result[0];

                // Build items in bag
                let in_bag_ids = await fireStore.find(
                    DB.IN_BAG.name(),
                    [DB.IN_BAG.ID_GEN, CON_TYPE.EQ, `${id}$${user.extData[0]._id}`],
                    [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
                );
                if (!isEmpty(in_bag_ids)) {
                    in_bag_ids = in_bag_ids.map(e => e._id_gen);

                    let { itemsInBag } = globalState;
                    for (let i = 0; i < in_bag_ids.length; i++) {
                        if (itemsInBag.indexOf(in_bag_ids[i]) === -1) {
                            itemsInBag.push(in_bag_ids[i]);
                        }
                    }
                    dispatch({ type: type.CHANGE_ITEM_IN_BAG, itemsInBag });
                }

                // Build items in bookmark
                let bookmark_ids = await fireStore.find(DB.BOOKMARK.name(), [DB.BOOKMARK.ID_GEN, CON_TYPE.EQ, `${id}$${user.extData[0]._id}`]);
                if (!isEmpty(bookmark_ids)) {
                    bookmark_ids = bookmark_ids.map(e => e._id_gen);

                    let { itemsInBookmark } = globalState;
                    for (let i = 0; i < bookmark_ids.length; i++) {
                        if (itemsInBookmark.indexOf(bookmark_ids[i]) === -1) {
                            itemsInBookmark.push(bookmark_ids[i]);
                        }
                    }
                    dispatch({ type: type.CHANGE_ITEM_IN_BOOKMARK, itemsInBookmark });
                }

                // Build items in design
                let design_ids = await fireStore.find(DB.IN_DESIGN.name(), [DB.IN_DESIGN.ID_GEN, CON_TYPE.EQ, `${id}$${user.extData[0]._id}`]);
                if (!isEmpty(design_ids)) {
                    design_ids = design_ids.map(e => e._id_gen);

                    let { itemsInDesign } = globalState;
                    for (let i = 0; i < design_ids.length; i++) {
                        if (itemsInDesign.indexOf(design_ids[i]) === -1) {
                            itemsInDesign.push(design_ids[i]);
                        }
                    }
                    dispatch({ type: type.CHANGE_ITEM_IN_DESIGN, itemsInDesign });
                }

                setLocalState({
                    ...localState,
                    data: result,
                    loading: false,
                    init: true
                })
            } else {
                setLocalState({ ...localState, loading: false });
            }
        }
    }

    const renderImageSlider = (image_photo) => {
        const {image_detail_index} = localState;
        let max = 0;
        if (!isEmpty(image_photo) && image_photo.length>1) {
            max = image_photo.length;
            if (image_photo.length >= 5) max = 5;
        }
        let result = [];
        if (max > 0) {
            for (let i = 0; i < max; i++) {
                result.push(
                    <div key={'imgslider_'+i} onClick={()=>{setLocalState({...localState, image_detail_index:i })}} className='pi noselect' style={image_detail_index==i?{background:'#777'}:{}}></div>
                )
            }
        }
        return result;
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [user,
        globalState.itemsInBag,
        globalState.itemsInBookmark,
        globalState.itemsInDesign,
        clothes_id
    ]);

    const { data, init, loading } = localState;

    if (loading && !init) {
        return (
            <div className={`zloading-item`}><BounceLoader
                sizeUnit={"px"}
                size={50}
                loading={true}
                color='#777'
            /></div>
        )
    } else if (isEmpty(data) && init) {
        return (
            <div className='zmain full product-detail-page-group'>
                <div className='row bar'>
                    <div className='col-md-6'>
                        <DataIsEmpty />
                    </div>
                </div>
            </div>
        )
    }

    if (isEmpty(user) || isEmpty(user.extData)) return null;

    const { itemsInBag, itemsInBookmark, itemsInDesign } = globalState;

    return (
        <div className='zmain full product-detail-page-group'>
            <div className='row bar'>

                <div className='col-md-6 cmenu'>
                    <div className='left'>
                   
                    </div>
                    <div className='center'>
                        {R.discover.detail}
                    </div>
                    <div className='right'>
                    </div>
                </div>

                <div className='col-md-6 zbor' style={{ background: '#fff', zIndex:'9' }}>
                    <div className='gr1'>
                        <div className='preview-group'>
                            <img key={'img_preview_'+data._id} className='zpreview' src={!isEmpty(data.image_detail) ? data.image_detail[localState.image_detail_index] : data.image} alt={data.name} />
                            <div className='p-select'>
                                {renderImageSlider(data.image_detail)}
                            </div>
                        </div>
                        <div className='in'>
                            <div className='noselect name'>
                                <div className='cntype'>
                                    {R.discover[data.type]}
                                </div>
                                {data.name}
                            </div>
                            <div className='noselect description'>
                                {data.description &&
                                    data.description.split("<br/>").map((item, i) => {
                                        return (
                                            <span key={`${data._id}_${i}`}>
                                                {ReactHtmlParser(Autolinker.link(item))}
                                                <br />
                                            </span>
                                        )
                                    })
                                }

                                <SizeComponent sex={data.type} clothes={data.filter_type}/>

                            </div>
                            <div className='price'>
                                <div className='old-price'>
                                    <CurrencyFormat value={data.old_price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                </div>
                                <div className='new-price'>
                                    <CurrencyFormat value={data.new_price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                </div>
                            </div>

                            <div className='size-style'>
                                {
                                    !isEmpty(data.size)
                                    &&
                                    <>
                                        <div className='lb'>
                                            {R.discover.size}
                                        </div>
                                        <div className='stypegroup'>
                                            {
                                                data.size.map((e, i) =>
                                                    <div onClick={() => sizeHandler(e)} key={'size_' + i} className={`zscale-effect zpointer stype ${localState.size === e ? 'active' : ''}`}>
                                                        {e}
                                                    </div>
                                                )
                                            }
                                        </div>
                                    </>
                                }
                            </div>

                            <div className='order-group'>
                                <div className='cbtn-gr'>
                                    <div onClick={removeBagHandler} className={`zb noselect ${itemsInBag.indexOf(id + '$' + user.extData[0]._id) === -1 ? 'zdisabled' : 'zscale-effect2'}`} style={{ marginRight: '0.25rem' }}>
                                        <CloseIcon color='#fff' width='16' height='16' style={{ marginTop: '0.25rem', marginRight: '0.5rem' }} />
                                        <div>
                                            {R.discover.removeBag}
                                        </div>
                                    </div>
                                    <div onClick={addBagHandler} className='zscale-effect2 zb noselect'>
                                        <BagIcon color='#fff' style={{ marginRight: '0.25rem' }} />
                                        <div>
                                            {R.discover.addBag}
                                        </div>
                                    </div>
                                    <div onClick={bookmarkHandler} style={{marginLeft:'0.5rem'}} >
                                        <BookmarkIcon width='22' height='22' color='#262626' isSelected={itemsInBookmark.indexOf(id + '$' + user.extData[0]._id) !== -1 || false} className='zscale-effect cfavorite noselect zpointer' style={{ marginRight: '0.15rem', marginTop: '0.6rem' }} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <LeftRightContentComponent
                LeftComponent={<LeftComponent itemsInBag={globalState.itemsInBag} />}
            />
        </div>
    )
}

export default withRouter(InBagCountComponent);