import React, { useContext, useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import R from '../../locale/R';
import { GlobalStateContext } from '../Layout/HomeLayout';

const Comp = ({ history }) => {
    return (
        <div className='right-content mnu shadow-s'>
            <ul>
                <li onClick={() => history.push('/setting/profile')}>
                    <i className='material-icons'>person_pin</i>
                    {R.common.profile}
                </li>
                <li onClick={() => history.push('/setting/account')}>
                    <i className='material-icons'>person</i>
                    {R.common.account}
                </li>
                <li onClick={() => history.push('/setting/notification')}>
                    <i className='material-icons'>notifications</i>
                    {R.common.notification}
                </li>
            </ul>
        </div>
    )
}

export default withRouter(Comp);
