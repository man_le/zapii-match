export default {
    questionType: {
        local_cafe: 'Ăn uống',
        airplanemode_active: 'Du Lịch',
        photo_camera: 'Nghệ Thuật',
        fitness_center: 'Thể Thao',
        local_movies: 'Phim Ảnh',
        music_note: 'Âm Nhạc',
        mood: 'Cảm Xúc',
        beach_access: 'Thời Trang',
        hdr_weak: 'Khác',
        select_all: 'Tất cả'
    },
    timeline:{
        comment_message: 'Có {total} bình luận. Bạn chỉ xem được bình luận của bạn, của tác giả và của người khác phản hồi cho bạn'
    }
}