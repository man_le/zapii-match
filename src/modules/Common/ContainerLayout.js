import React from 'react';
import './stylesheet.scss';

const ContainerLayout = ({children}) => {
    return (
        <div className='row zcon-group'>
            <div className='col-md-6 item'>
                {children}
            </div>
        </div>
    )
}

export default ContainerLayout