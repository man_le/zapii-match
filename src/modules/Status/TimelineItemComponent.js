import Autolinker from 'autolinker';
import * as firebase from "firebase/app";
import { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useContext, useEffect, useRef, useState } from 'react';
import Avatar from 'react-avatar';
import ReactHtmlParser from 'react-html-parser';
import ReactPlayer from 'react-player';
import NumericLabel from 'react-pretty-numbers';
import { Link } from 'react-router-dom';
import short from 'short-uuid';
import uuid from 'uuid';
import R from '../../locale/R';
import type from '../../reducers/type';
import { CON_TYPE, DB, fireStore, realTime } from '../../utils/DBUtils';
import { buildCommentNotification_nonRealTime, buildLikeStatusNotification_nonRealTime, buildScoreStatusNotification_nonRealTime, showNotification } from '../../utils/NotificationUtils';
import { deleteFile } from '../../utils/StorageUtils';
import { formatMoment } from '../../utils/StringUtils';
import { getPrefixURL } from '../../utils/URLUtils';
import CommentItemComponent from '../Common/CommentItemComponent';
import MoreActionComponent from '../Common/MoreActionComponent';
import TimelineItemLoader from '../Common/TimelineItemLoader';
import { GlobalStateContext } from '../Layout/HomeLayout';
import './stylesheet.scss';

const TOTAL_LIKE_HAVE_SCORE = 30;
const MAXIMUM_SCORE = 100;

const TimelineItemComponent = ({ history, parentState, setParentState, loggedUser, data: statusData, shareHandler, isTrendStatus, statusType, createdByMe }) => {
    const [localState, setLocalState] = useState({
        userInfo: null,
        init: false,
        likeUserIds: [],
        likeUsers: [],
        shareUserIds: [],
        shareUsers: [],
        currentAnswerUsers: [],
        isLike: null,
        comments: [],
        moreComment: true,
        replyUserInfo: {},
        showComment: true,
        totalComment: 0,
        isRemoved: false,
        totalNewCommentsSkip: -1,
        setClothesOwner: {},

        isPlayVideo: false
    });
    const [globalState, dispatch] = useContext(GlobalStateContext);
    const commentRef = useRef('');

    const statusDataItem = globalState.dataForStatusData.find(e => e.status_id === statusData._id);

    const fetchData = async () => {
        let { commentLastVisible } = localState;
        let {
            newComments,
            newLikes_userIds, newLikes_users,
            newShares_userIds, newShares_users,
        } = globalState;

        let result = statusDataItem ? [statusDataItem.userInfo] : [];

        if (!statusData || !statusData.user_id) {
            setLocalState({ ...localState, userInfo: result ? result[0] : {}, init: true });
            return;
        }

        // Get like user
        const likeUserIds = statusData.likes || [];
        let likeUsers = statusDataItem ? statusDataItem.likeUsers || [] : [];
        if (!isEmpty(likeUserIds)) {

            // Seperate big array to multiple array with 10 items inside
            let maxLen = 100;
            if (likeUserIds.length < 100) maxLen = likeUserIds.length;
            let arrayMax10 = [];
            let step = 0;
            for (let i = 0; i <= maxLen / 10; i++) {
                arrayMax10.push(likeUserIds.slice(step, step + 10));
                step += 10;
            }

            if (isEmpty(likeUsers)) {
                for (let i = 0; i < arrayMax10.length; i++) {
                    if (!isEmpty(arrayMax10[i]) && arrayMax10[i].length <= 10) {
                        const result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.IN, arrayMax10[i]]);
                        if (!isEmpty(result)) likeUsers.push(...result);
                    }
                }
            }

            // End seperating
        }

        // Get share user
        const shareUserIds = statusData.shares || [];
        let shareUsers = statusDataItem ? statusDataItem.shareUsers || [] : [];
        if (!isEmpty(shareUserIds)) {

            // Seperate big array to multiple array with 10 items inside
            let maxLen = 100;
            if (shareUserIds.length < 100) maxLen = shareUserIds.length;
            let arrayMax10 = [];
            let step = 0;
            for (let i = 0; i <= maxLen / 10; i++) {
                arrayMax10.push(shareUserIds.slice(step, step + 10));
                step += 10;
            }
            if (isEmpty(shareUsers)) {
                for (let i = 0; i < arrayMax10.length; i++) {
                    if (!isEmpty(arrayMax10[i]) && arrayMax10[i].length <= 10) {
                        const result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.IN, arrayMax10[i]]);
                        if (!isEmpty(result)) shareUsers.push(...result);
                    }
                }
            }

            // End seperating
        }

        if (isEmpty(result)) result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, statusData.user_id]);
        if (!isEmpty(result)) {

            let isOwner = loggedUser._id === statusData.user_id;

            // Get setClothesOwner user info
            let setClothesOwner = statusDataItem ? [statusDataItem.setClothesOwner] : [];
            if (isEmpty(setClothesOwner)) setClothesOwner = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, statusData.setClothesOwner || '--']);
            if (!isEmpty(setClothesOwner)) {
                setClothesOwner = setClothesOwner[0];
            }

            // Get all comments
            const condition = [DB.COMMENT.STATUS_ID, CON_TYPE.EQ, statusData._id];
            let commentResult = {};
            if (statusDataItem) {
                commentResult = statusDataItem.commentResult;
            } else {
                commentResult = await fireStore.findLimitMultiConditions(DB.COMMENT.name(), 3, commentLastVisible, null, DB.COMMENT.CREATED_DATE, null, condition);
            }

            let moreComment = true;
            let comments = [];
            let lastVisible = null;
            if (!isEmpty(commentResult) && !isEmpty(commentResult.data)) {
                comments = commentResult.data.reverse();
                lastVisible = commentResult.lastVisible;

                if (comments.length < 3) moreComment = false;
            } else {
                moreComment = false;
            }

            // Add new comment from real time db
            let totalNewCommentsSkip = localState.totalNewCommentsSkip;

            realTime.find(
                DB.RT_NOTIFICATION.name(),
                snap => {
                    const buildNewComments = [];

                    if (!isEmpty(snap.val())) {

                        if (totalNewCommentsSkip === -1) totalNewCommentsSkip = Object.keys(snap.val()).length;

                        let newCommentIndex = 0;
                        for (let [key, value] of Object.entries(snap.val())) {
                            newCommentIndex++;
                            if (totalNewCommentsSkip === 1 || newCommentIndex > totalNewCommentsSkip) {
                                let comment = value;
                                comment.realTime_id = key;

                                if (
                                    isEmpty(localState.comments.find(cmt => cmt._id_gen === comment._id_gen))
                                    &&
                                    isEmpty(buildNewComments.find(cmt => cmt._id_gen === comment._id_gen))
                                ) buildNewComments.push(comment);
                            }
                        }
                    }
                    newComments[statusData._id] = buildNewComments;
                    dispatch({ type: type.BUILD_NEW_COMMENT, newComments });

                },
                `${statusData._id}_comment`,
            );

            // Find user like in Realtime db
            realTime.find(
                DB.RT_NOTIFICATION.name(),
                snap => {
                    const buildNewUserIdsLikes = [];
                    const buildNewUserLikes = [];

                    if (!isEmpty(snap.val())) {

                        for (let [key, value] of Object.entries(snap.val())) {
                            if (likeUserIds.indexOf(key) === -1) {
                                buildNewUserIdsLikes.push(key);
                                buildNewUserLikes.push(value);
                            }
                        }
                        newLikes_userIds[statusData._id] = buildNewUserIdsLikes;
                        newLikes_users[statusData._id] = buildNewUserLikes;
                        dispatch({ type: type.BUILD_NEW_LIKE, newLikes_userIds, newLikes_users });

                    } else {
                        dispatch({ type: type.BUILD_NEW_LIKE, newLikes_userIds: [], newLikes_users: [] });
                    }
                },
                `${statusData._id}_like`);

            // Find user share in Realtime db
            realTime.find(
                DB.RT_NOTIFICATION.name(),
                snap => {
                    const buildNewUserIdsShares = [];
                    const buildNewUserShares = [];

                    if (!isEmpty(snap.val())) {

                        for (let [key, value] of Object.entries(snap.val())) {
                            if (shareUserIds.indexOf(key) === -1) {
                                buildNewUserIdsShares.push(key);
                                buildNewUserShares.push(value);
                            }
                        }
                        newShares_userIds[statusData._id] = buildNewUserIdsShares;
                        newShares_users[statusData._id] = buildNewUserShares;
                        dispatch({ type: type.BUILD_NEW_SHARE, newShares_userIds, newShares_users });

                    } else {
                        dispatch({ type: type.BUILD_NEW_SHARE, newShares_userIds: [], newShares_users: [] });
                    }
                },
                `${statusData._id}_share`);

            setLocalState({
                ...localState,
                userInfo: result[0],
                init: true,
                totalComment: statusData.totalComment || 0,
                likeUserIds, likeUsers,
                shareUserIds, shareUsers,
                comments,
                isOwner,
                moreComment,
                commentLastVisible: lastVisible,
                totalNewCommentsSkip,

                setClothesOwner
            });

            dispatch({
                type: type.BUILD_DATA_IN_SIDE_STATUS_DATA,
                likeUsers,
                shareUsers,
                userInfo: result[0],
                comments,
                commentLastVisible: lastVisible,
                status_id: statusData._id,
                setClothesOwner
            });
        }

    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, []);


    if (isEmpty(localState.userInfo) && !localState.init) return <TimelineItemLoader />;
    else if (isEmpty(localState.userInfo) && localState.init) return null;

    var translator = short();

    const loadMoreComment = async () => {
        let { commentLastVisible, comments } = localState;
        const condition = [DB.COMMENT.STATUS_ID, CON_TYPE.EQ, statusData._id];
        const commentResult = await fireStore.findLimit(DB.COMMENT.name(), condition, DB.COMMENT.CREATED_DATE, 3, commentLastVisible, null);
        if (!isEmpty(commentResult) && !isEmpty(commentResult.data)) {
            let cmts = commentResult.data.reverse();
            cmts = cmts.concat(comments);

            let moreComment = true;
            if (commentResult.data.length < 3) moreComment = false;

            setLocalState({ ...localState, comments: cmts, commentLastVisible: commentResult.lastVisible, moreComment });
        } else {
            setLocalState({ ...localState, moreComment: false })
        }

    }

    const likeHandler = async () => {
        let { likeUserIds, likeUsers } = localState;

        // Like
        if (isEmpty(likeUserIds) || likeUserIds.indexOf(loggedUser._id) === -1) {
            likeUserIds.push(loggedUser._id);
            likeUsers.push(loggedUser);
            setLocalState({ ...localState, isLike: true });

            const data = {
                likes: firebase.firestore.FieldValue.arrayUnion(loggedUser._id),
            }
            await fireStore.updateMultiConditions(
                DB.STATUS.name(),
                data, true,
                [DB.STATUS.ID, CON_TYPE.EQ, statusData._id],
            );

            // Check score accomulate, if >10 count 1 in score
            if (statusData.likes && statusData.likes.length >= TOTAL_LIKE_HAVE_SCORE - 1) {
                const scoreAccomulateStatus = await fireStore.findLimitStatic(
                    DB.SCORE_ACCUMULATE_STATUS.name(),
                    1,
                    [DB.SCORE_ACCUMULATE_STATUS.STATUS_ID, CON_TYPE.EQ, statusData._id],
                    [DB.SCORE_ACCUMULATE_STATUS.TYPE, CON_TYPE.EQ, 'like']
                );

                if (isEmpty(scoreAccomulateStatus)) {

                    let scoreAcc = await fireStore.find(DB.SCORE_ACCUMULATE.name(), [DB.SCORE_ACCUMULATE.USER_ID, CON_TYPE.EQ, loggedUser._id]);
                    if (!isEmpty(scoreAcc)) {
                        scoreAcc = scoreAcc[0];
                        const currentScore = scoreAcc.score;

                        if (currentScore < MAXIMUM_SCORE) {

                            const scoreData = {
                                score: firebase.firestore.FieldValue.increment(1),
                                user_id: statusData.user_id
                            }
                            await fireStore.updateMultiConditions(
                                DB.SCORE_ACCUMULATE.name(),
                                scoreData, true,
                                [DB.SCORE_ACCUMULATE.USER_ID, CON_TYPE.EQ, statusData.user_id],
                            );

                            await fireStore.update(
                                DB.SCORE_ACCUMULATE_STATUS.name(),
                                [DB.SCORE_ACCUMULATE_STATUS.STATUS_ID, CON_TYPE.EQ, statusData._id],
                                {
                                    [DB.SCORE_ACCUMULATE_STATUS.STATUS_ID]: statusData._id,
                                    [DB.SCORE_ACCUMULATE_STATUS.TYPE]: 'like'
                                },
                                true
                            )

                            // START - Notification Handler
                            // Update realtime for show notification panel on other page
                            const dataForNotiInfo = {
                                [loggedUser._id]: {
                                    user_id: loggedUser._id,
                                }
                            }
                            realTime.update(DB.RT_NOTIFICATION_INFO.name(), dataForNotiInfo, null, `${loggedUser._id}_score_status_notification`);

                            // Add notification info
                            const newData = {
                                user_id: statusData.user_id,
                                _id: statusData._id
                            }
                            buildScoreStatusNotification_nonRealTime(newData, loggedUser);
                            // END - Notification Handler


                        }
                    }
                }
            }

            // Add to realtime database
            realTime.update(
                DB.RT_NOTIFICATION.name(),
                { [loggedUser._id]: loggedUser },
                null,
                `${statusData._id}_like`);

            // Update realtime like-quiz for show notification on other page
            const dataForNotiInfo = {
                [loggedUser._id]: {
                    user_id: loggedUser._id,
                    status_id: statusData._id,
                }
            }
            realTime.update(DB.RT_NOTIFICATION_INFO.name(), dataForNotiInfo, null, `${statusData.user_id}_like_status_notification`);

            // Add notification info
            buildLikeStatusNotification_nonRealTime(statusData, loggedUser);

            // Un like
        } else {
            likeUserIds.splice(likeUserIds.indexOf(loggedUser._id), 1);
            const indexUserWillRemoved = likeUsers.findIndex(e => e._id === loggedUser._id);
            likeUsers.splice(indexUserWillRemoved, 1);

            setLocalState({ ...localState, isLike: false });
            const data = {
                likes: firebase.firestore.FieldValue.arrayRemove(loggedUser._id),
            }
            await fireStore.updateMultiConditions(
                DB.STATUS.name(),
                data, true,
                [DB.STATUS.ID, CON_TYPE.EQ, statusData._id],
            );

            // Add to realtime database
            realTime.update(
                DB.RT_NOTIFICATION.name(),
                { [loggedUser._id]: null },
                null,
                `${statusData._id}_like`);
        }
    }

    const commendHandler = e => {
        let { comments, totalComment } = localState;
        const value = commentRef.current.value;
        if (e.keyCode == 13) {
            if (isEmpty(value) || isEmpty(encodeURI(value).replace(/%0A/g, ''))) return;

            if (e.shiftKey || e.ctrlKey) {
            } else {
                totalComment++;

                let reply_user_id = loggedUser._id;
                let isReplyForMyself = false;

                if (!isEmpty(localState.replyUserInfo)) {
                    if (localState.replyUserInfo._id !== loggedUser._id) {
                        reply_user_id = localState.replyUserInfo._id;
                    }
                }

                if (reply_user_id === loggedUser._id) {
                    isReplyForMyself = true;
                }

                const _id = uuid.v4();

                const data = {
                    [DB.COMMENT.ID_GEN]: _id,
                    [DB.COMMENT.USER_ID]: loggedUser._id,
                    [DB.COMMENT.REPLY_USER_ID]: reply_user_id,
                    [DB.COMMENT.IS_REPLY_FOR_MYSELF]: isReplyForMyself,
                    [DB.COMMENT.STATUS_ID]: statusData._id,
                    [DB.COMMENT.CONTENT]: value.substring(0, 3000),
                }
                fireStore.insert(DB.COMMENT.name(), data);

                commentRef.current.value = null;
                e.preventDefault();

                // New comment
                const newComment = {
                    [DB.COMMENT.ID_GEN]: _id,
                    [DB.COMMENT.USER_ID]: loggedUser._id,
                    [DB.COMMENT.REPLY_USER_ID]: reply_user_id,
                    [DB.COMMENT.IS_REPLY_FOR_MYSELF]: isReplyForMyself,
                    [DB.COMMENT.STATUS_ID]: statusData._id,
                    [DB.COMMENT.STATUS_OWNER]: statusData.user_id,
                    [DB.COMMENT.CONTENT]: value,
                    [DB.COMMENT.CREATED_DATE]: 'now',
                }

                setLocalState({ ...localState, comments, replyUserInfo: null, totalComment });

                // Increment total comment
                const dataQuiz = {
                    totalComment: firebase.firestore.FieldValue.increment(1),
                }
                fireStore.updateMultiConditions(
                    DB.STATUS.name(),
                    dataQuiz, true,
                    [DB.STATUS.ID, CON_TYPE.EQ, statusData._id],
                );

                // Update realtime comment
                realTime.insert(DB.RT_NOTIFICATION.name(), newComment, `${statusData._id}_comment`);

                // Update realtime comment for show notification on other page
                if (isReplyForMyself === true || isEmpty(localState.replyUserInfo)) {
                    realTime.insert(DB.RT_NOTIFICATION_INFO.name(), newComment, `${statusData.user_id}_comment_notification`);
                } else {
                    realTime.insert(DB.RT_NOTIFICATION_INFO.name(), newComment, `${localState.replyUserInfo._id}_comment_notification`);
                }

                // Add notification info
                buildCommentNotification_nonRealTime(newComment, statusData, loggedUser);
            }
        }
    }

    const likeActiveHandler = () => {
        const { likeUserIds, userInfo, isLike } = localState;
        if (isLike == null && !isEmpty(likeUserIds) && !isEmpty(userInfo) && likeUserIds.indexOf(loggedUser._id) !== -1) {
            return 'active';
        } else if (isLike === true) return 'active';
        return '';
    }

    const likeModalHandler = () => {
        let { likeUsers, likeUserIds } = localState;
        setParentState({ ...parentState, likeUsers, likeUserIds, currentStatus_id: statusData._id });
    }

    const shareModalHandler = () => {
        let { shareUsers, shareUserIds } = localState;
        setParentState({ ...parentState, shareUsers, shareUserIds, currentStatus_id: statusData._id });
    }

    const replyHandler = (usr) => {
        setLocalState({ ...localState, replyUserInfo: usr });
        document.getElementById('comment_' + statusData._id).focus();
    }

    const closeReplyHandler = () => {
        setLocalState({ ...localState, replyUserInfo: null });
        document.getElementById('comment_' + statusData._id).focus();
    }

    const showHideCommentHandler = () => {
        const { showComment } = localState;
        setLocalState({ ...localState, showComment: !showComment });
    }

    const removeCommentHandler = (realTime_id) => {
        let { totalComment, comments } = localState;
        if (totalComment > 0) totalComment--;


        // Handle to remove new comment
        let newComments = globalState.newComments[statusData._id] || [];
        if (!isEmpty(newComments)) {
            realTime.update(DB.RT_NOTIFICATION.name(), null, null, `${statusData._id}_comment`, realTime_id);

            // Remove comment from locastate.comments
            comments = comments.filter(e => e._id_gen !== newComments[0]._id_gen);
        }

        setLocalState({ ...localState, totalComment, comments });
    }

    const reportHandler = async () => {
        const data = {
            [DB.REPORT.USER_ID]: loggedUser._id,
            [DB.REPORT.STATUS_ID]: statusData._id
        }
        await fireStore.updateMultiConditions(
            DB.REPORT.name(),
            data, true,
            [DB.REPORT.STATUS_ID, CON_TYPE.EQ, statusData._id],
        );
        showNotification('success', R.timeline.thanks_report)
    }

    const deleteHandler = async () => {
        const { totalTimeline } = parentState;
        if (localState.isOwner) {

            // Remove userid in shares array from statusid_owner
            const data = {
                shares: firebase.firestore.FieldValue.arrayRemove(loggedUser._id),
            }

            // statusid from owner
            await fireStore.updateMultiConditions(
                DB.STATUS.name(),
                data, true,
                [DB.STATUS.ID, CON_TYPE.EQ, statusData.statusId_Owner],
            );

            // status id
            await fireStore.updateMultiConditions(
                DB.STATUS.name(),
                data, true,
                [DB.STATUS.ID, CON_TYPE.EQ, statusData.statusId],
            );

            // Remove status
            fireStore.remove(DB.STATUS.name(),
                [DB.STATUS.ID, CON_TYPE.EQ, statusData._id],
            );

            // Remove status share in real time
            realTime.update(
                DB.RT_NOTIFICATION.name(),
                { [loggedUser._id]: null },
                null,
                `${statusData.statusId}_share`);

            // Remove comment
            fireStore.remove(DB.COMMENT.name(),
                [DB.COMMENT.STATUS_ID, CON_TYPE.EQ, statusData._id],
            );

            // Remove file in storage
            await deleteFile('file/status/', loggedUser._id + '/statusFile_' + statusData._id);

            setLocalState({ ...localState, isRemoved: true });
            setParentState({ ...parentState, totalTimeline: totalTimeline - 1 });
        }
    }

    const onErrorVideoPlay = err => {
        const { attachedFileActive, fileAttached } = statusData;
        document.getElementById(statusData._id).remove();
        if (attachedFileActive) {
            return <img src={fileAttached} />
        }
    }
    const onReadyVideoPlay = err => {
        const { attachedFileActive, fileAttached } = statusData;
        document.getElementById(statusData._id).style.display = '';
        if (attachedFileActive) {
            return <img src={fileAttached} />
        }
    }

    const onPauseVideoPlay = () => {
        setLocalState({ ...localState, isPlayVideo: false });
    }

    const onPlayVideoPlay = () => {
        setLocalState({ ...localState, isPlayVideo: true });
    }

    const checkPlaying = () => {
        const pathname = window.location.pathname;
        const hashname = window.location.hash;
        if (pathname === '/' || pathname === '/set' || pathname === '/status') {
            if (localState.isPlayVideo && isEmpty(hashname)) return true;
        }
        return false;
    }

    const renderFileAttached = () => {
        const { attachedFileActive, fileAttached, attachedLink } = statusData;
        if (!isEmpty(attachedLink)) {
            return <ReactPlayer id={statusData._id} width={'100%'} style={{ display: 'none' }} url={attachedLink} controls playing={checkPlaying()}
                onError={onErrorVideoPlay}
                onReady={onReadyVideoPlay}
                onPause={onPauseVideoPlay}
                onPlay={onPlayVideoPlay}
            />
        } else if (attachedFileActive) {
            return <img src={fileAttached} />
        }
    }

    const renderDescription = () => {
        const { description } = statusData;
        if (isEmpty(description) || isEmpty(description.replace(/\n/g, ''))) return null;
        const split = description.split('\n');
        const data = []
        for (let i = 0; i < split.length; i++) {
            data.push(<span key={`des_v_${i}`}>{ReactHtmlParser(Autolinker.link(split[i]))}<br /></span>);
        }
        if (!isEmpty(data)) return data;
        else return description;
    }

    const gotoURL = _id => {
        history.push('/_mix/' + short().fromUUID(_id));
    }

    const renderTotalLike = () => {
        const { likeUserIds } = localState;
        const { newLikes_userIds } = globalState;
        let count = 0;
        if (!isEmpty(likeUserIds)) {
            count += likeUserIds.length;
        }
        if (!isEmpty(newLikes_userIds) && !isEmpty(newLikes_userIds[statusData._id])) {
            count += newLikes_userIds[statusData._id].length;
        }
        return count;
    }

    const renderTotalShare = () => {
        const { shareUserIds } = localState;
        const { newShares_userIds } = globalState;
        let count = 0;
        if (!isEmpty(shareUserIds)) {
            count += shareUserIds.length;
        }
        if (!isEmpty(newShares_userIds) && !isEmpty(newShares_userIds[statusData._id])) {
            count += newShares_userIds[statusData._id].length;
        }
        return count;
    }

    const shareHandlerBuilder = () => {
        if (statusData.shares && statusData.shares.indexOf(loggedUser._id) !== -1) return;
        if (isEmpty(statusData.setClothesOwner)) {
            shareHandler(statusData.setClothes._id, statusData._id, statusData._id, statusData.createdDate, statusData.description, statusData.fileAttached)
        } else {
            shareHandler(statusData.setClothes._id, statusData._id, statusData.statusId_Owner, statusData.ownerStatusDate, statusData.ownerDescription)
        }
    }

    const renderOwnerDescription = () => {
        const { ownerDescription } = statusData;

        const split = ownerDescription.split('\n');
        const data = []
        for (let i = 0; i < split.length; i++) {
            data.push(<span key={`des_v_${i}`}>{ReactHtmlParser(Autolinker.link(split[i]))}<br /></span>);
        }
        if (!isEmpty(data)) return data;
        else return ownerDescription;
    }

    if (localState.isRemoved) return null;

    // Get clother set for preview
    let { setClothes } = statusData;
    if (!setClothes) setClothes = {};
  
    return (
        <>
            <div className={`timeline-group row`}>
                <div className='col-md timeline-content'>

                    {
                        !isTrendStatus
                            ?
                            <Link to={`/${translator.fromUUID(localState.userInfo._id)}`}>
                                <div className='avatar-group'>
                                    <Avatar className='avatar of_cover' src={localState.userInfo.avatar} name={localState.userInfo.name} size='3rem' round={true} />
                                    <div className='name-group'>
                                        <div className='name-detail'>
                                            <div className='zname zellipis'>{localState.userInfo.name}</div>
                                            {
                                                !isEmpty(setClothes)
                                                &&
                                                <div className='zmore'>{R.timeline.shared_clother_set}</div>
                                            }
                                        </div>
                                        <div className='zsince'>{formatMoment(moment, statusData.createdDate)}</div>
                                    </div>
                                </div>
                            </Link>
                            :
                            <div>
                                <div className='avatar-group'>
                                    <Avatar className='avatar of_cover' src={statusData.logo} name={statusData.name} size='1rem' round={true} />
                                    <div className='name-art-group'>
                                        {statusData.name} <span className='spr'>-</span>{formatMoment(moment, statusData.createdDate)}
                                    </div>
                                </div>
                            </div>
                    }
                    {
                        !isTrendStatus
                            ?

                            isEmpty(renderDescription())
                                ? null :
                                <div className='content-group'>
                                    {renderDescription()}
                                </div>
                            :
                            <div className='content-group'>{renderDescription()}<a id={'link-detail-' + statusData._id} className='viewmore noselect' href={statusData.linkDetail} target='_blank'>{R.timeline.viewmore}</a></div>
                    }
                    <div className='all-attached-group'>
                        {
                            isEmpty(setClothes) && isEmpty(statusData.ownerFileAttached)
                            &&
                            <div className='file-attach-group'>
                                {renderFileAttached()}
                            </div>
                        }
                        <div className='set-clother-group'>
                            {
                                !isEmpty(localState.setClothesOwner)
                                &&
                                <div className='q-t-owner'>
                                    <div className='ln1'>
                                        <div>
                                            {R.timeline.setBy2}
                                        </div>
                                        <Link to={`/${translator.fromUUID(localState.setClothesOwner._id)}`} className='ln1_1 zpointer'>
                                            <div className='avt_1'>
                                                <Avatar className='of_cover' src={localState.setClothesOwner.avatar} name={localState.setClothesOwner.name} size='1rem' round={true} />
                                            </div>
                                            <div className='n1_1 noselect'>
                                                {localState.setClothesOwner.name}
                                            </div>
                                        </Link>
                                    </div>
                                    <div className='ln2'>
                                        {formatMoment(moment, statusData.ownerStatusDate)}
                                    </div>
                                    <div className='ln3'>
                                        {renderOwnerDescription()}
                                    </div>
                                </div>
                            }

                            {
                                !isEmpty(setClothes)
                                &&
                                <div className='q-c-group' onClick={() => gotoURL(setClothes._id)}>

                                    <div className={`ql ${!statusData.fileAttached && !statusData.ownerFileAttached  ? 'center' : ''}`}>
                                        {
                                            setClothes && setClothes.image &&
                                            <img src={setClothes.image} />
                                        }
                                    </div>
                                    {
                                        (statusData.fileAttached || statusData.ownerFileAttached)
                                        &&
                                        <div className='qr'>
                                            <img src={statusData.ownerFileAttached || statusData.fileAttached} />
                                        </div>
                                    }
                                </div>

                            }

                        </div>
                    </div>

                    <div className='item-bottom-group'>
                        <div className='favorite-group first-group'>
                            <i onClick={() => likeHandler()} className={`noselect material-icons favorite ${likeActiveHandler()}`}>favorite</i>
                            <div onClick={likeModalHandler} className='nlike' data-toggle="modal" data-target={`#timeline_likeModal` + (statusType ? '_' + statusType : '')}><NumericLabel>{renderTotalLike()}</NumericLabel></div>
                        </div>
                        <div onClick={showHideCommentHandler} className='favorite-group first-group noselect'>
                            <i className={`noselect material-icons comment`}>mode_comment</i>
                            <div className='nlike'><NumericLabel>{localState.totalComment >= 0 ? localState.totalComment : 0}</NumericLabel></div>
                        </div>
                        {
                            !isEmpty(statusData.setClothes) && !createdByMe
                            &&
                            <div className='favorite-group first-group noselect'>
                                <i onClick={shareHandlerBuilder} className={`noselect material-icons share ${statusData.shares && statusData.shares.indexOf(loggedUser._id) !== -1 ? 'zdisabled' : ''}`}>reply</i>
                                <div onClick={shareModalHandler} className='nlike' data-toggle="modal" data-target={`#timeline_listShareModal` + (statusType ? '_' + statusType : '')}><NumericLabel>{renderTotalShare()}</NumericLabel></div>
                            </div>
                        }
                        <div className='tool-right-group'>
                            <MoreActionComponent data={

                                isTrendStatus ?
                                    [{
                                        label: R.timeline.report,
                                        onClickHandler: reportHandler
                                    }, {
                                        label: R.timeline.share,
                                        onClickHandler: () => {
                                            dispatch({ type: type.BUILD_LINK_TEST, linkTest: `${getPrefixURL()}/status/${translator.fromUUID(statusData._id)}` });
                                            document.getElementById('id_test_url').select();
                                            document.getElementById('id_btn_timeline_shareURL').click()
                                        }
                                    }]
                                    :
                                    localState.isOwner
                                        ?
                                        [
                                            {
                                                label: R.timeline.delete,
                                                onClickHandler: deleteHandler
                                            },
                                            {
                                                label: R.timeline.share,
                                                onClickHandler: () => {
                                                    dispatch({ type: type.BUILD_LINK_TEST, linkTest: `${getPrefixURL()}/status/${translator.fromUUID(statusData._id)}` });
                                                    document.getElementById('id_test_url').select();
                                                    document.getElementById('id_btn_timeline_shareURL').click()
                                                }
                                            }
                                        ]
                                        :
                                        [
                                            {
                                                label: R.timeline.report,
                                                onClickHandler: reportHandler
                                            },
                                            {
                                                label: R.timeline.share,
                                                onClickHandler: () => {
                                                    dispatch({ type: type.BUILD_LINK_TEST, linkTest: `${getPrefixURL()}/status/${translator.fromUUID(statusData._id)}` });
                                                    document.getElementById('id_test_url').select();
                                                    document.getElementById('id_btn_timeline_shareURL').click()
                                                }
                                            }
                                        ]
                            } />
                        </div>
                    </div>

                    {
                        localState.moreComment && localState.showComment
                        &&
                        <div onClick={loadMoreComment} className='m-cmt noselect'>
                            {R.timeline.view_more_comment}
                        </div>
                    }

                    {
                        localState.showComment
                        &&
                        <div className='list-comment-group'>
                            {
                                !isEmpty(localState.comments) &&
                                localState.comments.map((e, i) => {

                                    if (
                                        localState.comments.length === 1
                                        &&
                                        !isEmpty(globalState.newComments[statusData._id])
                                        &&
                                        !isEmpty(globalState.newComments[statusData._id].find(cmt => cmt._id_gen === e._id_gen))
                                    ) return null;
                                    return (<CommentItemComponent
                                        key={e._id_gen}
                                        isShow={true}
                                        commentObj={e}
                                        replyHandler={replyHandler}
                                        loggedUser={loggedUser}
                                        statusType={statusType}
                                        removeCommentHandler={removeCommentHandler} />)
                                }

                                )
                            }
                            {
                                !isEmpty(globalState.newComments[statusData._id]) &&
                                globalState.newComments[statusData._id].map((e, i) =>
                                    <CommentItemComponent
                                        key={e._id_gen}
                                        realTime_id={e.realTime_id}
                                        isShow={
                                            e.status_id === statusData._id &&
                                            (
                                                e.reply_user_id === loggedUser._id ||
                                                e.reply_user_id === 'all' ||
                                                e.user_id === loggedUser._id ||
                                                statusData.user_id === loggedUser._id)
                                        }
                                        commentObj={e}
                                        replyHandler={replyHandler}
                                        loggedUser={loggedUser}
                                        removeCommentHandler={removeCommentHandler} />
                                )
                            }
                        </div>
                    }

                    {
                        localState.showComment
                        &&
                        <div className='comment-group'>
                            <div className='avatar-group'>
                                <Avatar className='zavatar of_cover' src={loggedUser.avatar} name={loggedUser.name} size='2rem' round={true} />
                                <div className='name-group'>
                                    {
                                        !isEmpty(localState.replyUserInfo)
                                        &&
                                        <div onClick={closeReplyHandler} className='reply-group'>
                                            <span>
                                                {`${R.timeline.reply_to} `}<strong>{localState.replyUserInfo.name}</strong>
                                            </span>
                                        </div>
                                    }

                                    <div className='textarea-group'>
                                        <textarea maxLength={3000} id={`comment_${statusData._id}`} ref={commentRef} className='txt-comment' onKeyDown={commendHandler} type='text' placeholder={R.timeline.input_comment}></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    }

                </div>
            </div>
        </>
    )
}

export default TimelineItemComponent;