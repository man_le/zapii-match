import {isEmpty} from 'lodash';
import {NotificationManager} from 'react-notifications';
import R from './../locale/R';

const validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

const validateAddress = address => {
    let addressValid = false;
    if(
        (
            address.toLowerCase().indexOf('phường') !== -1 || 
            address.toLowerCase().indexOf('phuong') !== -1 ||
            address.toLowerCase().indexOf('xã') !== -1 ||
            address.toLowerCase().indexOf('xa') !== -1 ||
            address.toLowerCase().indexOf('thị trấn') !== -1 || 
            address.toLowerCase().indexOf('thi tran') !== -1 ||
            address.toLowerCase().indexOf('tt') !== -1
        )
        &&
        (
            address.toLowerCase().indexOf('huyện') !== -1 || 
            address.toLowerCase().indexOf('huyen') !== -1 ||
            address.toLowerCase().indexOf('quận') !== -1 ||
            address.toLowerCase().indexOf('quan') !== -1
            
        )
        &&
        (
            address.toLowerCase().indexOf('tỉnh') !== -1 || 
            address.toLowerCase().indexOf('tinh') !== -1 ||
            address.toLowerCase().indexOf('thành phố') !== -1 ||
            address.toLowerCase().indexOf('thanh pho') !== -1 ||
            address.toLowerCase().indexOf('tp') !== -1
        )
        
        ){
            addressValid = true;
    }
    return addressValid;
}

/*
Input ex:
    {
        email: 'aaa@gmi.com',
        password: 'value',
        len20: 'value',
        require: 'value'
    }
*/

const validate =(data)=>{
    let valid = false;
    if(!isEmpty(data)){

        Object.keys(data).forEach(item=>{
            switch(item){
                case 'email':
                    if(!isEmpty(data[item])) valid = validateEmail(data[item]);
                    if(!valid) NotificationManager.error(R.common.email_not_valid, '', 3000);
                    else if(isEmpty(data[item])) NotificationManager.error(R.common.email_require, '', 3000);
                    break;
                case 'require':
                    if(!isEmpty(data[item])) valid = true;
                    else NotificationManager.error(R.common.all_require, '', 3000);
                    break;
                case 'len20':
                    if(!isEmpty(data[item]) && data[item].length <= 20) valid=true;
                    else NotificationManager.error(R.common.went_wrong, '', 3000);
                    break;
            }
        })
    }
    return valid;
}

export {
    validate,
    validateAddress,
    validateEmail
}