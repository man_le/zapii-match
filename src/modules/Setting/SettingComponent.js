import React from 'react';
import {isFunction} from 'lodash';
import { Link } from 'react-router-dom';
import { signOut } from './../../utils/AuthUtils';
import CoverAllPage from '../Common/CoverAllPage';
import R from '../../locale/R';
import './stylesheet.scss';

const SettingComponent = () => {
    return (
        <div className='setting-group'>
            <ul className='g'>
                <li>
                    <Link to="/setting/profile" className='fullw'>
                        <div>
                            <i className='material-icons'>perm_device_information</i>
                        </div>
                        <div className='t'>
                            {R.setting.profile}
                        </div>
                    </Link>
                </li>
                <li>
                    <Link to="/setting/account" className='fullw'>
                        <div>
                            <i className='material-icons'>perm_identity</i>
                        </div>
                        <div className='t'>
                            {R.setting.account}
                        </div>
                    </Link>
                </li>
                <li>
                    <Link to="/setting/notification" className='fullw'>
                        <div>
                            <i className='material-icons'>settings_remote</i>
                        </div>
                        <div className='t'>
                            {R.setting.notification}
                        </div>
                    </Link>
                </li>
                <li onClick={signOut}>
                    <div>
                        <i className='material-icons'>power_settings_new</i>
                    </div>
                    <div className='t'>
                        {R.common.logout}
                    </div>
                </li>
            </ul>

            <CoverAllPage/>

        </div>
    )
}

export default SettingComponent;