import { isEmpty } from 'lodash';
import React, { useContext, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import NumericLabel from 'react-pretty-numbers';
import short from 'short-uuid';
import { RouteContext } from '../../routes/routes';
import MyProfilePageComponent from './MyProfilePageComponent';
import OtherProfilePageComponent from './OtherProfilePageComponent';
import './stylesheet.scss';

const ProfilePageComponent = ({ match, history }) => {
    const { params: { userId } } = match;
    const [user, setUser] = useContext(RouteContext);
    let translator = short();
    let id = null;

    if (userId.toLowerCase() === 'me') {
        if (!isEmpty(user) && !isEmpty(user.extData)) {
            id = user.extData[0]._id;
            history.push(`/${translator.fromUUID(id)}`);
        }
    }
    else {
        try{
            id = translator.toUUID(userId);
        }catch(err){
            id = null;
        } 
    }

    if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0]._id)) {
        if ( userId !== translator.fromUUID(user.extData[0]._id)) {
            return <OtherProfilePageComponent id={id} />;
        } else return <MyProfilePageComponent id={id} />;
    } else {
        return (
            <div className={`zloading-item`}>
                <BounceLoader
                    sizeUnit={"px"}
                    size={50}
                    loading={true}
                    color='#777'
                /></div>
        )
    }
}

export default withRouter(ProfilePageComponent);