import React, { useState, useContext, useEffect } from 'react';
import CurrencyFormat from 'react-currency-format';
import R from '../../../locale/R';
import short from 'short-uuid';
import { GlobalStateContext } from '../../Layout/HomeLayout';
import BagIcon from '../../Common/Icon/BagIcon';
import HatIcon from '../../Common/Icon/HatIcon';
import ShirtIcon from '../../Common/Icon/ShirtIcon';
import ShoesIcon from '../../Common/Icon/ShoesIcon';
import TrousersIcon from '../../Common/Icon/TrousersIcon';
import MoreActionComponent from '../../Common/MoreActionComponent';
import type from '../../../reducers/type';
import { isEmpty } from 'lodash';

const DesignTabProgressComponent = ({ removeFromInMixHandler, history, itemsInMix }) => {

    const [localState, setLocalState] = useState({
        isShow_hat: false,
        isShow_shirt: false,
        isShow_trousers: false,
        isShow_shoes: false,
        isShow_bag: false,
    });

    const trans = short();

    const [globalState, dispatch] = useContext(GlobalStateContext);

    const isShowHandler_hat = () => {
        let { isShow_hat } = localState;
        setLocalState({ ...localState, isShow_hat: !isShow_hat });
    }

    const isShowHandler_shirt = () => {
        let { isShow_shirt } = localState;
        setLocalState({ ...localState, isShow_shirt: !isShow_shirt });
    }

    const isShowHandler_trousers = () => {
        let { isShow_trousers } = localState;
        setLocalState({ ...localState, isShow_trousers: !isShow_trousers });
    }

    const isShowHandler_shoes = () => {
        let { isShow_shoes } = localState;
        setLocalState({ ...localState, isShow_shoes: !isShow_shoes });
    }

    const isShowHandler_bag = () => {
        let { isShow_bag } = localState;
        setLocalState({ ...localState, isShow_bag: !isShow_bag });
    }

    const deleteHandler = (_id) => {
        removeFromInMixHandler(_id);
    }

    const gotoDetail = _id => {
        dispatch({ type: type.CHANGE_ROUTER, currentRouter: window.location.pathname });
        history.push('/detail/product/' + trans.fromUUID(_id));
    }

    const {
        isShow_hat,
        isShow_shirt,
        isShow_trousers,
        isShow_shoes,
        isShow_bag,
    } = localState;


    const itemsInMix_hat = [];
    const itemsInMix_shirt = [];
    const itemsInMix_trousers = [];
    const itemsInMix_shoes = [];
    const itemsInMix_bag = [];

    for (let i = 0; i < itemsInMix.length; i++) {
        const item = itemsInMix[i];
        if (!item) continue;
        if (item.mix_type === 'hat') {
            itemsInMix_hat.push({ ...item, index: i });
        } else if (item.mix_type === 'shirt') {
            itemsInMix_shirt.push({ ...item, index: i });
        } else if (item.mix_type === 'trousers') {
            itemsInMix_trousers.push({ ...item, index: i });
        } else if (item.mix_type === 'shoes') {
            itemsInMix_shoes.push({ ...item, index: i });
        } else if (item.mix_type === 'bag') {
            itemsInMix_bag.push({ ...item, index: i });
        }
    }

    return (
        <div className='tabProgress'>
            <div className='tab-item-group first'>
                <div onClick={isShowHandler_hat} className='tab-title'>
                    <div className='left'>
                        <HatIcon color={itemsInMix_hat.length > 0 ? '#262626' : '#777'} />
                        <div className='l' style={itemsInMix_hat.length > 0 ? { color: '#262626' } : { color: '#777' }}>
                            {`${R.mix.hat} (${itemsInMix_hat.length})`}
                        </div>
                    </div>
                    <div className='right'>
                        <i className='material-icons'>{isShow_hat ? 'expand_more' : 'chevron_right'}</i>
                    </div>

                </div>
                <div className='tab-content'>
                    {
                        isShow_hat
                        &&
                        <div>
                            {
                                itemsInMix_hat.map((e, index) =>
                                    <div key={`items_in_mix_${e._id}`} className='item-progress-group'>
                                        <div className='item-group'>
                                            <div className='item-preview'>
                                                <img src={e.image} />
                                            </div>
                                            <div className='item-info'>
                                                <div className='n'>
                                                    {e.name}
                                                </div>
                                                <div className='item-price'>
                                                    <CurrencyFormat value={e.price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            {
                                                <MoreActionComponent data={
                                                    [
                                                        {
                                                            label: R.addressBook.delete,
                                                            onClickHandler: () => deleteHandler(e._id)
                                                        },
                                                        {
                                                            label: R.mix.detail,
                                                            onClickHandler: () => gotoDetail(e._id)
                                                        },
                                                    ]
                                                } />
                                            }
                                        </div>
                                    </div>
                                )
                            }

                        </div>
                    }
                </div>
            </div>

            <div className='tab-item-group'>
                <div onClick={isShowHandler_shirt} className='tab-title'>
                    <div className='left'>
                        <ShirtIcon width='16' height='16' style={{ marginTop: '0.2rem' }} color={itemsInMix_shirt.length > 0 ? '#262626' : '#777'} />
                        <div className='l' style={itemsInMix_shirt.length > 0 ? { color: '#262626' } : { color: '#777' }}>
                            {`${R.mix.shirt} (${itemsInMix_shirt.length})`}
                        </div>
                    </div>
                    <div className='right'>
                        <i className='material-icons'>{isShow_shirt ? 'expand_more' : 'chevron_right'}</i>
                    </div>

                </div>
                <div className='tab-content'>
                    {
                        isShow_shirt
                        &&
                        <div>
                            {
                                itemsInMix_shirt.map((e, index) =>
                                    <div key={`items_in_mix_${e._id}`} className='item-progress-group'>
                                        <div className='item-group'>
                                            <div className='item-preview'>
                                                <img src={e.image} />
                                            </div>
                                            <div className='item-info'>
                                                <div className='n'>
                                                    {e.name}
                                                </div>
                                                <div className='item-price'>
                                                    <CurrencyFormat value={e.price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <MoreActionComponent data={
                                                [
                                                    {
                                                        label: R.addressBook.delete,
                                                        onClickHandler: () => deleteHandler(e._id)
                                                    },
                                                    {
                                                        label: R.mix.detail,
                                                        onClickHandler: () => gotoDetail(e._id)
                                                    },
                                                ]
                                            } />
                                        </div>
                                    </div>
                                )
                            }

                        </div>
                    }
                </div>
            </div>

            <div className='tab-item-group'>
                <div onClick={isShowHandler_trousers} className='tab-title'>
                    <div className='left'>
                        <TrousersIcon color={itemsInMix_trousers.length > 0 ? '#262626' : '#777'} />
                        <div className='l' style={itemsInMix_trousers.length > 0 ? { color: '#262626' } : { color: '#777' }}>
                            {`${R.mix.trousers} (${itemsInMix_trousers.length})`}
                        </div>
                    </div>
                    <div className='right'>

                        <i className='material-icons'>{isShow_trousers ? 'expand_more' : 'chevron_right'}</i>
                    </div>

                </div>
                <div className='tab-content'>
                    {
                        isShow_trousers
                        &&
                        <div>
                            {
                                itemsInMix_trousers.map((e, index) =>
                                    <div key={`items_in_mix_${e._id}`} className='item-progress-group'>
                                        <div className='item-group'>
                                            <div className='item-preview'>
                                                <img src={e.image} />
                                            </div>
                                            <div className='item-info'>
                                                <div className='n'>
                                                    {e.name}
                                                </div>
                                                <div className='item-price'>
                                                    <CurrencyFormat value={e.price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <MoreActionComponent data={
                                                [
                                                    {
                                                        label: R.addressBook.delete,
                                                        onClickHandler: () => deleteHandler(e._id)
                                                    },
                                                    {
                                                        label: R.mix.detail,
                                                        onClickHandler: () => gotoDetail(e._id)
                                                    },
                                                ]
                                            } />
                                        </div>
                                    </div>
                                )
                            }

                        </div>
                    }
                </div>
            </div>

            <div className='tab-item-group'>
                <div onClick={isShowHandler_shoes} className='tab-title'>
                    <div className='left'>
                        <ShoesIcon color={itemsInMix_shoes.length > 0 ? '#262626' : '#777'} />
                        <div className='l' style={itemsInMix_shoes.length > 0 ? { color: '#262626' } : { color: '#777' }}>
                            {`${R.mix.shoes} (${itemsInMix_shoes.length})`}
                        </div>
                    </div>
                    <div className='right'>
                        <i className='material-icons'>{isShow_shoes ? 'expand_more' : 'chevron_right'}</i>
                    </div>

                </div>
                <div className='tab-content'>
                    {
                        isShow_shoes
                        &&
                        <div>
                            {
                                itemsInMix_shoes.map((e, index) =>
                                    <div key={`items_in_mix_${e._id}`} className='item-progress-group'>
                                        <div className='item-group'>
                                            <div className='item-preview'>
                                                <img src={e.image} />
                                            </div>
                                            <div className='item-info'>
                                                <div className='n'>
                                                    {e.name}
                                                </div>
                                                <div className='item-price'>
                                                    <CurrencyFormat value={e.price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <MoreActionComponent data={
                                                [
                                                    {
                                                        label: R.addressBook.delete,
                                                        onClickHandler: () => deleteHandler(e._id)
                                                    },
                                                    {
                                                        label: R.mix.detail,
                                                        onClickHandler: () => gotoDetail(e._id)
                                                    },
                                                ]
                                            } />
                                        </div>
                                    </div>
                                )
                            }

                        </div>
                    }
                </div>
            </div>

            <div className='tab-item-group'>
                <div onClick={isShowHandler_bag} className='tab-title'>
                    <div className='left'>
                        <BagIcon isSelected={true} width='15' height='15' color={itemsInMix_bag.length > 0 ? '#262626' : '#777'} />
                        <div className='l' style={itemsInMix_bag.length > 0 ? { color: '#262626' } : { color: '#777' }}>
                            {`${R.mix.more} (${itemsInMix_bag.length})`}
                        </div>
                    </div>
                    <div className='right'>
                        <i className='material-icons'>{isShow_bag ? 'expand_more' : 'chevron_right'}</i>
                    </div>

                </div>
                <div className='tab-content'>
                    {
                        isShow_bag
                        &&
                        <div>
                            {
                                itemsInMix_bag.map((e, index) =>
                                    <div key={`items_in_mix_${e._id}`} className='item-progress-group'>
                                        <div className='item-group'>
                                            <div className='item-preview'>
                                                <img src={e.image} />
                                            </div>
                                            <div className='item-info'>
                                                <div className='n'>
                                                    {e.name}
                                                </div>
                                                <div className='item-price'>
                                                    <CurrencyFormat value={e.price} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <MoreActionComponent data={
                                                [
                                                    {
                                                        label: R.addressBook.delete,
                                                        onClickHandler: () => deleteHandler(e._id)
                                                    },
                                                    {
                                                        label: R.mix.detail,
                                                        onClickHandler: () => gotoDetail(e._id)
                                                    },
                                                ]
                                            } />
                                        </div>
                                    </div>
                                )
                            }

                        </div>
                    }
                </div>
            </div>

        </div>
    )
}
export default DesignTabProgressComponent;