import React, {useContext} from 'react';
import { Link, withRouter } from 'react-router-dom';
import DesignComponent from './pc/DesignComponent';
import MobileDesignComponent from './mobile/DesignComponent';
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from '../../locale/R';
import './stylesheet.scss';

const Comp = ({match, history}) => {
    const { params: { mix_id } } = match;
    const [globalState, dispatch] = useContext(GlobalStateContext);

    return (
        <div className='adapt-design-page-group noselect'>
            <div className='pc'>
                <DesignComponent mix_id={mix_id} history={history} />
            </div>
            <div className='mobile'>
                <div className="zheader noselect">
                    <Link to={globalState.currentRouter === window.location.pathname ? '/' : globalState.currentRouter} className="close zscale-effect">
                        <i className="material-icons">keyboard_backspace</i>
                    </Link>
                    <div className='ztitle'>
                        {R.mix.title}
                    </div>
                </div>
                <div className="zbody">
                    <MobileDesignComponent mix_id={mix_id} history={history} />
                </div>

            </div>
        </div>
    )
}

export default withRouter(Comp);