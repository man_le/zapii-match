import React, { useState, useEffect, useContext } from 'react';
import short from 'short-uuid';
import * as firebase from "firebase/app";
import { isEmpty, isUndefined } from 'lodash';
import CurrencyFormat from 'react-currency-format';
import { BounceLoader } from 'react-spinners';
import { CON_TYPE, DB, fireStore, realTime } from '../../../utils/DBUtils';
import { GlobalStateContext } from '../../Layout/HomeLayout';
import { RouteContext } from '../../../routes/routes';
import R from './../../../locale/R';
import type from '../../../reducers/type';
import { Link, withRouter } from 'react-router-dom';
import DataIsEmpty from '../../Common/DataIsEmpty';
import './stylesheet.scss';
import BagIcon from '../../Common/Icon/BagIcon';

const InBagCountComponent = ({ history }) => {

    const [user, setUser] = useContext(RouteContext);
    const [globalState, dispatch] = useContext(GlobalStateContext);

    const [localState, setLocalState] = useState({
        init: false,
        data: [],
        loading: false,
    });

    const fetchData = () => {
        const {
            itemsInMix_bag,
            itemsInMix_shirt,
            itemsInMix_hat,
            itemsInMix_shoes,
            itemsInMix_trousers,
        } = globalState;

        const { data } = localState;

        setLocalState({ ...localState, loading: true });

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            itemsInMix_bag.forEach(e => { data.push({ ...e, total: e.outOfStock?0:1, key: 'M' }) });
            itemsInMix_shirt.forEach(e => { data.push({ ...e, total: e.outOfStock?0:1, key: 'M' }) });
            itemsInMix_hat.forEach(e => { data.push({ ...e, total: e.outOfStock?0:1, key: 'M' }) });
            itemsInMix_shoes.forEach(e => { data.push({ ...e, total: e.outOfStock?0:1, key: 'M' }) });
            itemsInMix_trousers.forEach(e => { data.push({ ...e, total: e.outOfStock?0:1, key: 'M' }) });
        }

        setLocalState({ ...localState, data, init: true, loading: false });
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [user]);



    const upHandler = async (currentItem) => {
        if (localState.loading || currentItem.outOfStock) return;
        let { total } = currentItem;

        if (total >= 10) total = 10;
        else total = total + 1;

        currentItem.total = total;

        const { data } = localState;
        const index = data.findIndex(e => e._id === currentItem._id);
        data[index] = currentItem;
        setLocalState({ ...localState, data });
    }

    const downHandler = async (currentItem) => {
        if (localState.loading || currentItem.outOfStock) return;
        let { total } = currentItem;

        if (total <= 0) total = 0;
        else total = total - 1;

        currentItem.total = total;

        const { data } = localState;
        const index = data.findIndex(e => e._id === currentItem._id);
        data[index] = currentItem;
        setLocalState({ ...localState, data });
    }

    const nextPanelHandler = () => {
        if (isEmpty(data) || buildTotalPrice() === 0) return;
        dispatch({ type: type.CHANGE_ROUTER, currentRouter: `/mix/addAll`, currentRouterIndex: 6 });

        dispatch({ type: type.UPDATE_IN_BAG });
        
        if (isEmpty(globalState.order_id)) {
            dispatch({ type: type.BUILD_ORDER_ID });
        }

        history.push('/bag/list');
    }

    const addToBagHandler = async () => {
        const { data } = localState;

        for (let i = 0; i < data.length; i++) {
            const newData = {
                image: data[i].image,
                clothes_id: data[i]._id,
                name: data[i].name,
                price: data[i].new_price,
                user_id: user.extData[0]._id,
                [data[i].key]: data[i].total,
                'order_id': 'new',
                user_id: user.extData[0]._id
            }

            delete newData.key;
            delete newData.total;

            await fireStore.updateMultiConditions(
                DB.IN_BAG.name(),
                newData,
                true,
                [DB.IN_BAG.CLOTHES_ID, CON_TYPE.EQ, data[i]._id],
                [DB.IN_BAG.ORDER_ID, CON_TYPE.EQ, 'new'],
                [DB.IN_BAG.USER_ID, CON_TYPE.EQ, user.extData[0]._id],
            );
        }

        nextPanelHandler();
    }

    const renderNum = (num) => {
        if (num >= 10) return 10;
        else if (num <= 0) return 0;
        else return num;
    }

    const changeSizeHandler = (currentItem, size) => {
        const { data } = localState;

        const index = data.findIndex(e => e._id === currentItem._id);
        data[index].key = size;

        setLocalState({ ...localState, data });
    }

    const buildTotalPrice = () => {
        const { data } = localState;
        let result = 0;
        for (let i = 0; i < data.length; i++) {
            let total = data[i].total;
            if (total < 0) total = 0;
            const value = data[i].new_price * total;
            result += value;
        }
        if (result <= 0) result = 0;
        return result;
    }

    const { data, init, loading } = localState;

    if (loading && !init) {
        return (
            <div className={`zloading-item`}><BounceLoader
                sizeUnit={"px"}
                size={50}
                loading={true}
                color='#777'
            /></div>
        )
    } else if (isEmpty(data) && init) {
        history.push('/mix')
    }

    return (
        <div className='zmain full set-in-bag-page-group'>
            <div className='row bar'>

                <div className='col-md-6 cmenu'>
                    <div className='left'>
                        
                    </div>
                    <div className='center'>
                        {R.mix.addAll1}
                    </div>
                    <div className='right'></div>
                </div>

                <div className='col-md-6 zbor'>
                    <div className='gr1'>
                        {
                            data.map(e =>
                                <div key={`items_order_${e._id}_${e.key}`} className='pro-item'>
                                    <div className='pro-preview'>
                                        <img src={e.image} />
                                    </div>
                                    <div className='pro-name'>
                                        {`${e.name}`}
                                        {
                                            e.outOfStock
                                            &&
                                            <div className='outofstock'>
                                                {R.bag.outofstock}
                                            </div>
                                        }
                                    </div>

                                    {
                                        !isEmpty(e.size)
                                        &&
                                        <div className='sizet noselect pc'>
                                            {
                                                e.size.map((ei, i) =>
                                                    <div key={'s1_' + i} onClick={() => changeSizeHandler(e, ei)} className={`ze zscale-effect ${i === 0 && i === ei.length - 1 ? '' : 'ce'} ${ei === e.key ? 'active' : ''}`}>{ei}</div>
                                                )
                                            }
                                        </div>
                                    }

                                    <div className='price-count noselect'>
                                        {
                                            !isEmpty(e.size)
                                            &&
                                            <div className='sizet noselect mobile'>
                                                {
                                                    e.size.map((ei, i) =>
                                                        <div key={'s2_' + i} onClick={() => changeSizeHandler(e, ei)} className={`ze zscale-effect ${i === 0 || i === ei.length - 1 ? '' : 'ce'} ${ei === e.key ? 'active' : ''}`}>{ei}</div>
                                                    )
                                                }
                                            </div>
                                        }

                                        <div className='pro-count'>
                                            <div onClick={() => downHandler(e)} className='zpointer up zscale-effect'>
                                                -
                                            </div>
                                            <div className='num'>
                                                {renderNum(e.total)}
                                            </div>
                                            <div onClick={() => upHandler(e)} className='zpointer down zscale-effect'>
                                                +
                                            </div>
                                        </div>
                                        <div className='pro-price'>
                                            <CurrencyFormat value={e.new_price * renderNum(e.total)} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                        </div>
                                    </div>
                                </div>
                            )
                        }

                        <div className='total-price-group'>
                            {
                                !isEmpty(data)
                                &&
                                <div className='total noselect'>
                                    <div className='l'>
                                        {R.bag.total}
                                    </div>
                                    <div className='r'>
                                        <CurrencyFormat value={buildTotalPrice()} displayType={'text'} thousandSeparator={true} suffix={' đ'} />
                                    </div>
                                </div>
                            }
                            <div className='set-btn-group'>
                                <div onClick={addToBagHandler} className={`g ${isEmpty(data) || buildTotalPrice() === 0 ? 'zdisabled' : ''}`}>
                                    <div className={`ins noselect zpointer ${isEmpty(data) || buildTotalPrice() === 0 ? '' : 'zscale-effect2'}`} style={{ width: '8rem', paddingRight: '1rem' }}>
                                        <BagIcon width='18' height='18' style={{ marginTop: '0.15rem', marginRight: '0.25rem' }} color='#fff' />
                                        <div className='l'>
                                            {
                                                R.bag.add
                                            }
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withRouter(InBagCountComponent);